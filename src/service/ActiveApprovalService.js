import { del, get, post } from "../util/http";

const ACTIVE_APPROVAL_BASE_URL = process.env.REACT_APP_API_URL_Active_Approval_Service;

const getAllActiveApproval = () => {
    return get(ACTIVE_APPROVAL_BASE_URL);
};

const getActiveApprovalByRequestId = id => {
    return get(ACTIVE_APPROVAL_BASE_URL + `/by-request?id=${id}`);
};

const getActiveApprovalByRequestId2 = id => {
    return get(ACTIVE_APPROVAL_BASE_URL + `/approval-list/${id}`);
};

const getActiveApprovalByNikToken = () => {
    return get(ACTIVE_APPROVAL_BASE_URL + `/approval-list`);
};

const getActiveApprovalByNikAndAssigned = (page, pageSize) => {
    return get(`${ACTIVE_APPROVAL_BASE_URL}/approval-list-assigned?type=Request&page=${page}&pageSize=${pageSize}`);
};

const getActiveApprovalByNikAndAssignedDiscrepancy = (page, pageSize) => {
    return get(`${ACTIVE_APPROVAL_BASE_URL}/approval-list-assigned?type=Discrepancy&page=${page}&pageSize=${pageSize}`);
};

const getActiveApprovalById = id => {
    return get(ACTIVE_APPROVAL_BASE_URL + `/${id}`);
};

const getApprovalStatusByRequestId = id => {
    return get(ACTIVE_APPROVAL_BASE_URL + `/check-approval-by-req?id=${id}`);
};


const approvalAction = data => {
    return post(ACTIVE_APPROVAL_BASE_URL + `/update-status`, data);
};


const ActiveApprovalService = {
    getAllActiveApproval,
    getActiveApprovalByRequestId,
    getActiveApprovalByRequestId2,
    getActiveApprovalByNikAndAssigned,
    getActiveApprovalByNikAndAssignedDiscrepancy,
    getActiveApprovalByNikToken,
    getActiveApprovalById,
    getApprovalStatusByRequestId,
    approvalAction
};

export default ActiveApprovalService;