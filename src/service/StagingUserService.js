import {del, get, post} from "../util/http";

const ACTIVE_APPROVAL_BASE_URL = process.env.REACT_APP_API_URL_Staging_User_Service;

const getDataLogin = () => {
    return get(ACTIVE_APPROVAL_BASE_URL+`/data-login`);
};


const StagingUserService = {
    getDataLogin,
};

export default StagingUserService;