import { get } from "../util/http"

const getDepartment = async () => {
   return  get(process.env.REACT_APP_API_URL_Department_Service)
}

const DepartmentService = {
    getDepartment
}

export default DepartmentService