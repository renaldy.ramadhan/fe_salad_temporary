import { del, get, post } from "../util/http";

const REACT_APP_API_URL = process.env.REACT_APP_API_URL;
const DISPOSAL_EVENT_BASE_URL = `${REACT_APP_API_URL}/disposal-group`;
const PART_SERVICE_BASE_URL =
  process.env.REACT_APP_API_URL_Part_Request_Service;

const getAllDisposalEvent = (params) => {
  return get(`${DISPOSAL_EVENT_BASE_URL}${params}`);
};
const getAllActiveDisposalEvent = () => {
  return get(`${DISPOSAL_EVENT_BASE_URL}/active`);
};

const generateDisposalSetting = async (data) => {
  return await post(`${REACT_APP_API_URL}/approval-setting/generate`, data);
};
const createDisposalEvent = (data) => {
  return post(`${DISPOSAL_EVENT_BASE_URL}/add`, data);
};
const bulkCreateDisposalEventRequest = (data) => {
  return post(`${REACT_APP_API_URL}/request/create-bulk`, data);
};

const getDetailEvent = (id) => {
  return get(`${DISPOSAL_EVENT_BASE_URL}/${id}`);
};
const getApprover = (id) => {
  return get(`${REACT_APP_API_URL}/active-request-list/by-event?id=${id}`);
};
const checkApprover = ({ id, type = "Disposal" }) => {
  return get(
    `${REACT_APP_API_URL}/active-request-list/check-approval-by-event?id=${id}&type=${type}`
  );
};
const settleEventRequest = async (data) => {
  return await post(
    `${REACT_APP_API_URL}/active-request-list/update-approval-event`,
    data
  );
};
const getRequest = async ({ type = "Disposal", page, pageSize, id }) => {
  return get(
    PART_SERVICE_BASE_URL +
      `?type=${type}&id_event=${id}&page=${page}&pageSize=${pageSize}`
  );
};

const getItemsRequestDetail = async ({
  id,
  type = "Disposal",
  page,
  pageSize,
}) => {
  return get(
    PART_SERVICE_BASE_URL +
      `/${id}?type=${type}&page=${page}&pageSize=${pageSize}`
  );
};

// const createRequest = (files,data) => {
//     return postMultipartRequest(PART_SERVICE_BASE_URL+`/add`,files,data);
// };

// const removeRequest = id => {
//     return del(PART_SERVICE_BASE_URL + `/delete/${id}`);
// };

const DiscrepancyService = {
  generateDisposalSetting,
  getAllDisposalEvent,
  getAllActiveDisposalEvent,

  getDetailEvent,
  getRequest,
  getApprover,
  checkApprover,
  settleEventRequest,
  createDisposalEvent,
  bulkCreateDisposalEventRequest,

  getItemsRequestDetail,
  // removeRequest
};

export default DiscrepancyService;
