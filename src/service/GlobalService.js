import { get} from "../util/http";

const GLOBAL_BASE_URL = process.env.REACT_APP_API_URL;
// searching part
const SEARCH_PART_SERVICE_BASE_URL = process.env.REACT_APP_API_URL_Search_Part_Service;

const fetchSearchParts = (query, query2, query3) => {
    return get(SEARCH_PART_SERVICE_BASE_URL + `/?search=${query}&purchasing_group_code=${query2}&location_code=${query3}`);
};

const fetchPlant = () => {
    return get(GLOBAL_BASE_URL+`/plant/`);
};


const fetchLocation = (id) =>{
    return get(GLOBAL_BASE_URL+`/plant/location/${id}`);
};


const fetchPurchasingGroup = () =>{
    return get(GLOBAL_BASE_URL+`/staging-purchasing-group/`);
};


const fetchReason = (type = 'DefaultType') =>{
    return get(GLOBAL_BASE_URL+`/staging/reasons?type=${type}`);
};

const GlobalService = {
  fetchLocation,
  fetchPlant,
  fetchPurchasingGroup,
  fetchReason,
  fetchSearchParts
};

export default GlobalService;