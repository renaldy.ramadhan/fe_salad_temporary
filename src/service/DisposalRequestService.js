import {del, get, postMultipartRequest} from "../util/http";

const PART_SERVICE_BASE_URL = process.env.REACT_APP_API_URL_Part_Request_Service;
const ACTIVE_APPROVAL_BASE_URL = process.env.REACT_APP_API_URL_Active_Approval_Service;


const getAllRequestDisposal = async(page,pageSize) => {
    return get(PART_SERVICE_BASE_URL+`?type=Disposal&page=${page}&pageSize=${pageSize}`);
};

const getRequestDisposal = async(page,pageSize) => {
    return get(ACTIVE_APPROVAL_BASE_URL+`/approval-list-assigned?type=Disposal&page=${page}&pageSize=${pageSize}`);
}

const createRequest = (files,data) => {
    return postMultipartRequest(PART_SERVICE_BASE_URL+`/add`,files,data);
};

const DisposalRequestService = {
    getAllRequestDisposal,
    getRequestDisposal,
    createRequest
};
  
export default DisposalRequestService;