import { get,post } from "../util/http";

const WORKFLOW_BASE_URL = process.env.REACT_APP_API_URL_Workflow_Service

const getWorkflow = () => {
    return get(WORKFLOW_BASE_URL + "/")
}

const createWorkflow = (payload) => {
    return post(WORKFLOW_BASE_URL + "/create",payload)
}

const WorkflowService = {
    getWorkflow,
    createWorkflow
}

export default WorkflowService