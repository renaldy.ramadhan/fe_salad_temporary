import {del, get, postMultipartRequest} from "../util/http";

const PART_SERVICE_BASE_URL = process.env.REACT_APP_API_URL_Part_Request_Service;

const getAllRequest = async(page,pageSize) => {
    return get(PART_SERVICE_BASE_URL+`?type=Request&page=${page}&pageSize=${pageSize}`);
};

const getRequest = id => {
    return get(PART_SERVICE_BASE_URL+`/${id}`);
};

const createRequest = (files,data) => {
    return postMultipartRequest(PART_SERVICE_BASE_URL+`/add`,files,data);
};

const removeRequest = id => {
    return del(PART_SERVICE_BASE_URL+`/delete/${id}`);
};


const PartRequestService = {
  getAllRequest,
  getRequest,
  createRequest,
  removeRequest
};

export default PartRequestService;