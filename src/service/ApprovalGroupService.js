import {get} from "../util/http";

const APPROVAL_GROUP_BASE_URL = process.env.REACT_APP_API_URL_Approver_Group_Service;

const checkApprover = () => {
    return get(APPROVAL_GROUP_BASE_URL+`/is_approver`);
};

const getApprover = () => {
    return get(APPROVAL_GROUP_BASE_URL + "/")
}


const ApprovalGroupService = {
    checkApprover,
    getApprover
};

export default ApprovalGroupService;