import {del, get, post, postMultipart, download} from "../util/http";

const DOCUMENT_BASE_URL = process.env.REACT_APP_API_URL_Document_Service;


const getDocumentByRequest = id => {
    return get(DOCUMENT_BASE_URL+`/request/${id}`);
};

const getDocumentByID = id => {
    return get(DOCUMENT_BASE_URL+`/${id}`);
};

const downloadDocumentByID = id => {
    return download(DOCUMENT_BASE_URL+`/download/${id}`);
    
};

const uploadAction = (file,data) => {
    return postMultipart(DOCUMENT_BASE_URL+`/add`, file, data);
};


const DocumentService = {
    downloadDocumentByID,
    getDocumentByID,
    getDocumentByRequest,
    uploadAction
};

export default DocumentService;