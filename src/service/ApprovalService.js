import { get} from "../util/http";

const APPROVAL_BASE_URL = process.env.REACT_APP_API_URL_Approver_Service;

const checkApprover = () => {
    return get(APPROVAL_BASE_URL+`/is_approver`);
};

const getApprover = () => {
    return get(APPROVAL_BASE_URL + '/')
}

const ApprovalService = {
    checkApprover,
    getApprover
};

export default ApprovalService;