import { get, post} from "../util/http";

const DISPOSAL_SERVICE_BASE_URL = process.env.REACT_APP_API_URL_Disposal_Service;

const getAllEvent = () => {
    return get(DISPOSAL_SERVICE_BASE_URL);
};

const getOneEvent = id => {
    return get(DISPOSAL_SERVICE_BASE_URL+`/${id}`);
};

const createEvent = (data) => {
    return post(DISPOSAL_SERVICE_BASE_URL+`/add`,data);
};



const RingiEventService = {
  getAllEvent,
  getOneEvent,
  createEvent,  
};

export default RingiEventService;