import { get } from "../util/http"

const getFlowstep = async () => {
    return get(process.env.REACT_APP_API_URL_Flowstep_Service) 
}

const FlowstepService = {
    getFlowstep
}

export default FlowstepService