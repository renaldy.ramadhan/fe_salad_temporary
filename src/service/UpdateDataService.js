import {del, put, get, post} from "../util/http";

const PART_SERVICE_BASE_URL = process.env.REACT_APP_API_URL;



const updateData = data => {
    return put(PART_SERVICE_BASE_URL+`/item-pivot/update/`, data);
};

const removeRequest = id => {
    return del(PART_SERVICE_BASE_URL+`/delete/${id}`);
};


const UpdateDataService = {
  updateData,
  removeRequest
};

export default UpdateDataService;