import {del, get, post} from "../util/http";

const PART_SERVICE_BASE_URL = process.env.REACT_APP_API_URL_Part_Request_Service;

const getAllRequest = () => {
    return get(PART_SERVICE_BASE_URL+`?type=Ringi`);
};

const getRequest = id => {
    return get(PART_SERVICE_BASE_URL+`/${id}`);
};

const createRequest = data => {
    return post(PART_SERVICE_BASE_URL+`/add`,data);
};

// const createRequest = (files,data) => {
//     return postMultipartRequest(PART_SERVICE_BASE_URL+`/add`,files,data);
// };

const removeRequest = id => {
    return del(PART_SERVICE_BASE_URL+`/delete/${id}`);
};


const RingiRequestService = {
  getAllRequest,
  getRequest,
  createRequest,
  removeRequest
};

export default RingiRequestService;