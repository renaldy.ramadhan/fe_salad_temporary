import { FaHome, FaToolbox, FaBoxOpen, FaExclamationTriangle, FaFileAlt, FaFileImport, FaUserClock, FaBell, FaSearch, FaDownload, FaEdit } from 'react-icons/fa';
import { FaSort, FaSortDown, FaSortUp, FaRegTrashCan, FaTrash, FaRegPenToSquare, FaCircleUser } from "react-icons/fa6";

export {
  //Side Menu
  FaHome, //dashboard
  FaToolbox, //part
  FaBoxOpen, //part request
  FaExclamationTriangle, //discrepancy
  FaTrash, //disposal event
  FaFileAlt, //disposal request
  FaFileImport, //ringi
  FaUserClock, //user log

  //Navbar
  FaCircleUser, //profile
  FaBell, //notification bell

  //Other component
  FaSearch, //search
  FaSort, //sort
  FaSortDown, //sort down
  FaSortUp, //sort up
  FaRegPenToSquare, //edit
  FaRegTrashCan, //delete
  FaDownload, //download
  FaEdit,
};