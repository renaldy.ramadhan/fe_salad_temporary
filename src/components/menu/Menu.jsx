// import React, { memo, useEffect, useState } from "react";
// import {
//   FaBoxOpen,
//   FaExclamationTriangle,
//   FaFileImport,
//   FaHome,
//   FaToolbox,
//   FaTrash,
//   FaUserClock,
// } from "../../icons/icon";
// import { NavLink, useLocation } from "react-router-dom";

// const RecursiveMenu = ({ link, isOpen, onToggle }) => {
//   const isLinkOpen = isOpen && link.name === isOpen;

//   return (
//     <li key={link.name}>
//       <div
//         className="flex items-center justify-between p-2 cursor-pointer"
//         onClick={() => onToggle(link.name)}
//       >
//         <div className="flex gap-2 items-center">
//           {link.logo}
//           <span>{link.name}</span>
//         </div>
//         <svg
//           data-accordion-icon
//           className={`w-3 h-3 transform transition-transform duration-300 ${isLinkOpen ? "rotate-0" : "rotate-180"
//             }`}
//           aria-hidden="true"
//           xmlns="http://www.w3.org/2000/svg"
//           fill="none"
//           viewBox="0 0 10 6"
//         >
//           <path
//             stroke="currentColor"
//             strokeLinecap="round"
//             strokeLinejoin="round"
//             strokeWidth="2"
//             d="M9 5 5 1 1 5"
//           />
//         </svg>
//       </div>
//       {isLinkOpen && link.children && (
//         <ul>
//           {link.children.map((subLink) => (
//             <RecursiveMenu
//               key={subLink.name}
//               link={subLink}
//               isOpen={isOpen}
//               onToggle={onToggle}
//             />
//           ))}
//         </ul>
//       )}
//     </li>
//   );
// };

// const Menu = memo(({ active, userRole, userDivision,sidebarVisible}) => {
//   const [open, setOpen] = useState(null);
//   const [isOpen, setIsOpen] = useState(false);
  
//   const activeLink =
//     "flex items-center bg-gray-100 font-medium p-2 h-full rounded-md";
//   const activeLinkDisposal =
//     "flex items-center bg-gray-100 font-medium hover:px-8 px-8 hover:p-2 p-2 h-full rounded-md";

//   const location = useLocation();

//   useEffect(() => {
//     if (
//       location.pathname.includes("/disposal/event") ||
//       location.pathname.includes("/disposal/request")
//     ) {
//       setIsOpen(true);
//       setOpen("Disposal");
//     } else {
//       setIsOpen(false);
//       setOpen(null);
//     }
//   }, [location]);

//   const links = [
//     {
//       name: "Dashboard",
//       logo: <FaHome />,
//       link: "/",
//     },
//     {
//       name: "Part Request",
//       logo: <FaBoxOpen />,
//       link: "/partrequest",
//     },
//     {
//       name: "Discrepancy",
//       logo: <FaExclamationTriangle />,
//       link: "/discrepancy",
//     },
//     {
//       name: "Disposal",
//       logo: <FaTrash />,
//       children: [
//         {
//           name: "General",
//           children: [
//             {
//               name: "Event",
//               link: "/disposal/event",
//             },
//             {
//               name: "Request",
//               link: "/disposal/request",
//             },
//           ],
//         },
//         {
//           name: "Ringi",
//           link: "/ringi",
//         },
//       ],
//     },

//     {
//       name: "Master Data",
//       logo: <FaUserClock />,

//       children: [
//         {
//           name: "Master Data Mgt",
//           link: "/masterdata",
//         },
//         {
//           name: "Approval Group",
//           link: "/approvalgroup",
//         },
//       ],
//     },
//   ];

//   const filteredLinks = links.filter((link) => {
//     if (link.name === "User Log" && userRole !== "Admin") {
//       return false; // Hide User Logs for non-admin users
//     }
//     return true; // Show other links for all other users
//   });
//   const [subLinkStates, setSubLinkStates] = useState(
//     links.map((link) => (link.children ? { [link.name]: false } : null))
//   );

//   // Function to toggle the isOpen state for a specific subLink
//   const toggleSubLink = (linkName) => {
//     setSubLinkStates((prevStates) => ({
//       ...prevStates,
//       [linkName]: !prevStates[linkName],
//     }));
//   };

//   return (
//     <div className={`w-36 md:w-44 pt-20 z-10 bg-white p-0 border-solid border-2 h-full  ${sidebarVisible ? 'visible' : 'hidden'}`}>
      
//     {sidebarVisible && (
//       <div className="max-w-full min-w-full">
//         <ul className="flex flex-col gap-2">
//           {console.log("filtered links", filteredLinks)}
//           {filteredLinks.map((link, index) => {
//             function openFunction() {
//               setIsOpen(!isOpen);
//               setOpen(link.name);
//             }
//             if (link.children) {
//               const isLinkOpen = isOpen && open === link.name;

//               return (
//                 <li key={index}>
//                   <div
//                     className="flex items-center justify-between p-2 cursor-pointer"
//                     onClick={openFunction}
//                   >
//                     <div className="flex gap-2 items-center">
//                       {link.logo}
//                       <span>{link.name}</span>
//                     </div>
//                     <svg
//                       data-accordion-icon
//                       className={`w-3 h-3 transform transition-transform duration-300 ${isLinkOpen ? "rotate-0" : "rotate-180"
//                         }`}
//                       aria-hidden="true"
//                       xmlns="http://www.w3.org/2000/svg"
//                       fill="none"
//                       viewBox="0 0 10 6"
//                     >
//                       <path
//                         stroke="currentColor"
//                         strokeLinecap="round"
//                         strokeLinejoin="round"
//                         strokeWidth="2"
//                         d="M9 5 5 1 1 5"
//                       />
//                     </svg>
//                   </div>
//                   {isLinkOpen && (
//                     <ul>
//                       {link.children.map((subLink, subIndex) => (
//                         <li key={subIndex}>
//                           {subLink.children ? (
//                             <div
//                               className="flex items-center justify-between py-2 pl-8 pr-2 cursor-pointer"
//                               onClick={() => toggleSubLink(subLink.name)}
//                             >
//                               <div className="flex gap-2 items-center">
//                                 <span>{subLink.name}</span>
//                               </div>
//                               <svg
//                                 data-accordion-icon
//                                 className={`w-3 h-3 transform transition-transform duration-300 ${subLinkStates[subLink.name]
//                                   ? "rotate-0"
//                                   : "rotate-180"
//                                   }`}
//                                 aria-hidden="true"
//                                 xmlns="http://www.w3.org/2000/svg"
//                                 fill="none"
//                                 viewBox="0 0 10 6"
//                               >
//                                 <path
//                                   stroke="currentColor"
//                                   strokeLinecap="round"
//                                   strokeLinejoin="round"
//                                   strokeWidth="2"
//                                   d="M9 5 5 1 1 5"
//                                 />
//                               </svg>
//                             </div>
//                           ) : (
//                             <NavLink to={subLink.link}>
//                               <div className="py-2 px-8 flex gap-2 items-center">
//                                 <span>{subLink.name}</span>
//                               </div>
//                             </NavLink>
//                           )}
//                           {subLink.children && subLinkStates[subLink.name] && (
//                             <ul>
//                               {subLink.children.map(
//                                 (nestedLink, nestedIndex) => (
//                                   <li key={nestedIndex}>
//                                     <NavLink
//                                       to={nestedLink.link}
//                                       className={`${active
//                                         ? activeLinkDisposal
//                                         : "flex items-center hover:bg-gray-200 pl-12 py-2 font-normal hover:h-full h-full"
//                                         }`}
//                                     >
//                                       <span className="flex gap-2 items-center">
//                                         {nestedLink.name}
//                                       </span>
//                                     </NavLink>
//                                   </li>
//                                 )
//                               )}
//                             </ul>
//                           )}
//                         </li>
//                       ))}
//                     </ul>
//                   )}
//                 </li>
//               );
//             } else if (link.name === "Disposal") {
//               return (
//                 <NavLink
//                   key={index}
//                   to={link.link}
//                   className={
//                     "flex items-center hover:bg-gray-200 hover:p-2 p-2 font-normal hover:h-full h-full"
//                   }
//                 >
//                   <span className="flex gap-2 items-center">
//                     {link.logo} {link.name}
//                   </span>
//                 </NavLink>
//               );
//             } else {
//               return (
//                 <NavLink
//                   key={index}
//                   to={link.link}
//                   className={({ isActive }) =>
//                     isActive
//                       ? activeLink
//                       : "flex items-center hover:bg-gray-200 hover:p-2 p-2 font-normal hover:h-full h-full"
//                   }
//                 >
//                   <span className="flex gap-2 items-center">
//                     {link.logo} {link.name}
//                   </span>
//                 </NavLink>
//               );
//             }
//           })}
//         </ul>
//       </div>
//       )}
//     </div>
//   );
// });

// export default Menu;
import * as React from 'react';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import InboxIcon from '@mui/icons-material/MoveToInbox';
import MailIcon from '@mui/icons-material/Mail';

export default function Sidebar() {
  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
          <ListItem key={text} disablePadding>
            <ListItemButton>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Divider />
      <List>
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
          <ListItem key={text} disablePadding>
            <ListItemButton>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <div>
      {['left'].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button>
          <Drawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
          >
            {list(anchor)}
          </Drawer>
        </React.Fragment>
      ))}
    </div>
  );
}
