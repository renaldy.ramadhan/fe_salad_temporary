import React, { useState } from "react";

const TabNav = ({ tabs, activeTab, handleTabChange }) => (
  <div className="flex flex-row justify-between items-center">
    <h2 className="text-lg font-semibold">Disposal</h2>
    <ul className="flex flex-wrap text-sm font-medium text-center text-gray-500 border-b border-gray-200">
      {tabs.map((tab, index) => (
        <li
          className={`mr-2 rounded-t-lg ${
            activeTab === tab.value ? "bg-gray-400" : "bg-gray-200"
          }`}
          key={index}
        >
          <button
            onClick={() => handleTabChange(tab.value)}
            className={`inline-block p-4 rounded-t-lg ${
              activeTab === tab.value
                ? "text-white"
                : "hover:text-gray-600 hover:bg-gray-300"
            }`}
          >
            {tab.label}
          </button>
        </li>
      ))}
    </ul>
  </div>
);

const DashboardTabs = ({ children }) => {
  // children = label, value & component
  const [activeTab, setActiveTab] = useState(children[0].value);

  const handleTabChange = (value) => {
    setActiveTab(value);
  };

  return (
    <div>
      <TabNav
        tabs={children}
        activeTab={activeTab}
        handleTabChange={handleTabChange}
      />

      {children.map((tab) => {
        if (tab.value === activeTab) {
          return <div key={tab.value}>{tab.component}</div>;
        }
        return null;
      })}
    </div>
  );
};

export default DashboardTabs;
