import React, { useState } from 'react';

const TabNav = ({ tabs, activeTab, handleTabChange }) => (
  <ul className="flex flex-wrap text-sm font-medium text-center text-gray-500 border-b border-gray-200">
    {tabs.map((tab, index) => (
      <li className={`mr-2 rounded-t-lg ${activeTab === tab.value ? 'bg-gray-400' : 'bg-gray-200'}`} key={index}>
        <button
          onClick={() => handleTabChange(tab.value)}
          className={`inline-block p-4 rounded-t-lg ${
            activeTab === tab.value ? 'text-white' : 'hover:text-gray-600 hover:bg-gray-300'
          }`}
        >
          {tab.label}
        </button>
      </li>
    ))}
  </ul>
);

const Tabs = ({ children }) => { // children = label, value & component
  const [activeTab, setActiveTab] = useState(children[0].value);

  const handleTabChange = (value) => {
    setActiveTab(value);
  };

  return (
    <div>
      <TabNav tabs={children} activeTab={activeTab} handleTabChange={handleTabChange} />

      {children.map((tab) => {
        if (tab.value === activeTab) {
          return <div key={tab.value}>{tab.component}</div>;
        }
        return null;
      })}
    </div>
  );
};

export default Tabs;
