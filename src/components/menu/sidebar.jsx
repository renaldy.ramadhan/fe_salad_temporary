export const menuItemsData = [
    // ...
    {
      title: 'Dashboard',
      url: '/',
      submenu: [
        {
          title: 'Part Request',
          url: 'partrequest',
        },
        {
          title: 'Discrepancy',
          url: 'discrepancy',
        },
        {
          title: 'Disposal',
          url: 'seo',
        },
        {
          title: 'SEO',
          url: 'seo',
        },
        {
          title: 'Master Data',
          url:'disposal',
          submenu: [
            {
              title: 'Part Request',
              url: 'partrequest',
            },
          ],
        },
      ],
      
    },
    // ...
  ];