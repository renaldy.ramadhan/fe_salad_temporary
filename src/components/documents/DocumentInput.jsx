import React, { useState } from 'react';

function DocumentInput() {
  const [selectedDocument, setSelectedDocument] = useState(null);

  const handleFileChange = (event) => {
    setSelectedDocument(event.target.files[0]);
  };

  return (
    <div className="">
      <label htmlFor="documentInput" className="block text-sm font-medium text-slate-700">
        Select a File
      </label>
      <input
        id="documentInput"
        type="file"
        className="border-2 border-gray-300 py-2 px-4 rounded-md md:w-1/2 w-2/3"
        onChange={handleFileChange}
      />
      {selectedDocument && (
        <div className="mt-4">
          <p className="font-medium">Selected File:</p>
          <p>{selectedDocument.name}</p>
        </div>
      )}
    </div>
  );
}

export default DocumentInput;
