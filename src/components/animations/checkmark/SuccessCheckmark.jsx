import React from 'react';
import '../../../css/animations/success-checkmark.css'

const SuccessCheckmark = ({ showSuccess, message }) => {
  if (!showSuccess) {
    return null;
  }

  return (
    <div className="max-w-sm rounded overflow-hidden shadow-lg bg-custom-gray fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-50">
        <div className="success-checkmark pt-10">
            <div className="check-icon">
                <span className="icon-line line-tip"></span>
                <span className="icon-line line-long"></span>
                <div className="icon-circle"></div>
                <div className="icon-fix"></div>
            </div>
        </div>
        <div className="flex flex-col px-6 py-4 justify-center items-center">
            <div className="font-bold text-xl mb-2 pt-5 ml-2 text-green-500">SUCCESS</div>
            <div className="text-md mb-2  ml-2 text-green-500 justify-center item-center">{message}</div>
        </div>
    </div>
  );
};

export default SuccessCheckmark;