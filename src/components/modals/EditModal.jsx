import React, { useState, useEffect } from 'react';
import useSearchStore from '../../store/searchStore';

const EditModal = ({ isOpen, onClose, index }) => {

  const [selectedItem, setSelectedItem] = useState(null);
  const [quantity, setQuantity] = useState(1);
  const [remark, setRemark] = useState('');
  const { tableData, editTableData } = useSearchStore((state) => state);

  const handleSubmit = () => {
    resetModal();
    editTableData(index, quantity, remark);
    onClose();
  };

  const resetModal = () => {
    setRemark('');
    setSelectedItem('');
    setQuantity(1);
  };

  useEffect(() => {
    if (index !== null && index !== undefined) {
        setSelectedItem(tableData[index]);
        setQuantity(tableData[index].ReqQty);
        setRemark(tableData[index].Remark);
      }
  }, [index]);

  return isOpen ? (
    <div className="fixed z-10 inset-0 overflow-y-auto flex items-center justify-center">
        <div className="flex items-center justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:px-20">
        <div className="fixed inset-0 transition-opacity" aria-hidden="true">
            <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div className="inline-block align-middle bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-xl sm:w-full max-h-90vh overflow-y-auto">
            <div className="bg-white p-6">
                {selectedItem && (
                    <div className="mt-4 border-t border-gray-200 pt-4">
                        <div className="font-semibold text-lg mb-2">Selected part: <span className="text-blue-600">{selectedItem.PartName}</span></div>
                        <div className="flex flex-col sm:flex-row sm:items-center sm:space-x-4">
                            <label className="block sm:inline-block mb-1 sm:mb-0 text-sm font-medium text-gray-700 mr-2">Quantity:</label>
                            <input
                                type="number"
                                value={quantity}
                                min={1}
                                max={1000}
                                onChange={(e) => setQuantity(e.target.value)}
                                className="px-3 py-2 border border-gray-300 rounded-md w-full sm:w-1/4 focus:outline-none"
                            />
                        </div>
                        <div className="flex flex-col sm:flex-row sm:items-center sm:space-x-4 pt-3">
                            <label className="block sm:inline-block mb-1 sm:mb-0 text-sm font-medium text-gray-700 mr-2">Remark   :</label>
                            <input
                                type="text"
                                value={remark}
                                onChange={(e) => setRemark(e.target.value)}
                                className="px-3 py-2 border border-gray-300 rounded-md w-full sm:w-1/2 focus:outline-none"
                            />
                        </div>
                    </div>
                )}
            </div>
            <div className="bg-gray-50 px-6 py-4 sm:flex sm:flex-row-reverse sm:space-x-reverse">
                <button onClick={handleSubmit} className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-custom-blue text-base font-medium text-white hover:bg-blue-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">
                    Edit
                </button>
                <button onClick={onClose} className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:mt-0 sm:w-auto sm:text-sm">
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>
  ) : null;
}

export default EditModal;
