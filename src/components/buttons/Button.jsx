import React from 'react'

const Button = ({children, type, onclick, disabled, classname, title}) => {
  return (
    <button 
      onClick={onclick} 
      type={type} 
      title = {title}
      className={classname} 
      disabled={disabled}>{children}</button>
  )
}

export default Button