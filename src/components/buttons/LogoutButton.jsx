import React, { useState, useEffect, useContext, useCallback } from "react";
import Button from '@mui/material/Button';
import { KeycloakContext } from "../../contexts/KeycloakProvider";

const LogoutButton = () => {
    const { keycloak, authenticated } = useContext(KeycloakContext);
    const name = keycloak.tokenParsed["name"];
    const role = keycloak.tokenParsed.resource_access?.["react-client"]?.roles;

  const handleLogout = () => {
    keycloak.logout();
  };

  return (
    <Button
      sx={{
        mr: 1,
        backgroundColor: '#17479D',
        border: 1,
        // Set the desired background color here
        '&:hover': {
          backgroundColor: 'white',
          color: '#17479D', // Set the desired hover color here
        },
      }}
      variant='contained'
      onClick={handleLogout}
    >
      Logout
    </Button>
  );
};

export default LogoutButton;
