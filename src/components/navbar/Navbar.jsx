// import React, { useContext, useState } from 'react'
// import { NavLink, useNavigate } from 'react-router-dom'
// import { FaBell, FaCircleUser } from "../../icons/icon";
// import { UserContext } from '../../contexts/UserContext'
// import LogoutModal from '../modals/LogoutModal';
// import { KeycloakContext } from '../../contexts/KeycloakProvider';
// import Button from '../buttons/Button';

// const Navbar = ({ src, toggleSidebar , active }) => {

//   const { keycloak, authenticated } = useContext(KeycloakContext);
//   const navigate = useNavigate()
//   const user = useContext(UserContext)
//   const [isModalOpen, setModalOpen] = useState(false);
//   const [sidebarVisible, setSidebarVisible] = useState(true);

//   const logoutAction = () => {
//     window.localStorage.removeItem('token')
//     keycloak.logout();
//   }

//   return (
//     <>
//       <div className='p-6 justify-between fixed top-0 flex w-full border-solid border drop-shadow-sm z-50 bg-white'>
//         <div className='flex items-center gap-4'>
//           <div className=' block items-center'>
//             <button onClick={toggleSidebar} className="flex h-8 items-center">
//               <div className="flex items-center justify-center rounded-full w-[40px] h-[40px] transform transition-all duration-200 shadow-md">
//                 <div className={`flex flex-col justify-between items-center w-[15px] h-[15px] transform duration-300 origin-center overflow-hidden ${active ? 'translate-x-1.5' : ''}`}>
//                   <div className={`bg-black h-[2px] w-7 transform transition-all duration-300 origin-left ${active ? 'rotate-[42deg] w-2/3 delay-150' : ''}`}></div>
//                   <div className={`bg-black h-[2px] w-7 rounded transform transition-all duration-300 ${active ? 'opacity-0' : ''}`}></div>
//                   <div className={`bg-black h-[2px] w-7 transform transition-all duration-300 origin-left ${active ? '-rotate-[42deg] w-2/3 delay-150' : ''}`}></div>
//                 </div>
//               </div>
//             </button>
//           </div>
//           <div className='flex justify-center items-center'>
//             <NavLink to={'/'}>
//               <img src={src} alt='./images/epson.png' className='w-14 md:w-20' />
//             </NavLink>
//           </div>
//         </div>
//         <div className='flex gap-2 sm:gap-6 items-center'>
//           <div className='cursor-pointer'>
//             <FaBell />
//           </div>
//           <div className={'cursor-pointer'}>
//             <FaCircleUser />
//           </div>
//           <Button
//             classname={'bg-custom-red py-1 px-3 w-full border-solid border-0 rounded text-sm text-white font-medium hover:opacity-90'}
//             onclick={() => setModalOpen(true)}>
//             Logout
//           </Button>
//         </div>
//       </div>
//       <LogoutModal isOpen={isModalOpen} onConfirm={logoutAction} onCancel={() => { setModalOpen(false) }} />
//     </>
//   )
// }

// export default Navbar

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            News
          </Typography>
          <Button color="inherit">Logout</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}