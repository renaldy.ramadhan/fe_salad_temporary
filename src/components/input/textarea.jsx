import React from "react";

export const Textarea = ({
  label,
  name,
  onChange,
  height,
  isRequired = false,
  value = "",
}) => {
  return (
    <div className="w-full space-y-4">
      <label className="font-medium">
        {label} <span className="text-red-500">{isRequired ? "*" : ""}</span>
      </label>
      <textarea
        name={name}
        id="textarea"
        rows={height}
        onChange={onChange}
        value={value}
        className="appearance-none border border-zinc-200 rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
      ></textarea>
    </div>
  );
};
