import React from "react";

export const SelectInput = ({
  label,
  name,
  onChange,
  children,
  isRequired = false,
  isDisabled = false,
}) => {
  return (
    <div className="w-full space-y-2">
      <label className="font-medium">
        {label} <span className="text-red-500">{isRequired ? "*" : ""}</span>
      </label>
      <select
        name={name}
        id="select_input"
        onChange={onChange}
        disabled={isDisabled}
        className="appearance-none border border-zinc-200 rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
      >
        <option selected>--Select--</option>
        {children}
      </select>
    </div>
  );
};
