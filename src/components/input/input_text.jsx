import React from "react";

export const InputText = ({
  label,
  onChange,
  type,
  name,
  isRequired = false,
  value = "",
  isDisabled = false,
}) => {
  return (
    <div className="w-full space-y-2">
      <label className="font-medium">
        {label} <span className="text-red-500">{isRequired ? "*" : ""}</span>
      </label>
      <input
        id="input"
        name={name}
        type={type}
        class="appearance-none border-2 border-zinc-300 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:border-blue-800 focus:outline-none focus:shadow-outline"
        onChange={onChange}
        value={value}
        disabled={isDisabled}
      />
    </div>
  );
};
