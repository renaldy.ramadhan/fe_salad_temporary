import React, { useMemo, useState, useEffect } from 'react';
import { useTable, useGlobalFilter, useSortBy } from 'react-table';
import axios from 'axios';
import { FaSortUp, FaSortDown, FaSort } from '../../../icons/icon'

const GlobalFilter = ({ filter, setFilter }) => {
  return (
    <input
      className="text-input border-2 text-gray-600 p-2 rounded"
      value={filter || ''}
      onChange={(e) => setFilter(e.target.value || undefined)}
      placeholder="Search..."
    />
  );
};

const SortableTable = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get('http://localhost:8080/part');
      console.log(result)
      setData(result.data.Parts);
    };

    fetchData();
  }, []);

  const columns = useMemo(
    () => [
      {
        Header: 'Part ID',
        accessor: 'PartId',
      },
      {
        Header: 'Part Name',
        accessor: 'PartName',
      },
      {
        Header: 'Stock',
        accessor: 'Stock',
      },
      {
        Header: 'Part Code',
        accessor: 'PartCode',
      },
      {
        Header: 'Part Desc',
        accessor: 'PartDesc',
      },
      {
        Header: 'Weight',
        accessor: 'Weight',
      },
      {
        Header: 'Amount',
        accessor: 'Amount',
      },
    ],
    []
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
    state,
    setGlobalFilter,
  } = useTable({ columns, data }, useGlobalFilter, useSortBy);

  const { globalFilter } = state;

  return (
    <div className="">
        <div className='mb-2 flex justify-end'>
            <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter}/>
        </div>
      {/* Table rendering code */}
      <table {...getTableProps()} className="min-w-full divide-y divide-gray-200">
        <thead>
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column) => (
                <th
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                  className="px-6 py-4 w-16 bg-gray-50 text-left text-xs leading-4 font-bold text-gray-500 uppercase tracking-wider"
                >
                  {column.render('Header')}
                  <span className='inline-flex'>
                    {column.isSorted ? (column.isSortedDesc ? <FaSortDown/> : <FaSortUp/>) : ''}
                  </span>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()} className="bg-white divide-y divide-gray-200">
          {rows.map((row) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map((cell) => {
                  return (
                    <td {...cell.getCellProps()} className="px-6 py-4 text-left whitespace-no-wrap max-h-20 overflow-y-auto">
                      {cell.render('Cell')}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default SortableTable;
