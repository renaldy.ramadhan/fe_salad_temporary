import React, { useState, useEffect, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {
  DataGrid,
} from '@mui/x-data-grid';
import CustomNoRowsOverlay from '../../icons/CustomNoRowsOverlay';

import {
  setActivePage, setActivePageSize,
} from "../../../slices/DiscrepancySlice";
import {
  getActiveApprovalByNikAndAssignedDiscrepancy,
} from "../../../slices/ActiveApprovalSlice";

const ActiveApproverTablesDiscrepancy = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch()

  const [rows, setRows] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});

  const [isLoading, setIsLoading] = useState(true);

  const { activePage, activePageSize } = useSelector((state) => state.discrepancy)
  const [pageState, setPageState] = useState({
    page: activePage,
    pageSize: activePageSize
  })

  const discrepancy = useSelector(state => state.activeapprovals.activeApprovalGetNikAssignedDiscrepancy)
  const discrepancyStatus = useSelector(state => state.activeapprovals.activeApprovalGetNikAssignedStatusDiscrepancy)
  const [rowCountState, setRowCountState] = useState(discrepancy.total);

  useEffect(() => {
    setRowCountState((prevRowCountState) =>
      discrepancy.total !== undefined ? discrepancy.total : prevRowCountState,
    );
    if (discrepancyStatus === "done") {
      setIsLoading(false)
      setRows(discrepancy.data.map((item) => ({ ...item, id: item.ID })));
    }
  }, [discrepancy, discrepancyStatus])

  useEffect(() => {
    dispatch(getActiveApprovalByNikAndAssignedDiscrepancy({ page: activePage, pageSize: activePageSize }));
  }, [dispatch, activePage, activePageSize]);

  const handleClick = (id) => {
    const selectedPartDiscrepancy = discrepancy.data.find(data => data.ID === id);
    navigate(`/discrepancy/view/approver/` + selectedPartDiscrepancy.Request.ID, { state: { data: selectedPartDiscrepancy, reqId: id } })
  };

  const columns = [
    {
      field: "PartRequestCode",
      headerName: "No",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      filterable: false,
      valueGetter: (params) => params.row?.Request.PartRequestCode,
    },
    {
      field: "Plant",
      headerName: "Plant",
      minWidth: 180,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.Request.LocationPlant.PlantCode
    },

    {
      field: "Requestor",
      headerName: "Requestor",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request.UserName,
    },
    {
      field: "RequestDepartment",
      headerName: "Request Departement",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Department.DepartmentName,
    },
    {
      field: "RequestDate",
      headerName: "Request Date",
      minWidth: 140,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request.CreatedAt,
    },
    {
      field: "RequestLocation",
      headerName: "GNS+Loc",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request.RequestLocation,
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 180,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: '100%',
        '& .actions': {
          color: 'text.secondary',
        },
        '& .textPrimary': {
          color: 'text.primary',
        },
        '& .MuiDataGrid-cell': {
          borderRight: '2px solid #ccc', // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        '& .MuiDataGrid-row': {
          borderBottom: '2px solid #ccc',
        },
      }}
    >
      <DataGrid
        // autoHeight
        rows={rows}
        columns={columns}
        editMode="row"
        loading={isLoading}
        rowCount={rowCountState}

        pageSize={activePageSize}
        pageSizeOptions={[5, 10, 20]}

        pagination
        paginationMode="server"
        paginationModel={{ page: activePage - 1, pageSize: activePageSize }}
        onPaginationModelChange={(val) => {
          dispatch(setActivePage(val.page + 1))
          dispatch(setActivePageSize(val.pageSize))
          setPageState(val)
        }}

        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>

  );
}

export default ActiveApproverTablesDiscrepancy;
