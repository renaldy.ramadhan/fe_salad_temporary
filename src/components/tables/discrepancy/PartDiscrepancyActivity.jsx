import React, { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { DataGrid } from "@mui/x-data-grid";
import { useDispatch, useSelector } from "react-redux";
import CustomNoRowsOverlay from "../../icons/CustomNoRowsOverlay";

import {
  setPage, setPageSize, retrieveDiscrepancy
} from "../../../slices/DiscrepancySlice";

const PartDiscrepancyActivity = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [rows, setRows] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});

  const [isLoading, setIsLoading] = useState(true);

  const { page, pageSize } = useSelector((state) => state.discrepancy)
  const [pageState, setPageState] = useState({
    page,
    pageSize
  })

  const discrepancy = useSelector(
    (state) => state.discrepancy.retrieveDiscrepancy
  );
  const discrepancyStatus = useSelector(
    (state) => state.discrepancy.retrieveDiscrepancyStatus
  );
  const [rowCountState, setRowCountState] = useState(discrepancy.data?.totalData);

  const [plantCode, setPlantCode] = useState("Default Plant Name");
  
  useEffect(() => {
    setRowCountState((prevRowCountState) =>
      discrepancy.data?.totalData !== undefined ? discrepancy.data?.totalData : prevRowCountState,
    );
    console.log("disc: ", discrepancy);
    if (discrepancy.status === 200 && discrepancyStatus === "done") {
      setIsLoading(false);
      setRows(discrepancy.data.data.map((item) => ({ ...item, id: item.ID })));
    }
  }, [discrepancy]);

  useEffect(() => {
    dispatch(retrieveDiscrepancy({ page, pageSize }));
  }, [dispatch, page, pageSize]);

  // const handleClick = (id) => {
  //   navigate(`/discrepancy/view/` + id, { state: { data: discrepancy[id], reqId: id } })
  // };

  const handleClick = (id) => {
    console.log(discrepancy.data);
    const selectedPartDiscrepancy = discrepancy.data.data.find(
      (data) => data.ID === id
    );
    console.log("selected part discrepancy: ", selectedPartDiscrepancy);
    navigate(`/discrepancy/view/` + id, {
      state: { data: selectedPartDiscrepancy, reqId: id },
    });
  };

  const formatDate = (dateString) => {
    if (!dateString) return ''; // Handle null or undefined dateString
    const date = new Date(dateString);
    const year = date.getFullYear().toString().padStart(4, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hour = date.getHours().toString().padStart(2, "0");
    const minute = date.getMinutes().toString().padStart(2, "0");
    const second = date.getSeconds().toString().padStart(2, "0");
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
};

  const columns = [
    {
      field: "PartRequestCode",
      headerName: "No",
      filterable: false,
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "Plant",
      headerName: "Plant",
      minWidth: 180,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.LocationPlant.PlantCode,
    },
    {
      field: "Requestor",
      headerName: "Requestor",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.UserName,
    },
    {
      field: "RequestDepartment",
      headerName: "Request Departement",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.User.Department,
    },
    {
      field: "RequestDate",
      headerName: "Request Date",
      minWidth: 140,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => formatDate(params.row?.RequestDate),
    },
    {
      field: "RequestLocation",
      headerName: "GNS+Loc",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "IFStatus",
      headerName: "IF Status",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.Status,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 120,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={rows}
        columns={columns}
        editMode="row"
        loading={isLoading}
        rowCount={rowCountState}

        pageSize={pageSize}
        pageSizeOptions={[5, 10, 20]}

        pagination
        paginationMode="server"
        paginationModel={{ page: page - 1, pageSize }}
        onPaginationModelChange={(val) => {
          dispatch(setPage(val.page + 1))
          dispatch(setPageSize(val.pageSize))
          setPageState(val)
        }}

        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>
  );
};

export default PartDiscrepancyActivity;
