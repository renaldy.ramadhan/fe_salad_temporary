import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { DataEditor, GridColumn, GridCellKind } from "@glideapps/glide-data-grid";




const MyGridComponent = (props ) => {
  const [data, setTableData] = useState(props.data);
  const [user, setUser] = useState(props.user);
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const getNavigationUrl = props.getNavigationUrl; // Function to determine the navigation URL
  const [gnsLocCount, setGnsLocCount] = useState('1A');
  // Sample data
  
  // const data = [
  //   { id: 1, name: "Alice", age: 30 },
  //   { id: 2, name: "Bob", age: 22 },
  //   { id: 3, name: "Carol", age: 33 }
  // ];
  useEffect(() => {
    console.log("props user:", props.user);
    setTableData(props.data);
    setUser(props.user);
    setLoading(false);
  }, [props.data, props.user]);
  
  // Define columns
  const columns = [
    { title: "PLANT", width: 100 },
    { title: "NUMBER", width: 200 },
    { title: "REQUESTOR", width: 100 },
    { title: "REQUEST DEPARTMENT", width: 200 },
    { title: "REQUEST DATE", width: 200 },
    { title: "GNS_LOC", width: 100 },
    { title: "STATUS", width: 100 },
    { title: "IF STATUS", width: 200 },
    { title: "ACTION", width: 100 }
  ];

  // Function to get cell data
  const getCellContent = (row, column) => {
    const item = data[row];
    switch (column) {
      case 0:
        return { kind: GridCellKind.Text, displayData: item.id.toString() };
      case 1:
        return { kind: GridCellKind.Text, displayData: item.name };
      case 2:
        return { kind: GridCellKind.Text, displayData: item.age.toString() };
      default:
        return { kind: GridCellKind.Text, displayData: "" };
    }
  };

  return (
    
    <table className="min-w-full divide-y divide-gray-200">
      <columns/>
    <tbody className="bg-white divide-y divide-gray-200">
      {data.map((row, idx) => (
        <tr key={row.id} className="cursor-pointer border-b hover:bg-gray-200">
          <td className="py-2 h-12 text-[18px]">{row.name}</td>
          <td className="py-2 h-12">{row.id}</td>
          <td className="py-2 h-12">{/* UserName placeholder */}</td>
          <td className="py-2 h-12">{/* RequestLocation placeholder */}</td>
          <td className="py-2 h-12">{/* CreatedAt placeholder */}</td>
          <td className="py-2 h-12 font-size-12">{/* GNS Location logic placeholder */}</td>
          <td className="py-2 h-12">{/* Status placeholder */}</td>
          <td className="py-2 h-12">
            <div className="flex gap-2 justify-center items-center pr-2">
              <button
                className="inline-flex items-center min-w-12 h-8 px-3 py-2 bg-gray-500 hover:bg-gray-400 text-white text-sm font-medium rounded-md"
                onClick={() => {
                  const navigationUrl = getNavigationUrl(row.id, row);
                  navigate(navigationUrl, { state: { key: row.id, data: row } });
                }}
                title='Details'
              >
                View
              </button>
            </div>
          </td>
        </tr>
      ))}
    </tbody>
  </table>
  );
};

export default MyGridComponent