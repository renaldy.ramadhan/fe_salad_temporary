import { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import { DataGrid } from "@mui/x-data-grid";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";
import VisibilityIcon from "@mui/icons-material/Visibility";
import CustomNoRowsOverlay from "../../../icons/CustomNoRowsOverlay";
import { unwrapResult } from '@reduxjs/toolkit';


import {
  setPageRequest, setPageSizeRequest, getDisposalEventRequest, setApprovalRequestStatus
} from "../../../../slices/DisposalEventSlice";

import { getRingiRequest } from '../../../../slices/RingiRequestSlice';

const PartDiscrepancyActivity = ({ type }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { id } = useParams();

  const [rows, setRows] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});

  const [isLoading, setIsLoading] = useState(true);

  const { pageRequest, pageSizeRequest } = useSelector((state) => state.disposalEvent)
  const [pageState, setPageState] = useState({
    page: pageRequest,
    pageSize: pageSizeRequest
  })

  const approvalRequest = useSelector(
    (state) => state.disposalEvent.approvalRequest
  );
  const approvalRequestStatus = useSelector(
    (state) => state.disposalEvent.approvalRequestStatus
  );
  const [rowCountState, setRowCountState] = useState(approvalRequest.data?.totalData);

  useEffect(() => {
    setRowCountState((prevRowCountState) =>
      approvalRequest.data?.totalData !== undefined ? approvalRequest.data?.totalData : prevRowCountState,
    );

    if (approvalRequest.status === 200 && approvalRequestStatus === "done") {
      setIsLoading(false);
      setRows(approvalRequest.data.data.map((item) => ({ ...item, id: item.ID })));
    }
  }, [approvalRequest]);

  // useEffect(() => {
  //   if (approvalRequestStatus === 'done') {
  //     dispatch(setApprovalRequestStatus('loading'))
  //   }
  // }, [dispatch, approvalRequestStatus])

  useEffect(() => {
    if (approvalRequestStatus === 'loading') {
      dispatch(getDisposalEventRequest({ type, id, page: pageRequest, pageSize: pageSizeRequest }));
    }
  }, [dispatch, pageRequest, pageSizeRequest]);

  const handleClick = (id) => {
    // Dispatch getRingiRequest action to fetch the request details by ID
    dispatch(getRingiRequest(id)).then((action) => {
      // Use unwrapResult to check if the promise was fulfilled
      const resultAction = unwrapResult(action);
      if (resultAction.status === 200) {
        // Navigate to ViewRingiRequest page with necessary state
        navigate(`/ringi/request/view/${id}`, {
          state: { data: resultAction.data },
        });
      } else {
        // Handle the case where fetching the request details fails
        console.error("Failed to fetch ringi request details");
      }
    });
  };
  

  const columns = [
    {
      field: "Plant",
      headerName: "Plant",
      minWidth: 180,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.LocationPlant.PlantCode,
    },
    {
      field: "PartRequestCode",
      headerName: "Number",
      minWidth: 180,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "Requestor",
      headerName: "Requestor",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.UserName,
    },
    {
      field: "RequestDepartment",
      headerName: "Request Departement",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.User.Department,
    },
    {
      field: "DisposalType",
      headerName: "Type",
      minWidth: 140,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "RequestLocation",
      headerName: "GNS+Loc",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 120,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={rows}
        columns={columns}
        editMode="row"
        loading={isLoading}
        rowCount={rowCountState}

        pageSize={pageSizeRequest}
        pageSizeOptions={[1, 5, 10, 20]}

        pagination
        paginationMode="server"
        paginationModel={{ page: pageRequest - 1, pageSize: pageSizeRequest }}
        onPaginationModelChange={(val) => {
          dispatch(setPageRequest(val.page + 1))
          dispatch(setPageSizeRequest(val.pageSize))
          setPageState(val)
          dispatch(getDisposalEventRequest({ id, page: val.page + 1, pageSize: val.pageSize }))
        }}

        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>
  );
};

export default PartDiscrepancyActivity;
