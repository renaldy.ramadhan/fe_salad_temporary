import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { DataGrid } from "@mui/x-data-grid";
import CustomNoRowsOverlay from "../../../icons/CustomNoRowsOverlay";
import {
  retrieveActiveDisposalEvent,
  setActiveDisposalPage,
  setActiveDisposalPageSize,
} from "../../../../slices/DisposalEventSlice";

const ActiveDisposalEventTable = ({ loading, parentColumn, parentRow }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [rows, setRows] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});

  const [isLoading, setIsLoading] = useState(true);
  const [isPageChange, setIsPageChange] = useState(false);

  const {
    activeDisposalEvent,
    activeDisposalEventQueryParams,
    retrieveDisposalEventStatus,
  } = useSelector((state) => state.disposalEvent);

  console.log("active disposal event", activeDisposalEvent);
  console.log("rows disposal event", rows);

  useEffect(() => {
    if (retrieveDisposalEventStatus === "done") {
      setIsLoading(false);
      setRows(
        activeDisposalEvent?.data?.map((event) => {
          return { ...event, id: event.ID };
        })
      );
    }
    return () => {
      setRows([]);
    };
  }, [activeDisposalEvent, retrieveDisposalEventStatus]);

  useEffect(() => {
    dispatch(retrieveActiveDisposalEvent());
  }, [dispatch, isPageChange]);

  const handleClick = (id) => {
    navigate(`/disposal/general/event/view/` + id, {});
  };

  console.log("rows : ", rows);

  const formatDate = (dateString) => {
    if (!dateString) return ""; // Handle null or undefined dateString
    const date = new Date(dateString);
    const year = date.getFullYear().toString().padStart(4, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hour = date.getHours().toString().padStart(2, "0");
    const minute = date.getMinutes().toString().padStart(2, "0");
    const second = date.getSeconds().toString().padStart(2, "0");
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
  };

  const columns = [
    {
      field: "PartRequestCode",
      headerName: "No",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      filterable: false,
      valueGetter: (params) => params.row?.Request.PartRequestCode,
    },
    {
      field: "Plant",
      headerName: "Plant",
      minWidth: 180,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.Request.LocationPlant.PlantCode,
    },

    {
      field: "Requestor",
      headerName: "Requestor",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request.UserName,
    },
    {
      field: "RequestDepartment",
      headerName: "Request Departement",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Department.DepartmentName,
    },
    {
      field: "RequestDate",
      headerName: "Request Date",
      minWidth: 140,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => formatDate(params.row?.Request.CreatedAt),
    },
    {
      field: "RequestLocation",
      headerName: "GNS+Loc",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request.RequestLocation,
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 180,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={parentRow !== undefined ? parentRow : rows}
        columns={parentColumn !== undefined ? parentColumn : columns}
        editMode="row"
        loading={loading !== undefined ? loading : isLoading}
        rowCount={activeDisposalEvent?.total}
        pageSize={activeDisposalEventQueryParams?.page_size}
        pageSizeOptions={[5, 10, 20]}
        pagination
        paginationMode="server"
        paginationModel={{
          page: activeDisposalEventQueryParams?.page - 1,
          pageSize: activeDisposalEventQueryParams?.page_size,
        }}
        onPaginationModelChange={(val) => {
          dispatch(setActiveDisposalPage(val.page + 1));
          dispatch(setActiveDisposalPageSize(val.pageSize));
          setIsPageChange(!isPageChange);
        }}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>
  );
};

export default ActiveDisposalEventTable;
