import React from "react";

const PartsGeneralDisposalEventTable = ({handleEditTable, handleDeleteTable, partRequest, tag }) => {

    const data = [
        {
            plant: 'D101' ,
            number:'101',
            requestor:"Vincent",
            requestdept:"DevOps",
            type:"System",
            GNSLOC:"D1A1",
            status:"Approved",
            partcode: "PC1002",
            partname: "Printer101",
            quantity: '101',
            weight: '10KG',
            amount: '101'
        },
        {
            plant: 'D102' ,
            number:'102',
            requestor:"Oscar",
            requestdept:"Data",
            type:"Non-System",
            GNSLOC:"D1A2",
            status:"Pending",
            partcode: "PC1003",
            partname: "Printer101",
            quantity: '101',
            weight: '10KG',
            amount: '102'
        },
        {
            plant: 'D103' ,
            number:'103',
            requestor:"Richie",
            requestdept:"Tech",
            type:"System",
            GNSLOC:"D1A3",
            status:"Approved",
            partcode: "PC1004",
            partname: "Printer101",
            quantity: '101',
            weight: '10KG',
            amount: '103'
        }
    ]

    return (
      <table className="w-full text-sm text-gray-500 text-center">
        {/* table headers */}
        <thead className="text-xs text-gray-700 uppercase bg-gray-100 sticky top-0">
          <tr>
                <th scope="col" className="px-3 py-6 w-20">
                  Plant
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  GNS+Loc
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Part Code
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Part Name
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Type
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Quantity
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Weight
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Amount
                </th>
          </tr>
        </thead>
        {/* table body */}
        <tbody>
          {data && data.length > 0 ? (
              data.map((item, index) => (
                  
                  <tr key={index} className="bg-white border-b hover:bg-gray-200">
                      <td className="px-6 py-4">{item.plant}</td>
                      <td scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                          {item.GNSLOC}
                      </td>
                      <td className="px-6 py-4">{item.partcode}</td>
                      <td className="px-6 py-4">{item.partname}</td>
                      <td className="px-6 py-4">{item.type}</td>
                      <td className="px-6 py-4">{item.quantity}</td>
                      <td className="px-6 py-4">{item.weight}</td>
                      <td className="px-6 py-4">{item.amount}</td>
                  </tr>
              ))
          ) : (
              <tr></tr>
          )}
        </tbody>
    </table>
    ) 
}

export default PartsGeneralDisposalEventTable