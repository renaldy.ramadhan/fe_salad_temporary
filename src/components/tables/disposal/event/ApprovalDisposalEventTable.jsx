import React from 'react';

const ApprovalDisposalEventTable = ({ dataTable, handleEditTable, handleDeleteTable, part }) => {

  const data = [
    {
        seq:1,
        user:'user 1',
        title:"title 1",
        start:"2023-06-26",
        end:"2023-07-10",
    },
    {
        seq:2,
        user:'user 2',
        title:"title 2",
        start:"2023-06-28",
        end:"2023-07-08",
    },
    {
        seq:3,
        user:'user 3',
        title:"title 3",
        start:"2023-06-25",
        end:"2023-07-05",
    }
]
  return (
    <table className="w-full text-sm text-gray-500 text-center">
      {/* table headers */}
      <thead className="text-xs text-gray-700 uppercase bg-gray-100 sticky top-0">
        <tr>
          <th scope="col" className="px-3 py-6 w-20">
            Seq
          </th>
          <th scope="col" className="px-3 py-6 w-20">
            Title
          </th>
          <th scope="col" className="px-3 py-6 w-20">
            User
          </th>
          <th scope="col" className="px-3 py-6 w-20">
            Due Date
          </th> 
        </tr>
      </thead>
      {/* table body */}
      <tbody>
        {data.map((item,index) => (
            <tr key={index} className="bg-white border-b hover:bg-gray-200">
            <td className="px-6 py-4">{item.seq}</td>
            <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                {item.title}
            </th>
            <td className="px-6 py-4">
                {item.user}
            </td>
            <td className="px-6 py-4">
                {item.end}
            </td>
        </tr>
        ))}
      </tbody>
    </table>
  );
};

export default ApprovalDisposalEventTable;
