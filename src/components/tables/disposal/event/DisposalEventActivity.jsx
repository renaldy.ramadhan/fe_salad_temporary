import React, { useState, useEffect, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {
  DataGrid,
} from '@mui/x-data-grid';
import { useSelector } from "react-redux";
import CustomNoRowsOverlay from '../../../icons/CustomNoRowsOverlay';

const DisposalEventActivity = () => {
  const navigate = useNavigate();
  const [rows, setRows] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});
  const [isLoading, setIsLoading] = useState(true);

//   const disposalEvents = useSelector((state) => state.disposalevents.disposalEvent);
//   const disposalEventsStatus = useSelector((state) => state.dispoalevents.disposalEventStatus);
const disposalEvents = useSelector((state) => state.disposalEvent.disposalEvent);
const disposalEventsStatus = useSelector((state) => state.disposalEvent.disposalEventStatus);

useEffect(() => {
    if (disposalEventsStatus === "done") {
      setIsLoading(false);
      setRows(disposalEvents.map((item) => ({ ...item, id: item.ID })));
    } 
  }, [disposalEvents, disposalEventsStatus]);
  

//   const handleClick = (id) => {
//     console.log(disposalEvents.data)
//     const selectedDisposalEvent = disposalEvents.data.find(data => data.ID === id);
//     console.log("selected part request: ", selectedDisposalEvent)
//     navigate(`/disposal/general/event/view/` + id, { state: { data: selectedDisposalEvent, reqId: id } })
//   };

// const handleClick = (id) => {
//     console.log('disposalEvents', disposalEvents)
//     if (disposalEvents && Array.isArray(disposalEvents.data)) {
//       const selectedDisposalEvent = disposalEvents.data.find(data => data.ID === id);
//       console.log("selected part request: ", selectedDisposalEvent)
//       navigate(`/disposal/general/event/view/` + id, { state: { data: selectedDisposalEvent, reqId: id } })
//     } else {
//       console.error('disposalEvents.data is undefined or not an array');
//     }
//   };

const handleClick = (id) => {
    // Since disposalEvents is already an array, you can directly call find on it
    const selectedDisposalEvent = disposalEvents.find(event => event.ID === id);
    console.log("selected disposal event: ", selectedDisposalEvent);
    navigate(`/disposal/general/event/view/` + id, { state: { data: selectedDisposalEvent, reqId: id } });
  };
  
  

  const columns = [
    {
        field: "ID",
        headerName: "ID",
        filterable: false,
        minWidth: 120, 
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
    {
        field: "Reference",
        headerName: "Reference  ",
        filterable: false,
        minWidth: 120, 
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
    { 
      field: 'Name', 
      headerName: 'Name', 
      minWidth: 180, 
      flex: 1,
      editable: false, 
      align: 'center',
      headerAlign: 'center',
      valueGetter: (params) => params.row?.Name
    },
    {
      field: 'Date',
      headerName: 'Date',
      minWidth: 180, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.EndDate
    },
    {
      field: 'Period Start',
      headerName: 'Period Start',
      minWidth: 180, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.StartDate    
    },
    {
      field: 'Period End',
      headerName: 'Period End',
      minWidth: 180, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.EndDate    
    },
    {
      field: 'Created By',
      headerName: 'Created By',
      minWidth: 140, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.CreatedBy
    },
    {
      field: 'Created Date',
      headerName: 'Created Date',
      minWidth: 140, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.CreatedAt
    },
    {
      field: 'Status',
      headerName: 'Status',
      minWidth: 140, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.Status
    },
    {
        field: 'actions',
        type: 'actions',
        headerName: 'Actions',
        minWidth: 120, 
        flex: 1,
        cellClassName: 'actions',
        align: 'center',
        headerAlign: 'center',
        getActions: ({ id }) => {
          return [
            <Button color="primary" style={{ backgroundColor: "#808080" }} variant="contained" startIcon={<VisibilityIcon />} onClick={() => handleClick(id)}>
                View
            </Button>,
          ];
        },
      },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: '100%',
        '& .actions': {
          color: 'text.secondary',
        },
        '& .textPrimary': {
          color: 'text.primary',
        },
        '& .MuiDataGrid-cell': {
          borderRight: '2px solid #ccc', // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        '& .MuiDataGrid-row': {
          borderBottom: '2px solid #ccc',
        },
      }}
    >
      <DataGrid
        rows={rows}
        columns={columns}
        editMode="row"
        loading={isLoading}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>
    
  );
}

export default DisposalEventActivity;