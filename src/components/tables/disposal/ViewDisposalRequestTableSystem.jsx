import React, { useCallback, useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import FactCheckIcon from "@mui/icons-material/FactCheck";
import { useParams } from "react-router-dom";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  GridRowModes,
  DataGrid,
  GridToolbarContainer,
  GridActionsCellItem,
  GridRowEditStopReasons,
} from "@mui/x-data-grid";

import { randomId } from "@mui/x-data-grid-generator";

import { useDispatch, useSelector } from "react-redux";
import {
  getActiveApprovalByRequestId,
  approvalAction,
} from "../../../slices/ActiveApprovalSlice";
import { editPartRequestData } from "../../../slices/updateDataSlice";
import CustomNoRowsOverlay from "../../icons/CustomNoRowsOverlay";

const ViewDisposalRequestTableSystem = ({
  itemsData,
  purcGroup,
  onDataSend,
}) => {
  const dispatch = useDispatch();
  const { id } = useParams();
  const partrequests = useSelector(
    (state) => state.partrequests.partGetRequest
  );
  const partrequestsStatus = useSelector(
    (state) => state.partrequests.partGetRequestStatus
  );
  console.log(id);
  const [rows, setRows] = useState([]);

  const [isLoading, setIsLoading] = useState(true);
  console.log("items data:", itemsData);
  const [rowModesModel, setRowModesModel] = React.useState({});

  // useEffect(() => {
  //   console.log("part: ", partrequests);
  //   if (partrequests.status === 200 && partrequestsStatus === "done") {
  //     setIsLoading(false);
  //     setRows(
  //       partrequests.data?.ItemsData.map((item) => ({ ...item, id: item.ID }))
  //     );

  //     console.log("row", rows);
  //   }
  // }, [partrequests]);

  useEffect(() => {
    setRows(itemsData)
    rows && setIsLoading(false)
  }, [itemsData])

  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;
  }

  const calculateAmount = (pricePerUnit, requestQuantity) => {
    const price = parseFloat(pricePerUnit.replace(",", "."));
    const quantity = parseFloat(requestQuantity);
    if (isNaN(price) || isNaN(quantity)) {
      return "";
    }
    return (price * quantity).toFixed(2);
  };

  const processRowUpdate = async (newRow) => {
    console.log("processRowUpdate")
    const updatedRow = { ...newRow, isNew: false };
    onDataSend(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));

    const actionResult = await dispatch(
      editPartRequestData([
        {
          IdItemPivot: updatedRow.id,
          ItemCode: updatedRow.ItemCode,
          RequestQuantity: updatedRow.RequestQuantity,
          ApprovedQuantity: updatedRow.ApprovedQuantity,
          Notes: updatedRow.Notes,
        },
      ])
    );
    const result = unwrapResult(actionResult);

    return updatedRow;
  };

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState("");

  const handleSaveClick = (params) => () => {
    setRowModesModel({
      ...rowModesModel,
      [params.id]: { mode: GridRowModes.View },
    });
    console.log("params:", params);
  };

  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      onDataSend(rows);
      event.defaultMuiPrevented = true;
    }
  };

  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleDeleteClick = (id) => () => {
    setRows(rows.filter((row) => row.id !== id));
  };

  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  // useEffect(() => {
  //   console.log('Rows updated:', rows);
  //   // handleRowModesModelChange();

  // }, [rows]);

  const columns = [
    {
      field: "ItemCodeFk",
      headerName: "Item Code",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
      // preProcessEditCellProps: (params) => {
      //   handleItemCodeChange(params.props.value, params.id);
      //   return params.props;
      // }
    },
    {
      field: "ItemName",
      headerName: "Item Name",
      minWidth: 280,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "RequestedQuantity",
      headerName: "Request Quantity",
      type: "number",
      maxWidth: 150,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "SystemQuantity",
      headerName: "System Quantity",
      maxWidth: 150,
      flex: 1,
      type: "number",
      align: "center",
      headerAlign: "center",
      editable: false
    },
    {
      field: "ApprovedQuantity",
      headerName: "Approved Quantity",
      minWidth: 150,
      flex: 1,
      type: "number",
      align: "center",
      headerAlign: "center",
      editable: true,
    },
    {
      field: "PricePerUnit",
      headerName: "Price Per Unit",
      minWidth: 120,
      flex: 1,
      type: "number",
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "Amount",
      headerName: "Amount",
      minWidth: 80,
      flex: 1,
      type: "number",
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "Notes",
      headerName: "Remark",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "NetWeight",
      headerName: "Weight",
      minWidth: 150,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 160,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: (params) => {
        // console.log("params",params);
        const isInEditMode =
          rowModesModel[params.id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: "primary.main",
              }}
              onClick={handleSaveClick(params)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(params.id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(params.id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(params.id)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={rows}
        columns={columns}
        editMode="row"
        rowModesModel={rowModesModel}
        loading={isLoading}
        onRowModesModelChange={handleRowModesModelChange}
        onRowEditStop={handleRowEditStop}
        processRowUpdate={processRowUpdate}
        slots={{
          toolbar: EditToolbar,
          noRowsOverlay: CustomNoRowsOverlay,
        }}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
      />
    </Box>
  );
};

export default ViewDisposalRequestTableSystem;
