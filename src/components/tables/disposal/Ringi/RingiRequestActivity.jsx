import React, { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { DataGrid } from "@mui/x-data-grid";
import { useDispatch,useSelector } from "react-redux";
import CustomNoRowsOverlay from "../../../icons/CustomNoRowsOverlay";


import {
  setPage, setPageSize,
  retrieveRingiRequest,
} from "../../../../slices/RingiRequestSlice";

const RingiRequestActivity = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [rows, setRows] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const { page, pageSize } = useSelector((state) => state.ringirequests)
  const [pageState, setPageState] = useState({
    page,
    pageSize
  });

const ringiRequests = useSelector(
    (state) => state.ringirequests.retrieveRingiRequest
  );
  const ringiRequestsStatus = useSelector(
    (state) => state.ringirequests.retrieveRingiRequestStatus
  );
  const [rowCountState, setRowCountState] = useState(ringiRequests.data?.totalData);


  // useEffect(() => {
  //   console.log("Ringi Request: ", ringiRequests);
  //   if (ringiRequests.status === 200 && ringiRequestsStatus === "done") {
  //     setIsLoading(false);
  //     setRows(ringiRequests.data.map((item) => ({ ...item, id: item.ID })));
  //     console.log(rows);
  //   }
  // }, [ringiRequests]);

  // const handleClick = (id) => {
  //   console.log(ringiRequests.data);
  //   const selectedRingiRequest = ringiRequests.data.find(
  //     (data) => data.ID === id
  //   );
  //   console.log("selected ringi : ", selectedRingiRequest);
  //   navigate(`/partrequest/view/` + id, {
  //     state: { data: selectedRingiRequest, reqId: id },
  //   });
  // };
  
  useEffect(() => {
    setRowCountState((prevRowCountState) =>
      ringiRequests.data?.totalData !== undefined ? ringiRequests.data?.totalData : prevRowCountState,
    );
    if (ringiRequests.status === 200 && ringiRequestsStatus === "done") {
      setIsLoading(false);
      setRows(ringiRequests.data.data.map((item) => ({ ...item, id: item.ID })));
    }
  }, [ringiRequests, ringiRequestsStatus]);

  useEffect(() => {
    dispatch(retrieveRingiRequest({ page, pageSize }));
  }, [dispatch, page, pageSize]);

  const handleClick = (id) => {
    const selectedRingiRequest = ringiRequests.data.data.find(
      (data) => data.ID === id
    );

    navigate(`/disposal/general/request/view/ringi/` + id, {
      state: { data: selectedRingiRequest, reqId: id },
    });
  };
  const columns = [
    {
      field: "PartRequestCode",
      headerName: "No",
      filterable: false,
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "Plant",
      headerName: "Plant",
      minWidth: 180,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.LocationPlant.PlantCode,
    },
    {
      field: "Requestor",
      headerName: "Requestor",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.UserName,
    },
    {
      field: "RequestDepartment",
      headerName: "Request Departement",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.User.Department,
    },
    {
      field: "RequestDate",
      headerName: "Request Date",
      minWidth: 140,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.RequestDate,
    },
    {
      field: "RequestLocation",
      headerName: "GNS+Loc",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "IFStatus",
      headerName: "IF Status",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.Status,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 120,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      {/* <DataGrid
        rows={rows}
        columns={columns}
        editMode="row"
        loading={isLoading}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      /> */}
      <DataGrid
        rows={rows}
        columns={columns}
        editMode="row"
        loading={isLoading}
        rowCount={rowCountState}

        pageSize={pageSize}
        pageSizeOptions={[5, 10, 20]}

        pagination
        paginationMode="server"
        paginationModel={{ page: page - 1, pageSize }}
        onPaginationModelChange={(val) => {
          dispatch(setPage(val.page + 1))
          dispatch(setPageSize(val.pageSize))
          setPageState(val)
        }}
        
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>
  );
};

export default RingiRequestActivity;
