import React from "react";

const ViewRingiRequestTable = ({handleEditTable, handleDeleteTable, partRequest, tag }) => {

    const data = [
        {
            plant: 'D101' ,
            number:'101',
            requestor:"Vincent",
            requestdept:"DevOps",
            type:"System",
            GNSLOC:"D1A1",
            status:"Approved",
        },
        {
            plant: 'D102' ,
            number:'102',
            requestor:"Oscar",
            requestdept:"Data",
            type:"Non-System",
            GNSLOC:"D1A2",
            status:"Pending",
        },
        {
            plant: 'D103' ,
            number:'103',
            requestor:"Richie",
            requestdept:"Tech",
            type:"System",
            GNSLOC:"D1A3",
            status:"Approved",
        }
    ]

    return (
      <table className="w-full text-sm text-gray-500 text-center">
        {/* table headers */}
        <thead className="text-xs text-gray-700 uppercase bg-gray-100 sticky top-0">
          <tr>
                <th scope="col" className="px-3 py-6 w-20">
                  Plant
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Number
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Requestor
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Request Department
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                Type                
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  GNS+Loc
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Status
                </th>
          </tr>
        </thead>
        {/* table body */}
        <tbody>
          {data && data.length > 0 ? (
              data.map((item, index) => (
                  
                  <tr key={index} className="bg-white border-b hover:bg-gray-200">
                      <td className="px-6 py-4">{item.plant}</td>
                      <td scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                          {item.number}
                      </td>
                      <td className="px-6 py-4">{item.requestor}</td>
                      <td className="px-6 py-4">{item.requestdept}</td>
                      <td className="px-6 py-4">{item.type}</td>
                      <td className="px-6 py-4">{item.GNSLOC}</td>
                      <td className="px-6 py-4">{item.status}</td>
                  </tr>
              ))
          ) : (
              <tr></tr>
          )}
        </tbody>
    </table>
    ) 
}

export default ViewRingiRequestTable