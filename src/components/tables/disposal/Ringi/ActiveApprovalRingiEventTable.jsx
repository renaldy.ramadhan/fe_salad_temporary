import React, { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { DataGrid } from "@mui/x-data-grid";
import CustomNoRowsOverlay from "../../../icons/CustomNoRowsOverlay";

import {
    setActivePage,
    setActivePageSize,
} from "../../../../slices/DisposalEventSlice";
import { retrieveActiveRingiEvent } from "../../../../slices/DisposalEventSlice";
import { buildQueryParams } from "../../../../util/global";

const ActiveApprovalRingiEventTable = ({ loading, parentColumn, parentRow }) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    retrieveActiveDisposalEventStatus,
    activeDisposalRingiEvent
  } = useSelector((state) => state.disposalEvent);

  const { activePage, activePageSize } = useSelector((state) => state.disposalEvent);

  const [rows, setRows] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});

  const [defaultActiveQueryParams, setDefaultActiveQueryParams] = useState({
    type: "Disposal Event Ringi",
    status: "Active",
    page: activePage,
    page_size: activePageSize,
  });

  const [rowCountState, setRowCountState] = useState(activeDisposalRingiEvent.total);

  const [isLoading, setIsLoading] = useState(true);

  const partRequests = useSelector((state) => state.partrequests.partRequest);
  const activeApprovals = useSelector(
    (state) => state.activeapprovals.activeApprovalGetNikAssigned
  );
  const activeApprovalsStatus = useSelector(
    (state) => state.activeapprovals.activeApprovalGetNikAssignedStatus
  );

  useEffect(() => {
    console.log("Row Count State: ", rowCountState);
    console.log("Rows", rows);
    console.log("Page:", activePage, "Page Size:", activePageSize);
    console.log("QUERY Page:", defaultActiveQueryParams.page, "Page Size:", defaultActiveQueryParams.page_size);
    
    setRowCountState((prevRowCountState) =>
    activeDisposalRingiEvent.total !== undefined
        ? activeDisposalRingiEvent.total
        : prevRowCountState
    );

    if (activeDisposalRingiEvent.status === 200 && retrieveActiveDisposalEventStatus === "done") {
      setIsLoading(false);
      setRows(
        activeDisposalRingiEvent?.data ?
        activeDisposalRingiEvent?.data?.map((event) => {
          return { ...event, id: event.ID };
        }) : ""
      );


      console.log(rows);
    }
  }, [activeDisposalRingiEvent, retrieveActiveDisposalEventStatus]);

  useEffect(() => {
    dispatch(
      retrieveActiveRingiEvent(buildQueryParams(defaultActiveQueryParams))
    );
  }, [dispatch, activePage, activePageSize]);

  const handleClick = (id) => {
    let selectedPartRequest;
    if (partRequests.length > 0) {
      selectedPartRequest = partRequests.data.data.find(
        (data) => data.IdRequest === id
      );
    } else if (activeApprovals.length > 0) {
      selectedPartRequest = activeApprovals.find(
        (data) => data.Request.ID === id
      );
    }

    navigate(`/partrequest/view/approver/` + id, {
      state: { data: selectedPartRequest, reqId: id },
    });
  };

  const formatDate = (dateString) => {
    if (!dateString) return ""; // Handle null or undefined dateString
    const date = new Date(dateString);
    const year = date.getFullYear().toString().padStart(4, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hour = date.getHours().toString().padStart(2, "0");
    const minute = date.getMinutes().toString().padStart(2, "0");
    const second = date.getSeconds().toString().padStart(2, "0");
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
  };

  const columns = [
    {
      field: "PartRequestCode",
      headerName: "No",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      filterable: false,
      valueGetter: (params) => params.row?.Request.PartRequestCode,
    },
    {
      field: "Plant",
      headerName: "Plant",
      minWidth: 180,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.Request.LocationPlant.PlantCode,
    },

    {
      field: "Requestor",
      headerName: "Requestor",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request.UserName,
    },
    {
      field: "RequestDepartment",
      headerName: "Request Departement",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Department.DepartmentName,
    },
    {
      field: "RequestDate",
      headerName: "Request Date",
      minWidth: 140,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => formatDate(params.row?.Request.CreatedAt),
    },
    {
      field: "RequestLocation",
      headerName: "GNS+Loc",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request.RequestLocation,
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 180,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={parentRow !== undefined ? parentRow : rows}
        columns={parentColumn !== undefined ? parentColumn : columns}
        editMode="row"
        loading={loading !== undefined ? loading : isLoading}
        rowCount={rowCountState}
        pageSize={activePageSize}
        pageSizeOptions={[5, 10, 20]}
        pagination
        paginationMode="server"
        paginationModel={{ page: activePage - 1, pageSize: activePageSize }}
        onPaginationModelChange={(val) => {
          dispatch(setActivePage(val.page + 1));
          dispatch(setActivePageSize(val.pageSize));
          // setPageState(val);
          setDefaultActiveQueryParams(prevParams => ({
            ...prevParams,
            page: val.page + 1,
            page_size: val.pageSize,
          }));
        }}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>
  );
};

export default ActiveApprovalRingiEventTable;
