import React, { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { DataGrid } from "@mui/x-data-grid";

import { useDispatch, useSelector } from "react-redux";
import CustomNoRowsOverlay from "../../../icons/CustomNoRowsOverlay";
import {
  retrieveDisposalEvent,
  retrieveRingiEvent,
  setDisposalEventPage,
  setDisposalEventPageSize,
  setPage,
  setPageSize,
} from "../../../../slices/DisposalEventSlice";
import { buildQueryParams } from "../../../../util/global";

const DisposalRingiTable = ({
  itemsData,
  purcGroup,
  onDataSend,
  parentColumn,
  parentRow,
  loading,
}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { retrieveDisposalEventStatus, disposalRingiEvent } = useSelector(
    (state) => state.disposalEvent
  );

  const { page, pageSize } = useSelector((state) => state.disposalEvent);

  const [rows, setRows] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [rowModesModel, setRowModesModel] = React.useState({});
  const [isPageChange, setIsPageChange] = useState(false);

  const [defaultQueryParams, setDefaultQueryParams] = useState({
    type: "Disposal Event Ringi",
    page: page,
    page_size: pageSize,
  });

  const [rowCountState, setRowCountState] = useState(disposalRingiEvent.total);

  useEffect(() => {
    console.log("Row Count State: ", rowCountState);
    console.log("Rows", rows);
    console.log("Page:", page, "Page Size:", pageSize);
    
    setRowCountState((prevRowCountState) =>
      disposalRingiEvent.total !== undefined
        ? disposalRingiEvent.total
        : prevRowCountState
    );

    if (disposalRingiEvent.status === 200 && retrieveDisposalEventStatus === "done") {
      setIsLoading(false);
      setRows(
        disposalRingiEvent?.data ?
        disposalRingiEvent?.data?.map((event) => {
          return { ...event, id: event.ID };
        }) : ""
      );


      console.log(rows);
    }
  }, [disposalRingiEvent, retrieveDisposalEventStatus]);

  useEffect(() => {
    dispatch(retrieveRingiEvent(buildQueryParams(defaultQueryParams)));
  }, [dispatch, page, pageSize]);


  const columns = [
    {
      field: "ItemCode",
      headerName: "Item Code",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "ItemName",
      headerName: "Item Name",
      minWidth: 280,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Items?.ItemName,
    },
    {
      field: "RequestQuantity",
      headerName: "Request Quantity",
      type: "number",
      minWidth: 160,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: true,
    },
    {
      field: "SystemQuantity",
      headerName: "System Quantity",
      minWidth: 160,
      flex: 1,
      type: "number",
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Items?.SystemQuantity,
    },
    {
      field: "Amount",
      headerName: "Amount",
      minWidth: 80,
      flex: 1,
      type: "number",
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "Notes",
      headerName: "Remark",
      minWidth: 240,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 120,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => navigate("/disposal/general/request/system/create/")}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={parentRow !== undefined ? parentRow : rows}
        columns={parentColumn !== undefined ? parentColumn : columns}
        editMode="row"
        // rowModesModel={rowModesModel}
        loading={loading !== undefined ? loading : isLoading}
        rowCount={rowCountState}
        pageSize={pageSize}
        pageSizeOptions={[5, 10, 20]}
        pagination
        paginationMode="server"
        paginationModel={{
          page: page - 1,
          pageSize,
        }}
        onPaginationModelChange={(val) => {
          dispatch(setPage(val.page + 1));
          dispatch(setPageSize(val.pageSize));
          // setIsPageChange(val);
          setDefaultQueryParams(prevParams => ({
            ...prevParams,
            page: val.page + 1,
            page_size: val.pageSize,
          }));
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
      />
    </Box>
  );
};

export default DisposalRingiTable;
