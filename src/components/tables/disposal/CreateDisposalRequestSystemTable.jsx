import React, { useCallback, useState, useEffect } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import SaveIcon from '@mui/icons-material/Save';
import CancelIcon from '@mui/icons-material/Close';
import {
  GridRowModes,
  DataGrid,
  GridToolbarContainer,
  GridActionsCellItem,
  GridRowEditStopReasons,
} from '@mui/x-data-grid';
import {
  randomId,
} from '@mui/x-data-grid-generator';

import { useDispatch, useSelector } from "react-redux";
import {
  fetchSearchParts,
} from "../../../slices/GlobalSlice";
import DeleteModal from '../../modals/DeleteModal';
import CustomNoRowsOverlay from '../../icons/CustomNoRowsOverlay'

const CreateDisposalRequestSystemTable = ({ purcGroup, locationCode , onDataSend, onDataLoad, onStatusSend }) => {

  const dispatch = useDispatch();
  const [rows, setRows] = useState(onDataLoad());
  const [rowModesModel, setRowModesModel] = useState({});
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedId, setSelectedId] = useState()

  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;
  
    const handleClick = () => {
      if (!locationCode) {
        alert('Please choose a location first.');
        return;
      }
      const id = randomId();
      setRows((oldRows) => [...oldRows, { id, ItemCode: '', ItemName: '' , RequestQuantity: '', SystemQuantity: '', Reason: '',Weight: '', Amount: '', isNew: true }]);
      setRowModesModel((oldModel) => ({
        ...oldModel,
        [id]: { mode: GridRowModes.Edit, fieldToFocus: 'ItemCode' },
      }));
    };
  
    return (
      <GridToolbarContainer>
        <Button color="primary" style={{ backgroundColor: "#17479d" }} variant="contained" startIcon={<AddIcon />} onClick={handleClick}>
          Add record
        </Button>
        {/* <Button color="primary" style={{ backgroundColor: "#17479d" }} variant="contained" startIcon={<FactCheckIcon />}>
         Get Sys Quantity
        </Button> */}
      </GridToolbarContainer>
    );
  }

  const calculateAmount = (pricePerUnit, requestQuantity) => {
    const price = parseFloat(pricePerUnit.replace(',', '.'));
    const quantity = parseFloat(requestQuantity);
    if (isNaN(price) || isNaN(quantity)) {
      return ''; 
    }
    return (price * quantity).toFixed(2); 
  };
  

  // const handleItemCodeChange = async (itemCode, rowId) => {
  //   try {
  //     const combinedObject = { itemCode, group:purcGroup };
  //     const result = await dispatch(fetchSearchParts(combinedObject)).unwrap();
  //     console.log(result);
  //     if (result && result.length >= 1) {
  //       const filter = result.find(obj => obj.ItemCode === itemCode);
  //       if (filter) {
  //         setRows(currentRows => currentRows.map(row => 
  //           row.id === rowId ? { ...row, ...filter } : row
  //         )); 
  //       }
  //     } else {
  //       alert('Invalid item code');
  //     }
  //   } catch (error) {
  //     console.error('Error in fetching item:', error);
  //     alert('Error in fetching item');
  //   }
  // };

  const handleItemCodeChange = async (itemCode, rowId) => {
    try {
      const combinedObject = { itemCode, group: purcGroup, location: locationCode };
      const result = await dispatch(fetchSearchParts(combinedObject)).unwrap();
      console.log(result);
      if (result && result.length >= 1) {
        const filter = result.find(obj => obj.ItemCode === itemCode);
        if (filter) {
          setRows(currentRows => currentRows.map(row => {
            if (row.id === rowId) {
              return { 
                ...row, 
                ...filter, 
                Amount: calculateAmount(filter.PricePerUnit, row.RequestQuantity) 
              };
            }
            return row;
          })); 
        }
      } else {
        alert('Invalid item code');
      }
    } catch (error) {
      console.error('Error in fetching item:', error);
      alert('Error in fetching item');
    }
  };
  

  const handleRowEditStop = (params, event) => {
    if (params.reason === GridRowEditStopReasons.rowFocusOut) {
      event.defaultMuiPrevented = true;
    }
  };

  const handleEditClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  };

  const handleSaveClick = (id) => () => {
    setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.View } });
    // onDataSend(rows);
  };

  const handleConfirm = () => { // confirm modal 
    setRows(rows.filter((row) => row.id !== selectedId));
    onDataSend(rows.filter((row) => row.id !== selectedId)); 
    setIsModalOpen(false);
  };

  const handleCancel = () => { // cancel modal
    setIsModalOpen(false);
  };

  const handleDeleteClick = (id) => () => {
    setIsModalOpen(true);
    setSelectedId(id);
  };

  const handleCancelClick = (id) => () => {
    setRowModesModel({
      ...rowModesModel,
      [id]: { mode: GridRowModes.View, ignoreModifications: true },
    });

    const editedRow = rows.find((row) => row.id === id);
    if (editedRow.isNew) {
      setRows(rows.filter((row) => row.id !== id));
    }
  };

  // const processRowUpdate = (newRow) => {
  //   const updatedRow = { ...newRow, isNew: false };
  //   setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
  //   onDataSend(rows.map((row) => (row.id === newRow.id ? updatedRow : row)))
  //   return updatedRow;
  // };

  const processRowUpdate = (newRow) => {
    const updatedRow = { 
      ...newRow, 
      isNew: false, 
      Amount: newRow.PricePerUnit ? calculateAmount(newRow.PricePerUnit, newRow.RequestQuantity) : ''
    };
    
    const checkRow = rows.map((row) => {
      console.log(row.id)
      console.log(newRow.id)
      if (row.id === newRow.id) {
        if (newRow.RequestQuantity > row.SystemQuantity) {
          alert('Request quantity cannot be more than system quantity');
          onStatusSend(false);
          return row; 
        } else if (newRow.RequestQuantity === "" || newRow.RequestQuantity === 0 || newRow.RequestQuantity === null) {
          alert('Request quantity cannot be empty');
          onStatusSend(false);
          return row; 
        } else {
          setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
          onDataSend(rows.map((row) => (row.id === newRow.id ? updatedRow : row)))
          onStatusSend(true);
          return updatedRow;
        }
      } else {
        onStatusSend(false);
        return row;
      }
    });
    console.log(checkRow)
  };
  
  

  const handleRowModesModelChange = (newRowModesModel) => {
    setRowModesModel(newRowModesModel);
  };

  const columns = [
    { 
      field: 'ItemCode', 
      headerName: 'Part Code', 
      minWidth: 120, 
      flex: 1,
      editable: true, 
      align: 'center',
      headerAlign: 'center',
      preProcessEditCellProps: (params) => {
        handleItemCodeChange(params.props.value, params.id);
        return params.props;
      }
    },
    {
      field: 'ItemName',
      headerName: 'Part Name',
      minWidth: 280, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
    },
    {
      field: 'RequestQuantity',
      headerName: 'Request Quantity',
      type: 'number',
      minWidth: 140, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: true,
    },
    {
      field: 'SystemQuantity',
      headerName: 'System Quantity',
      minWidth: 140, 
      flex: 1,
      type: 'number',
      align: 'center',
      headerAlign: 'center',
      editable: false,
    },
    {
      field: 'Reason',
      headerName: 'Reason',
      minWidth: 120, 
      flex: 1,
      type: 'text',
      align: 'center',
      headerAlign: 'center',
      editable: true,
    },
    {
      field: 'Weight',
      headerName: 'Weight',
      minWidth: 240, 
      flex: 1,
      editable: true,
      align: 'center',
      headerAlign: 'center',
      type: 'number',
    },
    ,
    {
      field: 'Amount',
      headerName: 'Amount',
      minWidth: 240, 
      flex: 1,
      editable: false,
      align: 'center',
      headerAlign: 'center',
      type: 'number',
    },
    {
      field: 'actions',
      type: 'actions',
      headerName: 'Actions',
      minWidth: 160, 
      flex: 1,
      cellClassName: 'actions',
      align: 'center',
      headerAlign: 'center',
      getActions: ({ id }) => {
        const isInEditMode = rowModesModel[id]?.mode === GridRowModes.Edit;

        if (isInEditMode) {
          return [
            <GridActionsCellItem
              icon={<SaveIcon />}
              label="Save"
              sx={{
                color: 'primary.main',
              }}
              onClick={handleSaveClick(id)}
            />,
            <GridActionsCellItem
              icon={<CancelIcon />}
              label="Cancel"
              className="textPrimary"
              onClick={handleCancelClick(id)}
              color="inherit"
            />,
          ];
        }

        return [
          <GridActionsCellItem
            icon={<EditIcon />}
            label="Edit"
            className="textPrimary"
            onClick={handleEditClick(id)}
            color="inherit"
          />,
          <GridActionsCellItem
            icon={<DeleteIcon />}
            label="Delete"
            onClick={handleDeleteClick(id)}
            color="inherit"
          />,
        ];
      },
    },
  ];

  const handleProcessRowUpdateError = React.useCallback((error) => {
    
  }, []);

  return (
    <Box
      sx={{
        height: 500,
        width: '100%',
        '& .actions': {
          color: 'text.secondary',
        },
        '& .textPrimary': {
          color: 'text.primary',
        },
        '& .MuiDataGrid-cell': {
          borderRight: '2px solid #ccc', // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        '& .MuiDataGrid-row': {
          borderBottom: '2px solid #ccc',
        },
      }}
    >
      <DataGrid
        rows={rows}
        columns={columns}
        editMode="row"
        rowModesModel={rowModesModel}
        onRowModesModelChange={handleRowModesModelChange}
        onRowEditStop={handleRowEditStop}
        processRowUpdate={processRowUpdate}
        onProcessRowUpdateError={handleProcessRowUpdateError}
        slots={{
          toolbar: EditToolbar,
          noRowsOverlay: CustomNoRowsOverlay,
        }}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
      />
      <DeleteModal isOpen={isModalOpen} onConfirm={handleConfirm} onCancel={handleCancel} />
    </Box>
    
  );
}

export default CreateDisposalRequestSystemTable;