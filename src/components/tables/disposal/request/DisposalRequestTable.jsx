import React, { useCallback, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import FactCheckIcon from "@mui/icons-material/FactCheck";
import VisibilityIcon from "@mui/icons-material/Visibility";
import { useParams } from "react-router-dom";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  GridRowModes,
  DataGrid,
  GridToolbarContainer,
  GridActionsCellItem,
  GridRowEditStopReasons,
} from "@mui/x-data-grid";

import { randomId } from "@mui/x-data-grid-generator";

import { useDispatch, useSelector } from "react-redux";
import {
  getActiveApprovalByRequestId,
  approvalAction,
} from "../../../../slices/ActiveApprovalSlice";
import { editPartRequestData } from "../../../../slices/updateDataSlice";
import CustomNoRowsOverlay from "../../../icons/CustomNoRowsOverlay";
import {
  setPage,
  setPageSize,
} from "../../../../slices/DisposalRequestSlice";
import { retrieveDisposalRequest } from "../../../../slices/DisposalRequestSlice";

const DisposalRequestTable = ({
  itemsData,
  purcGroup,
  onDataSend,
  parentColumn,
  parentRow,
  loading,
}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { id } = useParams();
  const { disposalRequest, disposalRequestStatus } = useSelector(
    (state) => state.disposalRequest
  );

  console.log(id);
  const [rows, setRows] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  console.log("items data:", itemsData);
  const [rowModesModel, setRowModesModel] = React.useState({});
  const { page, pageSize } = useSelector((state) => state.disposalRequest);
  const [pageState, setPageState] = useState({
    page,
    pageSize,
  });

  useEffect(() => {
    setRowCountState((prevRowCountState) =>
      disposalRequest.data?.totalData !== undefined
        ? disposalRequest.data?.totalData
        : prevRowCountState
    );

    if (disposalRequest.status === 200 && disposalRequestStatus === "done") {
      setIsLoading(false);
      setRows(
        disposalRequest?.data?.data
        ? disposalRequest?.data?.data.map((item) => ({
            ...item,
            id: item.ID,
          }))
        : ""
      );
      console.log(rows);
    }
  }, [disposalRequest, disposalRequestStatus]);

  const [rowCountState, setRowCountState] = useState(
    disposalRequest.data?.totalData
  );

  useEffect(() => {
    dispatch(retrieveDisposalRequest({ page, pageSize }));
  }, [dispatch, page, pageSize]);

  function EditToolbar(props) {
    const { setRows, setRowModesModel } = props;

    // const handleClick = () => {

    //   const id = randomId();
    //   setRows((oldRows) => [...oldRows, { id, ItemCode: '', ItemName: '' , RequestQuantity: '', SystemQuantity: '', Remark: '', isNew: true }]);
    //   setRowModesModel((oldModel) => ({
    //     ...oldModel,
    //     [id]: { mode: GridRowModes.Edit, fieldToFocus: 'ItemCode' },
    //   }));
    // };
  }

  const calculateAmount = (pricePerUnit, requestQuantity) => {
    const price = parseFloat(pricePerUnit.replace(",", "."));
    const quantity = parseFloat(requestQuantity);
    if (isNaN(price) || isNaN(quantity)) {
      return "";
    }
    return (price * quantity).toFixed(2);
  };

  // const processRowUpdate = async (newRow) => {
  //   // console.log("processRowUpdate")
  //   const updatedRow = { ...newRow, isNew: false };
  //   setRows(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));
  //   onDataSend(rows.map((row) => (row.id === newRow.id ? updatedRow : row)));

  //   const actionResult = await dispatch(
  //     editPartRequestData([
  //       {
  //         IdItemPivot: updatedRow.id,
  //         ItemCode: updatedRow.ItemCode,
  //         RequestQuantity: updatedRow.RequestQuantity,
  //         Notes: updatedRow.Notes,
  //       },
  //     ])
  //   );
  //   const result = unwrapResult(actionResult);

  //   return updatedRow;
  // };

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState("");

  // const handleSaveClick = (params) => () => {
  //   setRowModesModel({
  //     ...rowModesModel,
  //     [params.id]: { mode: GridRowModes.View },
  //   });
  // };

  // const handleRowEditStop = (params, event) => {
  //   if (params.reason === GridRowEditStopReasons.rowFocusOut) {
  //     onDataSend(rows);
  //     event.defaultMuiPrevented = true;
  //   }
  // };

  // const handleEditClick = (id) => () => {
  //   setRowModesModel({ ...rowModesModel, [id]: { mode: GridRowModes.Edit } });
  // };

  // const handleDeleteClick = (id) => () => {
  //   setRows(rows.filter((row) => row.id !== id));
  // };

  // const handleCancelClick = (id) => () => {
  //   setRowModesModel({
  //     ...rowModesModel,
  //     [id]: { mode: GridRowModes.View, ignoreModifications: true },
  //   });

  //   const editedRow = rows.find((row) => row.id === id);
  //   if (editedRow.isNew) {
  //     setRows(rows.filter((row) => row.id !== id));
  //   }
  // };

  // const handleRowModesModelChange = (newRowModesModel) => {
  //   setRowModesModel(newRowModesModel);
  // };

  // const disposal = useSelector(
  //   (state) => state.disposal.retrieveDisposal
  // );

  // const handleClick = (id) => {
  //   console.log(disposal.data);
  //   const selectedDisposal = disposal.data.find(
  //     (data) => data.ID === id
  //   );
  //   console.log("selected disposal: ", selectedDisposal);
  //   navigate(`/disposal/general/event/view/` + id, {
  //     state: { data: selectedDisposal, reqId: id },
  //   });
  // };

  const columns = [
    {
      field: "ItemCode",
      headerName: "Item Code",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
      // preProcessEditCellProps: (params) => {
      //   handleItemCodeChange(params.props.value, params.id);
      //   return params.props;
      // }
    },
    {
      field: "ItemName",
      headerName: "Item Name",
      minWidth: 280,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Items?.ItemName,
    },
    {
      field: "RequestQuantity",
      headerName: "Request Quantity",
      type: "number",
      minWidth: 160,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: true,
    },
    {
      field: "SystemQuantity",
      headerName: "System Quantity",
      minWidth: 160,
      flex: 1,
      type: "number",
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Items?.SystemQuantity,
    },
    // {
    //   field: "PricePerUnit",
    //   headerName: "Production Requirement (N+1 Shift)",
    //   minWidth: 120,
    //   flex: 1,
    //   type: "number",
    //   align: "center",
    //   headerAlign: "center",
    //   editable: false,
    //   valueGetter: (params) => params.row?.Items?.PricePerUnit,
    // },
    {
      field: "Amount",
      headerName: "Amount",
      minWidth: 80,
      flex: 1,
      type: "number",
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "Notes",
      headerName: "Remark",
      minWidth: 240,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 120,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => navigate("/disposal/general/request/system/create/")}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={parentRow !== undefined ? parentRow : rows}
        columns={parentColumn !== undefined ? parentColumn : columns}
        editMode="row"
        loading={loading !== undefined ? loading : isLoading}
        rowCount={rowCountState}
        pageSize={pageSize}
        pageSizeOptions={[5, 10, 20]}
        pagination
        paginationMode="server"
        paginationModel={{ page: page - 1, pageSize }}
        onPaginationModelChange={(val) => {
          dispatch(setPage(val.page + 1));
          dispatch(setPageSize(val.pageSize));
          setPageState(val);
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
      />
    </Box>
  );
};

export default DisposalRequestTable;
