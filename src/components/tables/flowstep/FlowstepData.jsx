import { Delete, Edit } from "@mui/icons-material";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import CustomNoRowsOverlay from "../../icons/CustomNoRowsOverlay";
import { useNavigate } from "react-router-dom";
import { del } from "../../../util/http";

export const FlowstepData = () => {
  const navigate = useNavigate();
  const [rows, setRows] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [openDialog, setOpenDialog] = useState(false);
  const [activeId, setActiveId] = useState(0);
  const flowsteps = useSelector((state) => state.flowsteps.flowstepData);
  const flowstepsStatus = useSelector((state) => state.flowsteps.status);

  useEffect(() => {
    if (flowstepsStatus === "done") {
      setIsLoading(false);
      setRows(flowsteps.map((item) => ({ ...item, id: item.ID })));
    }
    return () => {
      setRows([]);
    };
  }, [flowstepsStatus, flowsteps]);

  const handleEdit = (id) => {
    navigate("edit//" + id);
  };

  const handleOpenDialog = (id) => {
    setOpenDialog(true);
    setActiveId(id);
  };

  const handleCloseDialog = (id) => {
    setOpenDialog(false);
    setActiveId(0);
  };

  const handleDelete = async () => {
    const res = await del(
      process.env.REACT_APP_API_URL_Flowstep_Service + "/delete/" + activeId
    );

    if (res.status === 200) {
      window.location.reload();
    }
  };

  const columns = [
    {
      field: "number",
      headerName: "No",
      filterable: false,
      minWidth: 5,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "DepartmentName",
      headerName: "Department Name",
      filterable: false,
      minWidth: 150,
      valueGetter: (params) => {
        return params.row.Department.DepartmentName;
      },
    },
    {
      field: "StepName",
      headerName: "Step Name",
      filterable: false,
      minWidth: 200,
    },
    {
      field: "FlowSequence",
      headerName: "Sequence",
      filterable: false,
      minWidth: 200,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "Workflow",
      headerName: "Workflow",
      filterable: false,
      minWidth: 200,
      valueGetter: (params) => {
        return params.row.Workflow.Type;
      },
    },
    {
      field: "Type",
      headerName: "Type",
      filterable: false,
      minWidth: 200,
      flex: 1,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => {
        if (params.row.Type === 1) {
          return "Requestor";
        } else if (params.row.Type === 2) {
          return "Location";
        } else {
          return "Purchasing Group";
        }
      },
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 300,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="error"
            variant="contained"
            startIcon={<Delete />}
            size="small"
            onClick={() => handleOpenDialog(id)}
          >
            <p className="text-xs">Delete</p>
          </Button>,
          <Button
            color="success"
            variant="contained"
            startIcon={<Edit />}
            size="small"
            onClick={() => handleEdit(id)}
          >
            <p className="text-xs">Edit</p>
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={rows.map((item, index) => ({ ...item, number: index + 1 }))}
        columns={columns}
        editMode="row"
        loading={isLoading}
        slotProps={{
          toolbar: { setRows },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
      <Dialog
        open={openDialog}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          className="text-red-400 text-center"
        >
          {"Delete"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id="alert-dialog-description"
            className="text-center"
          >
            Are you sure want to delete this row?
          </DialogContentText>
        </DialogContent>
        <DialogActions className="flex items-center">
          <Button onClick={handleCloseDialog} variant="outlined">
            No
          </Button>
          <Button onClick={handleDelete} variant="contained" color="error">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};
