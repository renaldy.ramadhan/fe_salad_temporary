import React, { useState, useEffect, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import VisibilityIcon from '@mui/icons-material/Visibility';
import {
  DataGrid,
} from '@mui/x-data-grid';
import { useSelector } from "react-redux";
import CustomNoRowsOverlay from '../icons/CustomNoRowsOverlay';

const NoContentTable = () => {
  const navigate = useNavigate();
  const [rows, setRows] = useState([]);
  const [rowModesModel, setRowModesModel] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const partRequests = useSelector((state) => state.partrequests.partRequest);
  const partRequestsStatus = useSelector((state) => state.partrequests.partRequestStatus);

  useEffect(() => {
    if(partRequestsStatus != "loading"){
        setIsLoading(false)
        setRows(partRequests.data.data.map((item) => ({ ...item, id: item.ID })));
        console.log(rows)
    }
  },[partRequests])

  const handleClick = (id) => {
    navigate(`/partrequest/view/` + id, { state: { data: partRequests[id], reqId: id } })
  };

  const columns = [
    {
        field: "PartRequestCode",
        headerName: "No",
        filterable: false,
        minWidth: 120, 
        flex: 1,
        align: 'center',
        headerAlign: 'center',
    },
    { 
      field: 'PlantName', 
      headerName: 'Plant Name', 
      minWidth: 180, 
      flex: 1,
      editable: false, 
      align: 'center',
      headerAlign: 'center',
    },
    {
      field: 'Requestor',
      headerName: 'Requestor',
      minWidth: 180, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.UserName
    },
    {
      field: 'RequestDepartment',
      headerName: 'Request Departement',
      minWidth: 180, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.User.Department
    },
    {
      field: 'RequestDate',
      headerName: 'Request Date',
      minWidth: 140, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
      valueGetter: (params) => params.row?.RequestDate
    },
    {
      field: 'RequestLocation',
      headerName: 'GNS+Loc',
      minWidth: 120, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
    },
    {
      field: 'Status',
      headerName: 'Status',
      minWidth: 120, 
      flex: 1,
      align: 'center',
      headerAlign: 'center',
      editable: false,
    },
    {
      field: 'IFStatus',
      headerName: 'IF Status',
      minWidth: 120, 
      flex: 1,
      editable: false,
      align: 'center',
      headerAlign: 'center',
      valueGetter: (params) => params.row?.Status
    },
    {
        field: 'actions',
        type: 'actions',
        headerName: 'Actions',
        minWidth: 120, 
        flex: 1,
        cellClassName: 'actions',
        align: 'center',
        headerAlign: 'center',
        getActions: ({ id }) => {
          return [
            <Button color="primary" style={{ backgroundColor: "#808080" }} variant="contained" startIcon={<VisibilityIcon />} onClick={() => handleClick(id)}>
                View
            </Button>,
          ];
        },
      },
  ];

  return (
    <Box
      sx={{
        height: 500,
        width: '100%',
        '& .actions': {
          color: 'text.secondary',
        },
        '& .textPrimary': {
          color: 'text.primary',
        },
        '& .MuiDataGrid-cell': {
          borderRight: '2px solid #ccc', // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        '& .MuiDataGrid-row': {
          borderBottom: '2px solid #ccc',
        },
      }}
    >
      <DataGrid
        rows={[]}
        columns={columns}
        editMode="row"
        loading={isLoading}
        slotProps={{
          toolbar: { setRows, setRowModesModel },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>
    
  );
}

export default NoContentTable;