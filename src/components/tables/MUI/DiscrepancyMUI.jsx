import React, { useEffect, useState} from 'react';
import { useNavigate } from 'react-router-dom';
// import DeleteModal from './DeleteModal'
import axios from 'axios';
import LoadingSpinner from '../../animations/loading/LoadingSpinner';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';

const DiscrepancyMUI= (props) => {

  const [data, setTableData] = useState(props.data);
  const [user, setUser] = useState(props.user);
  const [selectedKey, setSelectedKey] = useState(null);
  const [isModalOpen, setModalOpen] = useState(false); // delete
  const [selectedId, setSelectedId] = useState(null); // delete 
  const navigate = useNavigate(); // edit
  // const [shouldNavigate, setShouldNavigate] = useState(false); // edit
  const [page, setPage] = useState(null);
  const [rowsPerPage, setRowsPerPage] = useState(5); // You can adjust the number of rows per page

  function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;
  
    const handleFirstPageButtonClick = (event) => {
      onPageChange(event, 0);
    };
  
    const handleBackButtonClick = (event) => {
      onPageChange(event, page - 1);
    };
  
    const handleNextButtonClick = (event) => {
      onPageChange(event, page + 1);
    };
  
    const handleLastPageButtonClick = (event) => {
      onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };
  
    return (
      <Box sx={{ flexShrink: 0, ml: 2.5 }}>
        <IconButton
          onClick={handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="first page"
        >
          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={handleBackButtonClick}
          disabled={page === 0}
          aria-label="previous page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="next page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="last page"
        >
          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </Box>
    );
  }
  TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
  };

  const handleChangePage = (props, newPage) => {
        setPage(newPage);
  };
  const startRow = page * rowsPerPage;
  const endRow = startRow + rowsPerPage;
  const slicedDataKeys = Object.keys(data).slice(startRow, endRow);

  const isBoxSelected = "bg-gray-200 border-gray-400"
  const [loading, setLoading] = useState(true);
  

  useEffect(() => {
    console.log("props user:", props.user)
    setTableData(props.data);
    setUser(props.user)
    setLoading(false)
  }, [props.data, props.user]);

  const handleKeyClick = (key) => {
    setSelectedKey(selectedKey === key ? null : key);
  };

    const getStatusColor = (status) => {
        if (status === 'Denied') {
            return 'text-red-500';
        } else if (status === 'Done') {
            return 'text-green-500';
        } else if (status === 'In Progress') {
            return 'text-yellow-500';
        }
        return 'text-gray-900';
    };

    const formatDate = (dateString) => {
      const date = new Date(dateString);
      const day = date.getDate().toString().padStart(2, '0');
      const month = (date.getMonth() + 1).toString().padStart(2, '0');
      const year = date.getFullYear().toString().substr(-2);
      return `${day}/${month}/${year}`;
    };

    if (loading) {
        return<td colSpan="7" className="text-center py-4 text-2xl md:text-3xl font-bold">
        {/* <LoadingSpinner/> */}
      </td>;
      }
    
      

  const handleChangeRowsPerPage = (props) => {
    setRowsPerPage(parseInt(props.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - Object.keys(data).length) : 0;
    
      return (
        <div className="relative overflow-x-auto overflow-y-auto max-h-[24rem]">
          <Table sx={{ minWidth: 650, borderAxis:"both" }} size="small" aria-label="a dense table">
            <TableHead className="text-xs text-gray-700 uppercase bg-gray-100 sticky -top-0.5">
              <TableRow>
              <TableCell align="center" sx={{ borderAxis:"ybeetween" }}>
                  Number
                </TableCell>
                <TableCell align="center">
                  Plant
                </TableCell>
                <TableCell align="center">
                  Requestor
                </TableCell>
                <TableCell align="center">
                  Request Department
                </TableCell>
                <TableCell align="center">
                  Request Date
                </TableCell>
                <TableCell align="center">
                  GNS+Loc
                </TableCell>
                <TableCell align="center">
                  Status
                </TableCell>
                <TableCell align="center">
                  IF Status
                </TableCell>
                <TableCell align="center">
                  Action
                </TableCell>
                
              </TableRow>
            </TableHead>
            <TableBody>
            {slicedDataKeys.map((key, idx) => {
            const counter = startRow + idx + 1;
            const mainRow = Array.isArray(data[key]) ? data[key][0] : data[key];

                return (
                  <React.Fragment key={key}>
                    <TableRow onClick={() => handleKeyClick(key)} className={`cursor-pointer border-b hover:bg-gray-200 ${selectedKey === key ? isBoxSelected : 'bg-white'}`}>
                    <TableCell align='center' sx={{ border:1 }}>{counter}</TableCell>
                    <TableCell align='center' sx={{ border:1 }}>
                        {mainRow.PlantName}
                        </TableCell>
                        <TableCell  align='center' sx={{ border:1 }}>{mainRow.UserName}</TableCell>
                        <TableCell  align='center' sx={{ border:1 }}>{mainRow.PurchasingGroup.Plant}</TableCell>
                        <TableCell  align='center' sx={{ border:1 }}>{formatDate(mainRow.CreatedAt)}</TableCell>
                        <TableCell  align='center' sx={{ border:1 }}>
                          {mainRow.RequestLocation}
                        </TableCell>
                        <TableCell  align='center' sx={{ border:1 }}>
                          {mainRow.Status}
                        </TableCell>
                        <TableCell  align='center' sx={{ border:0.1 }}>
                          {mainRow.Status}
                        </TableCell>
                        {/* <TableCell className="border-[2px]   py-2 whitespace-nowrap text-sm font-medium text-gray-900"></TableCell> */}
                      <TableCell align='center' sx={{ border:1 }}>
                        <div className="flex gap-2 justify-center items-center pr-2">
                            {/* <button
                            className="inline-flex items-center min-w-12 h-8 px-3 py-2 bg-gray-500 hover:bg-gray-400 text-white text-sm font-medium rounded-md"
                            onClick={() => {
                              navigate('/partrequest/view', { state: { data: data[key] } })
                            }}
                            title='Details'
                          >
                            View
                          </button> */}

                      <button
                        className="inline-flex items-center min-w-12 h-8 px-3 py-2 bg-gray-500 hover:bg-gray-400 text-white text-sm font-medium rounded-md"
                        onClick={() => {
                          // const navigationUrl = getNavigationUrl(key, mainRow);
                          // navigate(navigationUrl, { state: { key: key, data: mainRow } });
                          navigate(`/partrequest/view/`+ mainRow.ID , { state: { data: data[key], reqId : mainRow.ID }  })
                        }}
                        title='Details'
                      >
                        View
                      </button>
                        </div>
                      </TableCell>
                    </TableRow>
                    {selectedKey === key &&
                      (data[key].length > 1 ? (
                        data[key].slice(1).map((datas, index) => (
                          <tr
                            key={index}
                            className={`bg-white border-b hover:bg-gray-200 ${selectedKey === key ? 'expand-row' : ''}`}
                          >
                            <td>{console.log(datas)} {console.log(index)}</td>
                            <td className="py-2 h-24">{key}</td>
                            <td className="py-2 h-24">{data[key][0].Requester}</td>
                            <td className="py-2 h-24">{data[key][0].Department}</td>
                            <td className="py-2 h-24">{datas.CreatedAt}</td>
                            <td className="py-2 h-24">
                              {key}
                              </td>
                            <td className={`py-2 h-24 ${getStatusColor(datas.Status)}`}>
                              {datas.Status}
                            </td>
                            <td className={`py-2 h-24 ${getStatusColor(datas.IfStatus)}`}>
                              {datas.IfStatus}
                            </td>
                            <td></td>
                          </tr>
                        ))
                      ) : (
                        <tr className="bg-white border-b hover:bg-gray-200">
                          <td colSpan="9" className="py-2 h-24 text-center text-gray-500">
                            No other items!
                          </td>
                        </tr>
                      ))}
                   </React.Fragment>
                );
              })}
              {Object.keys(data).length === 0 && (
                <tr>
                  <td colSpan="9" className="text-center py-4 text-2xl md:text-3xl font-bold">
                    {/* <LoadingSpinner/> */}
                  </td>
                </tr>
              )}
              {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
            </TableBody>
            <TableFooter>
          <TableRow>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
            colSpan={3}
            count={Object.keys(data).length}
            rowsPerPage={rowsPerPage}
            page={page}
            SelectProps={{
              inputProps: {
                'aria-label': 'rows per page',
              },
              native: true,
            }}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
            ActionsComponent={TablePaginationActions}
          />
          </TableRow>
        </TableFooter>
            </Table>
            
        </div>
      );
}

export default DiscrepancyMUI;

<Layout>
      <div className="content-positioning pl-3 md:pl-48 md:pt-38 pr-2 w-full">
        <p className="page-title">Disposal Event List</p>
        <div className="flex justify-start gap-4 mb-4 h-10 md:h-10 text-sm">
          <Button
            type={"button"}
            onclick={() => {
              navigate("/disposal/general/event/create");
            }}
            classname={
              "bg-custom-blue text-center py-1 md:py-2 px-8 border-solid border-0 rounded-md md:rounded text-xs md:text-sm text-white font-semibold hover:opacity-90"
            }
          >
            Add
          </Button>
         
        </div>
        <DisposalEventTable withAction={true} />
      </div>
    </Layout>