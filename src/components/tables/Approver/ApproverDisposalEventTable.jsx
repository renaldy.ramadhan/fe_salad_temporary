import React, { useEffect, useState } from "react";
import { Box } from "@mui/material";
import Button from "@mui/material/Button";
import { DataGrid } from "@mui/x-data-grid";
import { DatePicker } from "react-nice-dates";
import { enGB } from "date-fns/locale";
import { useDispatch } from "react-redux";
import { setApprovalSetting } from "../../../slices/DisposalEventSlice";

const ApproverTable = ({ loading, data, onChangeRow, isView }) => {
  const dispatch = useDispatch();
  const [rows, setRows] = useState([]);

  useEffect(() => {
    if (data && Array.isArray(data) && rows.length < 1) {
      setRows(
        data
          .map((row, index) => ({
            id: index,
            ...row,
            ApproverDueDate: row?.ApproverDueDate || null,
            Actions: null,
            setDate: row?.setDate || false,
          }))
          .sort((a, b) => {
            return a.ApprovalSequence - b.ApprovalSequence;
          })
      );
    }
  }, [data, rows]);

  const formatDate = (dateString, time = false) => {
    if (dateString === null) return "";
    // return dateString.toString()
    const date = isView ? new Date(dateString) : dateString;
    const year = date.getFullYear().toString().padStart(4, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    if (time) {
      const hour = date.getHours().toString().padStart(2, "0");
      const minute = date.getMinutes().toString().padStart(2, "0");
      const second = date.getSeconds().toString().padStart(2, "0");

      return `${year}/${month}/${day} ${hour}:${minute}:${second}`;
    }
    return `${year}/${month}/${day}`;
  };

  const onClick = (e, params) => {
    e.stopPropagation(); // don't select this row after clicking
    if (params.row.ApproverDueDate === null) return;
    params.row.setDate = true;
    dispatch(setApprovalSetting(params.row));
  };
  const onClickEdit = (e, params) => {
    e.stopPropagation(); // don't select this row after clicking
    params.row.setDate = false;
    dispatch(setApprovalSetting(params.row));
  };

  const columns = [
    {
      field: "ApprovalSequence",
      headerName: "Sequence",
      minWidth: 60,
      maxWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "DepartmentName",
      headerName: "Title",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        return isView === true
          ? params.row.Department.DepartmentName
          : params.row.DepartmentName;
      },
    },
    {
      field: "ApproverNik",
      headerName: "NIK",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "ApproverName",
      headerName: "Name",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "ApproverDueDate",
      headerName: "Due Date",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        return params.row.setDate === true || isView ? (
          formatDate(params.row?.DueDateApproval || params.value)
        ) : (
          <DatePicker
            date={params.row.ApproverDueDate}
            onDateChange={(val) => {
              params.row.ApproverDueDate = val;
              params.value = val;
            }}
            locale={enGB}
            format="dd MMM yyyy"
            minimumDate={new Date()}
          >
            {({ inputProps, focused }) => (
              <input
                className={
                  "input-group justify-end input !w-full asbolute top-0" +
                  (focused ? " -focused" : "")
                }
                {...inputProps}
                placeholder="Due Date"
              />
            )}
          </DatePicker>
        );
      },
    },
    {
      field: "DateApproval",
      headerName: "Approval Date",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        return params.row.DateApproval != null
          ? formatDate(params.row?.DateApproval, true)
          : "-";
      },
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
  ];

  if (isView === undefined) {
    columns.push({
      field: "Actions",
      headerName: "Actions",
      width: 200,
      align: "center",
      headerAlign: "center",
      renderCell: (params) => {
        return params.row.setDate === true ? (
          <Button
            sx={{
              mr: 1,
              backgroundColor: "#17479D",
              border: 1,
              // Set the desired background color here
              "&:hover": {
                backgroundColor: "white",
                color: "#17479D", // Set the desired hover color here
              },
            }}
            variant="contained"
            onClick={(e) => {
              onClickEdit(e, params);
            }}
          >
            Edit
          </Button>
        ) : (
          <Button
            sx={{
              mr: 1,
              backgroundColor: "#17479D",
              border: 1,
              // Set the desired background color here
              "&:hover": {
                backgroundColor: "white",
                color: "#17479D", // Set the desired hover color here
              },
            }}
            variant="contained"
            onClick={(e) => {
              onClick(e, params);
            }}
          >
            Save
          </Button>
        );
      },
    });
  }

  return (
    <Box
      sx={{ height: isView ? undefined : 500, width: "100%" }}
      className={"relative"}
    >
      <DataGrid
        rows={rows}
        columns={columns}
        loading={loading}
        disableSelectionOnClick
        className="relative"
        hideFooter
        autoHeight={isView ? true : false}
      />
    </Box>
  );
};

export default ApproverTable;
