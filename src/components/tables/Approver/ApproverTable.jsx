import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Box } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";

const ApproverTable = ({ data }) => {
  const [localApprovals, setLocalApprovals] = useState([]);

  // useEffect(() => {
  //   setLocalApprovals(data);
  // }, [data]);

  // useEffect(() => {
  //   // Check if `data` exists and has the `data` property; otherwise, log or handle accordingly
  //   if (data && Array.isArray(data.data)) {
  //     setLocalApprovals(data.data);
  //   } else {
  //     // Optionally log or handle the unexpected structure/format of `data`
  //     console.warn(
  //       "ApproverTable: `data` prop is either undefined or does not contain a `data` array."
  //     );
  //     setLocalApprovals([]); // Set to empty array to avoid errors in rendering
  //   }
  // }, [data]);

  const [rows, setRows] = useState([]);

  useEffect(() => {
    if (data && Array.isArray(data.data)) {
      const newRows = data.data.map((row, index) => ({
        id: index,
        ...row,
      }));
      setRows(newRows);
    } else {
      console.warn(
        "ApproverTable: `data` prop is either undefined or does not contain a `data` array."
      );
      setRows([]);
    }
  }, [data]);

  const getStatusColor = (status) => {
    switch (status) {
      case "Denied":
        return "text-red-500";
      case "Done":
        return "text-green-500";
      case "Queued":
        return "text-yellow-500";
      default:
        return "text-gray-900";
    }
  };

  const formatDate = (dateString) => {
    if (dateString === null) return '';
    const date = new Date(dateString);
    const year = date.getFullYear().toString().padStart(4, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hour = date.getHours().toString().padStart(2, "0");
    const minute = date.getMinutes().toString().padStart(2, "0");
    const second = date.getSeconds().toString().padStart(2, "0");
    return `${year}/${month}/${day}`;
};
const formatDate2 = (dateString) => {
  if (dateString === null) return '';
  const date = new Date(dateString);
  const year = date.getFullYear().toString().padStart(4, "0");
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const day = date.getDate().toString().padStart(2, "0");
  const hour = date.getHours().toString().padStart(2, "0");
  const minute = date.getMinutes().toString().padStart(2, "0");
  const second = date.getSeconds().toString().padStart(2, "0");
  return `${year}/${month}/${day} ${hour}:${minute}:${second}`;
};

  // const formatDate = (dateString) => {
  //   const date = new Date(dateString);
  //   const day = date.getDate().toString().padStart(2, "0");
  //   const month = (date.getMonth() + 1).toString().padStart(2, "0");
  //   const year = date.getFullYear().toString().substr(-2);

  //   // Extracting time components
  //   const hours = date.getHours().toString().padStart(2, "0");
  //   const minutes = date.getMinutes().toString().padStart(2, "0");
  //   const seconds = date.getSeconds().toString().padStart(2, "0");

  //   return `${day}-${month}-${year}`;
  //   // return `${day}-${month}-${year} ${hours}:${minutes}`;
  // };

  const columns = [
    {
      field: "ApprovalSequence",
      headerName: "Approval Level",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "ApproverNik",
      headerName: "Approval NIK",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "ApproverName",
      headerName: "Approval Name",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "IdDepartment",
      headerName: "Department ID",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "DepartmentName",
      headerName: "Department Name",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row.Department?.DepartmentName || "",
    },
    {
      field: "CreatedAt",
      headerName: "Request Date",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "DateApproval",
      headerName: "Approval Date",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueFormatter: ({ value }) => formatDate2(value),
      // hide: (params) => params.getValue("status") !== "Done", // Conditionally hide based on status
      valueGetter: (params) =>
        params.row.status !== "Done" ? params.row.DateApproval : "",
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      editable: true,
      align: "center",
      headerAlign: "center",
      cellClassName: (params) => getStatusColor(params.value),
    },
    { field: "Notes", headerName: "Remarks", width: 200, align: "center" },
  ];

  return (
    <Box sx={{ height: 700, width: "100%" }}>
      <DataGrid
        rows={rows}
        // setLocalApprovals = {localApprovals}
        columns={columns}
        pageSize={10}
        rowsPerPageOptions={[5, 10, 20]}
        disableSelectionOnClick
      />
    </Box>
  );

  // return (
  //   <div className="relative overflow-x-hidden overflow-y-auto max-h-[34rem] border">
  //     <Table sx={{ width: "100%", borderAxis: "both" }} size="small">
  //       <TableHead className="text-xs text-gray-700 uppercase bg-gray-100 sticky top-0.5">
  //         <TableRow>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 5, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Appr Lvl
  //           </TableCell>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 5, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Appr NIK
  //           </TableCell>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 5, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Appr Name
  //           </TableCell>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 4, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Dprt ID
  //           </TableCell>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 5, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Dprt Name
  //           </TableCell>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 5, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Request Date
  //           </TableCell>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 3, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Approval Date
  //           </TableCell>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 5, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Status
  //           </TableCell>
  //           <TableCell
  //             align="center"
  //             sx={{ maxWidth: 4, p: 0, border: 1, borderColor: "grey.500" }}
  //           >
  //             Remarks
  //           </TableCell>
  //           {/* <th scope="col" className="px-3 py-6 w-10">If Status</th> */}
  //           {/* <th scope="col" className="px-3 py-6 w-10">Action</th> */}
  //         </TableRow>
  //       </TableHead>

  //       <TableBody className="bg-white divide-y divide-gray-200">
  //         {/* {console.log("data: " ,data.data)} */}

  //         {/* {data.data.length > 0 ? (
  //                      console.log("data: " ,data.data),
  //           data.data.map((approval, idx) => ( */}

  //         {localApprovals.length > 0 ? (
  //           localApprovals.map((approval, idx) => (
  //             <TableRow
  //               key={idx}
  //               className="cursor-pointer border-b hover:bg-gray-200 bg-white"
  //             >
  //               <TableCell
  //                 align="center"
  //                 sx={{ border: 1, borderColor: "grey.500" }}
  //               >
  //                 {approval.ApprovalSequence}
  //               </TableCell>
  //               <TableCell
  //                 align="center"
  //                 sx={{ border: 1, borderColor: "grey.500" }}
  //               >
  //                 {approval.ApproverNik}
  //               </TableCell>
  //               <TableCell
  //                 align="center"
  //                 sx={{ border: 1, borderColor: "grey.500" }}
  //               >
  //                 {approval.ApproverName}
  //               </TableCell>
  //               <TableCell
  //                 align="center"
  //                 sx={{ maxWidth: 4, border: 1, borderColor: "grey.500" }}
  //               >
  //                 {approval.IdDepartment}
  //               </TableCell>
  //               <TableCell
  //                 align="center"
  //                 sx={{ border: 1, borderColor: "grey.500" }}
  //               >
  //                 {approval.Department.DepartmentName}
  //               </TableCell>
  //               <TableCell
  //                 align="center"
  //                 sx={{ border: 1, borderColor: "grey.500" }}
  //               >
  //                 {formatDate(approval.CreatedAt)}
  //               </TableCell>
  //               <TableCell
  //                 align="center"
  //                 sx={{ border: 1, borderColor: "grey.500" }}
  //               >
  //                 {formatDate(approval.DateApproval)}
  //               </TableCell>
  //               <TableCell
  //                 align="center"
  //                 sx={{ border: 1, borderColor: "grey.500" }}
  //                 className={getStatusColor(approval.Status)}
  //               >
  //                 {approval.Status}
  //               </TableCell>
  //               <TableCell
  //                 align="center"
  //                 sx={{ border: 1, borderColor: "grey.500" }}
  //               >
  //                 {approval.Notes}
  //               </TableCell>
  //             </TableRow>
  //           ))
  //         ) : (
  //           <TableRow>
  //             <TableCell colSpan="5" align="center">
  //               No data available
  //             </TableCell>
  //           </TableRow>
  //         )}
  //       </TableBody>
  //     </Table>
  //   </div>
  // );
};

export default ApproverTable;
