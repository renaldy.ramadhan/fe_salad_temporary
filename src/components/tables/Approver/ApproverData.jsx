import { Delete, Edit } from "@mui/icons-material";
import {
  Alert,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import CustomNoRowsOverlay from "../../icons/CustomNoRowsOverlay";
import { useNavigate } from "react-router-dom";
import { del } from "../../../util/http";

export const ApproverData = () => {
  const navigate = useNavigate();
  const approvers = useSelector((state) => state.approvals.approver);
  const approverStatus = useSelector((state) => state.approvals.status);

  const [rows, setRows] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [openDialog, setOpenDialog] = useState(false);
  const [activeId, setActiveId] = useState(0);
  const [alert, setAlert] = useState({
    status: false,
    type: "success",
    message: "",
  });

  useEffect(() => {
    console.log(approverStatus);
    if (approverStatus === "done") {
      setIsLoading(false);
      if (approvers.length > 0) {
        setRows(
          approvers.map((approver) => ({ ...approver, id: approver.ID }))
        );
      }
    }
    return () => {
      setRows([]);
    };
  }, [approverStatus, approvers]);

  const handleEdit = (id) => {
    navigate("edit/" + id);
  };

  const handleOpenDialog = (id) => {
    setOpenDialog(true);
    setActiveId(id);
  };

  const handleCloseDialog = (id) => {
    setOpenDialog(false);
    setActiveId(0);
  };

  const handleDelete = async () => {
    const res = await del(
      `${process.env.REACT_APP_API_URL_Approver_Service}/delete/${activeId}`
    );
    if (res.status === 200) {
      window.location.reload();
    } else {
      setAlert({
        ...alert,
        type: "error",
        message: "Failed delete approver. Please try again later.",
        status: true,
      });
    }
  };

  const columns = [
    {
      field: "number",
      headerName: "No",
      filterable: false,
      minWidth: 50,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "ApproverGroupName",
      headerName: "Approver Group",
      filterable: false,
      minWidth: 200,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => {
        return params.row.ApproverGroup.map((group) => {
          return group.ApproverGroupName;
        });
      },
    },
    {
      field: "Nik",
      headerName: "NIK",
      filterable: false,
      minWidth: 200,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "EmployeeName",
      headerName: "Employee Name",
      filterable: false,
      minWidth: 300,
      valueGetter: (params) => {
        if (params.row.EmployeeName === null) {
          return "-";
        }
        return params.row.EmployeeName;
      },
    },
    {
      field: "Sequence",
      headerName: "Sequence",
      filterable: false,
      minWidth: 200,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 200,
      cellClassName: "actions",
      align: "center",
      flex: 1,
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="error"
            variant="contained"
            startIcon={<Delete />}
            size="small"
            onClick={() => handleOpenDialog(id)}
          >
            <p className="text-xs">Delete</p>
          </Button>,
          <Button
            color="success"
            variant="contained"
            startIcon={<Edit />}
            size="small"
            onClick={() => handleEdit(id)}
          >
            <p className="text-xs">Edit</p>
          </Button>,
        ];
      },
    },
  ];

  return (
    <Box
      sx={{
        height: 600,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={rows.map((item, index) => ({ ...item, number: index + 1 }))}
        columns={columns}
        editMode="row"
        loading={isLoading}
        slotProps={{
          toolbar: { setRows },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
      <Dialog
        open={openDialog}
        onClose={handleCloseDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          className="text-red-400 text-center"
        >
          {"Delete"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText
            id="alert-dialog-description"
            className="text-center"
          >
            {alert.status && (
              <Alert className="mb-2" severity={alert.type} variant="filled">
                {alert.message}
              </Alert>
            )}
            Are you sure want to delete this row?
          </DialogContentText>
        </DialogContent>
        <DialogActions className="flex items-center">
          <Button onClick={handleCloseDialog} variant="outlined">
            No
          </Button>
          <Button onClick={handleDelete} variant="contained" color="error">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};
