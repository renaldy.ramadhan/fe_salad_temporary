import React, { useEffect, useState } from 'react';
import DocumentService from '../../service/DocumentService';
import { useNavigate, useLocation, Navigate, useParams } from 'react-router-dom';
import HashMap from 'hashmap';
import { useDispatch,useSelector } from 'react-redux';
import { getDocumentByRequest } from '../../slices/DocumentSlice'; 

function DocumentTableCreate({ onDataSend, onTableLoad }) {

  const [selectedDocument, setSelectedDocument] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');
  const documents = useSelector(state => state.documents); 
  const [uploadedFiles, setUploadedFiles] = useState(onTableLoad()); 
  console.log('ontableload: ', onTableLoad())
  console.log(uploadedFiles)
  
  const handleFileChange = (event) => {
    const chosenFiles = Array.prototype.slice.call(event.target.files);
    onDataSend(chosenFiles)
    setUploadedFiles(chosenFiles);
  };


  const deleteRow = index => {
    // Create a copy of row data without the current row
    const newData = [...uploadedFiles.slice(0, index), ...uploadedFiles.slice(index + 1)];
    onDataSend(newData)
    setUploadedFiles(newData);
  }

  return (
    <div>
      <div className="flex flex-col items-center">
        <label htmlFor="documentInput" className="block text-sm font-medium text-slate-700">
          Select a File
        </label>
        <input
          id="documentInput"
          type="file"
          multiple
          className="border-2 border-gray-300 py-2 px-4 rounded-md md:w-1/2 w-2/3"
          onChange={handleFileChange}
        />
        {errorMessage && <p className="text-red-500">{errorMessage}</p>}
        {selectedDocument && (
          <div className="mt-4">
            {/* Display selected file name or other info */}
          </div>
        )}
      </div>

      <div className="mx-3 my-8">
        <div className="overflow-x-auto overflow-y-auto max-h-[34rem]">       
          <table className="w-full text-sm text-gray-500 text-center shadow-md border-2">
            {/* table headers */}
            <thead className="text-xs text-gray-700 uppercase bg-gray-100 sticky top-0">
                <tr>
                    <th scope="col" className="px-3 py-6 w-20">
                        File
                    </th>
                    <th scope="col" className="px-3 py-6 w-20">
                        Action
                    </th>  
                </tr>
            </thead>
            {/* table body */}
            <tbody>
                {
                uploadedFiles.map((file, index) => (
                    <tr key={index} className="bg-white border-b hover:-*bg-gray-200">
                        <td className="px-6 py-4">{file.name}</td>
                        <th scope="row" className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap">
                            <button className="inline-flex items-center max-w-[180px] px-4 py-2 bg-[#17479d] text-white text-sm font-medium rounded-md"
                            onClick={() => deleteRow(index)}>
                                Remove
                            </button>
                        </th>
                    </tr> 
                ))}
            </tbody>
          </table>
        </div>
      </div>                            
    </div>
  );
}

export default DocumentTableCreate;