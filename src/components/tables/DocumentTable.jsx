import React, { useEffect, useState } from "react";
import DocumentService from "../../service/DocumentService";
import {
  useNavigate,
  useLocation,
  Navigate,
  useParams,
} from "react-router-dom";
import HashMap from "hashmap";
import { useDispatch, useSelector } from "react-redux";
import { getDocumentByRequest } from "../../slices/DocumentSlice";
import { saveAs } from "file-saver";

function DocumentTable({ requestId }) {
  const [upload, setUpload] = useState(false);
  const [selectedDocument, setSelectedDocument] = useState(null);
  const [errorMessage, setErrorMessage] = useState("");
  const documents = useSelector((state) => state.documents);
  const [localStateDoc, setLocalStateDoc] = useState(documents);
  const dispatch = useDispatch();
  const [blob, setBlob] = useState(null);

  useEffect(() => {
    if (requestId) {
      dispatch(getDocumentByRequest(requestId));
    }
  }, [dispatch, requestId]);

  const uploadAction = async (file, data) => {
    try {
      const response = await DocumentService.uploadAction(file, data);
      setUpload(response.data);
      await dispatch(getDocumentByRequest(requestId));
    } catch (e) {
      console.error(e);
    }
  };

  const handleFileChange = (event) => {
    const file = event.target.files[0];

    if (file) {
      var form = new HashMap();
      form.set("IdRequest", requestId);
      form.set("DocumentNotes", "test upload");

      setSelectedDocument(file);
      uploadAction(file, form);
      dispatch(getDocumentByRequest(requestId));
      setLocalStateDoc(documents);
    }
  };

  useEffect(() => {
    setLocalStateDoc(documents);
  }, [documents]);

  const handleDownload = (doc) => {
    const mimeType = doc.DocumentExtensions
      ? `application/${doc.DocumentExtensions}`
      : "application/octet-stream";

    var link = document.createElement("a");
    document.body.appendChild(link);
    link.style = "display: none";
    link.download = doc.DocumentRealName;

    if (blob != null) {
      link.href = blob;
      link.click();
    } else {
      DocumentService.downloadDocumentByID(doc.ID)
        .then((response) => {
          const blob = new Blob([response.data], { type: mimeType });
          const fileURL = URL.createObjectURL(blob);
          setBlob(fileURL);
          link.href = fileURL;
          link.click();
          window.URL.revokeObjectURL(fileURL);
        })
        .catch((error) => {
          console.error("There was an error downloading the document:", error);
        });
    }
  };

  return (
    <div>
      <div className="flex flex-col items-center">
        <label
          htmlFor="documentInput"
          className="block text-sm font-medium text-slate-700"
        >
          Select a File
        </label>
        <input
          id="documentInput"
          type="file"
          className="border-2 border-gray-300 py-2 px-4 rounded-md md:w-1/2 w-2/3"
          onChange={handleFileChange}
        />
        {errorMessage && <p className="text-red-500">{errorMessage}</p>}
        {selectedDocument && (
          <div className="mt-4">
            {/* Display selected file name or other info */}
          </div>
        )}
      </div>

      <div className="mx-3 my-8">
        <div className="overflow-x-auto overflow-y-auto max-h-[34rem]">
          <table className="w-full text-sm text-gray-500 text-center shadow-md border-2">
            {/* table headers */}
            <thead className="text-xs text-gray-700 uppercase bg-gray-100 sticky top-0">
              <tr>
                <th scope="col" className="px-3 py-6 w-20">
                  File
                </th>
                <th scope="col" className="px-3 py-6 w-20">
                  Action
                </th>
              </tr>
            </thead>
            {/* table body */}
            <tbody>
              {localStateDoc.map((doc, index) => (
                <tr
                  key={index}
                  className="bg-white border-b hover:-*bg-gray-200"
                >
                  <td className="px-6 py-4">{doc.DocumentRealName}</td>
                  <th
                    scope="row"
                    className="px-6 py-4 font-medium text-gray-900 whitespace-nowrap"
                  >
                    <button
                      className="inline-flex items-center max-w-[180px] px-4 py-2 bg-[#17479d] text-white text-sm font-medium rounded-md"
                      onClick={() => handleDownload(doc)}
                    >
                      Download
                    </button>
                  </th>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default DocumentTable;
