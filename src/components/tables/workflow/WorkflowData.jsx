import { Add, Edit } from "@mui/icons-material";
import { Box, Button, Grid, Paper } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import CustomNoRowsOverlay from "../../icons/CustomNoRowsOverlay";
import { useNavigate } from "react-router-dom";
import { setWorkflowId } from "../../../slices/WorkflowSlice";

export const WorkflowData = () => {
  const workflows = useSelector((state) => state.workflows.workflowsData);
  const workflowStatus = useSelector((state) => state.workflows.status);
  const [rows, setRows] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (workflowStatus === "done") {
      setIsLoading(false);
      setRows(workflows.map((item) => ({ ...item, id: item.ID })));
    }
  }, [workflows]);
  const handleAction = (id) => {
    dispatch(setWorkflowId(id));
    navigate("/masterdata/flowstep/create");
  };

  const handleEdit = (id) => {
    navigate("edit/" + id);
  };

  const columns = [
    {
      field: "number",
      headerName: "No",
      filterable: false,
      minWidth: 10,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "Type",
      headerName: "Type",
      filterable: false,
      minWidth: 200,
    },
    {
      field: "Notes",
      headerName: "Notes",
      filterable: false,
      minWidth: 350,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 300,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<Add />}
            size="small"
            onClick={() => handleAction(id)}
          >
            <p className="text-xs">Add Flowstep</p>
          </Button>,
          <Button
            color="success"
            variant="contained"
            startIcon={<Edit />}
            size="small"
            onClick={() => handleEdit(id)}
          >
            <p className="text-xs">Edit</p>
          </Button>,
        ];
      },
    },
  ];
  return (
    <Box
      sx={{
        height: 500,
        width: "100%",
        "& .actions": {
          color: "text.secondary",
        },
        "& .textPrimary": {
          color: "text.primary",
        },
        "& .MuiDataGrid-cell": {
          borderRight: "2px solid #ccc", // Add right border to all cells
        },
        // If you want to add a bottom border to each row, you can use:
        "& .MuiDataGrid-row": {
          borderBottom: "2px solid #ccc",
        },
      }}
    >
      <DataGrid
        rows={rows.map((item, index) => ({ ...item, number: index + 1 }))}
        columns={columns}
        editMode="row"
        loading={isLoading}
        slotProps={{
          toolbar: { setRows },
        }}
        slots={{
          noRowsOverlay: CustomNoRowsOverlay,
        }}
      />
    </Box>
  );
};
