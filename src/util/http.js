import axios from "axios"

const REQ_TIMEOUT = 1000 * 80 // 80 seconds

export const get = async (url, customConfig) => {
  const config = {
    ...customConfig,
    timeout: REQ_TIMEOUT,
    headers: { Authorization: 'Bearer ' + window.localStorage.getItem('token') },
  }
  try {
    return await axios.get(url, config);
  } catch (e) {
    if (e.code === "ECONNABORTED") { // server no response, Service Unavailable
      return { data: [], status: 503 };;
    } else if (e.code === "ERR_NETWORK") { // host is down, Gatway Timeout
      return { data: [], status: 504 };
    } else if (e.code === 500) {
      return { data: [], status: 500 };
    } else if (e.code === 412) {
      return { data: [], status: 412 };
    } else if (e.code === 400) {
      return { data: [], status: 400 };
    }
    else {
      console.log("eresponse: ", e.response)
      const { data, status } = e.response;
      return { data, status }
    }
  }
};

export const post = async (url, data) => {
  const config = {
    timeout: REQ_TIMEOUT,
    headers: { Authorization: 'Bearer ' + window.localStorage.getItem('token') },
  }
  try {
    return await axios.post(url, data, config);
  } catch (e) {
    if (e.code === "ECONNABORTED") { // server no response, Service Unavailable
      return { data: null, status: 503 };;
    } else if (e.code === "ERR_NETWORK") { // host is down, Gatway Timeout
      return { data: null, status: 504 };
    } else {
      const { data, status } = e.response;
      return { data, status }
    }
  }
};

export const put = async (url, data) => {
  const config = {
    timeout: REQ_TIMEOUT,
    headers: { Authorization: 'Bearer ' + window.localStorage.getItem('token') },
  }
  try {
    return await axios.put(url, data, config);
  } catch (e) {
    if (e.code === "ECONNABORTED") { // server no response, Service Unavailable
      return { data: null, status: 503 };;
    } else if (e.code === "ERR_NETWORK") { // host is down, Gatway Timeout
      return { data: null, status: 504 };
    } else {
      const { data, status } = e.response;
      return { data, status }
    }
  }
};

export const del = async (url) => {
  const config = {
    timeout: REQ_TIMEOUT,
    headers: { Authorization: 'Bearer ' + window.localStorage.getItem('token') },
  }
  try {
    return await axios.delete(url, config);
  } catch (e) {
    if (e.code === "ECONNABORTED") { // server no response, Service Unavailable
      return { data: null, status: 503 };;
    } else if (e.code === "ERR_NETWORK") { // host is down, Gatway Timeout
      return { data: null, status: 504 };
    } else {
      const { data, status } = e.response;
      return { data, status }
    }
  }
};

export const postMultipartRequest = async (url, files, otherFormData) => {

  const formData = new FormData();

  if (files) {
    files.map((file, index) => (
      formData.append('files', file)
    ))
  }
  formData.append('jsonData', otherFormData);

  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: 'Bearer ' + window.localStorage.getItem('token'),
    },
    timeout: REQ_TIMEOUT,
  };

  try {
    console.log(url, formData, config)
    return await axios.post(url, formData, config);
  } catch (e) {
    console.log(e)
    if (e.code === "ECONNABORTED") {
      return { data: null, status: 503 };
    } else if (e.code === "ERR_NETWORK") {
      return { data: null, status: 504 };
    } else {
      const { data, status } = e.response;
      return { data, status };
    }
  }
};

export const postMultipart = async (url, file, otherFormData) => {

  const formData = new FormData();

  formData.append('file', file);

  otherFormData.forEach(function (value, key) {
    formData.append(key, otherFormData.get(key));
  });

  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: 'Bearer ' + window.localStorage.getItem('token'),
    },
    timeout: REQ_TIMEOUT,
  };

  try {
    return await axios.post(url, formData, config);
  } catch (e) {
    if (e.code === "ECONNABORTED") {
      return { data: null, status: 503 };
    } else if (e.code === "ERR_NETWORK") {
      return { data: null, status: 504 };
    } else {
      const { data, status } = e.response;
      return { data, status };
    }
  }
};

export const download = async (url) => {
  const config = {
    timeout: REQ_TIMEOUT,
    headers: { Authorization: 'Bearer ' + window.localStorage.getItem('token') },
    responseType: 'blob',
  }

  try {
    const response = await axios.get(url, config);
    return response;
  } catch (e) {
    if (e.code === "ECONNABORTED") {
      return { data: null, status: 503 };
    } else if (e.code === "ERR_NETWORK") {
      return { data: null, status: 504 };
    } else {
      if (e.response) {
        const { data, status } = e.response;
        return { data, status };
      } else {
        return { data: null, status: e.status || 500 };
      }
    }
  }
};

