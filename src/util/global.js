export function buildQueryParams(params) {
  if (!params || typeof params !== 'object') {
    return '';
  }

  const queryParams = Object.keys(params)
    .filter(key => params[key] !== undefined && params[key] !== null)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

  return queryParams ? `?${queryParams}` : '';
}

export function parseQueryParams(queryString) {
  const params = {};

  if (!queryString || typeof queryString !== 'string') {
    return params;
  }

  const pairs = queryString.startsWith('?') ? queryString.slice(1).split('&') : queryString.split('&');

  for (let pair of pairs) {
    const [key, value] = pair.split('=');

    if (!key) {
      continue;
    }

    const decodedKey = decodeURIComponent(key);
    const decodedValue = value ? decodeURIComponent(value) : true;

    if (params[decodedKey]) {
      if (Array.isArray(params[decodedKey])) {
        params[decodedKey].push(decodedValue);
      } else {
        params[decodedKey] = [params[decodedKey], decodedValue];
      }
    } else {
      params[decodedKey] = decodedValue;
    }
  }

  return params;
}

export function mergeQueryParams(queryString, obj) {
  const params = parseQueryParams(queryString);

  // Merge the object with the existing params
  for (const key in obj) {
    params[key] = obj[key];
  }

  // Convert the merged params back to a query string
  const queryParams = Object.entries(params)
    .map(([key, value]) => {
      if (Array.isArray(value)) {
        return value.map(v => `${encodeURIComponent(key)}=${encodeURIComponent(v)}`).join('&');
      }
      return `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;
    })
    .join('&');

  return queryParams ? `?${queryParams}` : '';
}