// const keycloakConfig = {
//   url: 'http://184.169.41.196:8080/auth', 
//   realm: 'react-keycloak', 
//   clientId: 'react-client',
//     onLoad: 'login-required',
//     KeycloakResponseType: 'code',
//     store: 'localStorage',
//     checkLoginIframe: false
//   };
  
//   export default keycloakConfig;  

const keycloakConfig = {
  url: 'https://ieiappslogin-dev.epson.biz/auth', 
  realm: 'epson', 
  clientId: 'salad-spa',
  onLoad: 'login-required',
  KeycloakResponseType: 'code',
  store: 'localStorage',
  checkLoginIframe: false
  };
export default keycloakConfig;  