import React, { useEffect, useState } from 'react';
import Keycloak from 'keycloak-js';
import keycloakConfig from '../Keycloak';
import LoadingSpinner from '../components/animations/loading/LoadingSpinner';

const KeycloakContext = React.createContext();

const KeycloakProvider = ({ children }) => {
  const [keycloak, setKeycloak] = useState(null);
  const [authenticated, setAuthenticated] = useState(false);

  useEffect(() => {
    const kc = new Keycloak(keycloakConfig);
    console.log("kc initialize:", kc)
    kc.init({ onLoad: keycloakConfig.onLoad }).then(async (auth) => {
      console.log("auth:", auth)
      setKeycloak(kc);
      setAuthenticated(auth);
      window.localStorage.setItem('token', kc.token);
      
    }).catch((err)=>{
      console.log(err)
    });

    kc.onTokenExpired = () => {
      kc.updateToken(30).then(refreshed => {
        if (refreshed) {
          setKeycloak({...kc});
          window.localStorage.setItem('token', keycloak.token);
        }
      }).catch(() => {
        window.localStorage.removeItem('token');
        kc.login();
      });
    };
            
  }, []);

  if (!keycloak) return (
    <div><LoadingSpinner/></div>
  );

  return (
    <KeycloakContext.Provider value={{ keycloak, authenticated }}>
      {children}
    </KeycloakContext.Provider>
  );
};

export { KeycloakContext, KeycloakProvider }