import React, { useState, useEffect, createContext } from 'react'
import { useLocation, useNavigate } from 'react-router-dom';
import LoadingSpinner from '../components/animations/loading/LoadingSpinner';
import axios from 'axios';

const UserContext = createContext();

const UserProvider = ({ children }) => {
  const USER_SERVICE_BASE_URL = process.env.REACT_APP_API_URL_User_Service;
  const navigate = useNavigate()
  const location = useLocation()
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(true);

  const getUser = () => {
    axios.get(`${USER_SERVICE_BASE_URL}auth/user`, { headers:{Authorization: 'Bearer ' + window.localStorage.getItem('token')}})
    .then((r) => {
       console.log("user di context",r.data)
       setUser(r.data)
    })
    .catch((e) => {
        console.log(e)
    })
    .finally(()=>{
      setLoading(false)
    });
  }

  const parseJwt = (token) => {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(
      atob(base64)
        .split('')
        .map((c) => {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join('')
    );

    return JSON.parse(jsonPayload);
  };

  // useEffect(() => {
  //   const token = window.localStorage.getItem('token');
  //   if ((!token || token === '') && (location.pathname !== '/register')) {
  //     navigate('/login');
  //     setLoading(false);
  //   } else {
  //     // Check if the token has expired
  //     if ((location.pathname !== '/register')){
  //       const decodedToken = parseJwt(token);
  //       const currentTimestamp = Math.floor(Date.now() / 1000);
  
  //       if ((decodedToken.Expired < currentTimestamp)) {
  //         window.localStorage.removeItem('token');
  //         navigate('/login');
  //         setLoading(false);
  //       }
  //     }
  //   }
  //   if (token) getUser();
  //   setLoading(false);
  // }, []);

  // if (loading) {
  //   return (
  //     <div>
  //       <LoadingSpinner/>
  //     </div>
  //   );
  // }

  return (
    <UserContext.Provider value={{user, setUser}}>
      {children}
    </UserContext.Provider>
  )
}

export { UserContext, UserProvider };