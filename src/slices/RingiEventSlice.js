import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import RingiEventService from "../service/DisposalEventService";

const initialState = {
  ringiEvent: [],
  ringiEventStatus: "loading" ,
  ringiGetEvent: [],
  ringiGetEventStatus : "loading",

//   partGetRequestItemsData: [],
//   partRequestStatus: "loading" ,
//   partGetRequestStatus : "loading",
//   partGetRequestTable: [],
//   updateTableData:[],
};

export const createRingiEvent = createAsyncThunk(
  "ringievent/create",
  async ( jsonData ) => {
    console.log(
      jsonData
    )
    const res = await RingiEventService.createEvent(jsonData);
    console.log(res.data);
    return res.data;
  }
);

export const retrieveRingiEvent = createAsyncThunk(
  "ringievent/getall",
  async () => {
    const res = await RingiEventService.getAllEvent();
    return {data: res.data, status: res.status};
  }
);

export const editRingiEventData = createAsyncThunk(
  "ringievent/put",
  async (id,data) => {
    const res = await RingiEventService.editDisposalEventData(id,data);
    return res.data;
  }
);

export const getRingiEvent = createAsyncThunk(
    "ringievent/get",
    async (id) => {
      const res = await RingiEventService.getOneEvent(id);
      console.log("res: ", res)
      return {data: res.data, status: res.status};
    }
  );

// export const deletePartRequest = createAsyncThunk(
//   "partrequest/delete",
//   async (data) => {
//     await PartRequestService.removeRequest(data);
//     return { data };
//   }
// );

// export const forceUpdatePartRequest = createAsyncThunk(
//   "partrequest/forceUpdatePartRequest",
//   async (data) => {
//     return data;
//   }
// );

// export const UpdateTablePartRequest = createAsyncThunk(
//   "partrequest/UpdatePartRequest",
//   (data) => {
//     return data;
//   }
// );

// export const getPartRequestTable = createAsyncThunk(
//   "partrequest/GetPartRequestTable",
//   (data) => {
//     return data;
//   }
// );



const ringiEventSlice = createSlice({
  name: "ringievent",
  initialState,
  extraReducers: builder => {
    builder.addCase(createRingiEvent.fulfilled , (state, action) => {
      state.ringiEvent.push(action.payload);
      state.ringiEventStatus= "done" 
    })
    .addCase(createRingiEvent.pending , (state, action) => {
      state.ringiEventStatus= "loading" 
    })
    // .addCase(retrieveDisposalEvent.fulfilled, (state, action) => {
    //   state.disposalEvent = action.payload ; 
    //   state.disposalEventStatus= "done" ;
    // })
    .addCase(retrieveRingiEvent.fulfilled, (state, action) => {
        // Assuming action.payload.data is the array of events
        state.ringiEvent = action.payload.data; 
        state.ringiEventStatus = "done";
      })
    .addCase(retrieveRingiEvent.pending, (state, action) => {
      state.ringiEvent = [] ; 
      state.ringiEventStatus= "loading" ;
    })
    // .addCase(UpdateTablePartRequest.fulfilled, (state, action) => {
    //   state.updateTableData = action.payload ; 
      
    // })
    // .addCase(deletePartRequest.fulfilled, (state, action) => {
    //   let index = state.findIndex(({ id }) => id === action.payload.id);
    //   state.splice(index, 1);
    //   state.partRequestStatus= "done" 
    // })
    // .addCase(deletePartRequest.pending, (state, action) => {
    //   state.partRequestStatus= "loading" 
    // })
    .addCase(getRingiEvent.fulfilled, (state, action) => {
        state.ringiGetEvent = action.payload;
        state.ringiGetEventStatus= "done" ; 
    })
    .addCase(getRingiEvent.pending, (state, action) => {
        state.ringiGetEventStatus= "loading" ; 
    })
    // .addCase(getPartRequestTable.fulfilled, (state, action) => {
    //     state.partGetRequestTable = action.payload; 
    // })
  }
});

const { reducer } = ringiEventSlice;
export default reducer;