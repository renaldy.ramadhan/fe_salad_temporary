import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import WorkflowService from "../service/WorkflowService"
const initialState = {
    workflowsData : [],
    status : "loading",
    createdSuccess : false,
    activeWorkflowId : ""
}

export const retrieveWorkflows = createAsyncThunk(
    "workflows/retrieveWorkflows",
    async () => {
        const res = await WorkflowService.getWorkflow()
        return res.data
    }
)

export const createWorkflow = createAsyncThunk(
    "workflows/createWorkflow",
    async ({payload}) => {
        const res = await WorkflowService.createWorkflow(payload)
        return res.data 
    }
)

const workflowSlices = createSlice({
    name: "workflows",
    initialState,
    reducers: {
        setWorkflowId: (state,action) => {
            state.activeWorkflowId = action.payload
        }
    },
    extraReducers: (builder) => {
        builder.addCase(retrieveWorkflows.fulfilled, (state,action) => {
            state.workflowsData = action.payload
            state.status = "done"
        })
        .addCase(retrieveWorkflows.pending, (state,action) => {
            state.workflowsData = []
            state.status = "loading"
        })
        .addCase(createWorkflow.fulfilled, (state,action) => {
            if (action.message === "Created!"){
                state.createdSuccess = true
            }   
        })
    }
})

export const { setWorkflowId } = workflowSlices.actions
const {reducer} = workflowSlices
export default reducer