import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import DisposalEventService from "../service/DisposalEventService";
import { buildQueryParams, parseQueryParams } from "../util/global";

const initialState = {
  activeDisposalEvent: [],
  activeDisposalRingiEvent: [],
  retrieveActiveDisposalEventStatus: "loading",
  disposalEvent: [],
  disposalRingiEvent: [],
  retrieveDisposalEventStatus: "loading",
  createDisposalStatus: "loading",
  retrieveDisposalEvent: [],
  deleteDisposalEvent: [],
  deleteDisposalEventStatus: "loading",
  canApprove: false,

  disposalEventDetail: [],
  getDisposalEventStatus: "loading",
  approvalSetting: [],
  approvalSettingStatus: "loading",
  approvalRequest: [],
  approvalRequestStatus: "loading",
  pageRequest: 1,
  pageSizeRequest: 5,

  bulkCreateDisposalRequest: "loading",
  itemsRequestDetail: [],
  itemsRequestDetailStatus: "loading",

  partGetDisposalEventTable: [],
  page: 1,
  pageSize: 5,
  activePage: 1,
  activePageSize:5,
  disposalEventQueryParams : {
    type: "Disposal Event",
    page: 1,
    page_size : 5,
  },
  activeDisposalEventQueryParams : {
    type: "Disposal Event",
    status: "Active",
    page: 1,
    page_size : 5,
  }
};

export const createDisposalEvent = createAsyncThunk(
  "disposalEvent/create",
  (data) => {
    return DisposalEventService.createDisposalEvent(data);
  }
);
export const bulkCreateDisposalEventRingiRequest = createAsyncThunk(
  "disposalEvent/bulkCreateReqeust",
  (data) => {
    return DisposalEventService.bulkCreateDisposalEventRequest(data);
  }
);

export const generateDisposalSetting = createAsyncThunk(
  "disposalEvent/generateDisposalSetting",
  (WorkflowType = "Disposal Event") => {
    return DisposalEventService.generateDisposalSetting({ WorkflowType });
  }
);

export const retrieveDisposalEvent = createAsyncThunk(
  "disposalEvent/getall",
  async (data,thunkAPI) => {
    const params = thunkAPI.getState().disposalEvent.disposalEventQueryParams;
    return DisposalEventService.getAllDisposalEvent(buildQueryParams(params));
  }
);

export const retrieveActiveDisposalEvent = createAsyncThunk(
  "disposalEvent/getallActive",
  (data,thunkAPI) => {
    const params = thunkAPI.getState().disposalEvent.activeDisposalEventQueryParams;
    return DisposalEventService.getAllDisposalEvent(buildQueryParams(params));
  }
);

export const retrieveRingiEvent = createAsyncThunk(
  "disposalRingiEvent/getall",
  async (params) => {
    return DisposalEventService.getAllDisposalEvent(params)
  }
);

export const retrieveActiveRingiEvent = createAsyncThunk(
  "disposalRingiEvent/getallActive",
  (params) => {
    return DisposalEventService.getAllDisposalEvent(params);
  }
);

// export const deleteDisposalEvent = createAsyncThunk(
//   "disposalEvent/delete",
//   async (data) => {
//     await DisposalEventService.removeRequest(data);
//     return { data };
//   }
// );

export const getDisposalEvent = createAsyncThunk(
  "disposalEvent/get",
  async (id) => {
    return DisposalEventService.getDetailEvent(id);
  }
);

export const getDisposalEventApprover = createAsyncThunk(
  "disposalEvent/getApprover",
  async (id) => {
    return DisposalEventService.getApprover(id);
  }
);
export const checkDisposalEventApprover = createAsyncThunk(
  "disposalEvent/checkApprover",
  async (id) => {
    return DisposalEventService.checkApprover(id);
  }
);
export const settleEventRequest = createAsyncThunk(
  "disposalEvent/settleEventRequest",
  (data) => {
    return DisposalEventService.settleEventRequest(data);
  }
);
export const getDisposalEventRequest = createAsyncThunk(
  "disposalEvent/getRequest",
  async (pageState) => {
    return DisposalEventService.getRequest(pageState);
  }
);

export const getItemsRequestDetail = createAsyncThunk(
  "disposalEvent/getItemsRequestDetail",
  async (pageState) => {
    return DisposalEventService.getItemsRequestDetail(pageState);
  }
);

// export const getDisposalEventTable = createAsyncThunk(
//   "disposalEvent/GetDiscrepancyTable",
//   (data) => {
//     return data;
//   }
// );

const disposalEventSlice = createSlice({
  name: "disposalEvent",
  initialState,
  reducers: {
    setActiveDisposalPage: (state, action) => {
      state.activeDisposalEventQueryParams.page = action.payload;
    },
    setActiveDisposalPageSize: (state, action) => {
      state.activeDisposalEventQueryParams.page_size = action.payload;
    },
    setDisposalEventPage : (state,action) => {
      state.disposalEventQueryParams.page = action.payload;
    },
    setDisposalEventPageSize : (state,action) => {
      state.disposalEventQueryParams.page_size = action.payload;
    },
    setPageRequest: (state, action) => {
      state.pageRequest = action.payload;
    },
    setPageSizeRequest: (state, action) => {
      state.pageSizeRequest = action.payload;
    },
    setApprovalRequestStatus: (state, action) => {
      state.approvalRequestStatus = action.payload;
    },
    setPage: (state, action) => {
      state.page = action.payload;
    },
    setPageSize: (state, action) => {
      state.pageSize = action.payload;
    },
    setActivePage: (state, action) => {
      state.activePage = action.payload;
    },
    setActivePageSize: (state, action) => {
      state.activePageSize = action.payload;
    },
    setApprovalSetting: (state, action) => {
      state.approvalSetting = state.approvalSetting.map((approval) => {
        if (approval.ApproverNik === action.payload.ApproverNik) {
          return action.payload;
        } else {
          return approval;
        }
      });
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(generateDisposalSetting.fulfilled, (state, action) => {
        state.approvalSetting = action.payload.data.result;
        state.approvalSettingStatus = "done";
      })
      .addCase(generateDisposalSetting.pending, (state, action) => {
        state.approvalSetting.push(action.payload);
        state.approvalSettingStatus = "loading";
      })
      .addCase(createDisposalEvent.fulfilled, (state, action) => {
        if (
          !state.disposalEvent.find(
            (current) => current.ID === action.payload.data.ID
          ) &&
          action.payload.data.Type === "Disposal Event"
        ) {
          state.disposalEvent.unshift(action.payload.data);
        } else {
          state.disposalRingiEvent.unshift(action.payload.data);
        }

        if (action.payload.data.Status === "Active") {
          if (
            !state.activeDisposalEvent.find(
              (current) => current.ID === action.payload.data.ID
            ) &&
            action.payload.data.Type === "Disposal Event"
          ) {
            state.activeDisposalEvent.unshift(action.payload.data);
          } else {
            state.activeDisposalRingiEvent.unshift(action.payload.data);
          }
        }
        state.createDisposalStatus = "done";
      })
      .addCase(bulkCreateDisposalEventRingiRequest.pending, (state, action) => {
        state.bulkCreateDisposalRequest = "loading";
      })
      .addCase(
        bulkCreateDisposalEventRingiRequest.fulfilled,
        (state, action) => {
          // if (
          //   !state.disposalEvent.find(
          //     (current) => current.ID === action.payload.data.ID
          //   ) &&
          //   action.payload.data.Type === "Disposal Event"
          // ) {
          //   state.disposalEvent.unshift(action.payload.data);
          // } else {
          //   state.disposalRingiEvent.unshift(action.payload.data);
          // }

          // if (action.payload.data.Status === "Active") {
          //   if (
          //     !state.activeDisposalEvent.find(
          //       (current) => current.ID === action.payload.data.ID
          //     ) &&
          //     action.payload.data.Type === "Disposal Event"
          //   ) {
          //     state.activeDisposalEvent.unshift(action.payload.data);
          //   } else {
          //     state.activeDisposalRingiEvent.unshift(action.payload.data);
          //   }
          // }
          console.log(action.payload);
          state.bulkCreateDisposalRequest = "done";
        }
      )
      .addCase(createDisposalEvent.pending, (state, action) => {
        state.createDisposalStatus = "loading";
      })
      //get active disposal event
      .addCase(retrieveActiveDisposalEvent.fulfilled, (state, action) => {
        state.activeDisposalEvent = action.payload.data;
        state.retrieveActiveDisposalEventStatus = "done";
      })
      .addCase(retrieveActiveDisposalEvent.pending, (state, action) => {
        state.retrieveActiveDisposalEventStatus = "loading";
      })
      //end - get active disposal event
      // get all disposal event
      .addCase(retrieveDisposalEvent.fulfilled, (state, action) => {
        state.disposalEvent = action.payload.data;
        state.retrieveDisposalEventStatus = "done";
      })
      .addCase(retrieveDisposalEvent.pending, (state, action) => {
        state.retrieveDisposalEventStatus = "loading";
      })
      //end - get all disposal event
      // get active ringi event
      .addCase(retrieveActiveRingiEvent.fulfilled, (state, action) => {
        state.activeDisposalRingiEvent = action.payload.data;
        state.retrieveActiveDisposalEventStatus = "done";
      })
      .addCase(retrieveActiveRingiEvent.pending, (state, action) => {
        state.retrieveActiveDisposalEventStatus = "loading";
      })
      //end - get active ringi event
      // get all ringi event
      .addCase(retrieveRingiEvent.fulfilled, (state, action) => {
        state.disposalRingiEvent = action.payload.data;
        state.retrieveDisposalEventStatus = "done";
      })
      .addCase(retrieveRingiEvent.pending, (state, action) => {
        state.retrieveDisposalEventStatus = "loading";
      })
      //end - get all ringi event
      // // Delete case
      // .addCase(deleteDisposalEvent.fulfilled, (state, action) => {
      //   let index = state.findIndex(({ id }) => id === action.payload.id);
      //   state.splice(index, 1);
      //   state.deleteDisposalEventStatus = "done"
      // })
      // .addCase(deleteDisposalEvent.pending, (state, action) => {
      //   state.deleteDisposalEventStatus = "loading"
      // })
      // // Get one case
      .addCase(getDisposalEvent.fulfilled, (state, action) => {
        state.disposalEventDetail = action.payload.data;
        state.getDisposalEventStatus = "done";
      })
      .addCase(getDisposalEvent.pending, (state, action) => {
        state.getDisposalEventStatus = "loading";
      })
      .addCase(getDisposalEventApprover.fulfilled, (state, action) => {
        state.approvalSetting = action.payload.data;
        state.approvalSettingStatus = "done";
      })
      .addCase(getDisposalEventApprover.pending, (state, action) => {
        state.approvalSettingStatus = "loading";
      })
      .addCase(checkDisposalEventApprover.fulfilled, (state, action) => {
        if (
          action.payload.data.statusApprover & action.payload.data.statusRequest
        ) {
          state.canApprove = true;
        } else {
          state.canApprove = false;
        }
      })
      .addCase(settleEventRequest.fulfilled, (state, action) => {
        state.approvalSetting = state.approvalSetting.map((approval) => {
          if (approval.ID === action.payload.data.ID) {
            return action.payload.data;
          } else {
            return approval;
          }
        });
      })
      .addCase(getDisposalEventRequest.fulfilled, (state, action) => {
        const { data, status } = action.payload;
        state.approvalRequest = { data, status };
        state.approvalRequestStatus = "done";
      })
      .addCase(getDisposalEventRequest.pending, (state, action) => {
        state.approvalRequestStatus = "on progress";
      })
      .addCase(getItemsRequestDetail.fulfilled, (state, action) => {
        state.itemsRequestDetail = action.payload.data;
        state.itemsRequestDetailStatus = "done";
      })
      .addCase(getItemsRequestDetail.pending, (state, action) => {
        state.itemsRequestDetailStatus = "loading";
      });
    // .addCase(getDisposalEventTable.fulfilled, (state, action) => {
    //   state.partGetDisposalEventTable = action.payload;
    // })
  },
});

const { reducer } = disposalEventSlice;
export const {
  setActiveDisposalPage,
  setActiveDisposalPageSize,
  setDisposalEventPage,
  setDisposalEventPageSize,
  setPageRequest,
  setPageSizeRequest,
  setApprovalSetting,
  setApprovalRequestStatus,
  setPage,
  setPageSize,
  setActivePage,
  setActivePageSize
} = disposalEventSlice.actions;
export default reducer;
