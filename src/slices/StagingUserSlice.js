// import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// import StagingUserService from "../service/StagingUserService";

// const initialState = [];


// export const getNikApproval = createAsyncThunk(
//   "staginguser/getNikApproval",
//   async () => {
//     const res = await StagingUserService.getDataLogin();
//     return res.data;
//   }
// );


// const stagingUserSlice = createSlice({
//   name: "stagingusers",
//   initialState,
//   extraReducers: builder => {
//     builder.addCase(getNikApproval.fulfilled , (state, action) => {
//       console.log("staging:" ,state)
//       console.log("action:" ,action)

//       const index = state.findIndex(request => request.nik === action.payload.nik);
//       console.log(index)
//       if (index !== -1){
//         state[index] = {
//           ...state[index],
//           ...action.payload,
//         };
//       }else{
//         state.push(action.payload);
//       }
//     })
//   }
// });

// const { reducer } = stagingUserSlice;
// export default reducer;

import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import StagingUserService from "../service/StagingUserService";

const initialState = {
  approvalGetNik: [],
  approvalGetNikStatus: "loading"
};


export const getNikApproval = createAsyncThunk(
  "staginguser/getNikApproval",
  async () => {
    const res = await StagingUserService.getDataLogin();
    return res.data;
  }
);


const stagingUserSlice = createSlice({
  name: "stagingusers",
  initialState,
  extraReducers: builder => {
    builder.addCase(getNikApproval.fulfilled , (state, action) => {
      console.log("staging:" ,state)
      console.log("action:" ,action)

      const index = state.findIndex(request => request.nik === action.payload.nik);
      console.log(index)
      if (index !== -1){
        state[index] = {
          ...state[index],
          ...action.payload,
        };
      }else{
        state.approvalGetNik = action.payload;
        state.approvalGetNikStatus = "done"
      }
    })
    .addCase(getNikApproval.pending , (state, action) => {
      console.log("staging:" ,state)
      console.log("action:" ,action)

      const index = state.findIndex(request => request.nik === action.payload.nik);
      console.log(index)
      if (index !== -1){
        state[index] = {
          ...state[index],
          ...action.payload,
        };
      }else{
        state.approvalGetNik = [];
        state.approvalGetNikStatus = "loading"
      }
    })
  }
});

const { reducer } = stagingUserSlice;
export default reducer;