import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import ApprovalService from "../service/ApprovalService";

const initialState = {
  approver:[],
  status:"loading",
};


export const checkApprover = createAsyncThunk(
  "approval/checkApprover",
  async () => {
    const res = await ApprovalService.checkApprover();
    return res.data;
  }
);

export const retrieveApprover = createAsyncThunk(
  "approval/retrieveApprover",
  async () => {
    const res = await ApprovalService.getApprover()
    return res.data
  }
)


const approvalSlice = createSlice({
  name: "approvals",
  initialState,
  extraReducers: builder => {
    builder.addCase(checkApprover.fulfilled , (state, action) => {
      state.approver = action.payload;
      state.status = 'done'
    })
    .addCase(checkApprover.pending , (state, action) => {
      state.approver = [];
      state.status = 'loading'
    })
    .addCase(retrieveApprover.fulfilled, (state,action) => {
      state.approver = action.payload
      state.status = "done"
    })
    .addCase(retrieveApprover.pending , (state, action) => {
      state.approver = [];
      state.status = 'loading'
    })
  }
});

const { reducer } = approvalSlice;
export default reducer;