import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import RingiRequestService from "../service/RingiRequestService";

const initialState = {
  ringiRequest: [],
  ringiRequestStatus: "loading",
  createRingiRequest: [],
  createRingiRequestStatus: "loading",
  retrieveRingiRequest: [],
  retrieveRingiRequestStatus: "loading",
  deleteRingiRequest: [],
  deleteRingiRequestStatus: "loading",
  getRingiRequest: [],
  getRingiRequestStatus: "loading",
  partGetDiscrepancyTable: [],
  page: 1,
  pageSize: 5,
  activePage: 1,
  activePageSize: 5
};

export const createRingiRequest = createAsyncThunk(
  "ringirequest/create",
  async ( data ) => {
    const res = await RingiRequestService.createRequest(data);
    return res.data;
  }
);

export const retrieveRingiRequest = createAsyncThunk(
  "ringirequest/getall",
   (pageState) => {
    return RingiRequestService.getAllRequest(pageState.page, pageState.pageSize);
    // return {data: res.data.data, status: res.status};
  }
);


export const deleteRingiRequest = createAsyncThunk(
  "ringirequest/delete",
  async (data) => {
    await RingiRequestService.removeRequest(data);
    return { data };
  }
);

export const getRingiRequest = createAsyncThunk(
  "ringirequest/get",
  // async (data) => {
  //   const res = await DiscrepancyService.getRequest(data);
  //   return res.data;
  // }
  async (id) => {
    const res = await RingiRequestService.getRequest(id);
    console.log("res: ", res)
    return {data: res.data, status: res.status};
  }
);

export const getDiscrepancyTable = createAsyncThunk(
  "discrepancy/GetDiscrepancyTable",
  (data) => {
    return data;
  }
);


const ringiRequestSlice = createSlice({
  name: "ringirequest",
  initialState,
  reducers: {
    setPage: (state, action) => {
      state.page = action.payload
    },
    setPageSize: (state, action) => {
      state.pageSize = action.payload
    },
    setActivePage: (state, action) => {
      state.activePage = action.payload
    },
    setActivePageSize: (state, action) => {
      state.activePageSize = action.payload
    },
  },
  extraReducers: builder => {
    builder
    // Create case
    .addCase(createRingiRequest.fulfilled , (state, action) => {
      state.createRingiRequest.push(action.payload)
      state.createRingiRequestStatus= "done" 
    })
    .addCase(createRingiRequest.pending , (state, action) => {
      state.createRingiRequestStatus= "loading" 
    })
    // Get all case
    .addCase(retrieveRingiRequest.fulfilled, (state, action) => {
      state.retrieveRingiRequest = action.payload
      state.retrieveRingiRequestStatus = "done"
    })
    .addCase(retrieveRingiRequest.pending, (state, action) => {
      state.retrieveRingiRequestStatus = "loading"
    })
    // Delete case
    .addCase(deleteRingiRequest.fulfilled, (state, action) => {
      let index = state.findIndex(({ id }) => id === action.payload.id);
      state.splice(index, 1);
      state.deleteRingiRequestStatus= "done" 
    })
    .addCase(deleteRingiRequest.pending, (state, action) => {
      state.deleteRingiRequestStatus= "loading" 
    })
    // Get one case
    .addCase(getRingiRequest.fulfilled, (state, action) => {
      state.getRingiRequest = action.payload;
      state.getRingiRequestStatus = "done"
    })
    .addCase(getRingiRequest.pending, (state, action) => {
      state.getRingiRequestStatus= "loading" ; 
    })
    .addCase(getDiscrepancyTable.fulfilled, (state, action) => {
      state.partGetDiscrepancyTable = action.payload; 
  })
    
  }
});

const { reducer } = ringiRequestSlice;
export const { setPage, setPageSize, setActivePage, setActivePageSize } = ringiRequestSlice.actions
export default reducer;