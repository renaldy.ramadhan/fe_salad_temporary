import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import GlobalService from "../service/GlobalService";

const initialState = {
  fetchLocation: [],
  fetchLocationStatus : "loading",
  fetchPlant: [],
  fetchPlantStatus : "loading",
  fetchPurchasingGroup: [],
  fetchPurchasingGroupStatus : "loading",
  fetchReason: [],
  fetchReasonStatus: "loading",
  fetchSearchParts: [],
  fetchSearchPartsStatus: "loading",
  

};


export const fetchPlant = createAsyncThunk(
  "global/fetchPlant",
  async () => {
    const res = await GlobalService.fetchPlant();
    return res.data;
  }
);

export const fetchLocation = createAsyncThunk(
  "global/fetchLocation",
  async (id) => {
    const res = await GlobalService.fetchLocation(id);
    return res.data;
  }
);

export const fetchPurchasingGroup = createAsyncThunk(
  "global/fetchPurchasingGroup",
  async () => {
    const res = await GlobalService.fetchPurchasingGroup();
    return res.data;
  }
);

export const fetchReason = createAsyncThunk(
  "global/fetchReason",
  async ( type ) => {
    const res = await GlobalService.fetchReason(type);
    return res.data;
  }
);

export const fetchSearchParts = createAsyncThunk( // Searching parts
  "global/fetchSearchParts",
  async ( query ) => {
    const res = await GlobalService.fetchSearchParts(query.itemCode, query.group, query.location);
    return res.data;
  }
);


const globalSlice = createSlice({
  name: "global",
  initialState,
  extraReducers: builder => {
    builder
    .addCase(fetchLocation.fulfilled, (state, action) => {
      state.fetchLocation = action.payload;
      state.fetchLocationStatus = "done";
    })
    .addCase(fetchLocation.pending, (state, action) => {
      state.fetchLocation = [];
      state.fetchLocationStatus = "loading";
    })
    .addCase(fetchPlant.fulfilled, (state, action) => {
      state.fetchPlant = action.payload;
      state.fetchPlantStatus = "done";
    })
    .addCase(fetchPlant.pending, (state, action) => {
      state.fetchPlant = [];
      state.fetchPlantStatus = "loading";
    })
    .addCase(fetchPurchasingGroup.fulfilled, (state, action) => {
      state.fetchPurchasingGroup = action.payload;
      state.fetchPurchasingGroupStatus = "done";
    })
    .addCase(fetchPurchasingGroup.pending, (state, action) => {
      state.fetchPurchasingGroup = [];
      state.fetchPurchasingGroupStatus = "loading";
    })
    .addCase(fetchReason.fulfilled, (state, action) => {
      state.fetchReason = action.payload.sort((a, b) =>
        a.ReasonName.localeCompare(b.ReasonName)
      );
      state.fetchReasonStatus = "done";
    })
    .addCase(fetchReason.pending, (state, action) => {
      state.fetchReason = [];
      state.fetchReasonStatus = "loading";
    })
    .addCase(fetchSearchParts.fulfilled, (state, action) => {
      state.fetchSearchParts = action.payload;
      state.fetchSearchPartsStatus = "done";
    })
    .addCase(fetchSearchParts.pending, (state, action) => {
      state.fetchSearchParts = [];
      state.fetchSearchPartsStatus = "loading";
    })
    
  }
});

const { reducer } = globalSlice;
export default reducer;