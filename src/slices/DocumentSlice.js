import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import DocumentService from "../service/DocumentService";

const initialState = [];

export const getDocumentByRequest = createAsyncThunk(
  "document/getDocumentByRequest",
  async (data) => {
    const res = await DocumentService.getDocumentByRequest(data);
    return res.data;
  }
);

export const getDocumentByID = createAsyncThunk(
  "document/getDocumentByID",
  async (data) => {
    const res = await DocumentService.getDocumentByID(data);
    return res.data;
  }
);

export const downloadDocumentByID  = createAsyncThunk(
  "document/downloadDocumentByID ",
  async (data) => {
    const res = await DocumentService.downloadDocumentByID (data);
    return res.data;
  }
);

export const uploadAction = createAsyncThunk(
  "document/uploadAction",
  async ({file,data}) => {
    const res = await DocumentService.uploadAction(file, data);
    return res.data;
  }
);

const documentSlice = createSlice({
  name: "documents",
  initialState,
  extraReducers: builder => {
    builder.addCase(uploadAction.fulfilled, (state, action) => {
      state.push (action.payload);
    })
    .addCase(getDocumentByRequest.fulfilled, (state, action) => { 
      return [...action.payload];
    })
    .addCase(getDocumentByID.fulfilled, (state, action) => {
      return [...action.payload];
    })
    .addCase(downloadDocumentByID .fulfilled, (state, action) => {
      return [...action.payload];
    })
  }
  
});

const { reducer } = documentSlice;
export default reducer;