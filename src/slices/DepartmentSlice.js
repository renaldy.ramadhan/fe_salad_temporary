import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import DepartmentService from "../service/DepartmentService";

const initialState = {
    departmentData : [],
    status: "loading"
}

export const retrieveDepartment = createAsyncThunk(
    "department/retrieveDepartment",
    async () => {
        const res = await DepartmentService.getDepartment()
        return res.data
    }
)

const departmentSlices = createSlice({
    name: "departmentSlices",
    initialState,
    extraReducers: (builder) => {
        builder.addCase(retrieveDepartment.fulfilled,(state,action) => {
            state.departmentData = action.payload
            state.status = "done"
        })
        .addCase(retrieveDepartment.pending,(state,action) => {
            state.departmentData = []
            state.status = "loading"
        })
    }
})

const {reducer} = departmentSlices
export default reducer