import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import DiscrepancyService from "../service/DiscrepancyService";

const initialState = {
  createDiscrepancy: [],
  createDiscrepancyStatus: "loading",
  retrieveDiscrepancy: [],
  retrieveDiscrepancyStatus: "loading",
  deleteDiscrepancy: [],
  deleteDiscrepancyStatus: "loading",
  getDiscrepancy: [],
  getDiscrepancyStatus: "loading",
  partGetDiscrepancyTable: [],
  page: 1,
  pageSize: 5,
  activePage: 1,
  activePageSize: 5
};

export const createDiscrepancy = createAsyncThunk(
  "discrepancy/create",
  async (data) => {
    const res = await DiscrepancyService.createRequest(data);
    return res.data;
  }
);

export const retrieveDiscrepancy = createAsyncThunk(
  "discrepancy/getall",
  async (pageState) => {
    return DiscrepancyService.getAllRequest(pageState.page, pageState.pageSize);
  }
);


export const deleteDiscrepancy = createAsyncThunk(
  "discrepancy/delete",
  async (data) => {
    await DiscrepancyService.removeRequest(data);
    return { data };
  }
);

export const getDiscrepancy = createAsyncThunk(
  "discrepancy/get",
  // async (data) => {
  //   const res = await DiscrepancyService.getRequest(data);
  //   return res.data;
  // }
  async (id) => {
    const res = await DiscrepancyService.getRequest(id);
    console.log("res: ", res)
    return { data: res.data, status: res.status };
  }
);

export const getDiscrepancyTable = createAsyncThunk(
  "discrepancy/GetDiscrepancyTable",
  (data) => {
    return data;
  }
);


const discrepancySlice = createSlice({
  name: "discrepancy",
  initialState,
  reducers: {
    setPage: (state, action) => {
      state.page = action.payload
    },
    setPageSize: (state, action) => {
      state.pageSize = action.payload
    },
    setActivePage: (state, action) => {
      state.activePage = action.payload
    },
    setActivePageSize: (state, action) => {
      state.activePageSize = action.payload
    },
  },
  extraReducers: builder => {
    builder
      // Create case
      .addCase(createDiscrepancy.fulfilled, (state, action) => {
        state.createDiscrepancy.push(action.payload)
        state.createDiscrepancyStatus = "done"
      })
      .addCase(createDiscrepancy.pending, (state, action) => {
        state.createDiscrepancyStatus = "loading"
      })
      // Get all case
      .addCase(retrieveDiscrepancy.fulfilled, (state, action) => {
        console.log(action.payload)
        const { data, status } = action.payload
        state.retrieveDiscrepancy = { data: data, status };
        state.retrieveDiscrepancyStatus = "done";
      })
      .addCase(retrieveDiscrepancy.pending, (state, action) => {
        state.retrieveDiscrepancyStatus = "loading"
      })
      // Delete case
      .addCase(deleteDiscrepancy.fulfilled, (state, action) => {
        let index = state.findIndex(({ id }) => id === action.payload.id);
        state.splice(index, 1);
        state.deleteDiscrepancyStatus = "done"
      })
      .addCase(deleteDiscrepancy.pending, (state, action) => {
        state.deleteDiscrepancyStatus = "loading"
      })
      // Get one case
      .addCase(getDiscrepancy.fulfilled, (state, action) => {
        state.getDiscrepancy = action.payload;
        state.getDiscrepancyStatus = "done"
      })
      .addCase(getDiscrepancy.pending, (state, action) => {
        state.getDiscrepancyStatus = "loading";
      })
      .addCase(getDiscrepancyTable.fulfilled, (state, action) => {
        state.partGetDiscrepancyTable = action.payload;
      })

  }
});

const { reducer } = discrepancySlice;
export const { setPage, setPageSize, setActivePage, setActivePageSize } = discrepancySlice.actions
export default reducer;