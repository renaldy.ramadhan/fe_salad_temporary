import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    role : ""
}

const userSlice = createSlice({
    name: "users",
    initialState,
    reducers: {
        setRoleName: (state,action) => {
            state.role = action.payload
        }
    }
})

export const {setRoleName} = userSlice.actions
const {reducer} = userSlice
export default reducer