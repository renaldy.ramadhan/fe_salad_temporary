import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import PartRequestService from "../service/PartRequestService";

const initialState = {
  partRequest: [],
  partGetRequest: [],
  partGetRequestItemsData: [],
  partRequestStatus: "loading",
  partGetRequestStatus: "loading",
  partGetRequestTable: [],
  updateTableData: [],
  page: 1,
  pageSize: 5,
  activePage: 1,
  activePageSize: 5,
};

export const createPartRequest = createAsyncThunk(
  "partrequest/create",
  async ({ files, jsonData }) => {
    console.log(jsonData);
    const res = await PartRequestService.createRequest(files, jsonData);
    console.log(res.data);
    return res.data;
  }
);

export const retrievePartRequests = createAsyncThunk(
  "partrequest/getall",
  (pageState) => {
    return PartRequestService.getAllRequest(pageState.page, pageState.pageSize);
    // console.log("SLICE page", pageState.page, "page size:", pageState.pageSize);
    // return { data: res.data, status: res.status };
  }
);
export const editPartRequestData = createAsyncThunk(
  "partrequest/put",
  async (id, data) => {
    const res = await PartRequestService.editPartRequestData(id, data);
    return res.data;
  }
);

export const deletePartRequest = createAsyncThunk(
  "partrequest/delete",
  async (data) => {
    await PartRequestService.removeRequest(data);
    return { data };
  }
);

export const getPartRequest = createAsyncThunk(
  "partrequest/get",
  async (id) => {
    return PartRequestService.getRequest(id);
    // console.log("res: ", res)
    // return { data: res.data, status: res.status };
  }
);
export const forceUpdatePartRequest = createAsyncThunk(
  "partrequest/forceUpdatePartRequest",
  async (data) => {
    return data;
  }
);
export const UpdateTablePartRequest = createAsyncThunk(
  "partrequest/UpdatePartRequest",
  (data) => {
    return data;
  }
);

export const getPartRequestTable = createAsyncThunk(
  "partrequest/GetPartRequestTable",
  (data) => {
    return data;
  }
);

const partRequestSlice = createSlice({
  name: "partrequest",
  initialState,
  reducers: {
    setPage: (state, action) => {
      state.page = action.payload;
    },
    setPageSize: (state, action) => {
      state.pageSize = action.payload;
    },
    setActivePage: (state, action) => {
      state.activePage = action.payload;
    },
    setActivePageSize: (state, action) => {
      state.activePageSize = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(createPartRequest.fulfilled, (state, action) => {
        // state.partRequest.data.data.push({
        //   ...action.payload,
        //   id: action.payload.Id,
        // });
        state.partRequestStatus = "done";
      })
      .addCase(createPartRequest.pending, (state, action) => {
        state.partRequestStatus = "loading";
      })
      .addCase(retrievePartRequests.fulfilled, (state, action) => {
        console.warn(action.payload);
        const { data, status } = action.payload;
        state.partRequest = { data, status };
        state.partRequestStatus = "done";
      })
      .addCase(retrievePartRequests.pending, (state, action) => {
        state.partRequest = [];
        state.partRequestStatus = "loading";
      })
      .addCase(UpdateTablePartRequest.fulfilled, (state, action) => {
        state.updateTableData = action.payload;
      })
      // [updateTutorial.fulfilled]: (state, action) => {
      //   const index = state.findIndex(tutorial => tutorial.id === action.payload.id);
      //   state[index] = {
      //     ...state[index],
      //     ...action.payload,
      //   };
      // },
      .addCase(deletePartRequest.fulfilled, (state, action) => {
        let index = state.findIndex(({ id }) => id === action.payload.id);
        state.splice(index, 1);
        state.partRequestStatus = "done";
      })
      .addCase(deletePartRequest.pending, (state, action) => {
        state.partRequestStatus = "loading";
      })
      // [deleteAllTutorials.fulfilled]: (state, action) => {
      //   return [];
      // },
      .addCase(getPartRequest.fulfilled, (state, action) => {
        state.partGetRequest = action.payload;
        // state.partGetRequestItemsData = action.payload.ItemsData;
        state.partGetRequestStatus = "done";
      })
      .addCase(getPartRequest.pending, (state, action) => {
        state.partGetRequestStatus = "loading";
      })
      .addCase(getPartRequestTable.fulfilled, (state, action) => {
        state.partGetRequestTable = action.payload;
      });
  },
});
const { reducer } = partRequestSlice;
export const { setPage, setPageSize, setActivePage, setActivePageSize } =
  partRequestSlice.actions;
export default reducer;
