import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import ActiveApprovalService from "../service/ActiveApprovalService";

const initialState = {
  activeApprovalGetAll: [],
  activeApprovalGetAllStatus: "loading",
  activeApprovalGetRequestId: [],
  activeApprovalGetRequestIdStatus: "loading",
  activeApprovalGetRequestId2Status: [],
  activeApprovalGetRequestId2: "loading",
  activeApprovalGetNikToken: [],
  activeApprovalGetNikTokenStatus: "Loading",
  activeApprovalGetNikAssigned: [],
  activeApprovalGetNikAssignedStatus: "loading",
  activeApprovalGetNikAssignedDiscrepancy: [],
  activeApprovalGetNikAssignedStatusDiscrepancy: "loading",
  activeApprovalGetId: [],
  activeApprovalGetIdStatus: "loading",
  activeApprovalStatusGetRequestId: [],
  activeApprovalStatusGetRequestIdStatus: "loading",
  approvalAction: [],
  approvalActionStatus: "loading"
};


export const getAllActiveApproval = createAsyncThunk(
  "approval/getAllActiveApproval",
  async () => {
    const res = await ActiveApprovalService.getAllActiveApproval();
    return res.data;
  }
);

export const getActiveApprovalByRequestId = createAsyncThunk(
  "approval/getActiveApprovalByRequestId",
  async (id) => {
    const res = await ActiveApprovalService.getActiveApprovalByRequestId(id);
    console.log("res: ", res)
    return { data: res.data, status: res.status };
  }
);

export const getActiveApprovalByRequestId2 = createAsyncThunk(
  "approval/getActiveApprovalByRequestId2",
  async (id) => {
    const res = await ActiveApprovalService.getActiveApprovalByRequestId(id);
    console.log("res: ", res)
    return { data: res.data, status: res.status };
  }
);

export const getActiveApprovalByNikToken = createAsyncThunk(
  "approval/getActiveApprovalByNikToken",
  async () => {
    const res = await ActiveApprovalService.getActiveApprovalByNikToken();
    return res.data;
  }
);

export const getActiveApprovalByNikAndAssigned = createAsyncThunk(
  "approval/getActiveApprovalByNikAndAssigned",
  (pageState) => {
    return ActiveApprovalService.getActiveApprovalByNikAndAssigned(pageState.page, pageState.pageSize);
  }
);

export const getActiveApprovalByNikAndAssignedDiscrepancy = createAsyncThunk(
  "approval/getActiveApprovalByNikAndAssignedDiscrepancy",
  (pageState) => {
    return ActiveApprovalService.getActiveApprovalByNikAndAssignedDiscrepancy(pageState.page, pageState.pageSize);
  }
);

export const getActiveApprovalById = createAsyncThunk(
  "approval/getActiveApprovalById",
  async (id) => {
    const res = await ActiveApprovalService.getActiveApprovalByRequestId(id);
    console.log("res: ", res)
    return { data: res.data, status: res.status };
  }
);

export const getApprovalStatusByRequestId = createAsyncThunk(
  "approval/getApprovalStatusById",
  async (id) => {
    const res = await ActiveApprovalService.getApprovalStatusByRequestId(id);
    console.log("res: ", res)
    return { data: res.data, status: res.status };
  }
);

export const approvalAction = createAsyncThunk(
  "approval/approvalAction",
  async (data) => {
    const res = await ActiveApprovalService.approvalAction(data);
    return res.data;
  }
);

const activeApprovalSlice = createSlice({
  name: "activeapproval",
  initialState,
  extraReducers: builder => {
    builder.addCase(getAllActiveApproval.fulfilled, (state, action) => {
      state.activeApprovalGetAll = action.payload;
      state.activeApprovalGetAllStatus = "done";
    })
      .addCase(getAllActiveApproval.pending, (state, action) => {
        state.activeApprovalGetAll = [];
        state.activeApprovalGetAllStatus = "loading";
      })
      .addCase(getActiveApprovalByRequestId.fulfilled, (state, action) => {
        state.activeApprovalGetRequestId = action.payload;
        state.activeApprovalGetRequestIdStatus = "done";
      })
      .addCase(getActiveApprovalByRequestId.pending, (state, action) => {
        state.activeApprovalGetRequestId = [];
        state.activeApprovalGetRequestIdStatus = "loading";
      })
      .addCase(getActiveApprovalByRequestId2.fulfilled, (state, action) => {
        state.activeApprovalGetRequestId2 = action.payload;
        state.activeApprovalGetRequestId2Status = "done";
      })
      .addCase(getActiveApprovalByRequestId2.pending, (state, action) => {
        state.activeApprovalGetRequestId2 = [];
        state.activeApprovalGetRequestId2Status = "loading";
      })
      .addCase(getActiveApprovalByNikToken.fulfilled, (state, action) => {
        state.activeApprovalGetNikToken = action.payload;
        state.activeApprovalGetNikTokenStatus = "done";
      })
      .addCase(getActiveApprovalByNikToken.pending, (state, action) => {
        state.activeApprovalGetNikToken = [];
        state.activeApprovalGetNikTokenStatus = "loading";
      })
      .addCase(getActiveApprovalByNikAndAssigned.fulfilled, (state, action) => {
        state.activeApprovalGetNikAssigned = action.payload.data;
        state.activeApprovalGetNikAssignedStatus = "done";
      })
      .addCase(getActiveApprovalByNikAndAssigned.pending, (state, action) => {
        state.activeApprovalGetNikAssigned = [];
        state.activeApprovalGetNikAssignedStatus = "loading";
      })
      .addCase(getActiveApprovalByNikAndAssignedDiscrepancy.fulfilled, (state, action) => {
        console.log(action.payload)
        state.activeApprovalGetNikAssignedDiscrepancy = action.payload.data;
        state.activeApprovalGetNikAssignedStatusDiscrepancy = "done";
      })
      .addCase(getActiveApprovalByNikAndAssignedDiscrepancy.pending, (state, action) => {
        state.activeApprovalGetNikAssignedDiscrepancy = [];
        state.activeApprovalGetNikAssignedStatusDiscrepancy = "loading";
      })
      .addCase(getActiveApprovalById.fulfilled, (state, action) => {
        state.activeApprovalGetId = action.payload;
        state.activeApprovalGetIdStatus = "done";
      })
      .addCase(getActiveApprovalById.pending, (state, action) => {
        state.activeApprovalGetId = [];
        state.activeApprovalGetIdStatus = "loading";
      })
      .addCase(getApprovalStatusByRequestId.fulfilled, (state, action) => {
        state.activeApprovalStatusGetRequestId = action.payload;
        state.activeApprovalStatusGetRequestIdStatus = "done";
      })
      .addCase(getApprovalStatusByRequestId.pending, (state, action) => {
        state.activeApprovalStatusGetRequestId = [];
        state.activeApprovalStatusGetRequestIdStatus = "loading";
      })
      .addCase(approvalAction.fulfilled, (state, action) => {
        const index = state.activeApprovalGetAll.findIndex(item => item.id === action.payload.id);

        if (index !== -1) {
          state.activeApprovalGetAll[index] = action.payload;
        }
      })


  }
});

const { reducer } = activeApprovalSlice;
export default reducer;