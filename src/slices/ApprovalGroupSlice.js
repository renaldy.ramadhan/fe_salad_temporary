import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import ApprovalGroupService from "../service/ApprovalGroupService";

const initialState = {
  approver:[],
  status: "loading"
};


export const checkApprover = createAsyncThunk(
  "approvalgroup/checkApprover",
  async () => {
    const res = await ApprovalGroupService.checkApprover();
    return res.data;
  }
);

export const retrieveApproverGroup = createAsyncThunk(
  "approvalgroup/retrieveApproverGroup",
  async () => {
    const res = await ApprovalGroupService.getApprover()
    return res.data
  }
)


const approvalGroupSlice = createSlice({
  name: "approvalgroups",
  initialState,
  extraReducers: builder => {
    builder.addCase(checkApprover.fulfilled , (state, action) => {
      state.approver = action.payload;
      state.status = "done"
    })
    .addCase(retrieveApproverGroup.fulfilled,(state,action) => {
      state.approver = action.payload
      state.status = "done"
    })
  }
});

const { reducer } = approvalGroupSlice;
export default reducer;