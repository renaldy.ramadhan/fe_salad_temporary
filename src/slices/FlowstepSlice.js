import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import FlowstepService from "../service/FlowstepService";

const initialState = {
    flowstepData : [],
    status : "loading"
}

export const retrieveFlowstep = createAsyncThunk(
    "flowstep/retrieveFlowstep",
    async () => {
        const res = await FlowstepService.getFlowstep()
        return res.data
    }
)

const flowstepSlices = createSlice({
    name: "flowstepSlices",
    initialState,
    extraReducers: (builder) => {
        builder.addCase(retrieveFlowstep.fulfilled, (state,action) => {
            state.flowstepData = action.payload
            state.status = "done"
        })
        .addCase(retrieveFlowstep.pending,(state,action) => {
            state.flowstepData = []
            state.status = "loading"
        })
    }
})

const {reducer} = flowstepSlices
export default reducer