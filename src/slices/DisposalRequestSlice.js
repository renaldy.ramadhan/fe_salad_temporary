import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import DisposalRequestService from "../service/DisposalRequestService";

const initialState = {
  disposalRequest: [],
  disposalRequestStatus: "loading",
  disposalRequestData: [],
  disposalRequestDataStatus: "loading",
  page: 1,
  pageSize: 5,
  activePage: 1,
  activePageSize: 5,
};

export const createDisposalRequest = createAsyncThunk(
  "partrequest/create",
  async ({ files, jsonData }) => {
    console.log(jsonData);
    const res = await DisposalRequestService.createRequest(files, jsonData);
    console.log(res.data);
    return res.data;
  }
);

export const retrieveDisposalRequest = createAsyncThunk(
  "disposalrequest/getall",
  (pageState) => {
    return DisposalRequestService.getAllRequestDisposal(
      pageState.page,
      pageState.pageSize
    );
  }
);

export const retrieveDisposalRequestData = createAsyncThunk(
  "disposalrequest/get",
  (pageState) => {
    return DisposalRequestService.getRequestDisposal(
      pageState.page,
      pageState.pageSize
    );
  }
);

const disposalRequestSlice = createSlice({
  name: "disposalRequest",
  initialState,
  reducers: {
    setPage: (state, action) => {
      state.page = action.payload;
    },
    setPageSize: (state, action) => {
      state.pageSize = action.payload;
    },
    setActivePage: (state, action) => {
      state.activePage = action.payload;
    },
    setActivePageSize: (state, action) => {
      state.activePageSize = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(createDisposalRequest.fulfilled, (state, action) => {
        state.partRequestStatus = "done";
      })
      .addCase(createDisposalRequest.pending, (state, action) => {
        state.partRequestStatus = "loading";
      })
      .addCase(retrieveDisposalRequest.fulfilled, (state, action) => {
        console.warn(action.payload);
        const { data, status } = action.payload;
        state.disposalRequest = { data, status };
        console.log("state disposal request data:", state.disposalRequestData);
        state.disposalRequestStatus = "done";
      })
      .addCase(retrieveDisposalRequest.pending, (state, action) => {
        // state.disposalRequest = [];
        state.disposalRequestStatus = "loading";
      })
      .addCase(retrieveDisposalRequestData.fulfilled, (state, action) => {
        console.warn(action.payload);
        const { data, status } = action.payload;
        state.disposalRequestData = { data, status };
        console.log("state disposal request data:", state.disposalRequestData);
        state.disposalRequestDataStatus = "done";
      })
      .addCase(retrieveDisposalRequestData.pending, (state, action) => {
        // state.disposalRequestData = [];
        state.disposalRequestDataStatus = "loading";
      });
  },
});

const { reducer } = disposalRequestSlice;
export const { setPage, setPageSize, setActivePage, setActivePageSize } =
  disposalRequestSlice.actions;
export default reducer;
