import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import UpdateDataService from "../service/UpdateDataService";

const initialState = [];


// export const getAllActiveApproval = createAsyncThunk(
//   "approval/getAllActiveApproval",
//   async (  ) => {
//     const res = await ActiveApprovalService.getAllActiveApproval();
//     return res.data;
//   }
// );

// export const getActiveApprovalByRequestId = createAsyncThunk(
//   "approval/getActiveApprovalByRequestId",
//   async (data) => { //ID Request
//     const res = await ActiveApprovalService.getActiveApprovalByRequestId(data);
//     return res.data;
//   }
// );

export const editPartRequestData = createAsyncThunk(
    "partrequest/put",
    async (data) => {
      const res = await UpdateDataService.updateData(data);
      return res.data;
    }
  );
  



const editPartRequestDataSlice = createSlice({
  name: "editPartRequest",
  initialState,
  extraReducers: builder => {
    builder.addCase(editPartRequestData.fulfilled , (state, action) => {
      state.push(action.payload);
    })
  }
});

const { reducer } = editPartRequestDataSlice;
export default reducer;