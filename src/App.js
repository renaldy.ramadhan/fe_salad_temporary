import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import PartDashboard from './pages/Part/Dashboard/PartDashboard';
import PartRequestDashboard from './pages/PartRequest/Dashboard/PartRequestDashboard';
import CreatePartRequest from './pages/PartRequest/Create/CreatePartRequest';
import EditPartRequestWHC from './pages/PartRequest/Edit/EditPartRequestWHC';
import ViewPartRequest from './pages/PartRequest/View/ViewPartRequest';
import DiscrepancyStockDashboard from './pages/DiscrepancyStock/Dashboard/DiscrepancyStockDashboard';
import CreateDiscrepancyStock from './pages/DiscrepancyStock/Create/CreateDiscrepancyStock';
import DisposalEventDashboard from './pages/Disposal/Event/Dashboard/DisposalEventDashboard';
import CreateDisposalEvent from './pages/Disposal/Event/Create/CreateDisposalEvent';
import ViewDisposalEvent from './pages/Disposal/Event/View/ViewDisposalEvent';
import DisposalRequestDashboard from './pages/Disposal/Request/Dashboard/DisposalRequestDashboard';
import CreateDisposalRequestSystem from './pages/Disposal/Request/Create/CreateDisposalRequestSystem';
import CreateDisposalRequestNonSystem from './pages/Disposal/Request/Create/CreateDisposalRequestNonSystem';
import ViewDisposalRequestSystem from './pages/Disposal/Request/View/ViewDisposalRequestSystem';
import ViewDisposalRequestNonSystem from './pages/Disposal/Request/View/ViewDisposalRequestNonSystem';
import ViewDisposalRequestRingi from './pages/Disposal/Request/View/ViewDisposalRequestRingi';
import DashboardRingi from './pages/Ringi/Event/Dashboard/RingiEventDashboard';
import RingiUpload from './pages/Ringi/RingiUpload';
import EditDisposalEvent from './pages/Disposal/Event/Edit/EditDisposalEvent';
import {ApprovalGroup} from './pages/Master Data MGT/Approval Group/ApprovalGroup';
import CreateRingiEvent from './pages/Ringi/Event/Create/CreateRingiEvent';
import ViewRingiEvent from './pages/Ringi/Event/View/ViewRingiEvent';
import EditRingiEvent from './pages/Ringi/Event/Edit/EditRingiEvent';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import ViewPartRequestApprover from './pages/PartRequest/View/ViewPartRequestApprover';
import DashboardMUI from './pages/HomeDashboard/DashboardMUI';
import ViewDiscrepancyStock from './pages/DiscrepancyStock/View/ViewDiscrepancyStock';
import ViewDiscrepancyStockApprover from './pages/DiscrepancyStock/View/ViewDiscrepancyStockApprover';
import WorkflowList from './pages/Master Data MGT/Workflow/WorkflowList';
import { WorkflowCreate } from './pages/Master Data MGT/Workflow/WorkflowCreate';
import { WorkflowEdit } from './pages/Master Data MGT/Workflow/WorkflowEdit';
import { DepartmentList } from './pages/Master Data MGT/Department/DepartmentList';
import { DepartmentCreate } from './pages/Master Data MGT/Department/DepartmentCreate';
import { DepartmentEdit } from './pages/Master Data MGT/Department/DepartmentEdit';
import { FlowstepList } from './pages/Master Data MGT/Flowstep/FlowstepList';
import { FlowstepCreate } from './pages/Master Data MGT/Flowstep/FlowstepCreate';
import { FlowstepEdit } from './pages/Master Data MGT/Flowstep/FlowstepEdit';
import { ApproverGroupCreate } from './pages/Master Data MGT/Approval Group/ApproverGroupCreate';
import { ApproverGroupEdit } from './pages/Master Data MGT/Approval Group/ApproverGroupEdit';
import { ApproverList } from './pages/Master Data MGT/Approver List/ApproverList';
import { ApproverCreate } from './pages/Master Data MGT/Approver List/ApproverCreate';
import { ApproverEdit } from './pages/Master Data MGT/Approver List/ApproverEdit';
import RingiEventDashboard from './pages/Ringi/Event/Dashboard/RingiEventDashboard';
import RingiRequestDashboard from './pages/Ringi/Request/RingiRequestDashboard';
import CreateRingiRequest from './pages/Ringi/Request/Create/CreateRingiRequest';
import { PartRequestPDF } from './pages/PartRequest/View/PartRequestPDF';
import ViewRingiRequest from './pages/Ringi/Request/View/ViewRingiRequest';
import ViewDisposalRequestApprover from './pages/Disposal/Request/View/ViewDisposalRequestApprover';


function App() {
  return (
    <div className="App">
        <Router basename="/salad/">
          {/* <KeycloakProvider> */}
            <Routes>
              <Route path='/' element={<DashboardMUI/>}/>
              <Route path='/part' element={<PartDashboard/>}/>
              <Route path='/partrequest' element={<PartRequestDashboard/>}/>
              <Route path='/partrequest/create' element={<CreatePartRequest/>}/>
              <Route path='/partrequest/view/:id' element={<ViewPartRequest/>}/>
              <Route path='/partrequest/view/approver/:id' element={<ViewPartRequestApprover/>}/>
              <Route path='/partrequest/document/:id' element={<PartRequestPDF/>}/>
              <Route path='/partrequest/edit' element={<EditPartRequestWHC/>}/>
              <Route path='/discrepancy' element={<DiscrepancyStockDashboard/>}/>
              <Route path='/discrepancy/create' element={<CreateDiscrepancyStock/>}/>
              <Route path='/discrepancy/view/:id' element={<ViewDiscrepancyStock/>}/>
              <Route path='/discrepancy/view/approver/:id' element={<ViewDiscrepancyStockApprover/>}/>
              <Route path='/disposal/general/event' element={<DisposalEventDashboard/>}/>
              <Route path='/disposal/general/event/create' element={<CreateDisposalEvent/>}/>
              <Route path='/disposal/general/event/edit' element={<EditDisposalEvent/>}/>
              <Route path='/disposal/general/event/view/:id' element={<ViewDisposalEvent/>}/>
              <Route path='/disposal/general/request' element={<DisposalRequestDashboard/>}/>
              <Route path='/disposal/general/request/system/create' element={<CreateDisposalRequestSystem/>}/>
              <Route path='/disposal/general/request/nonsystem/create' element={<CreateDisposalRequestNonSystem/>}/>
              <Route path='/disposal/general/request/view/system' element={<ViewDisposalRequestSystem/>}/>
              <Route path='/disposal/general/request/view/system/:id' element={<ViewDisposalRequestSystem/>}/>
              <Route path='/disposal/general/request/view/nonsystem/:id' element={<ViewDisposalRequestNonSystem/>}/>
              <Route path='/disposal/general/request/view/approver/:id' element={<ViewDisposalRequestApprover/>}/>
              <Route path='/disposal/general/request/view/ringi/:id' element={<ViewDisposalRequestRingi/>}/>
              <Route path='/ringi' element={<RingiEventDashboard/>}/>
              <Route path='/ringi/event/create' element={<CreateRingiEvent/>}/>
              <Route path='/ringi/event/view/:id' element={<ViewRingiEvent/>}/>
              <Route path='/ringi/event/edit' element={<EditRingiEvent/>}/>
              <Route path='/ringi/request/create/' element={<CreateRingiRequest/>}/>
              <Route path='/ringi/request/' element={<RingiRequestDashboard/>}/>
              <Route path='/ringi/request/view/:id' element={<ViewRingiRequest/>}/>
              <Route path='/ringi/upload' element={<RingiUpload/>}/>
              <Route path='/masterdata/workflow' element={<WorkflowList/>}/>
              <Route path='/masterdata/workflow/create' element={<WorkflowCreate/>}/>
              <Route path='/masterdata/workflow/edit/:id' element={<WorkflowEdit/>}/>
              <Route path='/masterdata/department' element={<DepartmentList/>} />
              <Route path='/masterdata/department/create' element={<DepartmentCreate/>} />
              <Route path='/masterdata/department/edit/:id' element={<DepartmentEdit/>}/>
              <Route path='/masterdata/flowstep' element={<FlowstepList/>} />
              <Route path='/masterdata/flowstep/create' element={<FlowstepCreate/>} />
              <Route path='/masterdata/flowstep/edit/:id' element={<FlowstepEdit/>}/>
              <Route path='/masterdata/approvalgroup' element={<ApprovalGroup/>}/>
              <Route path='/masterdata/approvalgroup/create' element={<ApproverGroupCreate/>}/>
              <Route path='/masterdata/approvalgroup/edit/:id' element={<ApproverGroupEdit/>}/>
              <Route path='/masterdata/approval' element={<ApproverList/>}/>
              <Route path='/masterdata/approval/create' element={<ApproverCreate/>}/>
              <Route path='/masterdata/approval/edit/:id' element={<ApproverEdit/>} />
              <Route path="/view/:id" element={<ViewDisposalEvent />} />
            </Routes>
          {/* </KeycloakProvider> */}
        </Router>
    </div>
  );
}

export default App;
