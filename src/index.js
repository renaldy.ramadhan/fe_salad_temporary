import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import './css/general/custom-style.css'
import App from './App';
import reportWebVitals from './reportWebVitals';
import { KeycloakProvider } from './contexts/KeycloakProvider';
import { Provider } from 'react-redux';
import store from './store/store';
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" integrity="sha512–8bHTC73gkZ7rZ7vpqUQThUDhqcNFyYi2xgDgPDHc+GXVGHXq+xPjynxIopALmOPqzo9JZj0k6OqqewdGO3EsrQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(

  <KeycloakProvider>
    <React.StrictMode> 
      <Provider store={store}>
        <App />
      </Provider>
    </React.StrictMode>
  </KeycloakProvider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
