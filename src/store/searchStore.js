// store.js
import { create } from 'zustand';
import axios from 'axios';

const useSearchStore = create((set, get) => ({
  tableData: [],
  parts: [],
  plants: [],
  locations: [],
  purchasingGroups: [],
  reasons: [],
  selectedItemIndex: null,
  loading: true,
  error: null,

  // create part req modal
  addToTable: (newItem) => {
    set((state) => {
      // Check for duplicate
      const existingItemIndex = state.tableData.findIndex((item) => item.ItemCode === newItem.ItemCode);

      if (existingItemIndex !== -1) {
        // Duplicate found, update RequestQuantity
        const updatedDataTable = [...state.tableData];
        updatedDataTable[existingItemIndex].RequestQuantity += parseInt(newItem.RequestQuantity);
        updatedDataTable[existingItemIndex].Remark = newItem.Remark;
        return { tableData: updatedDataTable };
      } else {
        // No duplicate found, add new item
        return { tableData: [...state.tableData, newItem] };
      }
    });
  },
  
  resetTableData: () => {
    set({ tableData: [] });
  },

  // create part req table
  editTableData: (index, quantity, remark) => {
    set((state) => {
      const updatedDataTable = [...state.tableData];
      updatedDataTable[index].RequestQuantity = parseInt(quantity);
      updatedDataTable[index].Remark = remark;
      return { tableData: updatedDataTable };
    });
  },

  // create part req table
  deleteTableData: (index) => {
    const updatedData = [...get().tableData];
    updatedData.splice(index, 1);
    set({ tableData: updatedData });
    console.log(index)
    console.log(updatedData)
  },
}));

export default useSearchStore;