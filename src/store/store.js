import { configureStore } from '@reduxjs/toolkit'
import partRequestReducer from '../slices/PartRequstSlice';
import discrepancyReducer from '../slices/DiscrepancySlice';
import globalReducer from '../slices/GlobalSlice';
import activeApprovalReducer from '../slices/ActiveApprovalSlice';
import stagingUserReducer from '../slices/StagingUserSlice';
import documentReducer from '../slices/DocumentSlice';
import approvalGroupReducer from '../slices/ApprovalGroupSlice';
import approvalReducer from '../slices/ApprovalSlice';
import workflowReducer from '../slices/WorkflowSlice'
import departmentReducer from '../slices/DepartmentSlice'
import flowstepReducer from '../slices/FlowstepSlice'
import disposalEventReducer from '../slices/DisposalEventSlice'
import ringiEventReducer from '../slices/RingiEventSlice'
import ringiRequestReducer from '../slices/RingiRequestSlice'
import disposalRequestReducer from '../slices/DisposalRequestSlice'


const reducer = {
  partrequests: partRequestReducer,
  discrepancy: discrepancyReducer,
  globals: globalReducer,
  activeapprovals: activeApprovalReducer,
  stagingusers: stagingUserReducer,
  documents: documentReducer,
  approvalgroups: approvalGroupReducer,
  approvals: approvalReducer,
  workflows: workflowReducer,
  departments: departmentReducer,
  flowsteps: flowstepReducer,
  disposalEvent: disposalEventReducer,
  ringiEvent : ringiEventReducer,
  ringirequests : ringiRequestReducer,
  disposalRequest : disposalRequestReducer
}

const store = configureStore({
  reducer: reducer,
  devTools: true,
})

export default store;