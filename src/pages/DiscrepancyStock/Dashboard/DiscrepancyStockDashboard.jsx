import { useNavigate } from "react-router-dom";

import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from "@mui/material/Typography";
import Button from '@mui/material/Button';
import Layout from "../../Layout/Layout";
import ActiveApproverTablesDiscrepancy from "../../../components/tables/discrepancy/ActiveApproverTablesDiscrepancy";
import PartDiscrepancyActivity from "../../../components/tables/discrepancy/PartDiscrepancyActivity";

const DiscrepancyStockDashboard = () => {
  const navigate = useNavigate();

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 'inherit',
          }}
        >
          <Typography variant="h5" component="div" sx={{ mb: 2 }}>
            Part Discrepancy Activity (Need Approval)
          </Typography>
          <ActiveApproverTablesDiscrepancy />
        </Paper>
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 'inherit',
          }}
        >
          <Grid>
            <Paper
              sx={{
                p: 2,
                alignContent: 'center',
                display: 'flex',
                flexDirection: 'row',
                height: 'inherit',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <Typography variant="h5" component="div" sx={{ justifyContent: 'center' }}>
                Part Discrepancy Activity
              </Typography>
              <Button color="primary" style={{ backgroundColor: "#17479d" }} variant="contained" onClick={() => navigate('/discrepancy/create', { state: { type: 'Discrepancy' } })}>
                Create
              </Button>
            </Paper>
          </Grid>
          <PartDiscrepancyActivity />
        </Paper>
      </Grid>

    </Layout>
  );
};

export default DiscrepancyStockDashboard;