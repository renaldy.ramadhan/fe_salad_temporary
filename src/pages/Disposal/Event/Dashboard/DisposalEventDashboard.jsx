import React, { useState, useEffect, useContext, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import VisibilityIcon from "@mui/icons-material/Visibility";
import Layout from "../../../Layout/Layout";
import ActiveApproverTables from "../../../../components/tables/Approver/ActiveApproverTables";
import DisposalEventTable from "../../../../components/tables/disposal/event/DisposalEventTable";

import {
  retrieveActiveDisposalEvent,
  retrieveDisposalEvent,
} from "../../../../slices/DisposalEventSlice";
import { buildQueryParams } from "../../../../util/global";
import ActiveDisposalEventTable from "../../../../components/tables/disposal/event/ActiveDisposalEventTable";

const DisposalEventDashboard = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    retrieveActiveDisposalEventStatus,
    activeDisposalEvent,
    retrieveDisposalEventStatus,
    disposalEvent,
    activeDisposalEventQueryParams,
  } = useSelector((state) => state.disposalEvent);

  const [activeEventRows, setActiveEventRows] = useState([]);
  const [eventRows, setEventRows] = useState([]);

  const initFetch = useCallback(() => {
    // if (activeDisposalEvent.length < 1) {
    //   dispatch(retrieveActiveDisposalEvent());
    // }
    // if (disposalEvent.length < 1) {
    //   dispatch(retrieveDisposalEvent());
    // }
  }, [dispatch, disposalEvent]);

  useEffect(() => {
    initFetch();
    setEventRows(
      disposalEvent?.data?.map((event) => {
        return { ...event, id: event.ID };
      })
    );
  }, [
    initFetch,
    activeDisposalEvent,
    disposalEvent,
    activeDisposalEventQueryParams,
  ]);

  const formatDate = (dateString) => {
    if (dateString === null) return "";
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0"); // Months are zero-based
    const day = String(date.getDate()).padStart(2, "0");

    return `${year}-${month}-${day}`;
  };

  const handleClick = (id) => {
    let selectedPartRequest;
    // if (partRequests.length > 0) {
    //   selectedPartRequest = partRequests.data.data.find(
    //     (data) => data.IdRequest === id
    //   );
    // } else if (activeApprovals.length > 0) {
    //   selectedPartRequest = activeApprovals.find(
    //     (data) => data.Request.ID === id
    //   );
    // }

    navigate(`/disposal/general/event/view/` + id, {
      // state: { data: selectedPartRequest, reqId: id },
    });
  };

  const columns = [
    {
      field: "ID",
      headerName: "Id",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      filterable: false,
    },
    {
      field: "DisposalCode",
      headerName: "Reference",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
    },

    {
      field: "Name",
      headerName: "Name",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "ExecutionDate",
      headerName: "Date",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "StartDate",
      headerName: "Period Start",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "EndDate",
      headerName: "Period End",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "CreatedBy",
      headerName: "Created By",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "CreatedAt",
      headerName: "Created Date",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 60,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <Typography variant="h5" component="div" sx={{ mb: 2 }}>
            Disposal Event (Active)
          </Typography>
          <ActiveDisposalEventTable
            loading={retrieveActiveDisposalEventStatus === "loading"}
            parentColumn={columns}
          />
        </Paper>
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <Grid>
            <Paper
              sx={{
                p: 2,
                alignContent: "center",
                display: "flex",
                flexDirection: "row",
                height: "inherit",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography
                variant="h5"
                component="div"
                sx={{ justifyContent: "center" }}
              >
                Disposal Event
              </Typography>
              <Button
                color="primary"
                style={{ backgroundColor: "#17479d" }}
                variant="contained"
                onClick={() =>
                  navigate("/disposal/general/event/create", {
                    state: { type: "Request" },
                  })
                }
              >
                Create
              </Button>
            </Paper>
          </Grid>
          <DisposalEventTable
            loading={retrieveDisposalEventStatus === "loading"}
            parentColumn={columns}
            parentRow={eventRows}
          />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default DisposalEventDashboard;
