import React, { useEffect, useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  DatePicker,
  START_DATE,
  END_DATE,
  DateRangePicker,
} from "react-nice-dates";
import { enGB } from "date-fns/locale";
import "../../../../css/date-picker/date-picker.css";

import Layout from "../../../Layout/Layout";
import Button from "../../../../components/buttons/Button";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box, useTheme } from "@mui/material";
import ApproverDisposalEventTable from "../../../../components/tables/Approver/ApproverDisposalEventTable.jsx";

import {
  generateDisposalSetting, createDisposalEvent
} from "../../../../slices/DisposalEventSlice.js";
import { KeycloakContext } from "../../../../contexts/KeycloakProvider.js";

const CreateDisposalEvent = () => {
  const theme = useTheme();
  const navigate = useNavigate()
  const dispatch = useDispatch();

  const { approvalSetting, approvalSettingStatus } = useSelector((state) => state.disposalEvent)

  //date picker
  const [executionDate, setExecutionDate] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();

  const [name, setName] = useState()
  const [disposalCode, setDisposalCode] = useState()
  const [locationName, setLocationName] = useState('-')

  const [activeValue, setActiveValue] = useState(0);
  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };

  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  useEffect(() => {
    dispatch(generateDisposalSetting());
  }, [dispatch])

  const { keycloak } = useContext(KeycloakContext);
  const createEvent = (e) => {
    e.stopPropagation()
    const allHasApproverDueDate = approvalSetting.every(arr => 'ApproverDueDate' in arr)
    if (!allHasApproverDueDate) return

    const mapedObject = approvalSetting.map(approval => {
      const object = { ...approval, DueDateApproval: approval.ApproverDueDate.toISOString() }
      delete object.Actions
      delete object.ApproverDueDate
      delete object.id
      delete object.setDate
      return object
    })

    try {
      dispatch(createDisposalEvent(
        {
          name,
          disposalCode,
          locationName,
          executionDate: executionDate.toISOString(),
          startDate: startDate.toISOString(),
          endDate: endDate.toISOString(),
          approverSetting: mapedObject,
          createdBy: keycloak.tokenParsed["name"],
          type: "Disposal Event",
        }
      ));
      navigate(`/disposal/general/event/`);
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <form name="create-disposal-event">
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                [theme.breakpoints.down("md")]: {
                  flexDirection: "column",
                  gap: theme.spacing(2),
                },
                mb: 1,
                pr: 6,
                justifyContent: "space-between",
              }}
            >
              <h1 className="page-title">Disposal Event Create</h1>
              <Button type={"button"} classname="general-button" onclick={(e) => createEvent(e)}>
                Submit
              </Button>{" "}
              {/* styling general custom-style.css */}
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                [theme.breakpoints.down("md")]: {
                  flexDirection: "column",
                },
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                  [theme.breakpoints.down("md")]: {
                    width: "100%",
                  },
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 81,
                    }}
                  >
                    <label className="label-group">Reference</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <input
                      type="text"
                      name=""
                      id=""
                      className="input-group !w-full"
                      placeholder="Reference"
                      value={disposalCode}
                      onInput={e => setDisposalCode(e.target.value)}
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 81,
                    }}
                  >
                    <label className="label-group ">Name</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                      
                    }}
                  >
                    <input
                      type="text"
                      name=""
                      id=""
                      className="input-group !w-full"
                      placeholder="Event Name"
                      value={name}
                      onInput={e => setName(e.target.value)}
                    />
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                  [theme.breakpoints.down("md")]: {
                    width: "100%",
                  },
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Disposal Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <DatePicker
                      date={executionDate}
                      onDateChange={setExecutionDate}
                      locale={enGB}
                      format="dd MMM yyyy"
                      minimumDate={new Date()}
                    >
                      {({ inputProps, focused }) => (
                        <input
                          className={
                            "input-group justify-end input !w-full" +
                            (focused ? " -focused" : "")
                          }
                          {...inputProps}
                          placeholder="Disposal Date"
                        />
                      )}
                    </DatePicker>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Period Start Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <DateRangePicker
                      startDate={startDate}
                      endDate={endDate}
                      onStartDateChange={setStartDate}
                      onEndDateChange={setEndDate}
                      minimumDate={new Date()}
                      minimumLength={1}
                      format="dd MMM yyyy"
                      locale={enGB}
                    >
                      {({ startDateInputProps, focus }) => (
                        <div className="date-range">
                          <input
                            // styling general custom-style.css
                            className={
                              "input-group input !w-full" +
                              (focus === START_DATE ? " -focused" : "")
                            }
                            {...startDateInputProps}
                            placeholder="Period Start Date"
                          />
                        </div>
                      )}
                    </DateRangePicker>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Period End Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <DateRangePicker
                      startDate={startDate}
                      endDate={endDate}
                      onStartDateChange={setStartDate}
                      onEndDateChange={setEndDate}
                      minimumDate={new Date()}
                      minimumLength={1}
                      format="dd MMM yyyy"
                      locale={enGB}
                    >
                      {({ endDateInputProps, focus }) => (
                        <div className="date-range">
                          <input
                            // styling general custom-style.css
                            className={
                              "input-group input !w-full" +
                              (focus === END_DATE ? " -focused" : "")
                            }
                            {...endDateInputProps}
                            placeholder="Period End Date"
                          />
                        </div>
                      )}
                    </DateRangePicker>
                  </Box>
                </Box>
              </Box>
            </Box>

            {/* tabel approval */}
            <div >
              <Tabs value={activeValue} onChange={handleActiveChange}>
                <Tab label="Approval" />
              </Tabs>

              <TabPanel value={activeValue} index={0}>
                <div className="px-2 py-6">
                  <ApproverDisposalEventTable loading={approvalSettingStatus === 'loading'} data={approvalSetting} />
                </div>
              </TabPanel>
            </div>
          </form>
        </Paper>
      </Grid>
    </Layout>
  );
};

export default CreateDisposalEvent;
