import React, { useState } from "react";
import Layout from "../../../Layout/Layout";
import Button from "../../../../components/buttons/Button";
import {
  DatePicker,
  START_DATE,
  END_DATE,
  DateRangePicker,
} from "react-nice-dates";
import { enGB } from "date-fns/locale";
import "../../../../css/date-picker/date-picker.css";
import Approval from "../../../../components/tables/disposal/approval/DisposalApprovalTable";

const EditDisposalEvent = () => {
  //date picker
  const [disposalDate, setDisposalDate] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();

  return (
    <Layout>
      <div className="content-positioning pl-3 md:pl-48 md:pt-38 pr-2 w-full">
        <form name="create-disposal-event">
          <div className="sm:flex-row sm:justify-between flex flex-col mb-6 pr-6">
            <p className="page-title">Disposal Event Edit</p>
            <Button type={"submit"} classname="general-button">
              Save
            </Button>{" "}
            {/* styling general custom-style.css */}
          </div>
          <div className="flex sm:flex-row flex-col">
            <div className="flex flex-col basis-1/2">
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group">Reference</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3">
                  <input
                    type="text"
                    name=""
                    id=""
                    className="input-group"
                    placeholder="Reference"
                    disabled
                  />
                </div>
              </div>
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group ">Name</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3">
                  <input
                    type="text"
                    name=""
                    id=""
                    className="input-group"
                    placeholder="Event Name"
                    disabled
                  />
                </div>
              </div>
            </div>
            <div className="flex flex-col basis-1/2">
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group ">Disposal Date</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3 ">
                  <DatePicker
                    date={disposalDate}
                    onDateChange={setDisposalDate}
                    locale={enGB}
                    format="dd MMM yyyy"
                    minimumDate={new Date()}
                  >
                    {({ inputProps, focused }) => (
                      <input
                        className={
                          "input-group justify-end input" +
                          (focused ? " -focused" : "")
                        }
                        {...inputProps}
                        placeholder="Disposal Date"
                        disabled
                      />
                    )}
                  </DatePicker>
                </div>
              </div>
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group ">Period Start Date</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3 ">
                  <DateRangePicker
                    startDate={startDate}
                    endDate={endDate}
                    onStartDateChange={setStartDate}
                    onEndDateChange={setEndDate}
                    minimumDate={new Date()}
                    minimumLength={1}
                    format="yyyy MMM dd"
                    locale={enGB}
                  >
                    {({ startDateInputProps, focus }) => (
                      <div className="date-range">
                        <input
                          // styling general custom-style.css
                          className={
                            "input-group input" +
                            (focus === START_DATE ? " -focused" : "")
                          }
                          {...startDateInputProps}
                          placeholder="Period Start Date"
                          disabled
                        />
                      </div>
                    )}
                  </DateRangePicker>
                </div>
              </div>
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group ">Period End Date</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3 ">
                  <DateRangePicker
                    startDate={startDate}
                    endDate={endDate}
                    onStartDateChange={setStartDate}
                    onEndDateChange={setEndDate}
                    minimumDate={new Date()}
                    minimumLength={1}
                    format="yyyy MMM dd"
                    locale={enGB}
                  >
                    {({ endDateInputProps, focus }) => (
                      <div className="date-range">
                        <input
                          // styling general custom-style.css
                          className={
                            "input-group input" +
                            (focus === END_DATE ? " -focused" : "")
                          }
                          {...endDateInputProps}
                          placeholder="Period End Date"
                          disabled
                        />
                      </div>
                    )}
                  </DateRangePicker>
                </div>
              </div>
            </div>
          </div>
          {/* tabel approval */}
          <div>
            <p className="flex justify-start my-4 font-medium">Approval</p>
            <Approval />
          </div>
        </form>
      </div>
    </Layout>
  );
};

export default EditDisposalEvent;
