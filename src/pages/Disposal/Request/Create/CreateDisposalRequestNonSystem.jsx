import React, { useEffect, useState } from "react";
import Layout from "../../../Layout/Layout";
import DocumentInput from "../../../../components/documents/DocumentInput";
import Modal from "../../../../components/modals/Modal";
import { DateRangePicker, START_DATE, END_DATE } from "react-nice-dates";
import Button from "../../../../components/buttons/Button";

import { enGB } from "date-fns/locale";
import useSearchStore from "../../../../store/searchStore";
import CreateDisposalRequestTable from "../../../../components/tables/disposal/CreateDisposalRequestNonSystemTable";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Autocomplete, Box, TextField, Typography } from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import DocumentTableCreate from "../../../../components/tables/DocumentTableCreate";
import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  fetchLocation,
  fetchPurchasingGroup,
  fetchPlant,
  fetchReason,
} from "../../../../slices/GlobalSlice";
import { get } from "../../../../util/http";
import { retrievePartRequests } from "../../../../slices/PartRequstSlice";
import { retrieveActiveDisposalEvent } from "../../../../slices/DisposalEventSlice";
import { createDisposalRequest } from "../../../../slices/DisposalRequestSlice";
import SuccessCheckmark from "../../../../components/animations/checkmark/SuccessCheckmark";
import FailureCheckmark from "../../../../components/animations/checkmark/FailureCheckmark";
import { useNavigate } from "react-router-dom";

const CreateDisposalRequestNonSystem = () => {
  const [noPartRequest, setNoPartRequest] = useState("");
  const [event, setEvent] = useState("");
  const [ValidQuantity, setValidQuantity] = useState("");
  const [isListOpen, setIsListOpen] = useState(false);

  const [isSubmitting, setIsSubmitting] = useState("");
  const [isLoading, setIsLoading] = useState("");

  const [plantSelectId, setPlantSelectId] = useState("");
  const [payload, setPayload] = useState({
    id: "",
    IdReason: "",
    PurchasingGroupCodeFk: "",
    RequestDate: "",
    DueDate: "",
    PlantName: "",
    LocationCode: "",
    RequestLocation: "",
    DestroyLocation: "",
    WarehouseLocation: "",
    IdParent: "",
    Items: {
      ItemCode: "",
      RequestQuantity: "",
      AdjustmentQuantity: "",
      Notes: "",
    },
  });

  const [payloadDetailPartReq, setPayloadDetailPartReq] = useState([])

  const [listPartReq, setListPartReq] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);
  const navigate = useNavigate();

  const [isModalOpen, setModalOpen] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState(false);

  const [activeValue, setActiveValue] = useState(0);
  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };

  const [locationSelectId, setLocationSelectId] = useState("");
  const [purchasingGroupSelectId, setPurchasingGroupSelectId] = useState("");

  const dispatch = useDispatch();

  const partRequests = useSelector((state) => state.partrequests.partRequest);
  const { activeDisposalEvent } = useSelector((state) => state.disposalEvent);

  const [errors, setErrors] = useState({});

  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  //Table data from pass to child
  const [tableData, setTableData] = useState([]);

  //files from child
  const [files, setFiles] = useState([]);

  const handleDataFromChild = async (data) => {
    console.log(data);
    //dispatch(getPartRequestTable(data))
    setTableData(data);
  };

  const handleStatusFromChild = async (valid) => {
    console.log(valid);
    setValidQuantity(valid);
  };

  const handleFilesFromChild = (files) => {
    console.log(files);
    setFiles(files);
  };

  const handleGetFiles = () => {
    return files;
  };

  const handleGetTableData = () => {
    return payloadDetailPartReq?.ItemsData ? payloadDetailPartReq?.ItemsData : tableData;
  };

  const getListPartReq = async (value) => {
    const res = await get(
      process.env.REACT_APP_API_URL_Part_Request_Service +
        "/search?code=" +
        value
    );
    setListPartReq(res.data);
    console.log("search value:", res.data);
    console.log("value search", value);
  };

  const onInputChange = (event, value, reason) => {
    setIsListOpen(true);
    if (value) {
      setIsLoading(true);
      let noPartReq = value.split(" ");
      payload.IdParent = noPartReq[0];
    } else {
      setListPartReq([]);
    }
  };

  const handleAutocompleteChange = async (event, value) => {
    setIsListOpen(false);
    if (value != null) {
      console.log("list part req", listPartReq);
      let noPartReq = value.split(" ");
      const searchNoPartReq = listPartReq.find((req) => req.PartRequestCode === noPartReq[0]);
      console.log("seach no part req:", searchNoPartReq);

      if(searchNoPartReq){
        const result = await handleSubmit(searchNoPartReq.ID)
        console.log("Result:",result);
        setPayload({
          id: payloadDetailPartReq?.Request?.ID,
          IdReason: payloadDetailPartReq?.Request?.IdReason,
          PurchasingGroupCodeFk: payloadDetailPartReq?.Request?.PurchasingGroupCodeFk,
          RequestDate: payloadDetailPartReq?.Request?.RequestDate,
          DueDate: payloadDetailPartReq?.Request?.DueDate,
          PlantName: payloadDetailPartReq?.Request?.PlantName,
          LocationCode: payloadDetailPartReq?.Request?.LocationPlant?.LocationCode,
          RequestLocation: payloadDetailPartReq?.Request?.RequestLocation,
          DestroyLocation: payloadDetailPartReq?.Request?.DestroyLocation,
          WarehouseLocation: payloadDetailPartReq?.Request?.WarehouseLocation,
          IdParent: noPartReq,
          Items: payloadDetailPartReq?.ItemsData && payloadDetailPartReq?.ItemsData.map((value) => ({
            ItemCode: value.ItemCode,
            RequestQuantity: parseInt(value.RequestQuantity, 10),
            AdjustmentQuantity: parseInt(value.AdjustmentQuantity,10),
            Notes: value.Notes,
          })),
        });
      }
    } else {
      setPayload({ ...payload, IdParent: "" });
    }
  };

  function dispatchLocation(id) {
    setTableData([]);
    setPlantSelectId(id);
    dispatch(fetchLocation(id));
  }

  function dispatchPlant(id) {
    setTableData([]);
    setLocationSelectId(id);
  }

  const handleSubmit = async (selectedValue) => {
    // event.preventDefault();
    // const selectedValue = event.target.value;
    // setNoPartRequest(selectedValue);

    const res = await get(
      process.env.REACT_APP_API_URL_Part_Request_Service +
        `/${selectedValue}?type=Request`
    );

    if (res.status === 200) {
      setPayloadDetailPartReq(res.data);
      console.log("DATA PART REQUEST", res.data);
      console.log("DATA PART REQUEST PAYLOAD", payload);
    } else {
      console.log(res.error);
    }
  };

  useEffect(() => {
    console.log("ID Part Req:", noPartRequest);
    console.log("Data Part Req", partRequests);
    // dispatch(retrievePartRequests(1, 5));
    dispatch(retrieveActiveDisposalEvent());
    if (payloadDetailPartReq?.ItemsData) {
      const total = payloadDetailPartReq?.ItemsData.reduce((sum, item) => {
        const amount = parseFloat(item.Amount) || 0;
        return sum + amount;
      }, 0);
      setTotalAmount(total);
    }
    console.log("Disposal Event", activeDisposalEvent);
    console.log("Payload Detail Part Req", payloadDetailPartReq);
    console.log("Payload", payload);
    console.log("Item Data", payloadDetailPartReq.ItemsData);
    console.log("Item Data To Table", handleGetTableData());
  }, [dispatch, payloadDetailPartReq]);

  // noPartRequest, activeDisposalEvent, payload

  useEffect(() => {
    console.log("Part Req Code:", payload.IdParent);
    if (payload.IdParent !== "") {
      setTimeout(() => {
        getListPartReq(payload.IdParent);
        setIsLoading(false);
      }, 1500);
    }
    return () => {
      setListPartReq([]);
    };
  }, [payload.IdParent]);

  const submitData = async (e) => {
    e.preventDefault();
    let payloadArray = {
      UserName: "Dape",
      IdReason: payloadDetailPartReq?.Request?.IdReason,
      PurchasingGroupCodeFk: payloadDetailPartReq?.Request?.PurchasingGroupCodeFk,
      Type: "Disposal",
      RequestDate: payloadDetailPartReq?.Request?.RequestDate,
      DueDate: payloadDetailPartReq?.Request?.DueDate,
      PlantName: payloadDetailPartReq?.Request?.PlantName,
      LocationCode: payloadDetailPartReq?.Request?.LocationPlant?.LocationCode,
      RequestLocation: payloadDetailPartReq?.Request?.RequestLocation,
      DestroyLocation: payloadDetailPartReq?.Request?.DestroyLocation,
      DisposalType: "Non - System",
      IdDisposalGroupEvent: parseInt(event, 10),
      IdParent: payloadDetailPartReq?.Request?.ID,
      Items: payloadDetailPartReq?.ItemsData.map((item) => ({
        ItemCode: item.ItemCode,
        RequestQuantity: parseInt(item.RequestQuantity, 10),
        AdjustmentQuantity: parseInt(item.AdjustmentQuantity, 10),
        Notes: item.Notes,
      })),
    };
    console.log("Payload Submit", payloadArray);
    let payloadString = JSON.stringify(payloadArray);
    console.log("Payload Submit JSON", payloadString);
    try {
      dispatch(
        createDisposalRequest({ files: files, jsonData: payloadString })
      ).then(unwrapResult);

      setShowSuccess(true);
      setMessage("Created Disposal Request!");
      setTimeout(() => {
        // setShowSuccess(true);
        navigate("/disposal/general/request");
        window.location.reload();
        {
          /*sementara untuk showing */
        }
      }, 3000);
    } catch (error) {
      console.log(error);
      setShowFailure(true);
      setMessage(error);
    } finally {
      setIsSubmitting(false);
      setIsLoading(false);
    }
  };

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <form name="create-disposal-request" onSubmit={submitData}>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                mb: 1,
                pr: 6,
                justifyContent: "space-between",
              }}
            >
              <h1 className="page-title">
                Create Disposal Request <i>(Non - System)</i>
              </h1>
              <Button
                type={"submit"}
                classname="general-button"
                disabled={isSubmitting}
              >
                Submit
              </Button>{" "}
              {/* styling general custom-style.css */}
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Event</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <Select
                      id="plant"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={event}
                      onChange={(e) => setEvent(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Event -- </MenuItem>
                      {activeDisposalEvent?.data?.map(
                        (event, index) =>
                          event.Status === "Active" && (
                            <MenuItem key={index} value={event.ID}>
                              {event.DisposalCode}
                            </MenuItem>
                          )
                      )}
                    </Select>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Total Amount</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      readOnly
                      value={totalAmount}
                      // type="number"
                      // onChange={(e) => {
                      //   setAmount(e.target.value);
                      // }}
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Request Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      type="text"
                      readOnly
                      value={payloadDetailPartReq?.Request?.RequestDate}
                    />
                    {errors.startDate && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.startDate}
                      </span>
                    )}
                  </Box>
                </Box>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 200,
                    }}
                  >
                    <label className="label-group">Plant</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      type="text"
                      readOnly
                      value={payloadDetailPartReq?.Request?.PlantName}
                    />
                    {errors.plant && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.plant}
                      </span>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 200,
                    }}
                  >
                    <label className="label-group ">Warehouse Location</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      type="text"
                      readOnly
                      value={payloadDetailPartReq?.Request?.WarehouseLocation}
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 200,
                    }}
                  >
                    <label className="label-group ">Destroy Location</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      type="text"
                      readOnly
                      value={payloadDetailPartReq?.Request?.DestroyLocation}
                    />
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 200,
                    }}
                  >
                    <label className="label-group">Part Request Code</label>{" "}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <Autocomplete
                      className="w-full"
                      onBlur={() => setIsListOpen(false)}
                      onChange={handleAutocompleteChange}
                      id="free-solo-demo"
                      open={isListOpen}
                      onInputChange={onInputChange}
                      loading={isLoading}
                      value={payload.IdParent}
                      options={listPartReq.map(
                        (option) => option?.PartRequestCode
                      )}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          size="small"
                          name="PartRequestCode"
                          className="w-full border-2 border-zinc-300"
                        />
                      )}
                    />
                    {/* <Select
                      id="plant"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={noPartRequest}
                      onChange={handleSubmit}
                    >
                      <MenuItem hidden>
                        {" "}
                        -- Select Part Request Code --{" "}
                      </MenuItem>
                      {partRequests?.data?.data.map((req, index) => (
                        <MenuItem key={index} value={req.ID}>
                          {req.PartRequestCode}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.plant && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.plant}
                      </span>
                    )} */}
                  </Box>
                </Box>
              </Box>
            </Box>
          </form>
          <div>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Parts" />
              <Tab label="Documents" />
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <div className="py-3 pr-3 md:w-1/3 w-full">
                <div className="flex gap-3"></div>
              </div>
              <CreateDisposalRequestTable
                purcGroup={purchasingGroupSelectId}
                locationCode={locationSelectId}
                onDataSend={handleDataFromChild}
                onDataLoad={handleGetTableData}
                onStatusSend={handleStatusFromChild}
                tag={"system"}
              />
              {errors.tableData && (
                <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                  {errors.tableData}
                </span>
              )}
            </TabPanel>
            <TabPanel value={activeValue} index={1}>
              <div className="px-2 py-6">
                <DocumentTableCreate
                  onDataSend={handleFilesFromChild}
                  onTableLoad={handleGetFiles}
                />
              </div>
            </TabPanel>
          </div>

          <Modal isOpen={isModalOpen} onClose={() => setModalOpen(false)} />
          <SuccessCheckmark showSuccess={showSuccess} message={message} />
          <FailureCheckmark showFailure={showFailure} message={message} />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default CreateDisposalRequestNonSystem;
