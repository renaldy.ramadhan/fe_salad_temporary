import React, { useCallback, useEffect, useState } from "react";
import Layout from "../../../Layout/Layout";
import DocumentInput from "../../../../components/documents/DocumentInput";
import Modal from "../../../../components/modals/Modal";
import { DateRangePicker, START_DATE, END_DATE } from "react-nice-dates";
import Button from "../../../../components/buttons/Button";

import { enGB } from "date-fns/locale";
import useSearchStore from "../../../../store/searchStore";
import CreateDisposalRequestTable from "../../../../components/tables/disposal/CreateDisposalRequestNonSystemTable";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box, Typography } from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import DocumentTableCreate from "../../../../components/tables/DocumentTableCreate";
import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  fetchLocation,
  fetchPurchasingGroup,
  fetchPlant,
  fetchReason,
} from "../../../../slices/GlobalSlice";
import { get } from "../../../../util/http";
import { retrievePartRequests } from "../../../../slices/PartRequstSlice";
import { retrieveActiveDisposalEvent } from "../../../../slices/DisposalEventSlice";
import CreateDisposalRequestSystemTable from "../../../../components/tables/disposal/CreateDisposalRequestSystemTable";
import { validateForm } from "../../../PartRequest/Create/Validation";
import SuccessCheckmark from "../../../../components/animations/checkmark/SuccessCheckmark";
import FailureCheckmark from "../../../../components/animations/checkmark/FailureCheckmark";
import { createDisposalRequest } from "../../../../slices/DisposalRequestSlice";
import { useNavigate } from "react-router-dom";

const CreateDisposalRequestSystem = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  const [event, setEvent] = useState("");
  const [totalAmount, setTotalAmount] = useState(0);

  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const navigate = useNavigate();

  const tableLocation = useSelector((state) => state.globals.fetchLocation);
  const tablePlant = useSelector((state) => state.globals.fetchPlant);
  const tablePurc = useSelector((state) => state.globals.fetchPurchasingGroup);
  const tableReason = useSelector((state) => state.globals.fetchReason);
  const plantcode = tablePlant.map((plant) => plant.PlantCode);
  const uniqueplantcode = [...new Set(plantcode)];

  const [validQuantity, setValidQuantity] = useState(false);

  const [isModalOpen, setModalOpen] = useState(false);

  const [activeValue, setActiveValue] = useState(0);

  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState(false);

  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };

  const [locationSelectId, setLocationSelectId] = useState("");
  const [purchasingGroupSelectId, setPurchasingGroupSelectId] = useState("");
  const [plantSelectId, setPlantSelectId] = useState("");
  const [reasonSelectId, setReasonSelectId] = useState("");
  const [destroyLocation, setDestroyLocation] = useState("");

  const dispatch = useDispatch();

  const partRequests = useSelector((state) => state.partrequests.partRequest);
  const { activeDisposalEvent } = useSelector((state) => state.disposalEvent);

  const [errors, setErrors] = useState({});

  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  //Table data from pass to child
  const [tableData, setTableData] = useState([]);

  //files from child
  const [files, setFiles] = useState([]);

  const handleDataFromChild = async (data) => {
    console.log(data);
    //dispatch(getPartRequestTable(data))
    setTableData(data);
  };

  const handleStatusFromChild = async (valid) => {
    console.log(valid);
    setValidQuantity(valid);
  };

  const handleFilesFromChild = (files) => {
    console.log(files);
    setFiles(files);
  };

  const handleGetFiles = () => {
    return files;
  };

  const handleGetTableData = () => {
    return tableData;
  };

  function dispatchLocation(id) {
    setTableData([]);
    setPlantSelectId(id);
    dispatch(fetchLocation(id));
  }

  function dispatchPlant(id) {
    setTableData([]);
    setLocationSelectId(id);
  }

  function loadParts(idPurchasingGroup) {
    setTableData([]);
    setPurchasingGroupSelectId(idPurchasingGroup);
  }

  const createDisposalRequestSystemAction = (e) => {
    e.preventDefault();

    const newError = validateForm(
      tableData,
      locationSelectId,
      // purchasingGroupSelectId,
      plantSelectId,
      reasonSelectId,
      startDate,
      endDate,
      15,
      destroyLocation
    );
    setErrors(newError);
    console.log(newError);

    if (Object.keys(newError).length === 0 && validQuantity) {
      setIsLoading(true);
      setIsSubmitting(true);
      submitData();
    }
  };

  const submitData = async () => {
    const selectedPlant = tablePlant.find(
      (plant) => plant.PlantCode === plantSelectId
    );

    let payloadArray = {
      UserName: "Dape",
      IdReason: parseInt(reasonSelectId, 10),
      // PurchasingGroupCodeFk: purchasingGroupSelectId,
      PurchasingGroupCodeFk: "D13",
      Type: "Disposal",
      RequestDate: startDate ? startDate.toISOString() : null,
      DueDate: endDate ? endDate.toISOString() : null,
      PlantName: selectedPlant ? selectedPlant.LocationName : "",
      LocationCode: locationSelectId ? locationSelectId : "",
      RequestLocation: locationSelectId,
      DestroyLocation: destroyLocation,
      DisposalType: "System",
      IdDisposalGroupEvent: parseInt(event, 10),
      Items: tableData.map((item) => ({
        ItemCode: item.ItemCode,
        RequestQuantity: parseInt(item.RequestQuantity, 10),
        ApprovedQuantity: parseInt(item.RequestQuantity, 10),
        AdjustmentQuantity: parseInt(item.AdjustmentQuantity, 10),
        Notes: item.Notes,
      })),
    };
    console.log("Payload Submit", payloadArray);
    let payloadString = JSON.stringify(payloadArray);
    console.log("Payload Submit JSON", payloadString);
    try {
      dispatch(
        createDisposalRequest({ files: files, jsonData: payloadString })
      ).then(unwrapResult);

      setShowSuccess(true);
      setMessage(`Created Disposal Request!`);
      setTimeout(() => {
        // setShowSuccess(true);
        navigate("/disposal/general/request");
        window.location.reload();
        {
          /*sementara untuk showing */
        }
      }, 3000);
    } catch (error) {
      console.log(error);
      setShowFailure(true);
      setMessage(error);
    } finally {
      setIsSubmitting(false);
      setIsLoading(false);
    }
  };

  const initFetch = useCallback(() => {
    // dispatch(getActiveApprovalByNikToken());
    dispatch(fetchPlant());
    dispatch(fetchPurchasingGroup());
    dispatch(fetchReason("Disposal"));
  }, [dispatch]);

  useEffect(() => {
    const total = tableData.reduce((sum, row) => {
      const amount = parseFloat(row.Amount || 0);
      return sum + amount;
    }, 0);
    setTotalAmount(total.toFixed(2));
  }, [tableData]);

  useEffect(() => {
    console.log("Data Part Req", partRequests);
    dispatch(retrievePartRequests(1, 5));
    dispatch(retrieveActiveDisposalEvent());
    dispatch(fetchPlant());

    initFetch();
  }, [dispatch]);

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <form
            name="create-disposal-request"
            onSubmit={createDisposalRequestSystemAction}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                mb: 1,
                pr: 6,
                justifyContent: "space-between",
              }}
            >
              <h1 className="page-title">
                Create Disposal Request <i>(System)</i>
              </h1>
              <Button type={"submit"} classname="general-button">
                Submit
              </Button>{" "}
              {/* styling general custom-style.css */}
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Event</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <Select
                      id="plant"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={event}
                      onChange={(e) => setEvent(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Event -- </MenuItem>
                      { activeDisposalEvent.data && activeDisposalEvent.data.map((event, index) => (
                        <MenuItem key={index} value={event.ID}>
                          {event.DisposalCode}
                        </MenuItem>
                      ))
                      }
                    </Select>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Request Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <DateRangePicker
                      startDate={startDate}
                      endDate={endDate}
                      onStartDateChange={setStartDate}
                      onEndDateChange={setEndDate}
                      minimumDate={new Date()}
                      minimumLength={1}
                      // format='dd MMM yyyy'
                      format="yyyy-MM-dd hh:mm"
                      locale={enGB}
                    >
                      {({ startDateInputProps, focus }) => (
                        <div className="date-range">
                          <input
                            className={
                              "py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3 input" +
                              (focus === START_DATE ? " -focused" : "")
                            }
                            {...startDateInputProps}
                            placeholder="Request Date"
                          />
                        </div>
                      )}
                    </DateRangePicker>
                    {errors.startDate && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.startDate}
                      </span>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group">Due Date</label>
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <DateRangePicker
                      startDate={startDate}
                      endDate={endDate}
                      onStartDateChange={setStartDate}
                      onEndDateChange={setEndDate}
                      minimumDate={new Date()}
                      minimumLength={1}
                      format="yyyy-MM-dd hh:mm"
                      locale={enGB}
                    >
                      {({ endDateInputProps, focus }) => (
                        <div className="date-range">
                          <input
                            className={
                              "py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3 input" +
                              (focus === END_DATE ? " -focused" : "")
                            }
                            {...endDateInputProps}
                            placeholder="Due date"
                          />
                        </div>
                      )}
                    </DateRangePicker>
                    {errors.endDate && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.endDate}
                      </span>
                    )}
                  </Box>
                </Box>
              </Box>

              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "500px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 200,
                    }}
                  >
                    <label className="label-group">Plant</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 350,
                    }}
                  >
                    <Select
                      id="plant"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={plantSelectId}
                      onChange={(e) => dispatchLocation(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Plant -- </MenuItem>
                      {uniqueplantcode.map((code, index) => (
                        <MenuItem key={index} value={code}>
                          {code}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.plant && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.plant}
                      </span>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 200,
                    }}
                  >
                    <label className="label-group">Location</label>

                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 350,
                    }}
                  >
                    <Select
                      id="location"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={locationSelectId}
                      onChange={(e) => dispatchPlant(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Location -- </MenuItem>
                      {tableLocation.map((plant, index) => (
                        <MenuItem key={index} value={plant.LocationCode}>
                          {plant.LocationCode + " - " + plant.LocationName}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.location && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.location}
                      </span>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 200,
                    }}
                  >
                    <label className="label-group">Reason</label>
                  </Box>
                  <Box
                    sx={{
                      width: 350,
                    }}
                  >
                    <Select
                      id="reason"
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={reasonSelectId}
                      onChange={(e) => setReasonSelectId(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Reason -- </MenuItem>
                      {tableReason.map((reason, index) => (
                        <MenuItem key={index} value={reason.ID}>
                          {reason.ReasonName}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.tableReason && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.tableReason}
                      </span>
                    )}
                  </Box>
                  {/* <Box
                    sx={{
                      mr: 2,
                      width: 200,
                    }}
                  >
                    <label className="label-group">Purc Group</label>
                  </Box>
                  <Box
                    sx={{
                      width: 350,
                    }}
                  >
                    <Select
                      id="purcgroup"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={purchasingGroupSelectId}
                      onChange={(e) => loadParts(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Purc Group -- </MenuItem>
                      {tablePurc
                        .filter(
                          (purchasingGroup) =>
                            purchasingGroup.PurchasingGroup === plantSelectId
                        )
                        .map((purchasingGroup, index) => (
                          <MenuItem
                            key={index}
                            value={purchasingGroup.PurchasingGroupCode}
                          >
                            {purchasingGroup.PurchasingGroupCode}
                          </MenuItem>
                        ))}
                    </Select>
                    {errors.purchasingGroup && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.purchasingGroup}
                      </span>
                    )}
                  </Box> */}
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "500px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                  }}
                ></Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 4,
                      width: 200,
                    }}
                  >
                    <label className="label-group">Destroy Location</label>
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <Select
                      id="location"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={destroyLocation}
                      onChange={(e) => setDestroyLocation(e.target.value)}
                    >
                      <MenuItem hidden>
                        {" "}
                        -- Select Destroy Location --{" "}
                      </MenuItem>
                      {tableLocation.map((plant, index) => (
                        <MenuItem key={index} value={plant.LocationCode}>
                          {plant.LocationCode + " - " + plant.LocationName}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.tableReason && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.tableReason}
                      </span>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 4,
                      width: 200,
                    }}
                  >
                    <label className="label-group ">Total Amount</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      readOnly
                      value={totalAmount}
                    />
                  </Box>
                </Box>
              </Box>
            </Box>
          </form>
          <div>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Parts" />
              <Tab label="Documents" />
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <div className="py-3 pr-3 md:w-1/3 w-full">
                <div className="flex gap-3"></div>
              </div>
              <CreateDisposalRequestSystemTable
                purcGroup={purchasingGroupSelectId}
                locationCode={locationSelectId}
                onDataSend={handleDataFromChild}
                onDataLoad={handleGetTableData}
                onStatusSend={handleStatusFromChild}
                tag={"system"}
              />
              {errors.tableData && (
                <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                  {errors.tableData}
                </span>
              )}
            </TabPanel>
            <TabPanel value={activeValue} index={1}>
              <div className="px-2 py-6">
                <DocumentTableCreate
                  onDataSend={handleFilesFromChild}
                  onTableLoad={handleGetFiles}
                />
              </div>
            </TabPanel>
          </div>

          <Modal isOpen={isModalOpen} onClose={() => setModalOpen(false)} />
          <SuccessCheckmark showSuccess={showSuccess} message={message} />
          <FailureCheckmark showFailure={showFailure} message={message} />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default CreateDisposalRequestSystem;
