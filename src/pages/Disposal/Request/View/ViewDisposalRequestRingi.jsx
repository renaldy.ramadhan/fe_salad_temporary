import React, { useEffect, useState } from "react";
import Layout from "../../../Layout/Layout";
import Modal from "../../../../components/modals/Modal";
import { DatePicker, DateRangePicker } from "react-nice-dates";
import Button from "../../../../components/buttons/Button";

import { enGB } from "date-fns/locale";
import useSearchStore from "../../../../store/searchStore";
import ViewDisposalRequestTable from "../../../../components/tables/disposal/ViewDisposalRequestTableNonSystem";
import DocumentTable from "../../../../components/tables/DocumentTable";
import ApprovalDisposalTable from "../../../../components/tables/disposal/ApprovalDisposalTable";
import ApproverTable from "../../../../components/tables/Approver/ApproverTable";

import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box } from "@mui/material";

import { getItemsRequestDetail } from "../../../../slices/DisposalEventSlice";
import { useParams } from "react-router-dom";

const ViewDisposalRequestRingi = () => {
  const dispatch = useDispatch();

  const { id } = useParams();

  const [requestDate, setRequestDate] = useState(new Date());

  const [isModalOpen, setModalOpen] = useState(false);
  const tableData = useSearchStore((state) => state.tableData);
  const [activeValue, setActiveValue] = useState(0);
  const [localStateApprovals, setLocalStateApprovals] = useState([]);
  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };
  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };
  const [errors, setErrors] = useState({});
  const partrequestsitemsdata = useSelector(
    (state) => state.partrequests.partGetRequestItemsData
  );
  const { itemsRequestDetail, itemsRequestDetailStatus } = useSelector(
    (state) => state.disposalEvent
  );
  const { itemsData, Request } = itemsRequestDetail;
  const [name, setName] = useState();
  const [location, setLocation] = useState();
  const [plantName, setPlantName] = useState();
  const [totalAmount, setTotalAmount] = useState(0);

  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    if (itemsRequestDetailStatus === "done") {
      setName(Request.DisposalGroup.Name);
      setLocation(Request.LocationCode);
      setPlantName(Request.LocationPlant.PlantCode);
      setRequestDate(new Date(Request.RequestDate));
    }
  }, [itemsRequestDetail, itemsRequestDetailStatus]);
  useEffect(() => {
    setIsLoading(true);
    dispatch(
      getItemsRequestDetail({ id, page: 1, pageSize: 5, type: "Ringi" })
    );
    setIsLoading(false);
  }, [dispatch, id]);

  useEffect(() => {
    if (itemsRequestDetail && itemsRequestDetail?.ItemsData.length > 0) {
      const total = itemsRequestDetail?.ItemsData.reduce((sum, item) => {
        const amount = parseFloat(item.Amount) || 0;
        return sum + amount;
      }, 0);
      setTotalAmount(total);
    }
  }, [itemsRequestDetail]);

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <form name="create-part-request">
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                mb: 1,
                pr: 6,
                justifyContent: "space-between",
              }}
            >
              <h1 className="page-title">
                View Disposal Request <i>(Ringi)</i>
              </h1>
              <div className="flex gap-2 ">
                <Button type={"submit"} classname="general-button">
                  Approve
                </Button>
                <Button type={"submit"} classname="general-button">
                  Reject
                </Button>
              </div>
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                width: "800px",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group ">Event</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={name}
                      readOnly
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group">Total Amount</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3" // styling general custom-style.css
                      value={totalAmount}
                      type="number"
                      onChange={(e) => {
                        setTotalAmount(e.target.value);
                      }}
                    />
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group">Plant</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={plantName}
                      readOnly
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group ">Location</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={location}
                      readOnly
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group ">Request Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <DatePicker
                      date={requestDate}
                      onDateChange={setRequestDate}
                      locale={enGB}
                      format="yyyy MMM dd"
                      minimumDate={new Date()}
                    >
                      {({ inputProps, focused }) => (
                        <input
                          className={
                            "py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3" +
                            (focused ? " -focused" : "")
                          }
                          {...inputProps}
                          placeholder="Request Date"
                          value={
                            requestDate
                              ? new Date(requestDate).toLocaleDateString(
                                  "en-GB",
                                  {
                                    year: "numeric",
                                    month: "long",
                                    day: "numeric",
                                  }
                                )
                              : ""
                          }
                          disabled
                        />
                      )}
                    </DatePicker>
                  </Box>
                </Box>
              </Box>
            </Box>
          </form>
          <Box>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Parts" />
              <Tab label="Approval" />
              <Tab label="Documents" />
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <ViewDisposalRequestTable itemsData={partrequestsitemsdata} />

              {errors.tableData && (
                <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                  {errors.tableData}
                </span>
              )}
            </TabPanel>
            <TabPanel value={activeValue} index={1}>
              <div className="px-2 py-6">
                <ApproverTable data={localStateApprovals} />
                {console.log("localstateapproval :", localStateApprovals)}
              </div>
            </TabPanel>
            <TabPanel value={activeValue} index={2}>
              <div className="px-2 py-6">
                <DocumentTable />
              </div>
            </TabPanel>
            {/* <Tabs>
              {{
                label: 'Parts',
                value: 'parts',
                component: (
                  <div>
                    
                    <div className="py-3 pr-3 md:w-1/3 w-full">
                    </div>
                      {console.log(tableData)}
                      <ViewDisposalRequestTable data={tableData} tag={"system"}/>
                      <Modal isOpen={isModalOpen} onClose={() => setModalOpen(false)} />
                  </div>
                ),
              }}
              {{
                    label: 'Documents',
                    value: 'document',
                    component: (
                      <div className='my-4'>
                        <DocumentTable/>
                      </div>
                    ),
                  }}
              {{ 
                    label: 'Approvals',
                    value: 'approvals',
                    component: (
                      <div className='my-4'>
                        <ApprovalDisposalTable/>
                      </div>
                    ),
                  }}
              
            </Tabs> */}
          </Box>
        </Paper>
      </Grid>
    </Layout>
  );
};

export default ViewDisposalRequestRingi;
