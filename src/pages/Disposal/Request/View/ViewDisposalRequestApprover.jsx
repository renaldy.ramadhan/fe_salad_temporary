import React, { useEffect, useState } from "react";
import Layout from "../../../Layout/Layout";
import Modal from "../../../../components/modals/Modal";
import { DatePicker, DateRangePicker } from "react-nice-dates";
import Button from "../../../../components/buttons/Button";

import useSearchStore from "../../../../store/searchStore";
import ViewDisposalRequestTableSystem from "../../../../components/tables/disposal/ViewDisposalRequestTableSystem";
import DocumentTable from "../../../../components/tables/DocumentTable";
import ApprovalDisposalTable from "../../../../components/tables/disposal/ApprovalDisposalTable";
import ApproverTable from "../../../../components/tables/Approver/ApproverTable";

import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box } from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import { getPartRequest } from "../../../../slices/PartRequstSlice";
import SubmitLoadingSpinner from "../../../../components/animations/loading/SubmitLoadingSpinner";
import {
  getActiveApprovalById,
  approvalAction,
  getApprovalStatusByRequestId,
} from "../../../../slices/ActiveApprovalSlice";
import FailureCheckmark from "../../../../components/animations/checkmark/FailureCheckmark";
import SuccessCheckmark from "../../../../components/animations/checkmark/SuccessCheckmark";

const ViewDisposalRequestApprover = () => {
  const { id, nik } = useParams();
  const [requestDate, setRequestDate] = useState(new Date());
  const [amount, setAmount] = useState();

  const [isModalOpen, setModalOpen] = useState(false);
  const tableData = useSearchStore((state) => state.tableData);
  const [activeValue, setActiveValue] = useState(0);
  const [localStateApprovals, setLocalStateApprovals] = useState([]);

  const [itemsData, setItemsData] = useState([]);

  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };
  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString().substr(-2);

    // Extracting time components
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");
    const seconds = date.getSeconds().toString().padStart(2, "0");

    return `${year}-${month}-${day} ${hours}:${minutes}`;
  };

  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [dataFromChild, setDataFromChild] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    dispatch(getActiveApprovalById(id))
      .then(unwrapResult)
      .then((data) => {
        setLocalStateApprovals(data);
      });
    dispatch(getPartRequest(id));
    dispatch(getApprovalStatusByRequestId(id));
    setIsLoading(false);
  }, [dispatch, id]);

  const [errors, setErrors] = useState({});
  const partrequests = useSelector(
    (state) => state.partrequests.partGetRequest
  );
  const partrequestsstatus = useSelector(
    (state) => state.partrequests.partGetRequestStatus
  );
  const partrequestsitemsdata = useSelector(
    (state) => state.partrequests.partGetRequestItemsData
  );
  const checkactiveapprovalsstatus = useSelector(
    (state) => state.activeapprovals.activeApprovalStatusGetRequestId
  );
  console.log("partrequestsitemsdata: ", partrequestsitemsdata);
  console.log("partrequestsitemsdata: ", checkactiveapprovalsstatus);
  console.log("partrequestsitemsdata: ", partrequests);

  console.log("items data on parent", itemsData);

  const [totalAmount, setTotalAmount] = useState(0);

  useEffect(() => {
    if (partrequests && partrequests.data?.ItemsData?.length > 0) {
      const total = partrequests.data?.ItemsData.reduce((sum, item) => {
        const amount = parseFloat(item.Amount) || 0;
        return sum + amount;
      }, 0);
      setTotalAmount(total);
    }
    if(partrequests.data?.ItemsData?.length > 0){
      setItemsData(partrequests.data?.ItemsData.map((item, idx) => ({ ...item, id: idx+=1})))
    }
  }, [partrequests]);

  const handleDataFromChild = (data) => {
    console.log(data);
    // setTableData(data);
    // dispatch(getPartRequestTable(data));
    console.log("Data From Child", data);
    setDataFromChild(data);
    setItemsData(data)
  };

  const handleApproval = async (status) => {
    setIsSubmitting(true);
    const parsedId = parseInt(id, 10);

    try {
      const actionResult = await dispatch(
        approvalAction({
          IdActiveRequestList: parsedId,
          Status: status,
          // Notes: document.getElementById("remarks").value,
          Nik: nik,
          ItemPivot:
          dataFromChild &&
          dataFromChild.map((item) => ({
            IdItemPivot: item.ID,
            ItemCode: item.ItemCode,
            RequestQuantity: item.RequestQuantity,
            ApprovedQuantity: item.ApprovedQuantity,
            AdjustmentQuantity: item.AdjustmentQuantity,
            Amount: item.Amount,
          })),
        })
      );
      unwrapResult(actionResult);
      setShowSuccess(true);
      setShowFailure(false);
      setMessage(
        `Request ${
          status === "Done" ? "Approval Success" : "Approval Rejected"
        }!`
      );

      setTimeout(() => {
        setShowSuccess(false);
        navigate("/disposal/general/request");
      }, 3000);

      // const updatedData = await dispatch(getActiveApprovalByRequestId(id)).then(unwrapResult);
      const updatedData = await dispatch(getActiveApprovalById(id)).then(
        unwrapResult
      );
      setLocalStateApprovals(updatedData);
    } catch (error) {
      console.error("Approval action failed:", error);
      setShowSuccess(false);
      setShowFailure(true);
      setMessage("Approval Failed!");
      setTimeout(() => {
        setShowFailure(false);
      }, 3000);
    } finally {
      setIsSubmitting(false);
    }
  };
  const handleApprove = () => handleApproval("Done");
  const handleReject = () => handleApproval("Rejected");

  return (
    <Layout>
      {isLoading && <SubmitLoadingSpinner />}

      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          {partrequestsstatus !== "loading" && (
            <form name="view-disposal-approver">
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  mb: 1,
                  pr: 6,
                  justifyContent: "space-between",
                }}
              >
                <h1 className="page-title">
                  View Disposal Request <i>(System)</i> -{" "}
                  {partrequests?.data?.Request.PartRequestCode || "No Code"}
                </h1>
                <Box className="sm:flex-row flex flex-cols gap-4">
                  <button
                    type="button"
                    className="general-button"
                    onClick={handleApprove}
                  >
                    Approve
                  </button>
                  <button
                    type="button"
                    className="general-button"
                    onClick={handleReject}
                  >
                    Reject
                  </button>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  width: "800px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group ">Event</label>{" "}
                      {/* styling general custom-style.css */}
                    </Box>
                    <Box
                      sx={{
                        width: 350,
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data?.Request?.DisposalGroup?.Name ||
                          "Default Plant Name"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        readOnly
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Total Amount</label>{" "}
                      {/* styling general custom-style.css */}
                    </Box>
                    <Box
                      sx={{
                        width: 350,
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3" // styling general custom-style.css
                        value={totalAmount}
                        type="text"
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        // onChange={(e) => {
                        //   setAmount(e.target.value);
                        // }}
                        readOnly
                      />
                    </Box>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Plant</label>{" "}
                      {/* styling general custom-style.css */}
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data?.Request?.PlantName ||
                          "Default Plant Name"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        readOnly
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group ">Location</label>{" "}
                      {/* styling general custom-style.css */}
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data?.Request.LocationPlant
                            .LocationName || "Default Plant Name"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        readOnly
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group ">Request Date</label>{" "}
                      {/* styling general custom-style.css */}
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          formatDate(partrequests?.data?.Request.RequestDate) ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>
            </form>
          )}
          <Box>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Parts" />
              <Tab label="Approval" />
              <Tab label="Documents" />
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <ViewDisposalRequestTableSystem
                itemsData={itemsData}
                onDataSend={handleDataFromChild}
                tag="system"
              />
              {errors.tableData && (
                <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                  {errors.tableData}
                </span>
              )}
            </TabPanel>
            <TabPanel value={activeValue} index={1}>
              <div className="px-2 py-6">
                <ApproverTable data={localStateApprovals} />
                {console.log("localstateapproval :", localStateApprovals)}
              </div>
            </TabPanel>
            <TabPanel value={activeValue} index={2}>
              <div className="px-2 py-6">
                <DocumentTable />
              </div>
            </TabPanel>
          </Box>
          <SuccessCheckmark showSuccess={showSuccess} message={message} />
          <FailureCheckmark showFailure={showFailure} message={message} />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default ViewDisposalRequestApprover;
