import React, { useState, useEffect, useContext, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Layout from "../../../../pages/Layout/Layout";
import ActiveApproverTables from "../../../../components/tables/Approver/ActiveApproverTables";
import DisposalEventTable from "../../../../components/tables/disposal/event/DisposalEventTable";
import VisibilityIcon from "@mui/icons-material/Visibility";

import {
  retrieveActiveDisposalEvent,
  retrieveDisposalEvent,
} from "../../../../slices/DisposalEventSlice";

import {
  retrieveDisposalRequest,
  retrieveDisposalRequestData,
} from "../../../../slices/DisposalRequestSlice";
import DisposalRequestTable from "../../../../components/tables/disposal/request/DisposalRequestTable";
import ActievApproverDisposalRequestTable from "../../../../components/tables/disposal/request/ActiveApproverDisposalRequestTable";

const DisposalRequestDashboard = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    disposalRequest,
    disposalRequestStatus,
    disposalRequestData,
    disposalRequestDataStatus,
  } = useSelector((state) => state.disposalRequest);

  const { page, pageSize, activePage, activePageSize } = useSelector(state => state.disposalRequest)


  const [activeEventRows, setActiveEventRows] = useState([]);
  const [eventRows, setEventRows] = useState([]);

  const initFetch = useCallback(() => {
    if (disposalRequest.length < 1) {
      dispatch(retrieveDisposalRequest({page, pageSize}));
    }
    if (disposalRequestData.length < 1) {
      dispatch(retrieveDisposalRequestData({page, pageSize}));
    }
  }, [dispatch, disposalRequest, disposalRequestData, page, pageSize, activePage, activePageSize]);

  useEffect(() => {
    initFetch();
    console.log("disposal request", disposalRequest?.data?.data);
    setEventRows(() =>
      disposalRequest?.data?.data
        ? disposalRequest?.data?.data.map((item) => ({
            ...item,
            id: item.ID,
          }))
        : ""
    );
    setActiveEventRows(() =>
      disposalRequestData?.data?.data
        ? disposalRequestData?.data?.data.map((item) => ({
            ...item,
            id: item.IdRequest,
          }))
        : ""
    );
    console.log("eventrows:", eventRows);
    console.log("active event rows:", activeEventRows);
    console.log("active approval disposal request", disposalRequestData?.data);
  }, [initFetch, disposalRequest, disposalRequestData]);

  const formatDate = (dateString) => {
    if (!dateString) return ""; // Handle null or undefined dateString
    const date = new Date(dateString);
    const year = date.getFullYear().toString().padStart(4, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hour = date.getHours().toString().padStart(2, "0");
    const minute = date.getMinutes().toString().padStart(2, "0");
    const second = date.getSeconds().toString().padStart(2, "0");
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
  };

  const handleClick = (id) => {

    const row = eventRows.find((row) => row.id === id);

    if (row) {
      // Extract DisposalType from the found row
      const disposalType = row.DisposalType;

      // Navigate based on DisposalType
      if (disposalType === "System") {
        navigate(`/disposal/general/request/view/system/${id}`);
      } else {
        navigate(`/disposal/general/request/view/nonsystem/${id}`);
      }
    }
  };

  const columns = [
    {
      field: "ID", //ini ganti mapping di bagian field
      headerName: "ID",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      filterable: false,
    },
    {
      field: "PartRequestCode",
      headerName: "Reference",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
    },
    {
      field: "PlantCode",
      headerName: "Plant",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.LocationPlant?.PlantCode,
    },

    {
      field: "IdDisposalGroupEvent",
      headerName: "Event",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "Requestor",
      headerName: "Requestor",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.UserName,
    },
    {
      field: "RequestDepartment",
      headerName: "Request Department",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.User.Department,
    },
    {
      field: "DisposalType",
      headerName: "Type",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },

    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 60,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  const columnsApprovalList = [
    {
      field: "IdRequest", //ini ganti mapping di bagian field
      headerName: "ID",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      filterable: false,
    },
    {
      field: "PartRequestCode",
      headerName: "Reference",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.Request?.PartRequestCode,
    },
    {
      field: "LocationCode",
      headerName: "Plant",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
      valueGetter: (params) => params.row?.Request?.LocationCode,
    },

    {
      field: "IdDisposalGroupEvent",
      headerName: "Event",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request?.IdDisposalGroupEvent,
    },
    {
      field: "Requestor",
      headerName: "Requestor",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request?.UserName,
    },
    {
      field: "RequestDepartment",
      headerName: "Request Department",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.User?.Department,
    },
    {
      field: "DisposalType",
      headerName: "Type",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueGetter: (params) => params.row?.Request?.DisposalType,
    },

    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 60,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            // onClick={() => handleClick(id)}
            onClick={() => navigate(`/disposal/general/request/view/approver/` +id,)}
          >
             
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <Typography variant="h5" component="div" sx={{ mb: 2 }}>
            Disposal Request (Need Approval)
          </Typography>
          <ActievApproverDisposalRequestTable
            loading={disposalRequestDataStatus === "loading"}
            parentColumn={columnsApprovalList}
            parentRow={activeEventRows}
          />
        </Paper>
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <Grid>
            <Paper
              sx={{
                p: 2,
                alignContent: "center",
                display: "flex",
                flexDirection: "row",
                height: "inherit",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography
                variant="h5"
                component="div"
                sx={{ justifyContent: "center" }}
              >
                Disposal Request
              </Typography>
              <div sx={{ display: "flex", justifyContent: "flex-end" }}>
                <Button
                  color="primary"
                  style={{ backgroundColor: "#17479d" }}
                  variant="contained"
                  onClick={() =>
                    navigate("/disposal/general/request/system/create", {
                      state: { type: "Request" },
                    })
                  }
                >
                  Create System
                </Button>
                <Button
                  color="primary"
                  style={{ backgroundColor: "#17479d", marginLeft: "10px" }}
                  variant="contained"
                  onClick={() =>
                    navigate("/disposal/general/request/nonsystem/create", {
                      state: { type: "Request" },
                    })
                  }
                >
                  Create Non - System
                </Button>
              </div>
            </Paper>
          </Grid>
          <DisposalRequestTable
            loading={disposalRequestStatus === "loading"}
            parentColumn={columns}
            parentRow={eventRows}
          />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default DisposalRequestDashboard;
