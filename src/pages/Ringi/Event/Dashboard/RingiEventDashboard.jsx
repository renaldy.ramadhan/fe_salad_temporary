import React, { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import VisibilityIcon from "@mui/icons-material/Visibility";
import Layout from "../../../Layout/Layout";
import ActiveApproverTables from "../../../../components/tables/Approver/ActiveApproverTables";
import DisposalEventTable from "../../../../components/tables/disposal/event/DisposalEventTable";

import {
  retrieveActiveDisposalEvent,
  retrieveActiveRingiEvent,
  retrieveDisposalEvent,
  retrieveRingiEvent,
} from "../../../../slices/DisposalEventSlice";
import { buildQueryParams } from "../../../../util/global";
import ActiveApprovalRingiEventTable from "../../../../components/tables/disposal/Ringi/ActiveApprovalRingiEventTable";
import DisposalRingiTable from "../../../../components/tables/disposal/Ringi/DisposalRingiTable";

const DashboardRingi = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    retrieveActiveDisposalEventStatus,
    activeDisposalRingiEvent,
    retrieveDisposalEventStatus,
    disposalRingiEvent,
  } = useSelector((state) => state.disposalEvent);

  const { page, pageSize, activePage, activePageSize } = useSelector((state) => state.disposalEvent);

  const [activeEventRows, setActiveEventRows] = useState([]);
  const [eventRows, setEventRows] = useState([]);

  const [defaultQueryParams, setDefaultQueryParams] = useState({
    type: "Disposal Event Ringi",
    page: page,
    page_size: pageSize,
  });
  const [defaultActiveQueryParams, setDefaultActiveQueryParams] = useState({
    type: "Disposal Event Ringi",
    status: "Active",
    page: activePage,
    page_size: activePageSize,
  });

  const initFetch = useCallback(() => {
    if (activeDisposalRingiEvent.length < 1) {
      dispatch(
        retrieveActiveRingiEvent(buildQueryParams(defaultActiveQueryParams))
      );
    }
    if (disposalRingiEvent.length < 1) {
      dispatch(retrieveRingiEvent(buildQueryParams(defaultQueryParams)));
    }
  }, [
    dispatch,
    activeDisposalRingiEvent,
    disposalRingiEvent,
    defaultQueryParams,
    defaultActiveQueryParams,
    page,
    pageSize
  ]);

  // useEffect(() => {
  //   console.log("Page:", page, "Page Size:", pageSize);
  //   initFetch();
  //   setActiveEventRows(
  //     activeDisposalRingiEvent?.data?.map((event) => {
  //       return { ...event, id: event.ID };
  //     })
  //   );
  //   setEventRows(
  //     disposalRingiEvent?.data?.map((event) => {
  //       return { ...event, id: event.ID };
  //     })
  //   );
  //   console.log("Disposal Ringi Event:", disposalRingiEvent);
  // }, [initFetch, activeDisposalRingiEvent, disposalRingiEvent]);


  useEffect(() => {
    console.log("Page:", page, "Page Size:", pageSize);
    initFetch();
  }, [initFetch, page, pageSize]);

  useEffect(() => {
    if (Array.isArray(activeDisposalRingiEvent?.data)) {
      setActiveEventRows(
        activeDisposalRingiEvent.data.map((event) => ({ ...event, id: event.ID }))
      );
    } else {
      setActiveEventRows([]); 
    }

    if (Array.isArray(disposalRingiEvent?.data)) {
      setEventRows(
        disposalRingiEvent.data.map((event) => ({ ...event, id: event.ID }))
      );
    } else {
      setEventRows([]);
    }

    console.log("Disposal Ringi Event:", disposalRingiEvent);
  }, [activeDisposalRingiEvent, disposalRingiEvent]);

  const formatDate = (dateString) => {
    if (dateString === null) return "";
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0"); 
    const day = String(date.getDate()).padStart(2, "0");

    return `${year}-${month}-${day}`;
  };

  const handleClick = (id) => {
    navigate(`/ringi/event/view/` + id);
  };

  const columns = [
    {
      field: "ID",
      headerName: "Id",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      filterable: false,
    },
    {
      field: "DisposalCode",
      headerName: "Reference",
      minWidth: 120,
      flex: 1,
      editable: false,
      align: "center",
      headerAlign: "center",
    },

    {
      field: "Name",
      headerName: "Name",
      minWidth: 180,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "ExecutionDate",
      headerName: "Date",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "StartDate",
      headerName: "Period Start",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "EndDate",
      headerName: "Period End",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "CreatedBy",
      headerName: "Created By",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "CreatedAt",
      headerName: "Created Date",
      minWidth: 100,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
      valueFormatter: ({ value }) => formatDate(value),
    },
    {
      field: "Status",
      headerName: "Status",
      minWidth: 120,
      flex: 1,
      align: "center",
      headerAlign: "center",
      editable: false,
    },
    {
      field: "actions",
      type: "actions",
      headerName: "Actions",
      minWidth: 60,
      flex: 1,
      cellClassName: "actions",
      align: "center",
      headerAlign: "center",
      getActions: ({ id }) => {
        return [
          <Button
            color="primary"
            style={{ backgroundColor: "#808080" }}
            variant="contained"
            startIcon={<VisibilityIcon />}
            onClick={() => handleClick(id)}
          >
            View
          </Button>,
        ];
      },
    },
  ];

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <Typography variant="h5" component="div" sx={{ mb: 2 }}>
            Disposal Ringi Event (Active)
          </Typography>
          <ActiveApprovalRingiEventTable
            loading={retrieveActiveDisposalEventStatus === "loading"}
            parentColumn={columns}
            parentRow={activeEventRows}
          />
        </Paper>
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <Grid>
            <Paper
              sx={{
                p: 2,
                alignContent: "center",
                display: "flex",
                flexDirection: "row",
                height: "inherit",
                alignItems: "center",
                justifyContent: "space-between",
              }}
            >
              <Typography
                variant="h5"
                component="div"
                sx={{ justifyContent: "center" }}
              >
                Disposal Ringi Event
              </Typography>
              <Button
                color="primary"
                style={{ backgroundColor: "#17479d" }}
                variant="contained"
                onClick={() =>
                  navigate("/ringi/event/create", {
                    state: { type: "Request" },
                  })
                }
              >
                Create
              </Button>
            </Paper>
          </Grid>
          <DisposalRingiTable
            loading={retrieveDisposalEventStatus === "loading"}
            parentColumn={columns}
            parentRow={eventRows}
          />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default DashboardRingi;
