import React, { useEffect, useState, useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { DatePicker } from "react-nice-dates";
import { enGB } from "date-fns/locale";
import Papa from "papaparse"; // Make sure to install papaparse package
import { jwtDecode } from "jwt-decode";

import Layout from "../../../Layout/Layout";
import Button from "../../../../components/buttons/Button";
import ViewDisposalEventRequestTable from "../../../../components/tables/disposal/event/ViewDisposalEventRequestTable";
import ApprovalDisposalEventTable from "../../../../components/tables/disposal/event/ApprovalDisposalEventTable";
import PartsDisposalEventTable from "../../../../components/tables/disposal/event/PartsGeneralDisposalEventTable";
import DocumentTable from "../../../../components/tables/DocumentTable";
import { FaRegPenToSquare } from "../../../../icons/icon";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box } from "@mui/material";
import ApproverDisposalEventTable from "../../../../components/tables/Approver/ApproverDisposalEventTable.jsx";
import { PDFDownloadLink } from "@react-pdf/renderer";
import useSearchStore from "../../../../store/searchStore";
import {
  getDisposalEvent,
  getDisposalEventApprover,
  bulkCreateDisposalEventRingiRequest,
  getDisposalEventRequest,
  checkDisposalEventApprover,
  settleEventRequest,
} from "../../../../slices/DisposalEventSlice";
import { LaporanPemusnahanPDF } from "../../Request/View/LaporanPemusnahanPDF.jsx";
// import LaporanPemusnahanPDF from "../../../Request/View/LaporanPemusnahanPDF.jsx";

const ViewDisposalEvent = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState(null);

  const tableData = useSearchStore((state) => state.tableData);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const [activeValue, setActiveValue] = useState(0);
  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };

  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  const { id } = useParams();
  const [errors, setErrors] = useState({});

  const { disposalEventDetail, getDisposalEventStatus, canApprove } =
    useSelector((state) => state.disposalEvent);
  const {
    approvalSetting,
    approvalSettingStatus,
    pageRequest,
    pageSizeRequest,
  } = useSelector((state) => state.disposalEvent);
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [executionDate, setExecutionDate] = useState();
  const [eventType, setEventType] = useState();
  const [disposalCode, setDisposalCode] = useState();
  const [name, setName] = useState();

  useEffect(() => {
    if (getDisposalEventStatus === "done") {
      setStartDate(new Date(disposalEventDetail.StartDate));
      setEndDate(new Date(disposalEventDetail.EndDate));
      setExecutionDate(new Date(disposalEventDetail.ExecutionDate));
      setEventType(disposalEventDetail.Type);
      setDisposalCode(disposalEventDetail.DisposalCode);
      setName(disposalEventDetail.Name);
    }
  }, [disposalEventDetail, getDisposalEventStatus]);

  useEffect(() => {
    dispatch(getDisposalEvent(id));
    dispatch(getDisposalEventApprover(id));
    dispatch(checkDisposalEventApprover({ id, type: "Ringi" }));
    setLoading(false);
  }, [dispatch, id]);

  const decoded = jwtDecode(window.localStorage.getItem("token"));
  const setEventRequest = async (status) => {
    await dispatch(
      settleEventRequest({
        IdEvent: parseInt(id),
        status,
        nik: decoded.employee_id,
      })
    );
    window.location.reload(false)
  };
  const fileInputRef = useRef(null);
  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <form name="view-ringi-event">
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                mb: 1,
                pr: 6,
                justifyContent: "space-between",
              }}
            >
              <h1 className="page-title">View Ringi Event</h1>
              <div className="flex gap-2 ">
                {/* <Button
                  classname="md:inline-flex items-center w-15 h-10 px-3 py-2 bg-custom-yellow hover:bg-yellow-500 text-white text-sm font-medium rounded-md"
                  title="Edit"
                  onclick={() => {
                    navigate("/approvalgroup");
                  }}
                >
                  <FaRegPenToSquare className="w-full h-full" />
                </Button> */}

                <input
                  ref={fileInputRef}
                  accept=".csv"
                  style={{ display: "none" }}
                  id="raised-button-file"
                  multiple
                  type="file"
                  onChange={(event) => {
                    const file = event.target.files[0];
                    if (file) {
                      Papa.parse(file, {
                        header: true,
                        complete: async function (results) {
                          const newRows = results.data.reduce((acc, obj) => {
                            const {
                              UserName,
                              IdReason,
                              Nik,
                              PurchasingGroupCodeFk,
                              Type,
                              RequestDate,
                              DueDate,
                              PlantName,
                              LocationCode,
                              RequestLocation,
                              DestroyLocation,
                              DisposalType,
                              ItemCode,
                              RequestQuantity,
                              ApprovedQuantity,
                              AdjustmentQuantity,
                              Notes,
                            } = obj;

                            const foundIndex = acc.findIndex(
                              (item) =>
                                item.UserName === UserName &&
                                item.IdReason === parseInt(IdReason) &&
                                item.Nik === Nik &&
                                item.PurchasingGroupCodeFk ===
                                PurchasingGroupCodeFk &&
                                item.Type === Type &&
                                item.RequestDate === RequestDate &&
                                item.DueDate === DueDate &&
                                item.PlantName === PlantName &&
                                item.LocationCode === LocationCode &&
                                item.RequestLocation === RequestLocation &&
                                item.DestroyLocation === DestroyLocation &&
                                item.DisposalType === DisposalType
                            );

                            if (foundIndex !== -1) {
                              acc[foundIndex].Items.push({
                                ItemCode,
                                RequestQuantity: parseInt(RequestQuantity),
                                ApprovedQuantity: parseInt(ApprovedQuantity),
                                AdjustmentQuantity:
                                  parseInt(AdjustmentQuantity),
                                Notes,
                              });
                            } else {
                              acc.push({
                                // id: randomId(),
                                UserName,
                                IdReason: parseInt(IdReason),
                                Nik,
                                PurchasingGroupCodeFk,
                                Type,
                                RequestDate,
                                DueDate,
                                PlantName,
                                LocationCode,
                                RequestLocation,
                                DestroyLocation,
                                DisposalType,
                                IdDisposalGroupEvent: parseInt(id),
                                Items: [
                                  {
                                    ItemCode,
                                    RequestQuantity: parseInt(RequestQuantity),
                                    ApprovedQuantity:
                                      parseInt(ApprovedQuantity),
                                    AdjustmentQuantity:
                                      parseInt(AdjustmentQuantity),
                                    Notes,
                                  },
                                ],
                              });
                            }

                            return acc;
                          }, []);
                          newRows.pop();
                          dispatch(
                            bulkCreateDisposalEventRingiRequest(newRows)
                          );
                          window.location.reload();
                          // dispatch(getDisposalEventRequest({ type: 'Ringi', id, page: pageRequest, pageSize: pageSizeRequest }))
                        },
                      });
                    }
                  }}
                />
                <label for="raised-button-file">
                  <Button
                    type={"button"}
                    onclick={() => fileInputRef.current.click()}
                    classname="general-button-xl"
                  >
                    Upload CSV File
                  </Button>
                </label>
                <Button type={"submit"} classname="general-button-xl">
                  Download Invoice
                </Button>
                <Button type={"submit"} classname="general-button">
                  Create BC 2.5
                </Button>
                {/* <Button type={"submit"} classname="general-button">Download Laporan</Button>  */}
                {/* <Button type={"submit"} classname="general-button-xl">
                  Download Laporan
                </Button> */}
                <Box className="flex gap-4">
  {/* ... other buttons ... */}
  {/* <PDFDownloadLink
    document={<LaporanPemusnahanPDF />}
    fileName="LaporanPemusnahan.pdf"
  >
    {({ blob, url, loading, error }) =>
      loading ? (
        <Button classname="general-button-xl" disabled>
          Loading...
        </Button>
      ) : (
        <Button 
        type="button" // make sure it's 'button', not 'submit'
        onClick={(e) => {
          e.preventDefault();
          // Add any additional logic if needed
        }}
        classname="general-button-xl">
          Download Laporan
        </Button>
      )
    }
  </PDFDownloadLink> */}
  {/* <PDFDownloadLink
  document={<LaporanPemusnahanPDF id={id} />}
  fileName="LaporanPemusnahan.pdf"
>
  {({ blob, url, loading, error }) =>
    loading ? (
      <Button classname="general-button-xl" disabled>
        Loading...
      </Button>
    ) : (
      <Button 
        type="button"
        onclick={() => {
          window.open(url, "_blank");
        }}
        classname="general-button-xl"
      >
        Download Laporan
      </Button>
    )
  }
</PDFDownloadLink>
   */}

<PDFDownloadLink
  document={<LaporanPemusnahanPDF id={id} />} // Pass the id to the PDF component
  fileName={`LaporanPemusnahan-${id}.pdf`} // Use the id to create a unique file name
>
  {({ blob, url, loading, error }) =>
    loading ? (
      <Button classname="general-button-xl" disabled>
        Loading...
      </Button>
    ) : (
      <Button 
        type="button"
        onclick={() => {
          // No need for window.open, PDFDownloadLink handles the download
        }}
        classname="general-button-xl"
      >
        Download Laporan
      </Button>
    )
  }
</PDFDownloadLink>
</Box>
                {/* <Box className="sm:flex-row flex flex-cols gap-4">
                  {!isLoading && partrequests?.data.PrintData ? (
                    <PDFDownloadLink
                      document={
                        <PartRequestPDF
                          requestData={partrequests?.data}
                          approvalList={
                            activeapprovalsstatus === "done"
                              ? activeapprovals.data
                              : []
                          }
                        />
                      }
                      fileName={partrequests?.data.Request.PartRequestCode}
                    >
                      {({ blob, url, loading, error }) =>
                        loading ? (
                          <button type="button" className="general-button">
                            Loading...
                          </button>
                        ) : (
                          <button type="button" className="general-button">
                            Print
                          </button>
                        )
                      }
                    </PDFDownloadLink>
                  ) : (
                    <></>
                  )}
                </Box> */}
                <Button type={"submit"} classname="general-button">
                  Execute
                </Button>
              </div>
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 81,
                    }}
                  >
                    <label className="label-group">Reference</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      type="text"
                      name=""
                      id=""
                      disabled
                      className="input-group !w-full"
                      placeholder="Reference"
                      value={disposalCode}
                      onInput={(e) => setDisposalCode(e.target.value)}
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 81,
                    }}
                  >
                    <label className="label-group ">Name</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      type="text"
                      name=""
                      id=""
                      disabled
                      className="input-group !w-full"
                      placeholder="Event Name"
                      value={name}
                      onInput={(e) => setName(e.target.value)}
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 81,
                    }}
                  >
                    <label className="label-group">Type</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      class="input-group justify-end input !w-full"
                      placeholder="Type"
                      type="text"
                      disabled
                      value={eventType}
                    />
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Disposal Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <DatePicker
                      date={executionDate}
                      onDateChange={setExecutionDate}
                      locale={enGB}
                      format="dd MMM yyyy"
                      minimumDate={new Date()}
                    >
                      {({ inputProps, focused }) => (
                        <input
                          className={
                            "input-group justify-end input !w-full" +
                            (focused ? " -focused" : "")
                          }
                          disabled
                          {...inputProps}
                          placeholder="Disposal Date"
                        />
                      )}
                    </DatePicker>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Period Start Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <DatePicker
                      date={startDate}
                      onDateChange={setStartDate}
                      locale={enGB}
                      format="dd MMM yyyy"
                      minimumDate={new Date()}
                    >
                      {({ inputProps, focused }) => (
                        <input
                          className={
                            "input-group justify-end input !w-full" +
                            (focused ? " -focused" : "")
                          }
                          {...inputProps}
                          placeholder="Period Start Date"
                          disabled
                          value={
                            startDate
                              ? new Date(startDate).toLocaleDateString(
                                "en-GB",
                                {
                                  year: "numeric",
                                  month: "long",
                                  day: "numeric",
                                }
                              )
                              : ""
                          }
                        />
                      )}
                    </DatePicker>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 150,
                    }}
                  >
                    <label className="label-group ">Period End Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <DatePicker
                      date={endDate}
                      onDateChange={setEndDate}
                      locale={enGB}
                      format="dd MMM yyyy"
                      minimumDate={new Date()}
                    >
                      {({ inputProps, focused }) => (
                        <input
                          className={
                            "input-group justify-end input !w-full" +
                            (focused ? " -focused" : "")
                          }
                          {...inputProps}
                          placeholder="Period End Date"
                          disabled
                          value={
                            endDate
                              ? new Date(endDate).toLocaleDateString("en-GB", {
                                year: "numeric",
                                month: "long",
                                day: "numeric",
                              })
                              : ""
                          }
                        />
                      )}
                    </DatePicker>
                  </Box>
                </Box>
              </Box>
              {canApprove ? (
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "end",
                    flexDirection: "column",
                    width: "400px",
                    pr: "48px",
                  }}
                >
                  <Button
                    type={"button"}
                    classname="general-button"
                    onclick={() => setEventRequest("Done")}
                  >
                    Approve
                  </Button>

                  <Button
                    type={"button"}
                    classname="general-button"
                    onclick={() => setEventRequest("Rejected")}
                  >
                    Reject
                  </Button>
                </Box>
              ) : (
                ""
              )}
            </Box>
          </form>
          <div>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Requests" />
              {/* <Tab label="Parts" /> */}
              <Tab label="Approval" />
              <Tab label="Documents" />
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <div className="py-3 pr-3 md:w-1/3 w-full">
                <div className="flex gap-3">
                  {/* <Button type={"submit"} classname="general-button" onclick={() => setModalOpen(true)}>Add</Button>  */}
                  {/* <Button type={"submit"} classname="general-button">Get Sys Qty</Button> { /* styling general custom-style.css */}
                </div>
              </div>
              <ViewDisposalEventRequestTable type="Ringi" />
            </TabPanel>
            {/* <TabPanel value={activeValue} index={1}>
              <div className="py-3 pr-3 md:w-1/3 w-full">
                <div className="flex gap-3">
                </div>
              </div>
              <PartsDisposalEventTable data={tableData} />
              {errors.tableData && (
                <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                  {errors.tableData}
                </span>
              )}
            </TabPanel> */}
            <TabPanel value={activeValue} index={1}>
              <div className="px-2 py-6">
                {/* <ApprovalDisposalEventTable /> */}
                <ApproverDisposalEventTable
                  isView
                  loading={approvalSettingStatus === "loading"}
                  data={approvalSetting}
                />
              </div>
            </TabPanel>
            <TabPanel value={activeValue} index={2}>
              <div className="px-2 py-6">
                <DocumentTable requestId={id} />
              </div>
            </TabPanel>
          </div>
        </Paper>
      </Grid>
    </Layout>
  );
};

export default ViewDisposalEvent;
