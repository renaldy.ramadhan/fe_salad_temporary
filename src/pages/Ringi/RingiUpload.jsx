import React, { useState } from "react";
import Layout from "../Layout/Layout";
import Modal from "../../components/modals/Modal";
import { DatePicker, DateRangePicker } from "react-nice-dates";
import Button from "../../components/buttons/Button";
import Tabs from "../../components/menu/Tabs";
import { enGB } from "date-fns/locale";
import useSearchStore from "../../store/searchStore";
import ViewDisposalEventRequestTable from "../../components/tables/disposal/event/ViewDisposalEventRequestTable";
import ApprovalDisposalEventTable from "../../components/tables/disposal/event/ApprovalDisposalEventTable";
import PartsDisposalEventTable from "../../components/tables/disposal/event/PartsGeneralDisposalEventTable";
import PartsRingiDisposalEventTable from "../../components/tables/disposal/Ringi/PartsRingiDisposalEventTable";
import Papa from "papaparse";

const RingiUpload = () => {
  const [requestDate, setRequestDate] = useState(new Date());
  const [amount, setAmount] = useState();
  const [selectedType, setSelectedType] = useState(""); // State to track selected type
  const [isModalOpen, setModalOpen] = useState(false);
  // const tableData = useSearchStore((state) => state.tableData);
  const [tableData, setTableData] = useState([]);
  const [csvFile, setCsvFile] = useState(null);

  const handleTypeChange = (event) => {
    setSelectedType(event.target.value);
  };
  const handleFileUpload = (event) => {
    const file = event.target.files[0];
    if (file) {
      Papa.parse(file, {
        header: true,
        complete: (results) => {
          setTableData(results.data);
        },
        error: (error) => {
          console.error("CSV Parsing Error:", error);
        },
      });
    }
  };

  const handleUpload = () => {
    if (csvFile) {
      const reader = new FileReader();
      reader.onload = (event) => {
        const csv = event.target.result;
      };
      reader.readAsText(csvFile);
    }
  };

  return (
    <Layout>
      <div className="content-positioning pl-3 md:pl-48 md:pt-38 pr-2 w-full">
        <form name="view-disposal-event">
          <div className="sm:flex-row justify-between flex flex-col mb-6 pr-6">
            <h1 className="page-title">View Disposal Event</h1>
            <div className="flex gap-2 h-[5vh]">
              <label htmlFor="file-upload" className="general-button">
                Upload Ringi
              </label>
              <input
                id="file-upload"
                type="file"
                accept=".csv"
                onChange={handleFileUpload}
                style={{ display: "none" }}
              />
              <Button type={"submit"} classname="general-button">
                Download Invoice
              </Button>
              <Button type={"submit"} classname="general-button">
                Create BC 2.5
              </Button>
              <Button type={"submit"} classname="general-button">
                Download Laporan
              </Button>
              <Button type={"submit"} classname="general-button">
                Upload Laporan
              </Button>
              <Button type={"submit"} classname="general-button">
                Execute
              </Button>
            </div>
          </div>
          <div className="flex sm:flex-row flex-col">
            <div className="flex flex-col basis-1/2">
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group ">Period Start</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3 ">
                  <DatePicker
                    date={requestDate}
                    onDateChange={setRequestDate}
                    locale={enGB}
                    format="dd MMM yyyy"
                    minimumDate={new Date()}
                  >
                    {({ inputProps, focused }) => (
                      <input
                        className={
                          "input-group justify-end input" +
                          (focused ? " -focused" : "")
                        }
                        {...inputProps}
                        placeholder="Request Date"
                        value={
                          requestDate
                            ? new Date(requestDate).toLocaleDateString(
                                "en-GB",
                                {
                                  year: "numeric",
                                  month: "long",
                                  day: "numeric",
                                }
                              )
                            : ""
                        }
                        disabled
                      />
                    )}
                  </DatePicker>
                </div>
              </div>
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group ">Period End</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3 ">
                  <DatePicker
                    date={requestDate}
                    onDateChange={setRequestDate}
                    locale={enGB}
                    format="dd MMM yyyy"
                    minimumDate={new Date()}
                  >
                    {({ inputProps, focused }) => (
                      <input
                        className={
                          "input-group justify-end input" +
                          (focused ? " -focused" : "")
                        }
                        {...inputProps}
                        placeholder="Request Date"
                        value={
                          requestDate
                            ? new Date(requestDate).toLocaleDateString(
                                "en-GB",
                                {
                                  year: "numeric",
                                  month: "long",
                                  day: "numeric",
                                }
                              )
                            : ""
                        }
                        disabled
                      />
                    )}
                  </DatePicker>
                </div>
              </div>
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group">Type</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3">
                  <select
                    id="type"
                    className="input-group"
                    onChange={handleTypeChange} // Handle type selection
                    value={selectedType}
                  >
                    {" "}
                    {/* styling general custom-style.css */}
                    <option hidden> -- Select Type -- </option>
                    <option value="General">General</option>
                    <option value="Ringi">Ringi</option>
                  </select>
                </div>
              </div>
            </div>
            <div className="flex flex-col basis-1/2">
              <div className="flex flex-row px-6 py-4">
                <div className="basis-1/3">
                  <label className="label-group ">Date</label>{" "}
                  {/* styling general custom-style.css */}
                </div>
                <div className="basis-2/3 ">
                  <DatePicker
                    date={requestDate}
                    onDateChange={setRequestDate}
                    locale={enGB}
                    format="dd MMM yyyy"
                    minimumDate={new Date()}
                  >
                    {({ inputProps, focused }) => (
                      <input
                        className={
                          "input-group justify-end input" +
                          (focused ? " -focused" : "")
                        }
                        {...inputProps}
                        placeholder="Request Date"
                        value={
                          requestDate
                            ? new Date(requestDate).toLocaleDateString(
                                "en-GB",
                                {
                                  year: "numeric",
                                  month: "long",
                                  day: "numeric",
                                }
                              )
                            : ""
                        }
                        disabled
                      />
                    )}
                  </DatePicker>
                </div>
              </div>
            </div>
          </div>
        </form>
        <div>
          <Tabs>
            {{
              label: "Requests",
              value: "requests",
              component: (
                <div>
                  <div className="py-3 pr-3 md:w-1/3 w-full"></div>
                  {console.log(tableData)}
                  <ViewDisposalEventRequestTable data={tableData} />
                  <Modal
                    isOpen={isModalOpen}
                    onClose={() => setModalOpen(false)}
                  />
                </div>
              ),
            }}
            {{
              label: "Parts",
              value: "parts",
              component: (
                <div className="my-4">
                  {selectedType === "General" ? (
                    <PartsDisposalEventTable data={tableData} />
                  ) : selectedType === "Ringi" ? (
                    <PartsRingiDisposalEventTable data={tableData} />
                  ) : null}
                </div>
              ),
            }}
            {{
              label: "Approvals",
              value: "approvals",
              component: (
                <div className="my-4">
                  <ApprovalDisposalEventTable />
                </div>
              ),
            }}
          </Tabs>
        </div>
      </div>
    </Layout>
  );
};

export default RingiUpload;
