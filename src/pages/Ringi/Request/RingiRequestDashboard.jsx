// import React, { useState, useEffect, useContext } from "react";
// import { useNavigate } from "react-router-dom";
// import axios from "axios";
// import Layout from "../../Layout/Layout";
// // import LoadingSpinner from "../../../components/animations/loading/LoadingSpinner";
// import { KeycloakContext } from "../../../contexts/KeycloakProvider";
// import Button from "@mui/material/Button";
// import DisposalRequestTable from "../../../components/tables/disposal/request/DisposalRequestTable";
// import { Typography } from "@mui/material";
// import Grid from "@mui/material/Grid";
// import Paper from "@mui/material/Paper";
// import ActiveApproverTables from "../../../components/tables/Approver/ActiveApproverTables";
// import RingiRequestActivity from "../../../components/tables/disposal/Ringi/RingiRequestActivity";

// const RingiRequestDashboard = () => {
//   const navigate = useNavigate();
//   const PART_SERVICE_BASE_URL = process.env.REACT_APP_API_URL_Part_Service;

//   const [data, setData] = useState([]);
//   const [loading, setLoading] = useState(true);

// //   const fetchData = async () => {
// //     try {
// //       const response = await axios.get(
// //         `${PART_SERVICE_BASE_URL}request?type=Ringi`,
// //         {
// //           // headers: {
// //           //   Authorization: "Bearer " + window.localStorage.getItem("token"),
// //           // },
// //         }
// //       );

//       console.log(response.data);
//       setData(response.data);
//       setLoading(false);
//     } catch (error) {
//       console.error(error);
//       setLoading(false);
//     }
//   };

//   useEffect(() => {
//     fetchData();
//   }, []);


//   return (
//     <Layout>
//       <Grid item xs={12} md={12} lg={12}>
//         {/* <div className="pl-3 md:pl-48 pt-32 md:pt-38 md:pr-4 w-full flex flex-col justify-center"> */}
//         <Paper
//           sx={{
//             p: 2,
//             display: "flex",
//             flexDirection: "column",
//             height: "inherit",
//           }}
//         >
//           <Typography variant="h5" component="div" sx={{ mb: 2 }}>Disposal Ringi Request </Typography>
//           <div className="flex justify-start gap-4 mb-4 h-10 md:h-10 text-sm">
//           <Button
//                 color="primary"
//                 style={{ backgroundColor: "#17479d" }}
//                 variant="contained"
//                 onClick={() =>
//                   navigate("/ringi/request/create", {
//                     state: { type: "Ringi" },
//                   })
//                 }
//               >
//                 Create
//               </Button>
//           </div>



//         <div className="w-full  mb-8 md:mb-16 ">
//           <div className="w-full max-h-[34rem] relative overflow-y">
//             {/* <DisposalRequestTable data={data} /> */}
//             <RingiRequestActivity/>
//           </div>
//           <div className="flex flex-col w-full mt-5">
//             <h1 className="page-title">
//               Disposal Ringi Activity (Need Approval)
//             </h1>
//           </div>
//           <div className="max-h-[34rem] relative overflow-y w-full">
//           <ActiveApproverTables/>
//           </div>
//         </div>
//         </Paper>
//         </Grid>
//     </Layout>
//   );
// };

// export default RingiRequestDashboard;

import React, { useState, useEffect, useContext, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Typography from "@mui/material/Typography";
import Button from '@mui/material/Button';
import Layout from "../../Layout/Layout";
import ActiveApproverTables from "../../../components/tables/Approver/ActiveApproverTables";
import { KeycloakContext } from "../../../contexts/KeycloakProvider";
import PartRequestActivity from "../../../components/tables/part-request/PartRequestActivity";
import { checkApprover } from "../../../slices/ApprovalSlice";

import { useDispatch, useSelector } from "react-redux";
import {
  retrievePartRequests,
  getPartRequest,

} from "../../../slices/PartRequstSlice";

import {
  getActiveApprovalByNikToken,
  getActiveApprovalByNikAndAssigned,
  approvalAction,
} from "../../../slices/ActiveApprovalSlice";

import {
  getNikApproval,
} from "../../../slices/StagingUserSlice";
import RingiRequestActivity from "../../../components/tables/disposal/Ringi/RingiRequestActivity";
import { retrieveRingiRequest } from "../../../slices/RingiRequestSlice";

const RingiRequestDashboard = () => {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  // const activeapprovals = useSelector(state => state.activeapprovals.activeApprovalGetNikAssigned)
  // const activeapprovalsstatus = useSelector(state => state.activeapprovals.activeApprovalGetNikAssignedStatus)
  // console.log("active approval table: ", activeapprovals)
  const stagingusers = useSelector(state => state.stagingusers.approvalGetNik);
  const approver = useSelector(state => state.approvals);

  const initFetch = useCallback(() => {
    dispatch(retrieveRingiRequest());
    // dispatch(getActiveApprovalByNikToken());
    dispatch(getActiveApprovalByNikAndAssigned());
    dispatch(getNikApproval());
    dispatch(checkApprover());
  }, [dispatch])

  useEffect(() => {
    initFetch()
  }, [initFetch])

  if (stagingusers.length > 0) {
    console.log("staging users NIK:", stagingusers[0].nik)
  }

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 'inherit',
          }}
        >
          <Typography variant="h5" component="div" sx={{ mb: 2 }}>
            Ringi Request Activity (Need Approval)
          </Typography>
          <ActiveApproverTables />
        </Paper>
      </Grid>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 'inherit',
          }}
        >
          <Grid>
            <Paper
              sx={{
                p: 2,
                alignContent: 'center',
                display: 'flex',
                flexDirection: 'row',
                height: 'inherit',
                alignItems: 'center',
                justifyContent: 'space-between'
              }}
            >
              <Typography variant="h5" component="div" sx={{ justifyContent: 'center' }}>
                Ringi Request Activity
              </Typography>
              <Button color="primary" style={{ backgroundColor: "#17479d" }} variant="contained" onClick={() => navigate('/ringi/request/create', { state: { type: 'Ringi' } })}>
                Create
              </Button>
            </Paper>
          </Grid>
          {/* <PartRequestActivity/> */}
          <RingiRequestActivity />
        </Paper>
      </Grid>

    </Layout>
  );
};

export default RingiRequestDashboard;
