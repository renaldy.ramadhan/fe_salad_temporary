import React, { useState, useEffect } from "react";
import Layout from "../../../Layout/Layout";
import Modal from "../../../../components/modals/Modal";
import { DatePicker, DateRangePicker } from "react-nice-dates";
import Button from "../../../../components/buttons/Button";
import { enGB } from "date-fns/locale";
import useSearchStore from "../../../../store/searchStore";
import ViewDisposalRequestTable from "../../../../components/tables/disposal/ViewDisposalRequestTableNonSystem";
import DocumentTable from "../../../../components/tables/DocumentTable";
import ApprovalDisposalTable from "../../../../components/tables/disposal/ApprovalDisposalTable";
import ApproverTable from "../../../../components/tables/Approver/ApproverTable";
import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box } from "@mui/material";
import { useLocation, useNavigate } from "react-router-dom";
import ViewPartDiscrepancyTable from "../../../../components/tables/discrepancy/ViewPartDiscrepancyTable";
import { useParams } from "react-router-dom";
import LoadingSpinner from "../../../../components/animations/loading/LoadingSpinner";

import {
  retrievePartRequests,
  getPartRequest,
  UpdateTablePartRequest,
  getPartRequestTable,
} from "../../../../slices/PartRequstSlice";

import {
  // getActiveApprovalByRequestId,
  getActiveApprovalById,
  approvalAction,
  getApprovalStatusByRequestId,
} from "../../../../slices/ActiveApprovalSlice";

import ViewPartRingiRequestTable from "../../../../components/tables/disposal/Ringi/ViewPartRingiRequestTable";

const ViewRingiRequest = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [requestDate, setRequestDate] = useState(new Date());
  const [amount, setAmount] = useState();

  const [isModalOpen, setModalOpen] = useState(false);
  const tableData = useSearchStore((state) => state.tableData);
  const [localStateApprovals, setLocalStateApprovals] = useState([]);
  const { id, nik } = useParams(); // Get the id from the URL
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [activeValue, setActiveValue] = useState(0);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState("");

  const activeapprovals = useSelector(
    (state) => state.activeapprovals.activeApprovalGetNikAssigned
  );
  const activeapprovalsstatus = useSelector(
    (state) => state.activeapprovals.activeApprovalGetNikAssignedStatus
  );
  const ringiRequestDetails = useSelector(
    (state) => state.partrequests.partGetRequest // This will hold your fetched data
  );
  const checkactiveapprovalsstatusstatus = useSelector(
    (state) => state.activeapprovals.activeApprovalStatusGetRequestIdStatus
  );

  useEffect(() => {
    setIsLoading(true);
    dispatch(getActiveApprovalById(id))
      .then(unwrapResult)
      .then((data) => {
        setLocalStateApprovals(data);
      });
    dispatch(getPartRequest(id));
    dispatch(getApprovalStatusByRequestId(id));
    setIsLoading(false);
  }, [dispatch, id]);

  const handleDataFromChild = (data) => {
    console.log(data);
    // setTableData(data);
    dispatch(getPartRequestTable(data));
  };

  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };

  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };
  const [errors, setErrors] = useState({});
  const partrequestsitemsdata = useSelector(
    (state) => state.partrequests.partGetRequestItemsData
  );

  const [totalAmount, setTotalAmount] = useState(0);

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString();

    return `${year}-${month}-${day}`;
  };

  const handleApproval = async (status) => {
    setIsSubmitting(true);
    const parsedId = parseInt(id, 10);

    try {
      const actionResult = await dispatch(
        approvalAction({
          IdActiveRequestList: parsedId,
          Status: status,
          // Notes: document.getElementById("remarks").value,
          Nik: nik,
        })
      );
      unwrapResult(actionResult);
      setShowSuccess(true);
      setShowFailure(false);
      setMessage(
        `Request ${
          status === "Done" ? "Approval Success" : "Approval Rejected"
        }!`
      );

      setTimeout(() => {
        setShowSuccess(false);
        // navigate("/discrepancy");
      }, 3000);

      const updatedData = await dispatch(getActiveApprovalById(id)).then(
        unwrapResult
      );
      setLocalStateApprovals(updatedData);
    } catch (error) {
      console.error("Approval action failed:", error);
      setShowSuccess(false);
      setShowFailure(true);
      setMessage("Approval Failed!");
      setTimeout(() => {
        setShowFailure(false);
      }, 3000);
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleApprove = () => handleApproval("Done");
  const handleReject = () => handleApproval("Rejected");

  console.log("status : ", checkactiveapprovalsstatusstatus);

  if (isLoading) {
    return <LoadingSpinner />;
  }

  return (
    <Layout>
      {isLoading && <LoadingSpinner />}
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <form name="create-part-request">
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                mb: 1,
                pr: 6,
                justifyContent: "space-between",
              }}
            >
              <h1 className="page-title">View Ringi Request</h1>
              <Box className="sm:flex-row flex flex-cols gap-4">
                {/* <button type="button" className="general-button" onClick={handlePrint}>Print</button> */}

                <button
                  type="button"
                  className="general-button"
                  onClick={handleApprove}
                >
                  Approve
                </button>
                <button
                  type="button"
                  className="general-button"
                  onClick={handleReject}
                >
                  Reject
                </button>
              </Box>
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                width: "800px",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group ">Event</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={
                        ringiRequestDetails.data?.Request?.DisposalGroup?.Name ||
                        "Event A"
                      }
                      readOnly
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group">Total Amount</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3" // styling general custom-style.css
                      value={amount}
                      type="number"
                      onChange={(e) => {
                        setAmount(e.target.value);
                      }}
                    />
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group">Plant</label>{" "}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={
                        ringiRequestDetails.data?.Request?.LocationPlant
                          .PlantCode || "Default Plant Name"
                      }
                      readOnly
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group ">Location</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    {/* <select id="location" className="input-group">
                      {" "}
                      
                      <option hidden> -- Select Location -- </option>
                      <option value="JS">Jakarta Selatan</option>
                      <option value="JU">Jakarta Utara</option>
                      <option value="JT">Jakarta Timur</option>
                      <option value="BE">Bekasi</option>
                      <option value="TS">Tangerang Selatan</option>
                    </select> */}
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={
                        ringiRequestDetails.data?.Request?.LocationPlant
                          .LocationName || "Default"
                      }
                      readOnly
                    />
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 100,
                    }}
                  >
                    <label className="label-group ">Request Date</label>{" "}
                    {/* styling general custom-style.css */}
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                    }}
                  >
                    <input
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={
                        formatDate(
                          ringiRequestDetails.data?.Request?.RequestDate
                        ) || "Default"
                      }
                      style={{
                        cursor: "not-allowed",
                      }}
                      disabled
                      onChange={(e) => {
                        onchange(e.target.value);
                      }}
                    />
                  </Box>
                </Box>
              </Box>
            </Box>
          </form>
          <Box>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Parts" />
              <Tab label="Approval" />
              <Tab label="Documents" />
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <ViewPartRingiRequestTable onDataSend={handleDataFromChild} />
              {errors.tableData && (
                <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                  {errors.tableData}
                </span>
              )}
            </TabPanel>
            <TabPanel value={activeValue} index={1}>
              <div className="px-2 py-6">
                <ApproverTable data={localStateApprovals} />
                {console.log("localstateapproval :", localStateApprovals)}
              </div>
            </TabPanel>
            <TabPanel value={activeValue} index={2}>
              <div className="px-2 py-6">
                <DocumentTable />
              </div>
            </TabPanel>
          </Box>
        </Paper>
      </Grid>
    </Layout>
  );
};

export default ViewRingiRequest;
