// import React, { useEffect, useState } from "react";
// import {
//   Document,
//   Page,
//   Text,
//   View,
//   StyleSheet,
//   PDFViewer,
// } from "@react-pdf/renderer";
// import { TableHeader } from "../../../PartRequest/View/PDF/TableHeader.jsx";
// import { TableRow } from "../../../PartRequest/View/PDF/TableRow.jsx";
// import { TableApprovalHeader } from "../../../PartRequest/View/PDF/TableApprovalHeader.jsx";
// import { TableApprovalRow } from "../../../PartRequest/View/PDF/TableApprovalRow.jsx";
// import { TableSignHeader } from "../../../PartRequest/View/PDF/TableSignHeader.jsx";
// import { TableSignRow } from "../../../PartRequest/View/PDF/TableSignRow.jsx";
// import { SummaryDataRow, SummaryTableHeader, TableRowPemusnahan } from "../../../PartRequest/View/PDF/TableRowPemusnahan.jsx";

// const styles = StyleSheet.create({
//   page: {
//     fontFamily: "Helvetica",
//     fontSize: 8,
//     padding: 40,
//     lineHeight: 1.5,
//     flexDirection: "column",
//   },
//   viewer: {
//     width: window.innerWidth, //the pdf viewer will take up all of the width and height
//     height: window.innerHeight,
//   },
//   companyName: {
//     fontFamily: "Helvetica-Bold",
//     marginBottom: 20,
//     fontSize: 8,
//   },
//   documentTitle: {
//     fontFamily: "Helvetica-Bold",
//     marginBottom: 8,
//     fontSize: 14,
//     textAlign: "center",
//   },
//   documentSubTitle: {
//     marginBottom: 12,
//     fontSize: 10,
//     textAlign: "center",
//   },
//   descriptionContainer: {
//     flexDirection: "row",
//     justifyContent: "space-between",
//     marginTop: 14,
//     marginBottom: 10,
//     flexWrap: "wrap",
//   },
//   tableView: {
//     marginBottom: 15,
//   },
//   approvalView: {
//     flexDirection: "row",
//   },
//   approvalTitle: {
//     fontFamily: "Helvetica-Bold",
//     textAlign: "center",
//   },
//   tableApprovalView: {
//     width: "45%",
//   },
//   tableSignView: {
//     width: "55%",
//     marginLeft: 25,
//   },
// });



// export const LaporanPemusnahanPDF = ({ backendData, approvalList }) => {
//   const [approverWHC, setApproverWHC] = useState(null);


  
//   const formatDate = (dateString) => {
//     const date = new Date(dateString);
//     const day = date.getDate().toString().padStart(2, "0"); 
//     const month = (date.getMonth() + 1).toString().padStart(2, "0");
//     const year = date.getFullYear();
//     return `${day}/${month}/${year}`;
//   };

  
//   useEffect(() => {
//     if (approvalList) {
//       const approverData = approvalList;
//       const whcapprover = approverData.filter((approver) => {
//         return approver.Department.DepartmentName === "WHC";
//       });
//       setApproverWHC(whcapprover);
//     }
//     return () => {
//       setApproverWHC(null);
//     };
//   }, [approvalList]);


  
//   return (
//     <Document>
//       {/*render a single page*/}
//       <Page size="A4" orientation="portrait" style={styles.page}>
//         <Text style={styles.companyName}>PT EPSON INDUSTRY INDONESIA</Text>
//         <View>
//           <Text style={styles.documentTitle}>KONFIRMASI LAPORAN HASIL PEMUSNAHAN</Text>
//           <Text style={styles.documentSubTitle}>
//             DISPOSAL CATEGORY : REGULER / RINGI{" "}
//           </Text>
//         </View>
//         <Text>
//           Date: &nbsp;
//           {/* {requestData.Request !== undefined
//             ? formatDate(requestData.Request.RequestDate)
//             : ""} */}
//         </Text>
//         <View style={styles.descriptionContainer}>
//           <Text>
//           This is to declare that the part Disposal in the PIB number has been done under the destruction in accordance with the attached data summary :
//           </Text>

//         </View>
//         <View style={styles.tableView}>
//           <SummaryTableHeader />
//           {/* <TableRowPemusnahan
//             itemsData={
//               requestData.ItemsData !== undefined ? requestData.ItemsData : []
//             }
//           /> */}
//           <SummaryDataRow data={backendData} />

//         </View>
//         <View style={styles.approvalView}>
//           <View style={styles.tableApprovalView}>
//             <Text style={styles.approvalTitle}>Approval List</Text>
//             <TableApprovalHeader />
//             <TableApprovalRow approvalList={approvalList} />
//           </View>
//           <View style={styles.tableSignView}>
//             <TableSignHeader />
//             <TableSignRow
//               // requestor={approvalList[0]}
//               approverWhc={approverWHC?.[0]}
//             />
//           </View>
//         </View>
//       </Page>
//     </Document>
//   );
// };

import React, { useEffect, useState } from "react";
import axios from 'axios';
import {
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  PDFViewer,
} from "@react-pdf/renderer";
import { TableHeader } from "../../../PartRequest/View/PDF/TableHeader.jsx";
import { TableRow } from "../../../PartRequest/View/PDF/TableRow.jsx";
import { TableApprovalHeader } from "../../../PartRequest/View/PDF/TableApprovalHeader.jsx";
import { TableApprovalRow } from "../../../PartRequest/View/PDF/TableApprovalRow.jsx";
import { TableSignHeader } from "../../../PartRequest/View/PDF/TableSignHeader.jsx";
import { TableSignRow } from "../../../PartRequest/View/PDF/TableSignRow.jsx";
import { SummaryTableHeader, SummaryDataRow } from "../../../PartRequest/View/PDF/TableRowPemusnahan.jsx";

const styles = StyleSheet.create({
  page: {
    fontFamily: "Helvetica",
    fontSize: 8,
    padding: 40,
    lineHeight: 1.5,
    flexDirection: "column",
  },
  viewer: {
    width: window.innerWidth, //the pdf viewer will take up all of the width and height
    height: window.innerHeight,
  },
  companyName: {
    fontFamily: "Helvetica-Bold",
    marginBottom: 20,
    fontSize: 8,
  },
  documentTitle: {
    fontFamily: "Helvetica-Bold",
    marginBottom: 8,
    fontSize: 14,
    textAlign: "center",
  },
  documentSubTitle: {
    marginBottom: 12,
    fontSize: 10,
    textAlign: "center",
  },
  descriptionContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 14,
    marginBottom: 10,
    flexWrap: "wrap",
  },
  tableView: {
    marginBottom: 15,
  },
  approvalView: {
    flexDirection: "row",
  },
  approvalTitle: {
    fontFamily: "Helvetica-Bold",
    textAlign: "center",
  },
  tableApprovalView: {
    width: "45%",
  },
  tableSignView: {
    width: "55%",
    marginLeft: 25,
  },
});

export const LaporanPemusnahanPDF = ({ approvalList , id }) => {
  const [backendData, setBackendData] = useState(null);
  const [approverWHC, setApproverWHC] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(`http://34.101.177.226:8080/disposal-group/generate/pdf/${id}`);
        setBackendData(response.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (approvalList) {
      const whcapprover = approvalList.filter((approver) => approver.DepartmentName === "WHC");
      setApproverWHC(whcapprover);
    }
    return () => {
      setApproverWHC(null);
    };
  }, [approvalList]);

  if (!backendData) {
    return <Text>Loading...</Text>;
  }

  return (
    <Document>
      <Page size="A4" orientation="portrait" style={styles.page}>
        <Text style={styles.companyName}>PT EPSON INDUSTRY INDONESIA</Text>
        <View>
          <Text style={styles.documentTitle}>KONFIRMASI LAPORAN HASIL PEMUSNAHAN</Text>
          <Text style={styles.documentSubTitle}>
            DISPOSAL CATEGORY : REGULER / RINGI{" "}
          </Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text>
            This is to declare that the part Disposal in the PIB number has been done under the destruction in accordance with the attached data summary :
          </Text>
        </View>
        <View style={styles.tableView}>
          <SummaryTableHeader />
          <SummaryDataRow data={backendData} />
        </View>
        <View style={styles.approvalView}>
          <View style={styles.tableApprovalView}>
            <Text style={styles.approvalTitle}>Approval List</Text>
            <TableApprovalHeader />
            <TableApprovalRow approvalList={approvalList} />
          </View>
          <View style={styles.tableSignView}>
            <TableSignHeader />
            <TableSignRow approverWhc={approverWHC?.[0]} />
          </View>
        </View>
      </Page>
    </Document>
  );
};

