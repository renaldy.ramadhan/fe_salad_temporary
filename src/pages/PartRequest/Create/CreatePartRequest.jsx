import React, { useState, useContext, useEffect, useCallback } from "react";
import Layout from "../../Layout/Layout";
import SubmitLoadingSpinner from "../../../components/animations/loading/SubmitLoadingSpinner";
import FailureCheckmark from "../../../components/animations/checkmark/FailureCheckmark";
import SuccessCheckmark from "../../../components/animations/checkmark/SuccessCheckmark";
import Button from "../../../components/buttons/Button";
import Modal from "../../../components/modals/Modal";
import axios from "axios";
import CreatePartRequestTable from "../../../components/tables/part-request/CreatePartRequestTable";
import { validateForm } from "./Validation"; // validation form
import { enGB } from "date-fns/locale";
import { DateRangePicker, START_DATE, END_DATE } from "react-nice-dates";
import "react-nice-dates/build/style.css";
import "../../../css/date-picker/date-picker.css";
import DocumentTableCreate from "../../../components/tables/DocumentTableCreate";
import { useNavigate } from "react-router-dom";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box, Typography, useTheme } from "@mui/material";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

// import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import {
  fetchLocation,
  fetchPurchasingGroup,
  fetchPlant,
  fetchReason,
} from "../../../slices/GlobalSlice";
import {
  createPartRequest,
  getPartRequestTable,
} from "../../../slices/PartRequstSlice";

const CreatePartRequest = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  //date picker
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  // animation success & failure
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState(false);

  // selected item
  const [locationSelectId, setLocationSelectId] = useState("");
  const [purchasingGroupSelectId, setPurchasingGroupSelectId] = useState("");
  const [plantSelectId, setPlantSelectId] = useState("");
  const [reasonSelectId, setReasonSelectId] = useState("");

  //Table data from pass to child
  const [tableData, setTableData] = useState([]);

  //files from child
  const [files, setFiles] = useState([]);

  // error validation
  const [errors, setErrors] = useState({});

  // Modal & Table data
  const [isModalOpen, setModalOpen] = useState(false);

  const [validQuantity, setValidQuantity] = useState(false);

  //for redirect
  // const history = useHistory();
  const navigate = useNavigate();

  const [activeValue, setActiveValue] = useState(0);
  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };

  const handleDataFromChild = async (data) => {
    console.log(data);
    //dispatch(getPartRequestTable(data))
    setTableData(data);
  };

  const handleStatusFromChild = async (valid) => {
    console.log(valid);
    setValidQuantity(valid);
  };

  const handleFilesFromChild = (files) => {
    console.log(files);
    setFiles(files);
  };

  const handleGetFiles = () => {
    return files;
  };

  const handleGetTableData = () => {
    return tableData;
  };

  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  const createRequest = useSelector(
    (state) => state.partrequests.createPartRequest
  );
  const tableLocation = useSelector((state) => state.globals.fetchLocation);
  const tablePlant = useSelector((state) => state.globals.fetchPlant);
  const tablePurc = useSelector((state) => state.globals.fetchPurchasingGroup);
  const tableReason = useSelector((state) => state.globals.fetchReason);
  const [totalAmount, setTotalAmount] = useState(0);
  //const tableData = useSelector(state => state.partrequests.partGetRequestTable);

  const dispatch = useDispatch();

  const plantcode = tablePlant.map((plant) => plant.PlantCode);
  const uniqueplantcode = [...new Set(plantcode)];

  useEffect(() => {
    const total = tableData.reduce((sum, row) => {
      const amount = parseFloat(row.Amount || 0);
      return sum + amount;
    }, 0);
    setTotalAmount(total.toFixed(2));
  }, [tableData]);

  const initFetch = useCallback(() => {
    // dispatch(getActiveApprovalByNikToken());
    dispatch(fetchPlant());
    dispatch(fetchPurchasingGroup());
    dispatch(fetchReason("Request"));
  }, [dispatch]);

  useEffect(() => {
    initFetch();
  }, [initFetch]);

  function dispatchLocation(id) {
    setTableData([]);
    setPlantSelectId(id);
    dispatch(fetchLocation(id));
  }

  function dispatchPlant(id) {
    setTableData([]);
    setLocationSelectId(id);
  }

  function loadParts(idPurchasingGroup) {
    setTableData([]);
    setPurchasingGroupSelectId(idPurchasingGroup);
    // dispatch(searchParts(''),idPurchasingGroup);
  }

  const createPartRequestAction = (e) => {
    //setValidationErrors({})
    e.preventDefault();

    const newError = validateForm(
      tableData,
      locationSelectId,
      purchasingGroupSelectId,
      plantSelectId,
      reasonSelectId,
      startDate,
      endDate
    );
    setErrors(newError);
    console.log(newError);

    if (Object.keys(newError).length === 0 && validQuantity) {
      setIsLoading(true);
      setIsSubmitting(true);
      submitData();
    }
  };

  const submitData = async () => {
    const selectedPlant = tablePlant.find(
      (plant) => plant.PlantCode === plantSelectId
    );

    let payload = {
      UserName: "Dape",
      IdReason: parseInt(reasonSelectId, 10),
      PurchasingGroupCodeFk: purchasingGroupSelectId,
      Type: "Request",
      RequestDate: startDate ? startDate.toISOString() : null,
      DueDate: endDate ? endDate.toISOString() : null,
      PlantName: selectedPlant ? selectedPlant.LocationName : "",
      LocationCode: locationSelectId ? locationSelectId : "",
      RequestLocation: locationSelectId, // harusnya berdasarkan dari yang login
      DestroyLocation: null,
      DisposalType: "System",
      Items: tableData.map((item) => ({
        ItemCode: item.ItemCode,
        RequestQuantity: parseInt(item.RequestQuantity, 10),
        ApprovedQuantity: parseInt(item.RequestQuantity, 10),
        AdjustmentQuantity: parseInt(item.AdjustmentQty, 10),
        Notes: item.Remark,
        PricePerUnit: item.PricePerUnit,
      })),
    };
    let payloadString = JSON.stringify(payload);
    try {
      // dispatch(
      //   createPartRequest({ files: files, jsonData: payloadString })
      // ).then(unwrapResult);

      dispatch(
        createPartRequest({ files: files, jsonData: payloadString })
      ).then((action) => {
        const resultAction = unwrapResult(action);
        if (resultAction.Id != null) {
          setShowSuccess(true);
          setMessage(`Created Part Request!`);
          setTimeout(() => {
            // setShowSuccess(true);
            navigate("/partrequest");
            // window.location.reload(); {/*sementara untuk showing */}
          }, 3000);
        } else {
          setShowFailure(true);
          setTimeout(() => {
            setShowFailure(false);
            setIsLoading(false);
          }, 2000);
          // setMessage(error);
        }
      });
    } catch (error) {
      console.log(error);
    }
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString().substr(-2);

    // Extracting time components
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");
    const seconds = date.getSeconds().toString().padStart(2, "0");

    return `${year}/${month}/${day} ${hours}:${minutes}`;
  };

  const theme = useTheme();

  return (
    <Layout>
      {isLoading && <SubmitLoadingSpinner />}
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <form
            name="create-part-request"
            onSubmit={(e) => {
              createPartRequestAction(e);
            }}
          >
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                [theme.breakpoints.down("md")]: {
                  flexDirection: "column",
                  gap: theme.spacing(2),
                },
                mb: 1,
                pr: 6,
                justifyContent: "space-between",
              }}
            >
              <h1 className="page-title w-full">Create Part Request</h1>
              <Button
                type={"submit"}
                classname="general-button"
                disabled={isSubmitting}
              >
                Submit
              </Button>
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                [theme.breakpoints.down("md")]: {
                  flexDirection: "column",
                },
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                  [theme.breakpoints.down("md")]: {
                    width: "100%",
                  },
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 81,
                    }}
                  >
                    <label className="label-group">Plant</label>
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <Select
                      id="plant"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={plantSelectId}
                      onChange={(e) => dispatchLocation(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Plant -- </MenuItem>
                      {uniqueplantcode.map((code, index) => (
                        <MenuItem key={index} value={code}>
                          {code}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.plant && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.plant}
                      </span>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                      width: 81,
                    }}
                  >
                    <label className="label-group">Location</label>
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      pr: 0,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <Select
                      id="location"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={locationSelectId}
                      onChange={(e) => dispatchPlant(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Location -- </MenuItem>
                      {tableLocation.map((plant, index) => (
                        <MenuItem key={index} value={plant.LocationCode}>
                          {plant.LocationCode + " - " + plant.LocationName}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.location && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.location}
                      </span>
                    )}
                  </Box>
                </Box>

                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 2,
                    }}
                  >
                    <label className="label-group">Purc Group</label>
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <Select
                      id="purcgroup"
                      className="py-0 px-6 max-h-10 border-black bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={purchasingGroupSelectId}
                      onChange={(e) => loadParts(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Purc Group -- </MenuItem>
                      {/* {tablePurc.map((purchasingGroup, index) => (
                        <option key={index} value={purchasingGroup.PurchasingGroupCode}>
                          {purchasingGroup.PurchasingGroupCode}
                        </option>
                      ))} */}
                      {tablePurc
                        .filter(
                          (purchasingGroup) =>
                            purchasingGroup.Plant === plantSelectId
                        )
                        .map((purchasingGroup, index) => (
                          <MenuItem
                            key={index}
                            value={purchasingGroup.PurchasingGroupCode}
                          >
                            {purchasingGroup.PurchasingGroupCode}
                          </MenuItem>
                        ))}
                    </Select>
                    {errors.purchasingGroup && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.purchasingGroup}
                      </span>
                    )}
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                  [theme.breakpoints.down("md")]: {
                    width: "100%",
                  },
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 4,
                    }}
                  >
                    <label className="label-group">Request Date</label>
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <DateRangePicker
                      startDate={startDate}
                      endDate={endDate}
                      onStartDateChange={setStartDate}
                      onEndDateChange={setEndDate}
                      minimumDate={new Date()}
                      minimumLength={1}
                      // format='dd MMM yyyy'
                      format="yyyy-MM-dd hh:mm"
                      locale={enGB}
                    >
                      {({ startDateInputProps, focus }) => (
                        <div className="date-range">
                          <input
                            className={
                              "py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3 input" +
                              (focus === START_DATE ? " -focused" : "")
                            }
                            {...startDateInputProps}
                            placeholder="Start date"
                          />
                        </div>
                      )}
                    </DateRangePicker>
                    {errors.startDate && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.startDate}
                      </span>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                    },
                    mx: 0,
                    ml: 2,
                  }}
                >
                  <Box
                    sx={{
                      mr: 4,
                      width: 98,
                    }}
                  >
                    <label className="label-group">Due Date</label>
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <DateRangePicker
                      startDate={startDate}
                      endDate={endDate}
                      onStartDateChange={setStartDate}
                      onEndDateChange={setEndDate}
                      minimumDate={new Date()}
                      minimumLength={1}
                      format="yyyy-MM-dd hh:mm"
                      locale={enGB}
                    >
                      {({ endDateInputProps, focus }) => (
                        <div className="date-range">
                          <input
                            className={
                              "py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3 input" +
                              (focus === END_DATE ? " -focused" : "")
                            }
                            {...endDateInputProps}
                            placeholder="Due date"
                          />
                        </div>
                      )}
                    </DateRangePicker>
                    {errors.endDate && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.endDate}
                      </span>
                    )}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 4,
                      width: 98,
                    }}
                  >
                    <label className="label-group">Reason</label>
                  </Box>
                  <Box
                    sx={{
                      width: 250,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                    }}
                  >
                    <Select
                      id="reason"
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={reasonSelectId}
                      onChange={(e) => setReasonSelectId(e.target.value)}
                    >
                      <MenuItem hidden> -- Select Reason -- </MenuItem>
                      {tableReason.map((reason, index) => (
                        <MenuItem key={index} value={reason.ID}>
                          {reason.ReasonName}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.tableReason && (
                      <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                        {errors.tableReason}
                      </span>
                    )}
                  </Box>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "400px",
                  [theme.breakpoints.down("md")]: {
                    width: "100%",
                  },
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    [theme.breakpoints.down("md")]: {
                      flexDirection: "column",
                      width: "100%",
                      pr: 2,
                    },
                    mx: 0,
                    ml: 2,
                    py: 1,
                  }}
                >
                  <Box
                    sx={{
                      mr: 4,
                    }}
                  >
                    <label className="label-group">Total Amount</label>
                  </Box>
                  <div className="basis-1/2">
                    <input
                      type="text"
                      className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                      value={totalAmount}
                      readOnly
                    />
                  </div>
                </Box>
              </Box>
            </Box>
          </form>
          <div>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Parts" />
              <Tab label="Documents" />
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <div className="py-3 pr-3 md:w-1/3 w-full">
                <div className="flex gap-3"></div>
              </div>
              <CreatePartRequestTable
                purcGroup={purchasingGroupSelectId}
                locationCode={locationSelectId}
                onDataSend={handleDataFromChild}
                onDataLoad={handleGetTableData}
                onStatusSend={handleStatusFromChild}
              />
              {errors.tableData && (
                <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                  {errors.tableData}
                </span>
              )}
            </TabPanel>
            <TabPanel value={activeValue} index={1}>
              <div className="px-2 py-6">
                <DocumentTableCreate
                  onDataSend={handleFilesFromChild}
                  onTableLoad={handleGetFiles}
                />
              </div>
            </TabPanel>
          </div>

          <Modal isOpen={isModalOpen} onClose={() => setModalOpen(false)} />
          <SuccessCheckmark showSuccess={showSuccess} message={message} />
          <FailureCheckmark showFailure={showFailure} message={message} />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default CreatePartRequest;
