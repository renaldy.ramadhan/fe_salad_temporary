// Validation.js
export const validateForm = (tableData, locationSelectId, purchasingGroupSelectId, plantSelectId, reasonSelectId , startDate, endDate, amount, destroyLocation) => {
    const newError = {};
  
    if (tableData.length === 0) {
        newError.tableData = 'Table data cannot be empty';
    }
    if (locationSelectId === '') {
        newError.location = 'Location cannot be empty';
    }
    if (purchasingGroupSelectId === '') {
      newError.purchasingGroup = 'Purchasing Group cannot be empty';
    }
    if (plantSelectId === '') {
      newError.plant = 'Plant cannot be empty';
    }
    if (reasonSelectId === '') {
      newError.reason = 'Reason cannot be empty';
    }
    if (startDate === null) {
      newError.startDate = 'Start Date cannot be empty';
    }
    if (endDate === null) {
      newError.endDate = 'End Date cannot be empty';
    }
    if (amount === 0) {
      newError.amount = 'Amount cannot be 0';
    }
    if(destroyLocation === '') {
      newError.destroyLocation = 'Destroy Location cannot be empty';
    }
  
    return newError;
  }
  