import React from 'react'
import { useState } from 'react'
import Layout from '../../Layout/Layout'
import LoadingSpinner from '../../../components/animations/loading/LoadingSpinner'
import FailureCheckmark from '../../../components/animations/checkmark/FailureCheckmark'
import SuccessCheckmark from '../../../components/animations/checkmark/SuccessCheckmark'
import DocumentInput from '../../../components/documents/DocumentInput'
import Button from '../../../components/buttons/Button'
import Modal from '../../../components/modals/Modal'
import Tabs from '../../../components/menu/Tabs'
import useSearchStore from '../../../store/searchStore'

import { enGB } from 'date-fns/locale'
import { DateRangePicker, START_DATE, END_DATE} from 'react-nice-dates'
import 'react-nice-dates/build/style.css'
import '../../../css/date-picker/date-picker.css'
import CreatePartRequestTable from '../../../components/tables/part-request/CreatePartRequestTable'


const EditPartRequestWHC = () => {
  
  const [isLoading, setIsLoading] = useState(false);

  // amount 
  const [amount, setAmount] = useState(1);

  //date picker 
  const [startDate, setStartDate] = useState()
  const [endDate, setEndDate] = useState()

  // animation success & failure
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState(false);

  // Modal & Table data
  const [isModalOpen, setModalOpen] = useState(false);
  const tableData = useSearchStore((state) => state.tableData);

  return (
    <Layout>
        {isLoading && (
          <LoadingSpinner/>
        )}
        <div className="content-positioning pl-3 md:pl-48 md:pt-38 pr-2 w-full">  
            {/* <form name="Edit-part-request" onSubmit={(e) => {EditPartRequestAction(e)}}> */}
              <form name="Edit-part-request">              
                <div className='sm:flex-row sm:justify-between flex flex-col mb-6 pr-6'>
                  <h1 className="page-title">Edit Part Request</h1>
                  <Button type={"submit"} classname="general-button">Submit</Button>  { /* styling general custom-style.css */ } 
                </div>
                <div className="flex sm:flex-row flex-col">
                  <div className="flex flex-col basis-1/2">
                    <div className="flex flex-row px-6 py-4">
                      <div className="basis-1/3">
                        <label className="label-group">Plant</label> { /* styling general custom-style.css */ }
                      </div>
                      <div className="basis-2/3">
                        <input 
                          className="input-group" // styling general custom-style.css
                          value="Plant C"
                          disabled
                          onChange={(e)=>{onchange(e.target.value)}}/>
                      </div>
                    </div> 
                    <div className="flex flex-row px-6 py-4">
                      <div className="basis-1/3">
                        <label className="label-group ">Location</label> { /* styling general custom-style.css */ }
                      </div>
                      <div className="basis-2/3">
                        <input 
                          className="input-group" // styling general custom-style.css
                          value="Location C"
                          disabled
                          onChange={(e)=>{onchange(e.target.value)}}/>
                      </div>
                    </div> 
                    <div className="flex flex-row px-6 py-4">
                      <div className="basis-1/3">
                        <label className="label-group ">Purc Group</label> { /* styling general custom-style.css */ }
                      </div>
                      <div className="basis-2/3">
                        <input 
                          className="input-group" // styling general custom-style.css
                          value="Group C"
                          disabled
                          onChange={(e)=>{onchange(e.target.value)}}/>
                      </div>
                    </div> 
                  </div>
                  <div className="flex flex-col basis-1/2"> 
                    <div className="flex flex-row px-6 py-4">
                      <div className="basis-1/3">
                        <label className="label-group ">Request Date</label> { /* styling general custom-style.css */ }
                      </div>
                      <div className="basis-2/3 ">
                        <DateRangePicker
                            startDate={startDate}
                            endDate={endDate}
                            onStartDateChange={setStartDate}
                            onEndDateChange={setEndDate}
                            minimumDate={new Date()}
                            minimumLength={1}
                            format='yyyy MMM dd'
                            locale={enGB} 
                          >
                              {({ startDateInputProps, focus }) => (
                                <div className='date-range'>
                                  <input disabled
                                    // styling general custom-style.css
                                    className={'input-group input' + (focus === START_DATE ? ' -focused' : '')}
                                    {...startDateInputProps}
                                    
                                    placeholder='Start date'
                                  />
                                </div>
                              )}
                        </DateRangePicker>
                      </div>
                    </div>
                    <div className="flex flex-row px-6 py-4">
                      <div className="basis-1/3">
                        <label className="label-group ">Due Date</label> { /* styling general custom-style.css */ }
                      </div>
                      <div className="basis-2/3 ">
                        <DateRangePicker
                          startDate={startDate}
                          endDate={endDate}
                          onStartDateChange={setStartDate}
                          onEndDateChange={setEndDate}
                          minimumDate={new Date()}
                          minimumLength={1}
                          format='yyyy MMM dd'
                          locale={enGB} 
                        >
                            {({ endDateInputProps, focus }) => (
                              <div className='date-range'>
                                <input disabled
                                  // styling general custom-style.css
                                  className={'input-group input' + (focus === END_DATE ? ' -focused' : '')}
                                  {...endDateInputProps}
                                  placeholder='Due date'
                                />
                              </div>
                            )}
                        </DateRangePicker>
                      </div>
                    </div>
                    <div className="flex flex-row px-6 py-4">
                      <div className="basis-1/3">
                        <label className="label-group ">Total Amount</label> { /* styling general custom-style.css */ }
                      </div>
                      <div className="basis-2/3">
                        <input disabled
                          className="input-group" // styling general custom-style.css
                          value={amount}
                          type="number"
                          onChange={(e)=>{setAmount(e.target.value)}}/>
                      </div>
                    </div>
                    <div className="flex flex-row px-6 py-4">
                      <div className="basis-1/3">
                        <label className="label-group ">Reason</label> { /* styling general custom-style.css */ }
                      </div>
                      <div className="basis-2/3">
                        <input disabled
                          className="input-group" // styling general custom-style.css
                          value="Reason C"
                          onChange={(e)=>{setAmount(e.target.value)}}/>
                      </div>
                    </div>  
                  </div> 
                </div>
              </form>
              <div>
              <Tabs>
                    {{
                      label: 'Parts',
                      value: 'parts',
                      component: (
                        <div>
                          {/* Put your Parts tab contents here */}
                          <div className="py-3 pr-3 md:w-1/3 w-full">
                            <div className="flex gap-3">
                              <Button type={"submit"} classname="general-button" onclick={() => setModalOpen(true)}>Add</Button> { /* styling general custom-style.css */ }
                              <Button type={"submit"} classname="general-button">Get Sys Qty</Button> { /* styling general custom-style.css */ }
                            </div>
                          </div>
                            {console.log(tableData)}
                            <CreatePartRequestTable data={tableData} />
                            <Modal isOpen={isModalOpen} onClose={() => setModalOpen(false)} />
                        </div>
                      ),
                    }}
                    {{
                      label: 'Documents',
                      value: 'document',
                      component: (
                        <div className="px-2 py-6">
                          <DocumentInput />
                        </div>
                      ),
                    }}
                  </Tabs>
              </div>
            
            
        </div>
        <SuccessCheckmark showSuccess={showSuccess} message={message} />
        <FailureCheckmark showFailure={showFailure} message={message} />
      </Layout>
  )
}

export default EditPartRequestWHC