import React, {
  useState,
  useContext,
  useEffect,
  useCallback,
  useRef,
} from "react";
import Layout from "../../Layout/Layout";
import SubmitLoadingSpinner from "../../../components/animations/loading/SubmitLoadingSpinner";
import FailureCheckmark from "../../../components/animations/checkmark/FailureCheckmark";
import SuccessCheckmark from "../../../components/animations/checkmark/SuccessCheckmark";
import DocumentInput from "../../../components/documents/DocumentInput";
import Button from "../../../components/buttons/Button";
import Modal from "../../../components/modals/Modal";
import ViewPartRequestTable from "../../../components/tables/part-request/ViewPartRequestTable";
import { validateForm } from "./Validation"; // validation form
import { KeycloakContext } from "../../../contexts/KeycloakProvider";
import axios from "axios";
import { enGB } from "date-fns/locale";
import { DateRangePicker, START_DATE, END_DATE } from "react-nice-dates";
import "react-nice-dates/build/style.css";
import "../../../css/date-picker/date-picker.css";
import DocumentTable from "../../../components/tables/DocumentTable";
import { useNavigate, useParams } from "react-router-dom";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box,useTheme } from "@mui/material";

import { useDispatch, useSelector } from "react-redux";

import { getActiveApprovalById } from "../../../slices/ActiveApprovalSlice";

import {
  getPartRequest,
  getPartRequestTable,
} from "../../../slices/PartRequstSlice";

import { editPartRequestData } from "../../../slices/PartRequstSlice";
import ApproverTable from "../../../components/tables/Approver/ApproverTable";
import { unwrapResult } from "@reduxjs/toolkit";
import { PDFDownloadLink } from "@react-pdf/renderer";
import { PartRequestPDF } from "./PartRequestPDF";

const ViewPartRequest = () => {
  const { id } = useParams();

  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);

  // amount
  // const [request, setRequest] = useState(null);

  //date picker
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  // animation success & failure
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState(false);

  // selected item
  const [locationSelectId, setLocationSelectId] = useState("");
  const [purchasingGroupSelectId, setPurchasingGroupSelectId] = useState("");
  const [plantSelectId, setPlantSelectId] = useState("");
  const [reasonSelectId, setReasonSelectId] = useState("");

  const [dataFromChild, setDataFromChild] = useState([]);
  const [itemsData, setItemsData] = useState([]);

  console.log("items data:", itemsData);

  // error validation
  const [errors, setErrors] = useState({});

  // Modal & Table data
  const [isModalOpen, setModalOpen] = useState(false);

  //for redirect
  const navigate = useNavigate();
  const [totalAmount, setTotalAmount] = useState(0);

  const [activeValue, setActiveValue] = useState(0);
  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };
  const router = useNavigate();

  const handleDataFromChild = (data) => {
    console.log(data);
    // setTableData(data);
    dispatch(getPartRequestTable(data));
    console.log("Data From Child", data);
    setDataFromChild(data);
    setItemsData(data);
  };

  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString().substr(-2);

    // Extracting time components
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");
    const seconds = date.getSeconds().toString().padStart(2, "0");

    return `${year}-${month}-${day} ${hours}:${minutes}`;
  };

  const partrequests = useSelector(
    (state) => state.partrequests.partGetRequest
  );
  const partrequestsitemsdata = useSelector(
    (state) => state.partrequests.partGetRequestItemsData
  );
  const updateTable = useSelector(
    (state) => state.partrequests.updateTableData
  );
  const partrequestsstatus = useSelector(
    (state) => state.partrequests.partGetRequestStatus
  );
  const tableData = useSelector(
    (state) => state.partrequests.partGetRequestTable
  );
  const tablePlant = useSelector((state) => state.globals.fetchPlant);
  const tablePurc = useSelector((state) => state.globals.fetchPurchasingGroup);
  const tableReason = useSelector((state) => state.globals.fetchReason);
  const activeapprovals = useSelector(
    (state) => state.activeapprovals.activeApprovalGetId
  );
  const activeapprovalsstatus = useSelector(
    (state) => state.activeapprovals.activeApprovalGetIdStatus
  );
  const dispatch = useDispatch();

  const plantcode = tablePlant.map((plant) => plant.PlantCode);
  const uniqueplantcode = [...new Set(plantcode)];
  const [localStateApprovals, setLocalStateApprovals] = useState([]);
  const reportTemplateRef = useRef(null);

  useEffect(() => {
    setIsLoading(true);
    // dispatch(getActiveApprovalByRequestId(id));
    // dispatch(getActiveApprovalById(id));
    dispatch(getActiveApprovalById(id))
      .then(unwrapResult)
      .then((data) => {
        setLocalStateApprovals(data);
      });
    dispatch(getPartRequest(id));
    dispatch(editPartRequestData(id));
    setIsLoading(false);
  }, [dispatch, id]);

  useEffect(() => {
    if (partrequests != null && partrequests?.data?.ItemsData?.length > 0) {
      const total = partrequests.data?.ItemsData.reduce((sum, item) => {
        const amount = parseFloat(item.Amount) || 0;
        return sum + amount;
      }, 0);
      setTotalAmount(total);
    }
    if(partrequests?.data?.ItemsData?.length > 0){
      setItemsData(
        partrequests?.data?.ItemsData.map((item, idx) => ({ ...item, id: idx+=1 }))
      );
    }
  }, [partrequests]);

  function loadParts(idPurchasingGroup) {
    setPurchasingGroupSelectId(idPurchasingGroup);
    // dispatch(searchParts(''),idPurchasingGroup);
  }

  const { keycloak } = useContext(KeycloakContext);

  const EditPartRequestAction = (e) => {
    //setValidationErrors({})
    e.preventDefault();

    const newError = validateForm(
      tableData,
      locationSelectId,
      purchasingGroupSelectId,
      plantSelectId,
      reasonSelectId,
      startDate,
      endDate
    );
    setErrors(newError);
    console.log(newError);

    if (Object.keys(newError).length === 0) {
      setIsLoading(true);
      setIsSubmitting(true);
      // submitData();
    }
  };

  const handlePrint = () => {
    router("/partrequest/document/" + id);
  };
  const theme = useTheme();

  return (
    <Layout>
      {isLoading && <SubmitLoadingSpinner />}
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          {partrequestsstatus !== "loading" && (
            <form name="view-part-request">
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                  mb: 1,
                  pr: 6,
                  justifyContent: "space-between",
                }}
              >
                {/* <h1 className="page-title">View Part Request</h1> */}
                <h1 className="page-title">
                  View Part Request -{" "}
                  {partrequests?.data.Request.PartRequestCode || "No Code"}
                </h1>
                <Box className="sm:flex-row flex flex-cols gap-4">
                  {/* <button type="button" className="general-button" onClick={handleSaveChanges}>Save</button> */}
                  {!isLoading && partrequests?.data.PrintData ? (
                    <PDFDownloadLink
                      document={
                        <PartRequestPDF
                          requestData={partrequests?.data}
                          approvalList={
                            activeapprovalsstatus === "done"
                              ? activeapprovals.data
                              : []
                          }
                        />
                      }
                      fileName={partrequests?.data.Request.PartRequestCode}
                    >
                      {({ blob, url, loading, error }) =>
                        loading ? (
                          <button type="button" className="general-button">
                            Loading...
                          </button>
                        ) : (
                          <button type="button" className="general-button">
                            Print
                          </button>
                        )
                      }
                    </PDFDownloadLink>
                  ) : (
                    <></>
                  )}
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  [theme.breakpoints.down("md")]: {
                    flexDirection: "column",
                    gap: theme.spacing(2),
                  },
                  mb: 1,
                  pr: 6,
                  justifyContent: "space-between",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                      
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Plant</label>
                    </Box>
                    <Box
                      sx={{
                        width: 150,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data.Request.LocationPlant.PlantCode ||
                          "Default Plant Name"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Location</label>
                    </Box>
                    <Box
                      sx={{
                        width: 150,
                        pr: 0,
                      [theme.breakpoints.down("md")]: {
                        width: "100%",
                      },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data.Request.RequestLocation ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Request Date</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          formatDate(partrequests?.data.Request.RequestDate) ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Due Date</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          formatDate(partrequests?.data.Request.DueDate) ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Purc Group</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data.Request.PurchasingGroupCodeFk ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Reason</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data.Request.Reason.ReasonName ||
                          "Deafult"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Total Amount</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        type="text"
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-[#f2f2f2] drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={totalAmount}
                        readOnly
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>
            </form>
          )}
          <div>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Parts" />
              <Tab label="Approval" />
              <Tab label="Documents" />
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <div className="py-3 pr-3 md:w-1/3 w-full">
                <div className="flex gap-3">
                  {/* <Button type={"submit"} classname="general-button" onclick={() => setModalOpen(true)}>Add</Button>  */}
                  {/* <Button type={"submit"} classname="general-button">Get Sys Qty</Button> { /* styling general custom-style.css */}
                </div>
              </div>
              <ViewPartRequestTable
                itemsData={itemsData}
                purcGroup={purchasingGroupSelectId}
                onDataSend={handleDataFromChild}
              />
              {errors.tableData && (
                <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                  {errors.tableData}
                </span>
              )}
            </TabPanel>
            <TabPanel value={activeValue} index={1}>
              <div className="px-2 py-6">
                {/* <ApproverTable data={activeapprovals} /> */}
                <ApproverTable data={localStateApprovals} />
              </div>
            </TabPanel>
            <TabPanel value={activeValue} index={2}>
              <div className="px-2 py-6">
                <DocumentTable requestId={id} />
              </div>
            </TabPanel>
          </div>

          <Modal isOpen={isModalOpen} onClose={() => setModalOpen(false)} />
          <SuccessCheckmark showSuccess={showSuccess} message={message} />
          <FailureCheckmark showFailure={showFailure} message={message} />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default ViewPartRequest;
