import React, { useEffect, useCallback } from "react";
import Button from "../../../components/buttons/Button";
import {
  useNavigate,
  useLocation,
  Navigate,
  useParams,
} from "react-router-dom";
import { useState } from "react";
import Layout from "../../Layout/Layout";
import LoadingSpinner from "../../../components/animations/loading/LoadingSpinner";
import { FaRegPenToSquare } from "../../../icons/icon";
import { enGB } from "date-fns/locale";
import "react-nice-dates/build/style.css";
import "../../../css/date-picker/date-picker.css";
import DocumentTable from "../../../components/tables/DocumentTable";
import ViewPartRequestTable from "../../../components/tables/part-request/ViewPartRequestTable";
import axios from "axios";
import ApproverTable from "../../../components/tables/Approver/ApproverTable";
import { useDispatch, useSelector } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { Box} from "@mui/material";
import { useTheme} from "@mui/material";
import Select from "@mui/material/Select";

import {
  // getActiveApprovalByRequestId,
  getActiveApprovalById,
  approvalAction,
  getApprovalStatusByRequestId,
} from "../../../slices/ActiveApprovalSlice";

import {
  retrievePartRequests,
  getPartRequest,
  getPartRequestTable,
} from "../../../slices/PartRequstSlice";

import { getNikApproval } from "../../../slices/StagingUserSlice";

import PartRequestService from "../../../service/PartRequestService";
import FailureCheckmark from "../../../components/animations/checkmark/FailureCheckmark";
import SuccessCheckmark from "../../../components/animations/checkmark/SuccessCheckmark";
import ViewApproverPartRequestTable from "../../../components/tables/part-request/ViewApproverPartRequestTable";

const ViewPartRequestApprover = () => {
  const theme = useTheme();
  const navigate = useNavigate();
  const { id, nik, data } = useParams();
  const dispatch = useDispatch();

  const [isLoading, setIsLoading] = useState(true);
  const [remarks, setRemarks] = useState(true);
  const [itemsData, setItemsData] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showFailure, setShowFailure] = useState(false);
  const [message, setMessage] = useState("");
  const [userRemark, setUserRemark] = useState("");
  const [totalAmount, setTotalAmount] = useState(0);
  const [dataFromChild, setDataFromChild] = useState([]);

  // const [request, setRequest] = useState(null);

  //selector untuk redux state
  const partrequests = useSelector(
    (state) => state.partrequests.partGetRequest
  );
  const partrequestsitemsdata = useSelector(
    (state) => state.partrequests.partGetRequestItemsData
  );
  const partrequestsstatus = useSelector(
    (state) => state.partrequests.partGetRequestStatus
  );
  // const activeapprovals = useSelector(state => state.activeapprovals);
  const activeapprovals = useSelector(
    (state) => state.activeapprovals.activeApprovalGetNikAssigned
  );
  const activeapprovalsstatus = useSelector(
    (state) => state.activeapprovals.activeApprovalGetNikAssignedStatus
  );
  const checkactiveapprovalsstatus = useSelector(
    (state) => state.activeapprovals.activeApprovalStatusGetRequestId
  );
  const checkactiveapprovalsstatusstatus = useSelector(
    (state) => state.activeapprovals.activeApprovalStatusGetRequestIdStatus
  );
  // console.log("active approval table: " , activeapprovals)
  console.log("check approval status: ", checkactiveapprovalsstatus);

  const [localStateApprovals, setLocalStateApprovals] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    dispatch(getActiveApprovalById(id))
      .then(unwrapResult)
      .then((data) => {
        setLocalStateApprovals(data);
      });
    dispatch(getPartRequest(id));
    dispatch(getApprovalStatusByRequestId(id));
    setIsLoading(false);
  }, [dispatch, id]);

  const [activeValue, setActiveValue] = useState(0);

  // const handleDataFromChild = (data) => {
  //   console.log(data)
  //   setTableData(data);
  //   dispatch(SetDataBaru(data))
  // };

  console.log("item data general: ", itemsData);

  useEffect(() => {
    if (partrequests && partrequests.data?.ItemsData?.length > 0) {
      const total = partrequests.data?.ItemsData.reduce((sum, item) => {
        const amount = parseFloat(item.Amount) || 0;
        return sum + amount;
      }, 0);
      setTotalAmount(total);
    }
    if(partrequests.data?.ItemsData?.length > 0){
      setItemsData(
        partrequests.data?.ItemsData.map((item, idx) => ({ ...item, id: idx+=1 }))
      );
    }
  }, [partrequests]);

  const handleActiveChange = (event, newValue) => {
    setActiveValue(newValue);
  };
  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  const handleDataFromChild = (data) => {
    console.log(data);
    // setTableData(data);
    dispatch(getPartRequestTable(data));
    console.log("Data From Child", data);
    setDataFromChild(data);
    setItemsData(data);
  };

  const [errors, setErrors] = useState({});

  const handleApproval = async (status) => {
    setIsSubmitting(true);
    const parsedId = parseInt(id, 10);
    // if (dataFromChild) {
    //   const data = dataFromChild.map((item) => ({
    //     IdItemPivot: item.ID,
    //     ItemCode: item.ItemCode,
    //     RequestQuantity: item.RequestQuantity,
    //     ApprovedQuantity: item.ApprovedQuantity,
    //     AdjustmentQuantity: item.AdjustmentQuantity,
    //     Amount: item.Amount,
    //   }));
    //   console.log("data from child", data);
    // }
    try {
      const actionResult = await dispatch(
        approvalAction({
          IdActiveRequestList: parsedId,
          Status: status,
          Notes: document.getElementById("remarks").value,
          Nik: nik,
          ItemPivot:
            dataFromChild &&
            dataFromChild.map((item) => ({
              IdItemPivot: item.ID,
              ItemCode: item.ItemCode,
              RequestQuantity: item.RequestQuantity,
              ApprovedQuantity: item.ApprovedQuantity,
              AdjustmentQuantity: item.AdjustmentQuantity,
              Amount: item.Amount,
            })),
        })
      );
      unwrapResult(actionResult);
      setShowSuccess(true);
      setShowFailure(false);
      setMessage(
        `Request ${
          status === "Done" ? "Approval Success" : "Approval Rejected"
        }!`
      );

      setTimeout(() => {
        setShowSuccess(false);
        navigate("/partrequest");
      }, 3000);

      // const updatedData = await dispatch(getActiveApprovalByRequestId(id)).then(unwrapResult);
      const updatedData = await dispatch(getActiveApprovalById(id)).then(
        unwrapResult
      );
      setLocalStateApprovals(updatedData);
    } catch (error) {
      console.error("Approval action failed:", error);
      setShowSuccess(false);
      setShowFailure(true);
      setMessage("Approval Failed!");
      setTimeout(() => {
        setShowFailure(false);
      }, 3000);
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleApprove = () => handleApproval("Done");
  const handleReject = () => handleApproval("Rejected");

  if (isLoading) {
    return <LoadingSpinner />;
  }

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear().toString().substr(-2);

    // Extracting time components
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");
    const seconds = date.getSeconds().toString().padStart(2, "0");

    return `${year}-${month}-${day} ${hours}:${minutes}`;
  };
  

  const handlePrint = () => {
    console.log("Print button clicked, attempting to fetch document...");
    const newTab = window.open("about:blank", "_blank");

    if (!newTab) {
      console.error(
        "Failed to open a new tab. This could be due to a popup blocker."
      );
      return;
    }

    console.log("New tab opened, starting API call...");

    const printUrl = `http://34.101.177.226:8080/document-print/${id}`;
    axios
      .get(printUrl, {
        headers: {
          Authorization: "Bearer " + window.localStorage.getItem("token"),
        },
        responseType: "blob",
      })
      .then((response) => {
        console.log("Document retrieved, creating blob URL...");

        const file = new Blob([response.data], { type: "application/pdf" });
        const fileURL = URL.createObjectURL(file);

        console.log("Blob URL created:", fileURL);

        newTab.location.href = fileURL;
      })
      .catch((error) => {
        newTab.close();
        console.error("There was an error fetching the document:", error);
      });
  };
  

  

  return (
    <Layout>
      {isLoading && <LoadingSpinner />}
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          {partrequestsstatus !== "loading" && (
            <form name="view-part-request">
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  [theme.breakpoints.down("md")]: {
                    flexDirection: "column",
                    gap: theme.spacing(2),
                  },
                  mb: 1,
                  pr: 6,
                  justifyContent: "space-between",
                }}
              >
                {/* <h1 className="page-title">View Part Request</h1> */}
                <h1 className="page-title">
                  View Part Request -{" "}
                  {partrequests?.data.Request.PartRequestCode || "No Code"}
                </h1>
                <Box className="sm:flex-row flex flex-cols gap-4">
                  {/* <button type="button" className="general-button" onClick={handlePrint}>Print</button> */}

                  <button
                    type="button"
                    className="general-button"
                    onClick={handleApprove}
                  >
                    Approve
                  </button>
                  <button
                    type="button"
                    className="general-button"
                    onClick={handleReject}
                  >
                    Reject
                  </button>
                </Box>
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  [theme.breakpoints.down("md")]: {
                    flexDirection: "column",
                  },
                }}
              >
                {/* <Box className="flex flex-col basis-1/2"> */}
                {/* <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                  }}
                > */}
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: 400,
                    [theme.breakpoints.down("md")]: {
                      width: "100%",
                    },
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Plant</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data.Request.PlantName ||
                          "Default Plant Name"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Location</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data.Request.RequestLocation ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Purc Group</label>
                    </Box>
                    <Box
                      sx={{
                        width: 150,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data.Request.PurchasingGroupCodeFk ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Total Amount</label>
                    </Box>
                    <Box
                      sx={{
                        width: 150,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        type="text"
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={totalAmount}
                        readOnly
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                      />
                    </Box>
                    {/* </Box> */}
                    {/* </Box> */}
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Request Date</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          formatDate(partrequests?.data.Request.RequestDate) ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Due Date</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          formatDate(partrequests?.data.Request.RequestDate) ||
                          "Default"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>
                </Box>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                  }}
                >
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Reason</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        value={
                          partrequests?.data.Request.Reason.ReasonName ||
                          "Deafult"
                        }
                        style={{
                          backgroundColor: "#f2f2f2",
                          cursor: "not-allowed",
                        }}
                        disabled
                        onChange={(e) => {
                          onchange(e.target.value);
                        }}
                      />
                    </Box>
                  </Box>

                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "row",
                      [theme.breakpoints.down("md")]: {
                        flexDirection: "column",
                        width: "100%",
                        pr: 2,
                      },
                      mx: 0,
                      ml: 2,
                      py: 1,
                    }}
                  >
                    <Box
                      sx={{
                        mr: 2,
                        width: 100,
                      }}
                    >
                      <label className="label-group">Remarks</label>
                    </Box>
                    <Box
                      sx={{
                        width: 250,
                        [theme.breakpoints.down("md")]: {
                          width: "100%",
                        },
                      }}
                    >
                      <input
                        className="py-3 px-6 max-h-10 border-[1px] border-[#ced4da] bg-white drop-shadow-md rounded-md text-sm  text-left w-full lg:w-2/3"
                        type="text"
                        id="remarks"
                      />
                    </Box>
                  </Box>
                </Box>
              </Box>
              {/* </Box> */}
            </form>
          )}

          <Tabs value={activeValue} onChange={handleActiveChange}>
            <Tab label="Parts" />
            <Tab label="Approval" />
            <Tab label="Documents" />
          </Tabs>
          <TabPanel value={activeValue} index={0}>
            <ViewApproverPartRequestTable
              itemsData={itemsData}
              onDataSend={handleDataFromChild}
            />
            {errors.tableData && (
              <span className="flex flex-row justify-center text-xs text-red-500 italic pt-2">
                {errors.tableData}
              </span>
            )}
          </TabPanel>
          <TabPanel value={activeValue} index={1}>
            <div className="px-2 py-6">
              <ApproverTable data={localStateApprovals} />
              {console.log("localstateapproval :", localStateApprovals)}
            </div>
          </TabPanel>
          <TabPanel value={activeValue} index={2}>
            <div className="px-2 py-6">
              <DocumentTable />
            </div>
          </TabPanel>

          <SuccessCheckmark showSuccess={showSuccess} message={message} />
          <FailureCheckmark showFailure={showFailure} message={message} />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default ViewPartRequestApprover;
