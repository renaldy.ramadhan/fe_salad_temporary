import { View, StyleSheet, Text } from "@react-pdf/renderer";
import React from "react";

const styles = StyleSheet.create({
  tableRow: {
    flexDirection: "row",
    alignItems: "center",
    fontSize: 8,
    border: 1,
    borderTopWidth: 0,
    borderColor: "gray",
    fontFamily: "Helvetica",
  },
  rowNo: {
    borderRight: 1,
    borderColor: "gray",
    width: "5%",
    textAlign: "center",
    flexDirection: "column",
    paddingTop: 5,
  },
  rowPartNumber: {
    borderRight: 1,
    borderColor: "gray",
    width: "15%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowPartName: {
    borderRight: 1,
    borderColor: "gray",
    width: "30%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowReqQty: {
    borderRight: 1,
    borderColor: "gray",
    width: "10%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowActQty: {
    borderRight: 1,
    borderColor: "gray",
    width: "10%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowRemark: {
    width: "30%",
    paddingTop: 5,
    paddingLeft: 5,
  },
});


// Define the header component
const SummaryTableHeader = () => (
    <View style={styles.tableRow}>
      <Text style={[styles.tableColHeader, { width: '20%' }]}>No</Text>
      <Text style={[styles.tableColHeader, { width: '40%' }]}>Description</Text>
      <Text style={[styles.tableColHeader, { width: '20%' }]}>System</Text>
      <Text style={[styles.tableColHeader, { width: '20%' }]}>Non System</Text>
    </View>
  );
  
  // Define the row component
  const SummaryDataRow = ({ data }) => (
    <View style={styles.tableRow}>
      <Text style={[styles.tableCol, { width: '20%' }]}>1</Text>
      <Text style={[styles.tableCol, { width: '40%' }]}>Total Quantity</Text>
      <Text style={[styles.tableCol, { width: '20%' }]}>{data.TotalSystemQty}</Text>
      <Text style={[styles.tableCol, { width: '20%' }]}>{data.TotalNonSystemQty}</Text>
    </View>
    // Repeat for each row, like Total Amount, Total Weight, etc.
  );

export const TableRowPemusnahan = ({ itemsData }) => {
  return (
    <>
      {itemsData.map((item, index) => {
        return (
          <View style={styles.tableRow} key={index}>
            <Text style={styles.rowNo}>{index + 1}</Text>
            {/* <Text style={styles.rowPartNumber}>{item.ItemCode}</Text> */}
            <Text style={styles.rowPartNumber}>{item.ItemCode}</Text>
            <Text style={styles.rowPartName}>{item.Items.ItemName}</Text>
            <Text style={styles.rowReqQty}>{item.RequestQuantity}</Text>
            <Text style={styles.rowActQty}>{item.AdjustmentQuantity}</Text>
            <Text style={styles.rowRemark}>
              {item.Notes !== "" ? item.Notes : "-"}
            </Text>
          </View>
        );
      })}
    </>
  );
};

export { SummaryTableHeader, SummaryDataRow };

