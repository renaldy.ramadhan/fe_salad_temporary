import React from "react";
import { StyleSheet, View, Text } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  tableApprovalHeader: {
    flexDirection: "row",
    alignItems: "center",
    fontSize: 8,
    fontFamily: "Helvetica-Bold",
  },
  headerNik: {
    width: "30%",
    textAlign: "center",
    flexDirection: "column",
    paddingTop: 5,
  },
  headerName: {
    width: "70%",
    textAlign: "center",
    paddingTop: 5,
  },
  headerDate: {
    width: "30%",
    textAlign: "center",
    paddingTop: 5,
  },
});

export const TableApprovalHeader = () => {
  return (
    <View style={styles.tableApprovalHeader}>
      <Text style={styles.headerNik}>NIK</Text>
      <Text style={styles.headerName}>Name</Text>
      <Text style={styles.headerDate}>Approval Date</Text>
    </View>
  );
};
