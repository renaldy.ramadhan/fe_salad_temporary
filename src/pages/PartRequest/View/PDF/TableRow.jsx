import { View, StyleSheet, Text } from "@react-pdf/renderer";
import React from "react";

const styles = StyleSheet.create({
  tableRow: {
    flexDirection: "row",
    alignItems: "center",
    fontSize: 8,
    border: 1,
    borderTopWidth: 0,
    borderColor: "gray",
    fontFamily: "Helvetica",
  },
  rowNo: {
    borderRight: 1,
    borderColor: "gray",
    width: "5%",
    textAlign: "center",
    flexDirection: "column",
    paddingTop: 5,
  },
  rowPartNumber: {
    borderRight: 1,
    borderColor: "gray",
    width: "15%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowPartName: {
    borderRight: 1,
    borderColor: "gray",
    width: "30%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowReqQty: {
    borderRight: 1,
    borderColor: "gray",
    width: "10%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowActQty: {
    borderRight: 1,
    borderColor: "gray",
    width: "10%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowRemark: {
    width: "30%",
    paddingTop: 5,
    paddingLeft: 5,
  },
});

export const TableRow = ({ itemsData }) => {
  return (
    <>
      {itemsData.map((item, index) => {
        return (
          <View style={styles.tableRow} key={index}>
            <Text style={styles.rowNo}>{index + 1}</Text>
            <Text style={styles.rowPartNumber}>{item.ItemCode}</Text>
            <Text style={styles.rowPartName}>{item.Items.ItemName}</Text>
            <Text style={styles.rowReqQty}>{item.RequestQuantity}</Text>
            <Text style={styles.rowActQty}>{item.AdjustmentQuantity}</Text>
            <Text style={styles.rowRemark}>
              {item.Notes !== "" ? item.Notes : "-"}
            </Text>
          </View>
        );
      })}
    </>
  );
};
