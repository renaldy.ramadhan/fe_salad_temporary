import React from "react";
import { StyleSheet, View } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  signBox: {
    height: "40%",
    borderBottomWidth: 1,
    marginBottom: 5,
  },
});

export const SignBox = () => {
  return <View style={styles.signBox}></View>;
};
