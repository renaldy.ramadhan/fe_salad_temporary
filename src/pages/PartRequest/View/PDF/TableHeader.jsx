import React from "react";
import { StyleSheet, View, Text } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  tableHeader: {
    flexDirection: "row",
    alignItems: "center",
    fontSize: 8,
    border: 1,
    borderColor: "gray",
    fontFamily: "Helvetica-Bold",
  },
  headerNo: {
    borderRight: 1,
    borderColor: "gray",
    width: "5%",
    textAlign: "center",
    flexDirection: "column",
    paddingTop: 5,
  },
  headerPartNumber: {
    borderRight: 1,
    borderColor: "gray",
    width: "15%",
    textAlign: "center",
    paddingTop: 5,
  },
  headerPartName: {
    borderRight: 1,
    borderColor: "gray",
    width: "30%",
    textAlign: "center",
    paddingTop: 5,
  },
  headerReqQty: {
    borderRight: 1,
    borderColor: "gray",
    width: "10%",
    textAlign: "center",
    paddingTop: 5,
  },
  headerActQty: {
    borderRight: 1,
    borderColor: "gray",
    width: "10%",
    textAlign: "center",
    paddingTop: 5,
  },
  headerRemark: {
    width: "30%",
    textAlign: "center",
    paddingTop: 5,
  },
});

export const TableHeader = () => {
  return (
    <>
      <View style={styles.tableHeader}>
        <Text style={styles.headerNo}>NO</Text>
        <Text style={styles.headerPartNumber}>PART NUMBER</Text>
        <Text style={styles.headerPartName}>PART NAME</Text>
        <Text style={styles.headerReqQty}>REQ QTY</Text>
        <Text style={styles.headerActQty}>ACT QTY</Text>
        <Text style={styles.headerRemark}>REMARK</Text>
      </View>
    </>
  );
};
