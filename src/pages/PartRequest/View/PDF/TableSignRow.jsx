import React from "react";
import { StyleSheet, View, Text } from "@react-pdf/renderer";
import { SignBox } from "./SignBox";

const styles = StyleSheet.create({
  tableSignRow: {
    flexDirection: "row",
    alignItems: "center",
    fontSize: 8,
    fontFamily: "Helvetica",
    border: 1,
    borderTopWidth: 0,
    borderColor: "gray",
    height: "35%",
  },
  rowRequestor: {
    borderRight: 1,
    borderColor: "gray",
    paddingLeft: 5,
    paddingRight: 5,
    width: "50%",
    flexDirection: "column",
    paddingTop: 5,
    height: "100%",
  },
  rowWH: {
    borderRight: 1,
    borderColor: "gray",
    width: "50%",
    paddingTop: 5,
    paddingLeft: 5,
    paddingRight: 5,
    height: "100%",
  },
  rowWHC: {
    width: "50%",
    paddingTop: 5,
    paddingLeft: 5,
    paddingRight: 5,
    height: "100%",
  },
});

export const TableSignRow = ({ requestor, approverWhc }) => {
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  };

  return (
    <View style={styles.tableSignRow}>
      <View style={styles.rowRequestor}>
        <SignBox />
        <Text>NIK: {requestor.ApproverNik}</Text>
        <Text>Name: {requestor.ApproverName}</Text>
        <Text>
          Date:{" "}
          {requestor.DateApproval !== null
            ? formatDate(requestor.DateApproval)
            : "-"}
        </Text>
      </View>
      <View style={styles.rowWH}>
        <SignBox />
        <Text>Nik: </Text>
        <Text>Name: </Text>
        <Text>Date: </Text>
      </View>
      <View style={styles.rowWHC}>
        <SignBox />
        <Text>Nik: {approverWhc?.ApproverNik}</Text>
        <Text>Name: {approverWhc?.ApproverName}</Text>
        <Text>
          Date:{" "}
          {approverWhc?.DateApproval !== null
            ? formatDate(approverWhc?.DateApproval)
            : "-"}
        </Text>
      </View>
    </View>
  );
};
