import React from "react";
import { StyleSheet, View, Text } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  tableApprovalRow: {
    flexDirection: "row",
    alignItems: "center",
    fontSize: 8,
    fontFamily: "Helvetica",
  },
  rowNik: {
    width: "30%",
    textAlign: "center",
    flexDirection: "column",
    paddingTop: 5,
  },
  rowName: {
    width: "70%",
    textAlign: "center",
    paddingTop: 5,
  },
  rowDate: {
    width: "30%",
    textAlign: "center",
    paddingTop: 5,
  },
});

export const TableApprovalRow = ({ approvalList }) => {
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  };
  return (
    <>
      {approvalList.map((approval, index) => {
        return (
          <View style={styles.tableApprovalRow} key={index}>
            <Text style={styles.rowNik}>{approval.ApproverNik}</Text>
            <Text style={styles.rowName}>{approval.ApproverName}</Text>
            <Text style={styles.rowDate}>
              {approval.DateApproval !== null
                ? formatDate(approval.DateApproval)
                : "-"}
            </Text>
          </View>
        );
      })}
    </>
  );
};
