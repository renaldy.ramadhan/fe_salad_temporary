import React from "react";
import { StyleSheet, View, Text } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  tableSignHeader: {
    flexDirection: "row",
    alignItems: "center",
    fontSize: 8,
    fontFamily: "Helvetica-Bold",
    border: 1,
    borderColor: "gray",
  },
  headerRequestor: {
    borderRight: 1,
    borderColor: "gray",
    width: "50%",
    textAlign: "center",
    flexDirection: "column",
    paddingTop: 5,
  },
  headerWH: {
    borderRight: 1,
    borderColor: "gray",
    width: "50%",
    textAlign: "center",
    paddingTop: 5,
  },
  headerWHC: {
    width: "50%",
    textAlign: "center",
    paddingTop: 5,
  },
});

export const TableSignHeader = () => {
  return (
    <View style={styles.tableSignHeader}>
      <Text style={styles.headerRequestor}>Requestor</Text>
      <Text style={styles.headerWH}>WH DEPT</Text>
      <Text style={styles.headerWHC}>WHC DEPT</Text>
    </View>
  );
};
