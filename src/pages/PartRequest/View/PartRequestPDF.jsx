import React, { useEffect, useState } from "react";
import {
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  PDFViewer,
} from "@react-pdf/renderer";
import { TableHeader } from "./PDF/TableHeader";
import { TableRow } from "./PDF/TableRow";
import { TableApprovalHeader } from "./PDF/TableApprovalHeader";
import { TableApprovalRow } from "./PDF/TableApprovalRow";
import { TableSignHeader } from "./PDF/TableSignHeader";
import { TableSignRow } from "./PDF/TableSignRow";

const styles = StyleSheet.create({
  page: {
    fontFamily: "Helvetica",
    fontSize: 8,
    padding: 40,
    lineHeight: 1.5,
    flexDirection: "column",
  },
  viewer: {
    width: window.innerWidth, //the pdf viewer will take up all of the width and height
    height: window.innerHeight,
  },
  companyName: {
    fontFamily: "Helvetica-Bold",
    marginBottom: 20,
    fontSize: 8,
  },
  documentTitle: {
    fontFamily: "Helvetica-Bold",
    marginBottom: 8,
    fontSize: 14,
    textAlign: "center",
  },
  documentSubTitle: {
    marginBottom: 12,
    fontSize: 10,
    textAlign: "center",
  },
  descriptionContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 14,
    marginBottom: 10,
    flexWrap: "wrap",
  },
  tableView: {
    marginBottom: 15,
  },
  approvalView: {
    flexDirection: "row",
  },
  approvalTitle: {
    fontFamily: "Helvetica-Bold",
    textAlign: "center",
  },
  tableApprovalView: {
    width: "45%",
  },
  tableSignView: {
    width: "55%",
    marginLeft: 25,
  },
});

export const PartRequestPDF = ({ requestData, approvalList }) => {
  const [approverWHC, setApproverWHC] = useState(null);

  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const year = date.getFullYear();
    return `${day}/${month}/${year}`;
  };

  useEffect(() => {
    if (approvalList) {
      const approverData = approvalList;
      const whcapprover = approverData.filter((approver) => {
        return approver.Department.DepartmentName === "WHC";
      });
      setApproverWHC(whcapprover);
    }
    return () => {
      setApproverWHC(null);
    };
  }, [approvalList]);

  return (
    <Document>
      {/*render a single page*/}
      <Page size="A4" orientation="portrait" style={styles.page}>
        <Text style={styles.companyName}>PT EPSON INDUSTRY INDONESIA</Text>
        <View>
          <Text style={styles.documentTitle}>PART REQUEST</Text>
          <Text style={styles.documentSubTitle}>
            NO.{" "}
            {requestData.Request !== undefined
              ? requestData.Request.PartRequestCode
              : ""}
          </Text>
        </View>
        <Text>
          Date: &nbsp;
          {requestData.Request !== undefined
            ? formatDate(requestData.Request.RequestDate)
            : ""}
        </Text>
        <View style={styles.descriptionContainer}>
          <Text>
            Dept. Requestor : &nbsp;
            {requestData.Request !== undefined
              ? requestData.Request.User.Department
              : ""}
          </Text>
          <Text>
            Location : &nbsp;
            {requestData.Request !== undefined
              ? requestData.Request.LocationCode
              : ""}
          </Text>
          <Text>
            Reason : &nbsp;{" "}
            {requestData.Request !== undefined
              ? requestData.Request.Reason.ReasonName
              : ""}
          </Text>
        </View>
        <View style={styles.tableView}>
          <TableHeader />
          <TableRow
            itemsData={
              requestData.ItemsData !== undefined ? requestData.ItemsData : []
            }
          />
        </View>
        <View style={styles.approvalView}>
          <View style={styles.tableApprovalView}>
            <Text style={styles.approvalTitle}>Approval List</Text>
            <TableApprovalHeader />
            <TableApprovalRow approvalList={approvalList} />
          </View>
          <View style={styles.tableSignView}>
            <TableSignHeader />
            <TableSignRow
              requestor={approvalList[0]}
              approverWhc={approverWHC?.[0]}
            />
          </View>
        </View>
      </Page>
    </Document>
  );
};
