  import React, { useState, useEffect, useContext, useCallback } from "react";
  import { useNavigate } from "react-router-dom";
  import Layout from "../Layout/Layout";
  import "../../css/general/custom-style.css";
  import { KeycloakContext } from "../../contexts/KeycloakProvider";
  import PartRequestMUI from "../../components/tables/MUI/PartRequestMUI";
  import Tabs from "@mui/material/Tabs";
  import Tab from "@mui/material/Tab";
  import Box from "@mui/material/Box";
  import Card from "@mui/material/Card";
  import CardContent from "@mui/material/CardContent";
  import Typography from "@mui/material/Typography";
  import useSearchStore from "../../store/searchStore";
  import Button from "@mui/material/Button";

  import { useDispatch, useSelector } from "react-redux";
  import {
    retrievePartRequests,
    getPartRequest,
  } from "../../slices/PartRequstSlice";

  import {
    getActiveApprovalByNikAndAssigned,
    getActiveApprovalByNikAndAssignedDiscrepancy,
  } from "../../slices/ActiveApprovalSlice";
  import { retrieveDiscrepancy } from "../../slices/DiscrepancySlice";
  import { checkApprover } from "../../slices/ApprovalSlice";
  import Grid from "@mui/material/Grid";
  import Paper from "@mui/material/Paper";
  import ActiveApproverTables from "../../components/tables/Approver/ActiveApproverTables";
  import ActiveApproverTablesDiscrepancy from "../../components/tables/discrepancy/ActiveApproverTablesDiscrepancy";
  import PartRequestActivity from "../../components/tables/part-request/PartRequestActivity";
  import PartDiscrepancyActivity from "../../components/tables/discrepancy/PartDiscrepancyActivity";
  import NoContentTable from "../../components/tables/NoContentTable";
  import DisposalEventTable from "../../components/tables/disposal/event/DisposalEventTable";
  import VisibilityIcon from "@mui/icons-material/Visibility";
  import { useLocation } from 'react-router-dom';
  import CircularProgress from '@mui/material/CircularProgress';

  import {
    retrieveDisposalRequest,
    retrieveDisposalRequestData,
  } from "../../slices/DisposalRequestSlice";

  import {
    retrieveActiveDisposalEvent, retrieveDisposalEvent
  } from "../../slices/DisposalEventSlice";

  const DashboardMUI = () => {
    const navigate = useNavigate();
    const approver = useSelector((state) => state.approvals);
    const location = useLocation();
    const [isLoading, setIsLoading] = useState(true); 

    const { page, pageSize, activePage, activePageSize } = useSelector(
      (state) => state.partrequests
    );
    const {
      page: pageDiscrepancy,
      pageSize: pageSizeDiscrepancy,
      activePage: activePageDiscrepancy,
      activePageSize: activePageSizeDiscrepancy,
    } = useSelector((state) => state.discrepancy);

    const dispatch = useDispatch();
    const [eventRowsDisposalRequest, setEventRowsDisposalRequest] = useState([]);
    const [activeEventRowsDisposalRequest, setActiveEventRowsDisposalRequest] = useState([]);
    const [activeEventRowsRingi, setActiveEventRowsRingi] = useState([]);
    const [eventRowsRingi, setEventRowsRingi] = useState([]);
    
    const finalizeLoading = () => {
      setIsLoading(false);
    };

    const {
      disposalRequest,
      disposalRequestStatus,
      disposalRequestData,
      disposalRequestDataStatus,
    } = useSelector((state) => state.disposalRequest);

    const { 
      retrieveActiveDisposalEventStatus, 
      activeDisposalRingiEvent, 
      retrieveDisposalEventStatus, 
      disposalRingiEvent } 
    = useSelector(state => state.disposalEvent)
    

    // const initFetch = useCallback(() => {
    //   dispatch(retrievePartRequests({ page, pageSize }));
    //   if (disposalRequest.length < 1) {
    //     dispatch(retrieveDisposalRequest(1, 5));
    //   }
    //   if (disposalRequestData.length < 1) {
    //     dispatch(retrieveDisposalRequestData(1, 5));
    //   }
    //   if (activeDisposalRingiEvent.length < 1) {
    //     dispatch(retrieveActiveDisposalEvent());
    //   }
    //   if (disposalRingiEvent.length < 1) {
    //     dispatch(retrieveDisposalEvent());
    //   }
    //   dispatch(
    //     getActiveApprovalByNikAndAssigned({
    //       page: activePage,
    //       pageSize: activePageSize,
    //     })
    //   );
    //   dispatch(
    //     getActiveApprovalByNikAndAssignedDiscrepancy({
    //       page: activePageDiscrepancy,
    //       pageSize: activePageSizeDiscrepancy,
    //     })
    //   );
    //   dispatch(
    //     retrieveDiscrepancy({
    //       page: pageDiscrepancy,
    //       pageSize: pageSizeDiscrepancy,
    //     })
    //   );
    //   dispatch(checkApprover());
    // }, [
    //   dispatch,
    //   page,
    //   pageSize,
    //   pageDiscrepancy,
    //   pageSizeDiscrepancy,
    //   activePage,
    //   activePageSize,
    //   activePageDiscrepancy,
    //   activePageSizeDiscrepancy,
    //   disposalRequest, 
    //   disposalRequestData,
    //   activeDisposalRingiEvent, 
    //   disposalRingiEvent
    // ]);


    const initFetch = useCallback(async () => {
      setIsLoading(true); 
    
      const fetchPromises = [];
    
      fetchPromises.push(dispatch(retrievePartRequests({ page, pageSize })));
    
      if (disposalRequest.length < 1) {
        fetchPromises.push(dispatch(retrieveDisposalRequest(1, 5)));
      }
      if (disposalRequestData.length < 1) {
        fetchPromises.push(dispatch(retrieveDisposalRequestData(1, 5)));
      }
      if (activeDisposalRingiEvent.length < 1) {
        fetchPromises.push(dispatch(retrieveActiveDisposalEvent()));
      }
      if (disposalRingiEvent.length < 1) {
        fetchPromises.push(dispatch(retrieveDisposalEvent()));
      }
    
      fetchPromises.push(dispatch(getActiveApprovalByNikAndAssigned({
        page: activePage,
        pageSize: activePageSize,
      })));
      fetchPromises.push(dispatch(getActiveApprovalByNikAndAssignedDiscrepancy({
        page: activePageDiscrepancy,
        pageSize: activePageSizeDiscrepancy,
      })));
      fetchPromises.push(dispatch(retrieveDiscrepancy({
        page: pageDiscrepancy,
        pageSize: pageSizeDiscrepancy,
      })));
      fetchPromises.push(dispatch(checkApprover()));
    
      try {
      
        await Promise.all(fetchPromises);
      } catch (error) {
        console.error("An error occurred during data fetching", error);
      } finally {
        setIsLoading(false); 
      }
    }, [
      dispatch,
      page,
      pageSize,
      pageDiscrepancy,
      pageSizeDiscrepancy,
      activePage,
      activePageSize,
      activePageDiscrepancy,
      activePageSizeDiscrepancy,
      disposalRequest, 
      disposalRequestData,
      activeDisposalRingiEvent, 
      disposalRingiEvent
    ]);
    
    useEffect(() => {
      initFetch();
    }, [initFetch]);
    
    useEffect(() => {
      if (Array.isArray(disposalRequest?.data?.data)) {
        setEventRowsDisposalRequest(
          disposalRequest.data.data.map((item) => ({
            ...item,
            id: item.ID,
          }))
        );
      }

      if (Array.isArray(disposalRequestData?.data?.data)) {
        setActiveEventRowsDisposalRequest(
          disposalRequestData.data.data.map((item) => ({
            ...item,
            id: item.IdRequest,
          }))
        );
      }

      if (Array.isArray(activeDisposalRingiEvent)) {
        setActiveEventRowsRingi(
          activeDisposalRingiEvent.map(event => ({ ...event, id: event.ID }))
        );
      }

      if (Array.isArray(disposalRingiEvent)) {
        setEventRowsRingi(
          disposalRingiEvent.map(event => ({ ...event, id: event.ID }))
        );
      }
    }, [disposalRequest, disposalRequestData, activeDisposalRingiEvent, disposalRingiEvent]);


    
    const [activeValue, setActiveValue] = useState(0);
    
    const [needYourApprovalValue, setNeedYourApprovalValue] = useState(0);

    const handleActiveChange = (event, newValue) => {
      setActiveValue(newValue);
    };

    const handleNeedYourApprovalValueChange = (event, newValue) => {
      setNeedYourApprovalValue(newValue);
    };

    const handleClickDisposalRequest = (id) => {
      // let selectedPartRequest;
      // if (partRequests.length > 0) {
      //   selectedPartRequest = partRequests.data.data.find(
      //     (data) => data.IdRequest === id
      //   );
      // } else if (activeApprovals.length > 0) {
      //   selectedPartRequest = activeApprovals.find(
      //     (data) => data.Request.ID === id
      //   );
      // }

      const row = eventRowsDisposalRequest.find((row) => row.id === id);

      if (row) {
        // Extract DisposalType from the found row
        const disposalType = row.DisposalType;

        // Navigate based on DisposalType
        if (disposalType === "System") {
          navigate(`/disposal/general/request/view/system/${id}`);
        } else {
          navigate(`/disposal/general/request/view/nonsystem/${id}`);
        }
      }
    };

    const handleClickRingi = (id) => {
      navigate(`/ringi/event/view/` + id);
    };

    const formatDate = (dateString) => {
      if (dateString === null) return '';
      const date = new Date(dateString);
      const year = date.getFullYear();
      const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
      const day = String(date.getDate()).padStart(2, '0');

      return `${year}-${month}-${day}`;
    };

    const columnsRingi = [
      {
        field: "ID",
        headerName: "Id",
        minWidth: 120,
        flex: 1,
        align: "center",
        headerAlign: "center",
        filterable: false,
      },
      {
        field: "DisposalCode",
        headerName: "Reference",
        minWidth: 120,
        flex: 1,
        editable: false,
        align: "center",
        headerAlign: "center",
      },

      {
        field: "Name",
        headerName: "Name",
        minWidth: 180,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
      },
      {
        field: "ExecutionDate",
        headerName: "Date",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueFormatter: ({ value }) => formatDate(value),
      },
      {
        field: "StartDate",
        headerName: "Period Start",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueFormatter: ({ value }) => formatDate(value),
      },
      {
        field: "EndDate",
        headerName: "Period End",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueFormatter: ({ value }) => formatDate(value),
      },
      {
        field: "CreatedBy",
        headerName: "Created By",
        minWidth: 120,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
      },
      {
        field: "CreatedAt",
        headerName: "Created Date",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueFormatter: ({ value }) => formatDate(value),
      },
      {
        field: "Status",
        headerName: "Status",
        minWidth: 120,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
      },
      {
        field: "actions",
        type: "actions",
        headerName: "Actions",
        minWidth: 60,
        flex: 1,
        cellClassName: "actions",
        align: "center",
        headerAlign: "center",
        getActions: ({ id }) => {
          return [
            <Button
              color="primary"
              style={{ backgroundColor: "#808080" }}
              variant="contained"
              startIcon={<VisibilityIcon />}
              onClick={() => handleClickRingi(id)}
            >
              View
            </Button>
          ];
        },
      }
    ];
    

    const columnsDisposalRequest = [
      {
        field: "ID", //ini ganti mapping di bagian field
        headerName: "ID",
        minWidth: 120,
        flex: 1,
        align: "center",
        headerAlign: "center",
        filterable: false,
      },
      {
        field: "PartRequestCode",
        headerName: "Reference",
        minWidth: 120,
        flex: 1,
        editable: false,
        align: "center",
        headerAlign: "center",
      },
      {
        field: "PlantCode",
        headerName: "Plant",
        minWidth: 120,
        flex: 1,
        editable: false,
        align: "center",
        headerAlign: "center",
        valueGetter: (params) => params.row?.LocationPlant?.PlantCode,
      },

      {
        field: "IdDisposalGroupEvent",
        headerName: "Event",
        minWidth: 180,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
      },
      {
        field: "Requestor",
        headerName: "Requestor",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueGetter: (params) => params.row?.UserName,
      },
      {
        field: "RequestDepartment",
        headerName: "Request Department",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueGetter: (params) => params.row?.User.Department,
      },
      {
        field: "DisposalType",
        headerName: "Type",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
      },

      {
        field: "Status",
        headerName: "Status",
        minWidth: 120,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
      },
      {
        field: "actions",
        type: "actions",
        headerName: "Actions",
        minWidth: 60,
        flex: 1,
        cellClassName: "actions",
        align: "center",
        headerAlign: "center",
        
        getActions: ({ id }) => {
          return [
            <Button
              color="primary"
              style={{ backgroundColor: "#808080" }}
              variant="contained"
              startIcon={<VisibilityIcon />}
              onClick={() => handleClickDisposalRequest(id)}
            >
              View
            </Button>,
          ];
        },
      },
    ];

    const columnsApprovalList = [
      {
        field: "IdRequest", //ini ganti mapping di bagian field
        headerName: "ID",
        minWidth: 120,
        flex: 1,
        align: "center",
        headerAlign: "center",
        filterable: false,
      },
      {
        field: "PartRequestCode",
        headerName: "Reference",
        minWidth: 120,
        flex: 1,
        editable: false,
        align: "center",
        headerAlign: "center",
        valueGetter: (params) => params.row?.Request?.PartRequestCode,
      },
      {
        field: "LocationCode",
        headerName: "Plant",
        minWidth: 120,
        flex: 1,
        editable: false,
        align: "center",
        headerAlign: "center",
        valueGetter: (params) => params.row?.Request?.LocationCode,
      },

      {
        field: "IdDisposalGroupEvent",
        headerName: "Event",
        minWidth: 180,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueGetter: (params) => params.row?.Request?.IdDisposalGroupEvent,
      },
      {
        field: "Requestor",
        headerName: "Requestor",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueGetter: (params) => params.row?.Request?.UserName,
      },
      {
        field: "RequestDepartment",
        headerName: "Request Department",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueGetter: (params) => params.row?.User?.Department,
      },
      {
        field: "DisposalType",
        headerName: "Type",
        minWidth: 100,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
        valueGetter: (params) => params.row?.Request?.Type,
      },

      {
        field: "Status",
        headerName: "Status",
        minWidth: 120,
        flex: 1,
        align: "center",
        headerAlign: "center",
        editable: false,
      },
      {
        field: "actions",
        type: "actions",
        headerName: "Actions",
        minWidth: 60,
        flex: 1,
        cellClassName: "actions",
        align: "center",
        headerAlign: "center",
        getActions: ({ id }) => {
          return [
            <Button
              color="primary"
              style={{ backgroundColor: "#808080" }}
              variant="contained"
              startIcon={<VisibilityIcon />}
              // onClick={() => handleClick(id)}
              onClick={() => navigate(`/disposal/general/request/view/approver/` +id,)}
            >
              
              View
            </Button>,
          ];
        },
      },
    ];

    if (isLoading) {
      return (
        <Layout>
          <Grid container justifyContent="center" alignItems="center" style={{ height: '100vh' }}>
            <CircularProgress />
          </Grid>
        </Layout>
      );
    }

    return (
      <Layout>
        {/* <div className="pt-[80px] w-full"></div> */}

        {approver.approver.message && (
          <Grid item xs={12} md={12} lg={12}>
            <Paper
              sx={{
                p: 2,
                // ml:-10,
                // width:1300,
                display: "flex",
                flexDirection: "column",
                height: "inherit",
              }}
            >
              <Typography variant="h5" component="div">
                Active Table (Need Approval)
              </Typography>
              <Tabs
                value={needYourApprovalValue}
                onChange={handleNeedYourApprovalValueChange}
              >
                <Tab label="Part Requests" />
                <Tab label="Discrepancy" />
                <Tab label="Disposal Request" />
                <Tab label="Disposal Ringi" />
              </Tabs>
              <TabPanel value={needYourApprovalValue} index={0}>
                <ActiveApproverTables />
              </TabPanel>
              <TabPanel value={needYourApprovalValue} index={1}>
                <ActiveApproverTablesDiscrepancy />
              </TabPanel>
              <TabPanel value={needYourApprovalValue} index={2}>
              <ActiveApproverTables
              loading={disposalRequestDataStatus === "loading"}
              parentColumn={columnsApprovalList}
              parentRow={activeEventRowsDisposalRequest}
            />
              </TabPanel>
              <TabPanel value={needYourApprovalValue} index={3}>
              <ActiveApproverTables loading={retrieveActiveDisposalEventStatus === 'loading'} parentColumn={columnsRingi} parentRow={activeEventRowsRingi} />
              </TabPanel>
            </Paper>
          </Grid>
        )}

        <Grid item xs={12} md={12} lg={12}>
          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "column",
              height: "inherit",
            }}
          >
            <Typography variant="h5" component="div">
              Active Table
            </Typography>
            <Tabs value={activeValue} onChange={handleActiveChange}>
              <Tab label="Part Requests" />
              <Tab label="Discrepancy" />
              <Tab label="Disposal Request" />
              <Tab label="Disposal Ringi" />
              {/* Add more tabs if needed */}
            </Tabs>
            <TabPanel value={activeValue} index={0}>
              <PartRequestActivity />
            </TabPanel>
            <TabPanel value={activeValue} index={1}>
              <PartDiscrepancyActivity />
            </TabPanel>
            <TabPanel value={activeValue} index={2}>
            <DisposalEventTable
              loading={disposalRequestStatus === "loading"}
              parentColumn={columnsDisposalRequest}
              parentRow={eventRowsDisposalRequest}
            />
            </TabPanel>
            <TabPanel value={activeValue} index={3}>
            <DisposalEventTable loading={retrieveDisposalEventStatus === 'loading'} parentColumn={columnsRingi} parentRow={eventRowsRingi} />
            </TabPanel>
            {/* Add more TabPanel components for additional tabs */}
          </Paper>
        </Grid>

        <Grid item xs={12} md={6} lg={6}>
          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "row",
              height: "inherit",
              justifyContent: "space-between",
            }}
          >
            <Typography variant="h5" component="div">
              Active Disposal Event
            </Typography>
            <Button
              variant="text"
              align="center"
              sx={{ ml: 25, py: 0, mt: 0.5 }}
              onClick={() => {
                navigate("/disposal/general/event/create");
              }}
            >
              Add Event
            </Button>
          </Paper>
        </Grid>
        <Grid item xs={12} md={6} lg={6}>
          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "row",
              height: "inherit",
              justifyContent: "space-between",
            }}
          >
            <Typography variant="h5" component="div">
              Active Ringi Event
            </Typography>
            <Button
              variant="text"
              align="center"
              sx={{ ml: 30, py: 0, mt: 0.5 }}
              onClick={() => {
                navigate("/ringi/event/create");
              }}
            >
              Add Event
            </Button>
          </Paper>
        </Grid>
      </Layout>
    );
  };

  const TabPanel = ({ children, value, index }) => {
    return (
      <div hidden={value !== index}>
        {value === index && <Box>{children}</Box>}
      </div>
    );
  };

  export default DashboardMUI;
