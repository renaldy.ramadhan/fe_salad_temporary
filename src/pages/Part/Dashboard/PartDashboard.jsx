import React from 'react'
import Layout from '../../Layout/Layout'
import PartTable from '../../../components/tables/part/PartTable'

const PartDashboard = () => {
  return (
    <Layout>
      <div className='content-positioning pl-3 md:pl-48 md:pt-38 pr-2 w-full'>
        <p className='page-title'>Part</p>
        <PartTable/>
      </div>
    </Layout>
  )
}

export default PartDashboard