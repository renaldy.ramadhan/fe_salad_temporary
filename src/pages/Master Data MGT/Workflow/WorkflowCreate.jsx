import React, { useState } from "react";
import Layout from "../../Layout/Layout";
import { Alert, Button, Grid, Paper, Snackbar } from "@mui/material";
import { InputText } from "../../../components/input/input_text";
import { SelectInput } from "../../../components/input/select_input";
import { Textarea } from "../../../components/input/textarea";
import { createWorkflow } from "../../../slices/WorkflowSlice";
import { useDispatch } from "react-redux";
import { unwrapResult } from "@reduxjs/toolkit";
import { useNavigate } from "react-router-dom";
import { ArrowBack } from "@mui/icons-material";

export const WorkflowCreate = () => {
  const [payload, setPayload] = useState({
    LocationName: "",
    PlantName: "",
    Type: "",
    Notes: "",
  });
  const [alert, setAlert] = useState({
    status: false,
    type: "success",
    message: "",
  });

  const options = [
    {
      value: "Request",
      title: "Request",
    },
    {
      value: "Discrepancy",
      title: "Discrepancy",
    },
    {
      value: "Disposal",
      title: "Disposal",
    },
    {
      value: "Ringi",
      title: "Ringi",
    },
    {
      value: "Disposal Event",
      title: "Disposal Event",
    },
    {
      value: "Disposal Event Ringi",
      title: "Disposal Event Ringi",
    },
  ];

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setPayload({ ...payload, [name]: value });
  };

  const handleSubmit = async () => {
    const response = await dispatch(createWorkflow({ payload })).then(
      unwrapResult
    );

    if (response.message === "Created!") {
      setAlert({
        ...alert,
        status: true,
        type: "success",
        message: "Create workflow successfully. This page is auto redirect....",
      });
      setTimeout(() => {
        navigate("/masterdata/workflow");
      }, 4000);
    } else {
      setAlert({
        ...alert,
        type: "error",
        message: "Failed create workflow. Please check your input.",
        status: true,
      });
    }
  };

  const handleAlertOnClose = () => {
    setAlert({
      ...alert,
      status: false,
    });
  };

  return (
    <Layout>
      <Snackbar
        open={alert.status}
        autoHideDuration={3000}
        onClose={handleAlertOnClose}
      >
        <Alert severity={alert.type} variant="filled">
          {alert.message}
        </Alert>
      </Snackbar>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <p className="page-title">Create Workflow</p>
          <div className="space-y-4">
            <SelectInput
              name="Type"
              label="Type"
              isRequired
              onChange={(e) => {
                handleChange(e);
              }}
            >
              {options.map((option, index) => {
                return (
                  <option value={option.value} key={index}>
                    {option.title}
                  </option>
                );
              })}
            </SelectInput>
            <Textarea
              label="Notes"
              name="Notes"
              height={5}
              onChange={(e) => {
                handleChange(e);
              }}
              value={payload.Notes}
            />
          </div>
          <div className="flex justify-end mt-4 gap-2">
            <Button
              className="w-min"
              variant="contained"
              style={{ backgroundColor: "#808080" }}
              onClick={() => navigate("/masterdata/workflow")}
            >
              <ArrowBack />
              Back
            </Button>
            <Button
              className="w-min"
              variant="contained"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </div>
        </Paper>
      </Grid>
    </Layout>
  );
};
