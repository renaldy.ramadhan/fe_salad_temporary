import React, { useEffect, useCallback } from "react";
import Layout from "../../Layout/Layout";
import { useDispatch, useSelector } from "react-redux";
import { retrieveWorkflows } from "../../../slices/WorkflowSlice";
import { Button, Grid, Paper } from "@mui/material";
import { WorkflowData } from "../../../components/tables/workflow/WorkflowData";
import { useNavigate } from "react-router-dom";

const WorkflowList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const workflows = useSelector((state) => state.workflows.workflowsData);

  const initFetch = useCallback(() => {
    dispatch(retrieveWorkflows());
  }, [dispatch]);

  useEffect(() => {
    initFetch();
  }, [initFetch]);

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <div className="flex justify-between mb-4">
            <p className="page-title">Master Data Workflow</p>
            <Button
              variant="contained"
              className="h-min"
              onClick={() => navigate("/masterdata/workflow/create")}
              disabled={workflows.length === 6 ? true : false}
            >
              Create
            </Button>
          </div>
          <WorkflowData />
        </Paper>
      </Grid>
    </Layout>
  );
};

export default WorkflowList;
