import React, { useEffect, useState } from "react";
import Layout from "../../Layout/Layout";
import { useNavigate, useParams } from "react-router-dom";
import { get, put } from "../../../util/http";
import { Alert, Button, Grid, Paper, Snackbar } from "@mui/material";
import { InputText } from "../../../components/input/input_text";
import { SelectInput } from "../../../components/input/select_input";
import { Textarea } from "../../../components/input/textarea";
import { ArrowBack } from "@mui/icons-material";

export const WorkflowEdit = () => {
  let { id } = useParams();
  const navigate = useNavigate();

  const [payload, setPayload] = useState({});
  const [alert, setAlert] = useState({
    status: false,
    type: "success",
    message: "",
  });

  const options = [
    {
      value: "Request",
      title: "Request",
    },
    {
      value: "Discrepancy",
      title: "Discrepancy",
    },
    {
      value: "Disposal",
      title: "Disposal",
    },
    {
      value: "Ringi",
      title: "Ringi",
    },
  ];

  useEffect(() => {
    const fetchDetail = async () => {
      const res = await get(
        process.env.REACT_APP_API_URL_Workflow_Service + "/find/" + id
      );
      if (res.status === 200) {
        setPayload(res.data);
      }
    };
    fetchDetail();
    return () => {
      setPayload({});
    };
  }, [id]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setPayload({ ...payload, [name]: value });
  };

  const handleAlertOnClose = () => {
    setAlert({
      ...alert,
      status: false,
    });
  };

  const handleSubmit = async () => {
    let body = {
      LocationName: payload.LocationName,
      PlantName: payload.PlantName,
      Type: payload.Type,
      Notes: payload.Notes,
    };

    const res = await put(
      process.env.REACT_APP_API_URL_Workflow_Service + "/update/" + id,
      body
    );
    if (res.status === 200) {
      setAlert({
        ...alert,
        status: true,
        type: "success",
        message: "Update workflow successfully. This page is auto redirect....",
      });
      setTimeout(() => {
        navigate("/masterdata/workflow");
      }, 3000);
    } else {
      setAlert({
        ...alert,
        type: "error",
        message: "Failed create workflow. Please check your input.",
        status: true,
      });
    }
  };

  return (
    <Layout>
      <Snackbar
        open={alert.status}
        autoHideDuration={3000}
        onClose={handleAlertOnClose}
      >
        <Alert severity={alert.type} variant="filled">
          {alert.message}
        </Alert>
      </Snackbar>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <p className="page-title">Edit Workflow</p>
          <div className="space-y-4">
            <SelectInput
              name="Type"
              label="Type"
              isRequired
              isDisabled
              onChange={(e) => {
                handleChange(e);
              }}
            >
              {options.map((option, index) => {
                if (option.value === payload.Type) {
                  return (
                    <option value={option.value} key={index} selected>
                      {option.title}
                    </option>
                  );
                }
                return (
                  <option value={option.value} key={index}>
                    {option.title}
                  </option>
                );
              })}
            </SelectInput>
            <Textarea
              label="Notes"
              name="Notes"
              height={5}
              onChange={(e) => {
                handleChange(e);
              }}
              value={payload.Notes}
            />
          </div>
          <div className="flex justify-end mt-4 gap-2">
            <Button
              className="w-min"
              variant="contained"
              style={{ backgroundColor: "#808080" }}
              onClick={() => navigate("/masterdata/workflow")}
            >
              <ArrowBack />
              Back
            </Button>
            <Button
              className="w-min"
              variant="contained"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </div>
        </Paper>
      </Grid>
    </Layout>
  );
};
