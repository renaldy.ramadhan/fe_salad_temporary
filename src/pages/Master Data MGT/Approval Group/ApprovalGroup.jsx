import React, { useCallback, useEffect } from "react";
import Layout from "../../Layout/Layout";
import { Button, Grid, Paper } from "@mui/material";
import { useDispatch } from "react-redux";
import { retrieveApproverGroup } from "../../../slices/ApprovalGroupSlice";
import { ApproverGroupData } from "../../../components/tables/approver-group/ApproverGroupData";
import { useNavigate } from "react-router-dom";

export const ApprovalGroup = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const initFetch = useCallback(() => {
    dispatch(retrieveApproverGroup());
  }, [dispatch]);

  useEffect(() => {
    initFetch();
  }, [initFetch]);

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <div className="flex justify-between mb-4">
            <p className="page-title">Master Data Approver Group</p>
            <Button
              variant="contained"
              color="primary"
              className="h-min"
              onClick={() => navigate("create")}
            >
              Create
            </Button>
          </div>
          <ApproverGroupData />
        </Paper>
      </Grid>
    </Layout>
  );
};
