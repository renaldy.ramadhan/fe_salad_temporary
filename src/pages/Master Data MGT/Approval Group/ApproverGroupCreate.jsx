import React, { useCallback, useEffect, useState } from "react";
import Layout from "../../Layout/Layout";
import {
  Alert,
  Button,
  Grid,
  MenuItem,
  Paper,
  Radio,
  Select,
  Snackbar,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { retrieveDepartment } from "../../../slices/DepartmentSlice";
import { fetchPurchasingGroup } from "../../../slices/GlobalSlice";
import { InputText } from "../../../components/input/input_text";
import { post } from "../../../util/http";
import { useNavigate } from "react-router-dom";
import { ArrowBack } from "@mui/icons-material";

export const ApproverGroupCreate = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const departments = useSelector((state) => state.departments.departmentData);
  const stgPurchasingGroup = useSelector(
    (state) => state.globals.fetchPurchasingGroup
  );

  const [payload, setPayload] = useState({
    IdDepartment: 0,
    ApproverGroupName: "",
    GroupType: "",
    LocationCode: "",
    ApproverGroupType: "Location",
  });
  const [alert, setAlert] = useState({
    status: false,
    type: "success",
    message: "",
  });

  const [isPurchasingGroup, setIsPurchasingGroup] = useState(false);
  const [isLocationCode, setIsLocationCode] = useState(true);

  const initFetch = useCallback(() => {
    dispatch(retrieveDepartment());
    dispatch(fetchPurchasingGroup());
  }, [dispatch]);

  useEffect(() => {
    initFetch();
  }, [initFetch]);

  const handleChange = (e) => {
    const { value, name, type } = e.target;

    if (type === "radio") {
      if (value === "Location") {
        setIsLocationCode(true);
        setIsPurchasingGroup(false);
      } else {
        setIsPurchasingGroup(true);
        setIsLocationCode(false);
      }
    }
    setPayload({ ...payload, [name]: value });
  };

  const options = [
    {
      value: "Request",
      title: "Request",
    },
    {
      value: "Discrepancy",
      title: "Discrepancy",
    },
    {
      value: "Disposal",
      title: "Disposal",
    },
    {
      value: "Ringi",
      title: "Ringi",
    },
    {
      value: "Disposal Event",
      title: "Disposal Event",
    },
    {
      value: "Disposal Event Ringi",
      title: "Disposal Event Ringi",
    },
  ];

  const handleSubmit = async () => {
    const res = await post(
      process.env.REACT_APP_API_URL_Approver_Group_Service + "/create",
      payload
    );

    if (res.status === 201) {
      setAlert({
        ...alert,
        status: true,
        type: "success",
        message:
          "Create approver group successfully. This page is auto redirect....",
      });
      setTimeout(() => {
        navigate("/masterdata/approvalgroup");
      }, 4000);
    } else {
      setAlert({
        ...alert,
        type: "error",
        message: "Failed create approver group. Please check your input.",
        status: true,
      });
    }
  };

  const handleAlertOnClose = () => {
    setAlert({
      ...alert,
      status: false,
    });
  };

  return (
    <Layout>
      <Snackbar
        open={alert.status}
        autoHideDuration={3000}
        onClose={handleAlertOnClose}
      >
        <Alert severity={alert.type} variant="filled">
          {alert.message}
        </Alert>
      </Snackbar>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <p className="page-title">Create Approver Group</p>
          <div className="space-y-4">
            <div className="space-y-2">
              <label className="font-medium">Approver Group Type</label>
              <div className="flex gap-4">
                <div className="flex gap-1 items-center">
                  <Radio
                    checked={payload.ApproverGroupType === "Location"}
                    value="Location"
                    name="ApproverGroupType"
                    onChange={(e) => handleChange(e)}
                  />
                  <label>Location</label>
                </div>
                <div className="flex gap-1 items-center">
                  <Radio
                    checked={payload.ApproverGroupType === "Purchasing Group"}
                    value="Purchasing Group"
                    name="ApproverGroupType"
                    onChange={(e) => handleChange(e)}
                  />
                  <label>Purchasing Group</label>
                </div>
              </div>
            </div>
            {isLocationCode && (
              <InputText
                label="Location Code"
                value={payload.LocationCode}
                name="LocationCode"
                type="text"
                isRequired
                onChange={(e) => handleChange(e)}
              />
            )}
            {isPurchasingGroup && (
              <div className="space-y-2">
                <label className="font-medium">
                  Purchasing Group <span className="text-red-500">*</span>
                </label>
                <Select
                  className="appearance-none border border-zinc-200 rounded-lg w-full text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
                  id="id_workflow"
                  name="PurchasingGroupCodeFk"
                  size="small"
                  onChange={(e) => handleChange(e)}
                >
                  {stgPurchasingGroup.map((group, index) => {
                    return (
                      <MenuItem value={group.PurchasingGroupCode} key={index}>
                        {group.PurchasingGroupCode}
                      </MenuItem>
                    );
                  })}
                </Select>
              </div>
            )}
            <div className="space-y-2">
              <label className="font-medium">
                Department <span className="text-red-500">*</span>
              </label>
              <Select
                className="appearance-none border border-zinc-200 rounded-lg w-full text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
                id="id_department"
                name="IdDepartment"
                size="small"
                onChange={(e) => handleChange(e)}
              >
                {departments.map((department, index) => {
                  return (
                    <MenuItem value={department.ID} key={index}>
                      {department.DepartmentName}
                    </MenuItem>
                  );
                })}
              </Select>
            </div>
            <InputText
              name="ApproverGroupName"
              type="text"
              label="Approver Group Name"
              isRequired
              value={payload.ApproverGroupName}
              onChange={(e) => handleChange(e)}
            />
            <div className="space-y-2">
              <label className="font-medium">
                Type <span className="text-red-500">*</span>
              </label>
              <Select
                className="appearance-none border border-zinc-200 rounded-lg w-full text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
                id="type_group"
                name="GroupType"
                size="small"
                onChange={(e) => handleChange(e)}
              >
                {options.map((option, index) => {
                  return (
                    <MenuItem value={option.value} key={index}>
                      {option.title}
                    </MenuItem>
                  );
                })}
              </Select>
            </div>
            <div className="flex justify-end mt-4 gap-2">
              <Button
                className="w-min"
                variant="contained"
                style={{ backgroundColor: "#808080" }}
                onClick={() => navigate("/masterdata/approvalgroup")}
              >
                <ArrowBack />
                Back
              </Button>
              <Button
                className="w-min"
                variant="contained"
                onClick={handleSubmit}
              >
                Submit
              </Button>
            </div>
          </div>
        </Paper>
      </Grid>
    </Layout>
  );
};
