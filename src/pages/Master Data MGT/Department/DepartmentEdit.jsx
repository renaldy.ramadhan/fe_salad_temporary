import React, { useCallback, useEffect, useState } from "react";
import Layout from "../../Layout/Layout";
import {
  Alert,
  Button,
  Grid,
  MenuItem,
  Paper,
  Select,
  Snackbar,
} from "@mui/material";
import { InputText } from "../../../components/input/input_text";
import { useDispatch, useSelector } from "react-redux";
import { fetchPlant } from "../../../slices/GlobalSlice";
import { useNavigate } from "react-router-dom";
import { get, put } from "../../../util/http";
import { useParams } from "react-router-dom";
import { ArrowBack } from "@mui/icons-material";

export const DepartmentEdit = () => {
  const { id } = useParams();
  const [payload, setPayload] = useState({
    DepartmentName: "",
    LocationCodeFk: "D100",
  });
  const [alert, setAlert] = useState({
    status: false,
    type: "success",
    message: "",
  });

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const locationPlants = useSelector((state) => state.globals.fetchPlant);

  const initFetch = useCallback(() => {
    dispatch(fetchPlant());
  }, [dispatch]);

  useEffect(() => {
    const fetchDetail = async () => {
      const res = await get(
        process.env.REACT_APP_API_URL_Department_Service + "/" + id
      );
      if (res.status === 200) {
        setPayload({
          DepartmentName: res.data.DepartmentName,
          LocationCodeFk: res.data.LocationCode,
        });
      }
    };
    fetchDetail();
    initFetch();
    return () => {
      setPayload({
        DepartmentName: "",
        LocationCodeFk: "NULL",
      });
    };
  }, [id, initFetch]);

  const handleChange = (e) => {
    const { value, name } = e.target;
    setPayload({ ...payload, [name]: value });
  };

  const handleSubmit = async () => {
    const res = await put(
      process.env.REACT_APP_API_URL_Department_Service + "/update/" + id,
      payload
    );

    if (res.status === 200) {
      setAlert({
        ...alert,
        status: true,
        type: "success",
        message:
          "Update department successfully. This page is auto redirect....",
      });
      setTimeout(() => {
        navigate("/masterdata/department");
      }, 4000);
    } else {
      setAlert({
        ...alert,
        type: "error",
        message: "Failed update department. Please check your input.",
        status: true,
      });
    }
  };

  const handleAlertOnClose = () => {
    setAlert({
      ...alert,
      status: false,
    });
  };

  return (
    <Layout>
      <Snackbar
        open={alert.status}
        autoHideDuration={3000}
        onClose={handleAlertOnClose}
      >
        <Alert severity={alert.type} variant="filled">
          {alert.message}
        </Alert>
      </Snackbar>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <p className="page-title">Edit Department</p>
          <div className="space-y-4">
            <InputText
              type="text"
              label="Department Name"
              name="DepartmentName"
              isRequired
              value={payload.DepartmentName}
              onChange={(e) => handleChange(e)}
            />
            {/* <div className="space-y-2">
              <label className="font-medium">Location Code</label>
              <Select
                className="appearance-none border border-zinc-200 rounded-lg w-full text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
                id="location_code"
                name="LocationCodeFk"
                size="small"
                value={payload.LocationCodeFk}
                onChange={(e) => handleChange(e)}
              >
                {locationPlants.map((location, index) => {
                  if (location.LocationCode !== "NULL") {
                    return (
                      <MenuItem value={location.LocationCode} key={index}>
                        {location.LocationCode + " - " + location.LocationName}
                      </MenuItem>
                    );
                  }
                })}
              </Select>
            </div> */}
          </div>
          <div className="flex justify-end mt-4 gap-2">
            <Button
              className="w-min"
              variant="contained"
              style={{ backgroundColor: "#808080" }}
              onClick={() => navigate("/masterdata/department")}
            >
              <ArrowBack />
              Back
            </Button>
            <Button
              className="w-min"
              variant="contained"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </div>
        </Paper>
      </Grid>
    </Layout>
  );
};
