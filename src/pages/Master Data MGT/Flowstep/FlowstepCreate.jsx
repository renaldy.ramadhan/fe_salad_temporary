import React, { useCallback, useEffect, useState } from "react";
import Layout from "../../Layout/Layout";
import { useDispatch, useSelector } from "react-redux";
import { retrieveWorkflows } from "../../../slices/WorkflowSlice";
import { retrieveDepartment } from "../../../slices/DepartmentSlice";
import {
  Alert,
  Button,
  Grid,
  MenuItem,
  Paper,
  Select,
  Snackbar,
} from "@mui/material";
import { InputText } from "../../../components/input/input_text";
import { useNavigate } from "react-router-dom";
import { post } from "../../../util/http";
import { ArrowBack } from "@mui/icons-material";

export const FlowstepCreate = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const workflows = useSelector((state) => state.workflows.workflowsData);
  const departments = useSelector((state) => state.departments.departmentData);
  const workflowId = useSelector((state) => state.workflows.activeWorkflowId);

  const [alert, setAlert] = useState({
    status: false,
    type: "success",
    message: "",
  });

  const [payload, setPayload] = useState({
    IdWorkflow: 0,
    IdDepartment: 0,
    StepName: "",
    FlowSequence: 1,
    Type: 1,
  });

  const initFetch = useCallback(() => {
    dispatch(retrieveWorkflows());
    dispatch(retrieveDepartment());
  }, [dispatch]);

  useEffect(() => {
    if (workflowId !== "") {
      payload.IdWorkflow = workflowId;
    }
    initFetch();
  }, [initFetch, workflowId]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setPayload({ ...payload, [name]: value });
  };

  const handleSubmit = async () => {
    payload.FlowSequence = parseInt(payload.FlowSequence);
    payload.Type = parseInt(payload.Type);
    const res = await post(
      process.env.REACT_APP_API_URL_Flowstep_Service + "/create",
      payload
    );

    if (res.status === 201) {
      setAlert({
        ...alert,
        status: true,
        type: "success",
        message: "Create flowstep successfully. This page is auto redirect....",
      });
      setTimeout(() => {
        navigate("/masterdata/flowstep");
      }, 4000);
    } else {
      setAlert({
        ...alert,
        type: "error",
        message: "Failed create flowstep. Please check your input.",
        status: true,
      });
    }
  };

  const handleAlertOnClose = () => {
    setAlert({
      ...alert,
      status: false,
    });
  };

  const typeOption = [
    {
      value: 1,
      name: "Requestor",
    },
    {
      value: 2,
      name: "Location",
    },
    {
      value: 3,
      name: "Purchasing Group",
    },
  ];

  return (
    <Layout>
      <Snackbar
        open={alert.status}
        autoHideDuration={3000}
        onClose={handleAlertOnClose}
      >
        <Alert severity={alert.type} variant="filled">
          {alert.message}
        </Alert>
      </Snackbar>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <p className="page-title">Create Flowstep</p>
          <div className="space-y-4">
            <div className="grid lg:grid-cols-2 grid-cols-1 gap-4">
              <div className="space-y-2">
                <label className="font-medium">
                  Department Name <span className="text-red-500">*</span>
                </label>
                <Select
                  className="appearance-none border border-zinc-200 rounded-lg w-full text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
                  id="id_department"
                  name="IdDepartment"
                  size="small"
                  onChange={(e) => handleChange(e)}
                >
                  {departments.map((department, index) => {
                    return (
                      <MenuItem value={department.ID} key={index}>
                        {department.DepartmentName}
                      </MenuItem>
                    );
                  })}
                </Select>
              </div>
              <div className="space-y-2">
                <label className="font-medium">
                  Workflow <span className="text-red-500">*</span>
                </label>
                <Select
                  className="appearance-none border border-zinc-200 rounded-lg w-full text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
                  id="id_workflow"
                  name="IdWorkflow"
                  size="small"
                  value={payload.IdWorkflow}
                  disabled={workflowId !== "" ? true : false}
                  onChange={(e) => handleChange(e)}
                >
                  {workflows.map((workflow, index) => {
                    return (
                      <MenuItem value={workflow.ID} key={index}>
                        {workflow.Type}
                      </MenuItem>
                    );
                  })}
                </Select>
              </div>
            </div>
            <InputText
              type="text"
              label="Step Name"
              onChange={(e) => handleChange(e)}
              isRequired
              value={payload.StepName}
              name="StepName"
            />
            <InputText
              type="number"
              label="Flow Sequence"
              onChange={(e) => handleChange(e)}
              isRequired
              value={payload.FlowSequence}
              name="FlowSequence"
            />
            <div className="space-y-2">
              <label className="font-medium">
                Type <span className="text-red-500">*</span>
              </label>
              <Select
                className="appearance-none border border-zinc-200 rounded-lg w-full text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
                id="type"
                name="Type"
                size="small"
                onChange={(e) => handleChange(e)}
              >
                {typeOption.map((option, index) => {
                  return (
                    <MenuItem value={option.value} key={index}>
                      {option.name}
                    </MenuItem>
                  );
                })}
              </Select>
            </div>
          </div>
          <div className="flex justify-end mt-4 gap-2">
            <Button
              className="w-min"
              variant="contained"
              style={{ backgroundColor: "#808080" }}
              onClick={() => navigate("/masterdata/flowstep")}
            >
              <ArrowBack />
              Back
            </Button>
            <Button
              className="w-min"
              variant="contained"
              onClick={handleSubmit}
            >
              Submit
            </Button>
          </div>
        </Paper>
      </Grid>
    </Layout>
  );
};
