import React, { useCallback, useEffect } from "react";
import Layout from "../../Layout/Layout";
import { Button, Grid, Paper } from "@mui/material";
import { useDispatch } from "react-redux";
import { retrieveFlowstep } from "../../../slices/FlowstepSlice";
import { FlowstepData } from "../../../components/tables/flowstep/FlowstepData";
import { useNavigate } from "react-router-dom";

export const FlowstepList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const initFetch = useCallback(() => {
    dispatch(retrieveFlowstep());
  }, [dispatch]);

  useEffect(() => {
    initFetch();
  }, [initFetch]);

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <div className="flex justify-between mb-4">
            <p className="page-title">Master Data Flowstep</p>
            <Button
              variant="contained"
              color="primary"
              className="h-min"
              onClick={() => navigate("create")}
            >
              Create
            </Button>
          </div>
          <FlowstepData />
        </Paper>
      </Grid>
    </Layout>
  );
};
