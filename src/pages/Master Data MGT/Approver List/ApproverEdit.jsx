import React, { useCallback, useEffect, useState } from "react";
import Layout from "../../Layout/Layout";
import {
  Alert,
  Button,
  Grid,
  MenuItem,
  Paper,
  Select,
  Snackbar,
} from "@mui/material";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { retrieveDepartment } from "../../../slices/DepartmentSlice";
import { retrieveApproverGroup } from "../../../slices/ApprovalGroupSlice";
import TextField from "@mui/material/TextField";
import Stack from "@mui/material/Stack";
import Autocomplete from "@mui/material/Autocomplete";
import { get, put } from "../../../util/http";
import { InputText } from "../../../components/input/input_text";
import { ArrowBack } from "@mui/icons-material";

export const ApproverEdit = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [payload, setPayload] = useState({
    Nik: "",
    EmployeeName: "",
    Sequence: 1,
    IdApproverGroup: [],
    ApprovalStage: 1,
  });
  const [employee, setEmployee] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isListOpen, setIsListOpen] = useState(false);
  const [alert, setAlert] = useState({
    status: false,
    type: "success",
    message: "",
  });

  const getEmployee = async (value) => {
    const res = await get(
      process.env.REACT_APP_API_URL_User_Service +
        "/staging-user/?search=" +
        value
    );
    setEmployee(res.data);
  };

  const onInputChange = (event, value, reason) => {
    if (event !== null) {
      setIsListOpen(true);
    }
    if (value) {
      setIsLoading(true);
      payload.Nik = value;
    } else {
      setEmployee([]);
    }
  };

  const handleAutocompleteChange = (event, value) => {
    setIsListOpen(false);
    if (value != null) {
      const searchEmployee = employee.find(
        (employee) => employee.Nik === value
      );
      setPayload({ ...payload, EmployeeName: searchEmployee.Name });
    } else {
      setPayload({ ...payload, Nik: "" });
    }
  };

  const handleSubmit = async () => {
    payload.Sequence = parseInt(payload.Sequence);
    const res = await put(
      process.env.REACT_APP_API_URL_Approver_Service + "/update/" + id,
      payload
    );

    if (res.status === 200) {
      setAlert({
        ...alert,
        status: true,
        type: "success",
        message: "Update approver successfully. This page is auto redirect....",
      });
      setTimeout(() => {
        navigate("/masterdata/approval");
      }, 4000);
    } else {
      setAlert({
        ...alert,
        type: "error",
        message: "Failed update approver. Please check your input.",
        status: true,
      });
    }
  };

  const handleChange = (e) => {
    const { value, name } = e.target;
    setPayload({ ...payload, [name]: value });
  };

  const handleAlertOnClose = () => {
    setAlert({
      ...alert,
      status: false,
    });
  };

  const approverGroups = useSelector((state) => state.approvalgroups.approver);

  const initFetch = useCallback(() => {
    dispatch(retrieveDepartment());
    dispatch(retrieveApproverGroup());
  }, [dispatch]);

  useEffect(() => {
    const fetchDetail = async () => {
      const res = await get(
        process.env.REACT_APP_API_URL_Approver_Service + "/" + id
      );
      const data = res.data;

      let idApproverGroup = [];
      data.ApproverGroup.forEach((approverGroup) => {
        idApproverGroup.push(approverGroup.ID);
      });

      let inputValue = {
        Nik: data.Nik,
        ApprovalStage: data.ApprovalStage,
        EmployeeName: data.EmployeeName,
        IdApproverGroup: idApproverGroup,
        Sequence: data.Sequence,
      };
      setPayload(inputValue);
    };
    fetchDetail();
    initFetch();
  }, [initFetch, id]);

  useEffect(() => {
    if (payload.Nik !== "") {
      setTimeout(() => {
        getEmployee(payload.Nik);
        setIsLoading(false);
      }, 1500);
    }
    return () => {
      setEmployee([]);
    };
  }, [payload.Nik]);

  return (
    <Layout>
      <Snackbar
        open={alert.status}
        autoHideDuration={3000}
        onClose={handleAlertOnClose}
      >
        <Alert severity={alert.type} variant="filled">
          {alert.message}
        </Alert>
      </Snackbar>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <p className="page-title">Update Approver</p>
          <div className="space-y-4">
            <div className="space-y-2">
              <label className="font-medium">
                Approver Group <span className="text-red-500">*</span>
              </label>
              <Select
                className="appearance-none border border-zinc-200 rounded-lg w-full text-gray-700 leading-tight focus:border-zinc-400 focus:outline-none focus:shadow-outline"
                id="id_ApproverGroup"
                name="IdApproverGroup"
                size="small"
                multiple
                value={payload.IdApproverGroup}
                onChange={(e) => handleChange(e)}
              >
                {approverGroups.map((approverGroup, index) => {
                  return (
                    <MenuItem value={approverGroup.ID} key={index}>
                      {approverGroup.ApproverGroupName}
                    </MenuItem>
                  );
                })}
              </Select>
            </div>
            <div className="lg:flex justify-between gap-4">
              <div className="space-y-2">
                <label>Employee ID</label>
                <Stack spacing={2} sx={{ width: 300 }}>
                  <Autocomplete
                    className="w-full"
                    onBlur={() => setIsListOpen(false)}
                    onChange={handleAutocompleteChange}
                    id="free-solo-demo"
                    open={isListOpen}
                    onInputChange={onInputChange}
                    loading={isLoading}
                    value={payload.Nik}
                    options={employee.map((option) => option.Nik)}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        size="small"
                        name="EmployeeId"
                        className="w-full border-2 border-zinc-300"
                      />
                    )}
                  />
                </Stack>
              </div>
              <InputText
                label="Employee Name"
                name="Employee Name"
                isDisabled
                isRequired
                value={payload.EmployeeName}
              />
            </div>
            <InputText
              label="Sequnce"
              name="Sequence"
              type="number"
              value={payload.Sequence}
              onChange={handleChange}
              isRequired
            />
            <div className="flex justify-end mt-4 gap-2">
              <Button
                className="w-min"
                variant="contained"
                style={{ backgroundColor: "#808080" }}
                onClick={() => navigate("/masterdata/approval")}
              >
                <ArrowBack />
                Back
              </Button>
              <Button
                className="w-min"
                variant="contained"
                onClick={handleSubmit}
              >
                Submit
              </Button>
            </div>
          </div>
        </Paper>
      </Grid>
    </Layout>
  );
};
