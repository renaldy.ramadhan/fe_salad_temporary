import React, { useCallback, useEffect } from "react";
import Layout from "../../Layout/Layout";
import { Button, Grid, Paper } from "@mui/material";
import { ApproverData } from "../../../components/tables/Approver/ApproverData";
import { useDispatch } from "react-redux";
import { retrieveApprover } from "../../../slices/ApprovalSlice";
import { useNavigate } from "react-router-dom";

export const ApproverList = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const initFetch = useCallback(() => {
    dispatch(retrieveApprover());
  }, [dispatch]);

  useEffect(() => {
    initFetch();
  }, [initFetch]);

  return (
    <Layout>
      <Grid item xs={12} md={12} lg={12}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
            height: "inherit",
          }}
        >
          <div className="flex justify-between mb-4">
            <p className="page-title">Master Data Approver</p>
            <Button
              className="h-min"
              variant="contained"
              color="primary"
              onClick={() => navigate("create")}
            >
              Create
            </Button>
          </div>
          <ApproverData />
        </Paper>
      </Grid>
    </Layout>
  );
};
