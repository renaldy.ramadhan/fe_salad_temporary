import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Layout from "../Layout/Layout";
import LoadingSpinner from "../../components/animations/loading/LoadingSpinner";
import { KeycloakContext } from "../../contexts/KeycloakProvider";
import ApproverTable from "../../components/tables/Approver/ApproverTable";
import Button from "../../components/buttons/Button";

const MasterDataMGT = () => {
  const PART_SERVICE_BASE_URL = process.env.REACT_APP_API_URL_Part_Service;

  const navigate = useNavigate();
  //   const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  const { keycloak, authenticated } = useContext(KeycloakContext);
  const name = keycloak.tokenParsed["name"];
  const role = keycloak.tokenParsed.resource_access?.["react-client"]?.roles;

  //   console.log("keycloak:", keycloak)
  //   console.log("realm access:", keycloak.realmAccess)
  // console.log("token parsed:", keycloak.tokenParsed);
  //   console.log('Name:', name);
  //   console.log('Roles:', role);
  const data = [
    {
      id: "",
      wfs: "Procurement (PPC)",
      type: "Part Request",
    },
    {
      id: "",
      wfs: "Procurement (PPC)",
      type: "Part Request",
    },
    {
      id: "",
      wfs: "Procurement (PPC)",
      type: "Part Request",
    },
  ];

  //   const fetchData = async () => {
  //     try {
  //       const response = await axios.get(`${PART_SERVICE_BASE_URL}part_request`, {
  //         headers: {
  //           Authorization: "Bearer " + window.localStorage.getItem("token"),
  //         },
  //       });

  //       console.log(response.data);
  //       setData(response.data);
  //       setLoading(false);
  //     } catch (error) {
  //       console.error(error);
  //       setLoading(false);
  //     }
  //   };

  //   useEffect(() => {
  //     fetchData();
  //   }, []);

  return (
    <Layout>
      <div className="content-positioning pl-3 md:pl-48 md:pt-38 pr-2 w-full">
        <p className="page-title">Master Data Management</p>
        <div className="flex justify-start gap-4 mb-4 h-10 md:h-10 text-sm">
          <Button
            type={"button"}
            onclick={() => {
              navigate("/approvalgroup");
            }}
            classname={
              "bg-custom-blue text-center py-1 md:py-2 px-8 border-solid border-0 rounded-md md:rounded text-xs md:text-sm text-white font-semibold hover:opacity-90"
            }
          >
            Add
          </Button>
        </div>
        <div className="max-w-[100vh] table-content h-96 sm:h-96 md:h-96 lg:h-128">
          <div className="table-wrapper">
            <table className="table">
              {/* table headers */}
              <thead className="sticky -top-0.5">
                <tr className="text-xs text-gray-700 uppercase">
                  <th scope="col" className=" bg-gray-100 px-1 py-6 w-5">
                    ID
                  </th>
                  <th scope="col" className=" bg-gray-100 px-2 py-6 w-10">
                    Workflow Step
                  </th>
                  <th scope="col" className=" bg-gray-100 px-3 py-6 w-32">
                    Type
                  </th>
                  <th scope="col" className=" bg-gray-100 px-3 py-6 w-32">
                    Action
                  </th>
                </tr>
              </thead>
              {/* table body */}
              <tbody>
                {data.map((item, index) => {
                  return (
                    <tr
                      key={index}
                      className="bg-white border-b hover:bg-gray-200"
                    >
                      <td className="border-[2px]   py-2 whitespace-nowrap text-sm font-medium text-gray-900 font-medium">
                        {index + 1}
                      </td>
                      <td className="border-[2px]   py-2 whitespace-nowrap text-sm font-medium text-gray-900">
                        {item.wfs}
                      </td>
                      <td className="border-[2px]   py-2 whitespace-nowrap text-sm font-medium text-gray-900">
                        {item.type}
                      </td>
                      <td className="border-[2px]   py-2 whitespace-nowrap text-sm font-medium text-gray-900">
                        <div className="flex gap-4 justify-center items-center">
                          <button
                            className="inline-flex items-center min-w-12 h-8 px-3 py-2 bg-gray-500 hover:bg-gray-400 text-white text-sm font-medium rounded-md"
                            onClick={() => {
                              navigate("/approvalgroup");
                            }}
                            title="Details"
                          >
                            View
                          </button>
                        </div>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </Layout>
  );
};

export default MasterDataMGT;
