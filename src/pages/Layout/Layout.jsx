// import React, { useContext, useState } from 'react';
// import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
// import MuiDrawer from '@mui/material/Drawer';
// import MuiAppBar from '@mui/material/AppBar';
// import Navbar from '../../components/navbar/Navbar';
// import Menu from '../../components/menu/Menu';
// import { UserContext } from '../../contexts/UserContext';
// import ButtonAppBar from '../../components/navbar/Navbar';
// import Sidebar from '../../components/menu/Menu';
import React, { useContext, useEffect, useState } from "react";
import { styled, createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import MuiDrawer from "@mui/material/Drawer";
import Box from "@mui/material/Box";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import Button from "@mui/material/Button";
import CallSplitIcon from "@mui/icons-material/CallSplit";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import LayersIcon from "@mui/icons-material/Layers";
import AccountTreeIcon from "@mui/icons-material/AccountTree";
import { Link, useLocation } from "react-router-dom";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Collapse from "@mui/material/Collapse";
import StarBorder from "@mui/icons-material/StarBorder";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import DashboardIcon from "@mui/icons-material/Dashboard";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import PeopleIcon from "@mui/icons-material/People";
import { KeycloakContext } from "../../contexts/KeycloakProvider";
import LogoutButton from "../../components/buttons/LogoutButton";
import Container from "@mui/material/Container";
import ApartmentIcon from "@mui/icons-material/Apartment";
import Grid from "@mui/material/Grid";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import CheckBoxIcon from "@mui/icons-material/CheckBox";
import { jwtDecode } from "jwt-decode";

// import {mainListItems} from './listItems';

// TODO remove, this demo shouldn't need to reset the theme.
const defaultTheme = createTheme();

const Layout = ({ children }) => {
  // const user = useContext(UserContext)

  // const toggleActive = () => {
  //   setActive(!active);
  // };

  const drawerWidth = 240;

  const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== "open",
  })(({ theme, open }) => ({
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "#17479D",
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    }),
  }));

  const Drawer = styled(MuiDrawer, {
    shouldForwardProp: (prop) => prop !== "open",
  })(({ theme, open }) => ({
    "& .MuiDrawer-paper": {
      position: "relative",
      whiteSpace: "nowrap",
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      boxSizing: "border-box",
      ...(!open && {
        overflowX: "hidden",
        transition: theme.transitions.create("width", {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up("sm")]: {
          width: theme.spacing(9),
        },
      }),
    },
  }));
  const [open, setOpen] = React.useState(true);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  const [role, setRole] = useState("");

  const [openDisposal, setOpenDisposal] = React.useState(false);
  const handleClickDisposal = () => {
    setOpenDisposal(!openDisposal);
  };
  const [openRingi, setOpenRingi] = React.useState(false);
  const handleClickRingi = () => {
    setOpenRingi(!openRingi);
  };
  const [openGeneral, setOpenGeneral] = React.useState(false);
  const handleClickGeneral = () => {
    setOpenGeneral(!openGeneral);
  };
  const [openMasterData, setOpenMasterData] = useState(false);

  const { keycloak } = useContext(KeycloakContext);
  const name = keycloak.tokenParsed["name"];
  // const role = keycloak.tokenParsed.resource_access?.["react-client"]?.roles;

  const location = useLocation();
  const pathname = location.pathname;

  const defaultTitle = "Dashboard";
  const mainPage = pathname.split("/")[1];

  const mainPageTitles = {
    "": "Dashboard",
    partrequest: "Part Request",
    discrepancy: "Discrepancy",
    disposal: "Disposal",
    ringi: "Ringi",

    // Add more main pages as needed
  };

  useEffect(() => {
    if (pathname.split("/")[1] === "masterdata") {
      setOpenMasterData(true);
    }

    if (pathname.split("/")[1] === "disposal") {
      setOpenDisposal(true);
    }
    if (pathname.split("/")[1] === "ringi") {
      setOpenDisposal(true);
      setOpenRingi(true);
    }

    if (pathname.split("/")[2] === "general") {
      setOpenGeneral(true);
    }

    return () => {
      setOpenMasterData(false);
      setOpenDisposal(false);
      setOpenRingi(false)
      setOpenGeneral(false);
    };
  }, [pathname]);

  useEffect(() => {
    const checkRole = async () => {
      const token = window.localStorage.getItem("token");
      const decoded = jwtDecode(token);
      console.warn(decoded)
      if (decoded.role == null) {
        setRole("user");
      } else if (decoded.role.includes('administrator')) {
        setRole('administrator');
      }
    };
    checkRole();
    return () => {
      setRole("");
    };
  }, []);

  const pageTitle = mainPageTitles[mainPage] || defaultTitle;

  const mainListItems = (
    <List
      sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
      component="nav"
      aria-labelledby="nested-list-subheader"
    >
      <ListItemButton
        component={Link}
        to="/"
        selected={pathname.split("/")[1] === "" ? true : false}
      >
        <ListItemIcon>
          <DashboardIcon />
        </ListItemIcon>
        <ListItemText primary="Dashboard" />
      </ListItemButton>
      <ListItemButton
        component={Link}
        to="/partrequest"
        selected={pathname.split("/")[1] === "partrequest" ? true : false}
      >
        <ListItemIcon>
          <ShoppingCartIcon />
        </ListItemIcon>
        <ListItemText primary="Part Request" />
      </ListItemButton>
      <ListItemButton
        component={Link}
        to="/discrepancy"
        selected={pathname.split("/")[1] === "discrepancy" ? true : false}
      >
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="Discrepancy" />
      </ListItemButton>
      <ListItemButton onClick={handleClickDisposal}>
        <ListItemIcon>
          <InboxIcon />
        </ListItemIcon>
        <ListItemText primary="Disposal" />
        {!openDisposal ? <ExpandMore /> : <ExpandLess />}
      </ListItemButton>
      <Collapse in={openDisposal} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>

          <ListItemButton sx={{ pl: 4 }} onClick={handleClickGeneral}>
            <ListItemIcon>
              <StarBorder />
            </ListItemIcon>
            <ListItemText primary="Regular" />
            {openGeneral ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>

          <Collapse in={openGeneral} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItemButton
                sx={{ pl: 8 }}
                component={Link}
                to="/disposal/general/event"
                selected={
                  pathname.split("/")[2] === "general" &&
                    pathname.split("/")[3] === "event"
                    ? true
                    : false
                }
              >
                <ListItemIcon>
                  <StarBorder />
                </ListItemIcon>
                <ListItemText primary="Event" />
              </ListItemButton>
              <ListItemButton
                sx={{ pl: 8 }}
                component={Link}
                to="/disposal/general/request"
                selected={
                  pathname.split("/")[2] === "general" &&
                    pathname.split("/")[3] === "request"
                    ? true
                    : false
                }
              >
                <ListItemIcon>
                  <StarBorder />
                </ListItemIcon>
                <ListItemText primary="Request" />
              </ListItemButton>
            </List>
          </Collapse>

          <ListItemButton sx={{ pl: 4 }} onClick={handleClickRingi}>
            <ListItemIcon>
              <StarBorder />
            </ListItemIcon>
            <ListItemText primary="Ringi" />
            {openRingi ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>

          <Collapse in={openRingi} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItemButton sx={{ pl: 8 }} component={Link} to="/ringi"
                selected={
                  pathname.split('/')[1] === "ringi" && pathname.split('/')[2] !== 'request'
                    ? true
                    : false
                }>
                <ListItemIcon>
                  <StarBorder />
                </ListItemIcon>
                <ListItemText primary="Event" />
              </ListItemButton>
              {/* <ListItemButton sx={{ pl: 8 }} component={Link} to="/ringi/request"
                selected={
                  pathname.split("/")[1] === "ringi" &&
                    pathname.split("/")[2] === "request"
                    ? true
                    : false
                }>
                <ListItemIcon>
                  <StarBorder />
                </ListItemIcon>
                <ListItemText primary="Request" />
              </ListItemButton> */}
            </List>
          </Collapse>
        </List>
      </Collapse>
      {role === "administrator" && (
        <>
          <ListItemButton
            component={Link}
            onClick={() => setOpenMasterData(!openMasterData)}
          >
            <ListItemIcon>
              <LayersIcon />
            </ListItemIcon>
            <ListItemText primary="Master Data" />
            {!openMasterData ? <ExpandMore /> : <ExpandLess />}
          </ListItemButton>
          <Collapse in={openMasterData} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItemButton
                sx={{ pl: 5 }}
                component={Link}
                to="/masterdata/workflow"
                selected={pathname.split("/")[2] === "workflow" ? true : false}
              >
                <ListItemIcon>
                  <AccountTreeIcon />
                </ListItemIcon>
                <ListItemText primary="Workflow" />
              </ListItemButton>
              <ListItemButton
                sx={{ pl: 5 }}
                component={Link}
                to="/masterdata/department"
                selected={
                  pathname.split("/")[2] === "department" ? true : false
                }
              >
                <ListItemIcon>
                  <ApartmentIcon></ApartmentIcon>
                </ListItemIcon>
                <ListItemText primary="Department" />
              </ListItemButton>
              <ListItemButton
                sx={{ pl: 5 }}
                component={Link}
                to="/masterdata/flowstep"
                selected={pathname.split("/")[2] === "flowstep" ? true : false}
              >
                <ListItemIcon>
                  <CallSplitIcon></CallSplitIcon>
                </ListItemIcon>
                <ListItemText primary="Flowstep" />
              </ListItemButton>
              <ListItemButton
                sx={{ pl: 5 }}
                component={Link}
                to="/masterdata/approvalgroup"
                selected={
                  pathname.split("/")[2] === "approvalgroup" ? true : false
                }
              >
                <ListItemIcon>
                  <PeopleAltIcon></PeopleAltIcon>
                </ListItemIcon>
                <ListItemText primary="Approver Group" />
              </ListItemButton>
              <ListItemButton
                sx={{ pl: 5 }}
                component={Link}
                to="/masterdata/approval"
                selected={pathname.split("/")[2] === "approval" ? true : false}
              >
                <ListItemIcon>
                  <CheckBoxIcon></CheckBoxIcon>
                </ListItemIcon>
                <ListItemText primary="Approver" />
              </ListItemButton>
            </List>
          </Collapse>
        </>
      )}
    </List>
  );

  return (
    <ThemeProvider theme={defaultTheme}>
      <Box sx={{ display: "flex" }}>
        <CssBaseline />
        <AppBar position="absolute" open={open}>
          <Toolbar
            sx={{
              pr: "24px", // keep right padding when drawer closed
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: "36px",
                ...(open && { display: "none" }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              {pageTitle}
            </Typography>
            <Button
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ mr: 3 }}
            >
              Welcome {`${name}`}
              {/* , You are logged in as {`${role}`} */}
            </Button>
            <LogoutButton />
            {/* <Button color="inherit" onclick={() => setModalOpen(true)}>Logout</Button> */}
          </Toolbar>
        </AppBar>

        {/* <Menu sidebarVisible={sidebarVisible} /> */}
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "flex-end",
              px: [1],
            }}
          >
            <img
              src={process.env.PUBLIC_URL+`/images/saladlogobig.png`}
              alt="logo salad"
              className="md:w-[160px]"
            />
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          <Divider />
          <List component="nav">{mainListItems}</List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === "light"
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: "100vh",
            overflow: "auto",
          }}
        >
          <Toolbar />
          <Container maxWidth="false" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={2}>
              {/* <div className={`h-full  transform transition-transform duration-300 ${active ? "translate-x-40" : "translate-x-0"} pl-0 md:pl-10 w-full`}> */}
              {children}
              {/* </div> */}
            </Grid>
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
};

export default Layout;
