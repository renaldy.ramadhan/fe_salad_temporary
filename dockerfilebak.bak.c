
# build environment
FROM node:current-alpine3.17 as build

ENV PUBLIC_URL=/salad
ENV REACT_APP_API_URL_User_Service=http://34.101.177.226:8080
ENV REACT_APP_API_URL_Part_Request_Service=http://34.101.177.226:8080/request
ENV REACT_APP_API_URL_Active_Approval_Service=http://34.101.177.226:8080/active-request-list
ENV REACT_APP_API_URL_Staging_User_Service=http://34.101.177.226:8080/staging-user
ENV REACT_APP_API_URL_Document_Service=http://34.101.177.226:8080/document
ENV REACT_APP_API_URL_Approver_Group_Service=http://34.101.177.226:8080/approver-group
ENV REACT_APP_API_URL_Approver_Service=http://34.101.177.226:8080/approver
ENV REACT_APP_API_URL=http://34.101.177.226:8080
ENV REACT_APP_API_URL_Search_Part_Service=http://34.101.177.226:8080/staging-items
ENV REACT_APP_API_URL_Workflow_Service=http://34.101.177.226:8080/workflow
ENV REACT_APP_API_URL_Department_Service=http://34.101.177.226:8080/department
ENV REACT_APP_API_URL_Flowstep_Service=http://34.101.177.226:8080/flow-step

WORKDIR /app
COPY . .
RUN yarn
RUN yarn build

# production environment
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
COPY --from=build /app/nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]