/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./src/**/*.{js,jsx,ts,tsx}",],
    darkMode: false,
    theme: {
      container: {
        center: true,
      },
      extend: {
        colors: {
          'custom-gray': '#f3f3f3',  // custom color checkmark bg
          'custom-light-blue': '#5AA6FF',
          'custom-blue': '#17479d',
          'custom-red': '#d24d57',
          'custom-yellow': '#FACC15'
        },
        width: {
          '128':'32rem',
          '144':'36rem',
        },
        height: {
          '128':'32rem',
          '144':'36rem',
        }
      },
    },
    variants: {
      extend: {
        ringWidth: ['responsive', 'hover', 'focus', 'active', 'focus-within'],
        ringColor: ['responsive', 'hover', 'focus', 'active', 'focus-within'],
        ringOpacity: ['responsive', 'hover', 'focus', 'active', 'focus-within'],
        outline: ['responsive', 'hover', 'focus', 'active', 'focus-within'],
      },
    },
    plugins: [],
  }